#!/bin/bash

# Takes a single argument which is the filename for a new sermon.
if [[ $# -eq 0 ]] ;
then
  echo "Please specify the sermon's filename" && exit 0
elif [ -f content/sermons/$1 ] ;
then
  echo "Filename already exists" &&  exit 0
elif [ ${1: -3} != ".md" ]
then
  echo "File must be markdown (.md)" && exit 0
fi

# If we've got a valid filename, then use hugo to create and open in Typora for editing.
hugo new sermons/$1

if [ "$?" -ne "0" ];
then
  echo "Hugo failed to create content/sermons/$1"
  exit 1
fi

open -a typora content/sermons/$1

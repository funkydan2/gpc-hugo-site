Website of [Gympie Presbyterian Church](https://gympiepresbyterian.org.au). Built using [hugo](//gohugo.io) using the [alpha-church](https://github.com/funkydan2/alpha-church) theme.

[![Netlify Status](https://api.netlify.com/api/v1/badges/26fcdd75-ee76-4015-8fd1-798af7f9e8a6/deploy-status)](https://app.netlify.com/sites/keen-saha-3f6b43/deploys)

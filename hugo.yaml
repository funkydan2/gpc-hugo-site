baseURL: 'https://gympiepresbyterian.org.au/'
theme: alpha-church
enableEmoji: true

languageCode: en-au
rssLimit: 100

services:
  googleAnalytics:
    ID: 'G-KP1KYDTPR9'

security:
  enableInlineShortcodes:  true

markup:
  goldmark:
    renderer:
      unsafe: true

minify:
  minifyOutput: true
  disableJS: true

title: Gympie Presbyterian Church

taxonomies:
  preacher: preachers
  series: series
  tag: tags

menu:
  main:
    - name: Home
      url: /
      weight: 1

    - name: I'm New
      url: /im-new
      weight: 2

    - name: Plan Your Visit
      parent: I'm New
      url: /plan-your-visit/
      weight: 1

    - name: About Jesus
      parent: I'm New
      url: /about-jesus/
      weight: 2

    - identifier: about-us-new
      name: About Us
      parent: I'm New
      url: /about/
      weight: 3

    - name: FAQ
      parent: I'm New
      url: /frequently-asked-questions/
      weight: 4

    - name: About
      url: /about/
      weight: 3

    - identifier: about-us-about
      name: About Us
      parent: About
      url: /about/
      weight: 1

    - name: Church on Sunday
      parent: About
      url: /sundays/
      weight: 2

    - name: Midweek
      parent: About
      url: /midweek/
      weight: 3

    - name: Leaders
      parent: About
      url: /leaders/
      weight: 4

    - name: Giving
      parent: About
      url: /giving/
      weight: 5

    - name: Resources
      url: /resources/
      weight: 4

    - name: Sermons
      url: /sermons/
      parent: Resources
      weight: 1

    - name: All
      url: /sermons/
      parent: Sermons
      weight: 1

    - name: Series
      url: /series/
      parent: Sermons
      weight: 2

    - name: News
      url: /posts/
      parent: Resources
      weight: 2

    - name: Contact
      url: /contact/
      weight: 5

params:
  title: Gympie Presbyterian Church
  subtitle: Knowing God, Loving One Another, Reaching our Region
  author: 
    name: Gympie Presbyterian Church
    email: church@gympiepresbyterian.org.au
  description: Gympie Presbyterian Church
  keywords:
    - "Church"
    - "Presbyterian"
    - "Gympie"
    - "Christian"
    - "Christian Church"
    - "Bible"
    - "Evangelical"
    - "Reformed"
    - "Cooloola"
    - "Mary Valley"
    - "Kilkivan"
    - "Curra"
    - "Tin Can Bay"
    - "Community"
    - "Jesus"
    - "God"
  images: ["/uploads/2018/07/Our Vision.png"]
  bible_popups: faithlife
  bible_version: NIV
  instant_page: false

  favicon:
    sml: /favicon-16x16.png
    med: /favicon-32x32.png

  banner:
    image: uploads/2018/06/congregation.jpg

  podcast:
    title: Gympie Presbyterian Sermons, Bible Talks, and Messages
    summary: Weekly sermon podcast from Gympie Presbyterian Church
    copyright: Copyright Gympie Presbyterian Church
    image: uploads/2016/02/podcast_cover.jpg
    category: Religion & Spirituality
    sub_category: Christianity
    feed: https://gympiepresbyterian.org.au/sermons/index.xml
    iTunes: https://itunes.apple.com/au/podcast/gympie-presbyterian-sermons/id1086494939?mt=2
    spotify: https://open.spotify.com/show/1bflnIm9C6Hsmtk6XKceuU

  map:
    service: google
    api_key: AIzaSyDsLwLTHraJKtrZNgb7W-4eL4OK1TkqbtQ
    latitude: -26.199850
    longitude: 152.674713
    zoom: 19

  contact:
    service: netlify
    confirm_page: /contact-thanks

  footer:
    copyright:
      - '&copy; Gympie Presbyterian Church'
      - 'Built with [hugo](//gohugo.io)'
      - 'Theme: [Alpha Church](//github.com/funkydan2/alpha-church)'
      - '[Privacy Policy](/privacy)'

  links:
    - service: Facebook
      icon: fa-facebook
      icon_pack: fab
      link: https://facebook.com/gympiepresbyterian
    - service: Podcast
      icon: fa-podcast
      icon_pack: fas
      link: https://gympiepresbyterian.org.au/sermons/index.xml
    - service: YouTube
      icon: fa-youtube
      icon_pack: fab
      link: https://www.youtube.com/channel/UCMGgxNs2VQ427Fg_NKW-duw/
    - service: Full RSS Feed
      icon: fa-rss
      icon_pack: fas
      link: https://gympiepresbyterian.org.au/index.xml

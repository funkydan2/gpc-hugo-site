---
date: 2021-01-27T13:26:57+10:00
lastmod: 2022-03-25T09:33:50+10:00
title: Church on Sunday
images: [uploads/2020/10/FenceBanner.jpg]
---
# When is church?
We meet at 9:30 am on Sundays.

## How do I get there?
We meet in the One Mile State School hall, which is on [McLeod Lane, Gympie](https://www.google.com/maps/place/One+Mile+State+School/@-26.1997787,152.6742752,19z/data=!4m5!3m4!1s0x6b94a30d9a0bf369:0xde32fe78650ce6b!8m2!3d-26.1998437!4d152.6752904 "Find us on Google Maps").

{{< loc.inline >}}{{ partial "map" . }}{{< /loc.inline >}}

Parking is available along McLeod Lane as well as in the large car park off Phoenix Street.

If you're planning a visit and would like us to reserve a parking spot close to the hall, [please get in touch](/plan-your-visit/).

# What happens during the church service?
During the service, we’ll sing  together (don’t worry, you don’t have to sing if you don’t want to), be led in prayer, and listen to the Bible read and explained. We sometimes have a song or video for children (though, adults appreciate it too!). Our service runs for about an hour. After the service, you’re invited to join us for morning tea in the covered area outside the hall.

During the school term, we run KidsChurch for lower primary aged children. Find out more in our [FAQs](/frequently-asked-questions/).

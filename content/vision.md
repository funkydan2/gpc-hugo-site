+++
date = "2018-07-24T19:19:51+10:00"
images = ["/uploads/2018/07/Our Vision.png"]
title = "Vision"
+++
Our vision is to see each member of our church grow in their _knowledge_ of God. To know God isn't just to know about him but to respond to God in repentance and faith. Our vision is to see people who know God demonstrate this in _loving_ the people of our church, caring for each other spiritually and physically. Our vision is to see _more people_ in our region come to know God, which will happen as we _go out_ with the good news of Jesus.

We will be a church which desires to see people, both believers and non-believers, to come to know God (Ephesians 1:17 Galatians 4:9). We'll do this through hearing God's Word, the Bible, in groups large and small, in families, and personally (Acts 2:42). We'll prayerfully work towards building a community who loves one-another and demonstrates our love through serving and speaking God's Word to each other (John 13:34-35; Colossians 3:16). We'll be a church devoted to reaching our community with the good news of Jesus. We know this will only happen as we go into our community living wisely and speaking of Christ (Romans 10:14-15; 1 Peter 2:9-10).

If you want to find out more about our vision, [join us on a Sunday](/sundays) or [get in touch](/contact).

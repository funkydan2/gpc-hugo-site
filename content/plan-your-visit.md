---
title: "Plan Your Visit"
date: 2018-06-21T20:07:47+10:00
lastmod: 2024-02-10T10:01:30+10:00
images: ["/uploads/2020/10/FenceBanner.jpg"]
draft: false
---

We love meeting new people, and we would love to meet you! Every week new people come to our [Sunday Service](/sundays). At Gympie Presbyterian Church we don't pretend to be perfect, and we don't expect you to either. We all come from a variety of circumstances to meet with each other and our God who is perfect. So if you would like let us know you are coming, we can answer any questions you may have via email, or we can even shout you a coffee and have a chat during the week at a local café.

{{< contact.inline >}}{{ partial "contact" . }}{{< /contact.inline >}}

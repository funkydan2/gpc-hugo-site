---
title: "Giving"
subtitle: "Gospel Generosity"
date: 2023-08-01T11:58:34+10:00
lastmod: 2024-12-17T09:25:54+10:00
---

We encourage those who call Gympie Presbyterian Church their home to consider how they can generously support the work of the gospel in our region. To help you do that, we encourage you to look at this short, personal Bible study, [Gospel Generosity](https://drive.google.com/file/d/1X9BypirHAWaaW_40PUOJcIjnDAOq2zqF/view?usp=share_link).

You can give to the ministry of Gympie Presbyterian through the box on the welcome desk or by direct deposit into our bank account (NAB):

> Account Name: Crown Road Presbyterian Church  
> BSB: 084-691  
> Account Number: 50875-1913

You can also give as you recycle drinking bottles through [Containers for Change](https://www.containersforchange.com.au/qld/). Our church's account number is **C11078465**.

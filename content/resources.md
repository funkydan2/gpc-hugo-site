---
title: "Resources"
date: 2018-06-21T20:21:58+10:00
lastmod: 2023-10-16T14:37:44+10:00
---

Sermon recordings are available on our [website](/sermons/), via [podcast](http://feeds.feedburner.com/GympiePresbyterianChurch), or you can [contact us](/contact/) for a CD.

You can see a list of [all sermons](/sermons/) or look through our [series](/series/).

And get the latest news on our [website](/posts) or by liking our [Facebook page](https://facebook.com/gympiepresbyterian).

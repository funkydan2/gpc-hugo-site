+++
[banner]
#  [[banner.button]]
#    url = "/posts/christmas-2024/"
#    text = "Christmas Services"
#    type = "primary"

  [[banner.button]]
    text = "Plan Your Visit"
    url = "/plan-your-visit/"

#  [[banner.button]]
#    text = "Christianity Explored"
#    url = "/explore/"
#    type = "primary"

  [[banner.button]]
    text = "Find Out More"
    url = "#feature-icons"
    type = "primary"

#  [[banner.button]]
#    text = "Flood Update"
#    url = "/posts/2022-02-26-flood-update"

#Details for the box below the banner
[services]
  title = "Church on Sunday"
  text = "We meet 9:30 am Sunday in the [One Mile State School hall, McLeod Lane, Gympie](/sundays/)."
  map_location = "Gympie Presbyterian Church"

[feature_icons]
  #These feature icons look best if there's an even number of them.
  enable = true

  #Accent is a colour defined in the CSS file. Choose between 1 and 5
  [[feature_icons.tile]]
    icon = "fa-bible"
    icon_pack = "fas"
    accent = "1"
    title = "About Jesus"
    text = "Read more"
    url = "/about-jesus/"

  [[feature_icons.tile]]
    icon = "fa-users"
    icon_pack = "fas"
    accent = "2"
    title = "About Us"
    text = "Find out more"
    url = "/about/"


[feature_images]
  #These feature images look best if there's an even number of them.
  enable = true

  [[feature_images.tile]]
    image = "uploads/2015/11/lightstock_5068_medium_daniel_saunders-300x200.jpg"
    title = "I'm New"
    text = "Ever felt out of place? You belong here."
    url = "/im-new/"
    button_text = "Get Started"

  [[feature_images.tile]]
    image = "uploads/2018/06/question-300x200.jpg"
    title = "FAQs"
    text = "What should I expect at Gympie Presbyterian?"
    url = "/frequently-asked-questions/"
    button_text = "Read More"

  [[feature_images.tile]]
    image = "uploads/2015/11/BibleStudy_Stock-500x333.jpg"
    title = "Midweek Ministries"
    text = "Want more during the week?"
    url = "/midweek/"
    button_text="Join A Group Today!"

  [[feature_images.tile]]
    image = "uploads/2016/02/sermons.jpg"
    title = "Sermons"
    text = "Preaching and teaching"
    url = "/sermons/"
    button_text="Listen Online"

[CTA]
  enable = true
  heading = "Get In Touch!"
  message = "We'd love to hear from you."
+++

---
date: 2015-12-07 09:49:09 +0000
title: About Jesus
images: ["uploads/2018/08/lightstock_510355_download_vector_daniel_saunders.png"]
---
# Why Jesus?
Being a Christian isn't about following religious traditions, obeying the 'golden rule', or attending church services. It's about knowing and trusting Jesus and finding forgiveness and life in Him.

Christians believe that Jesus is like no other person who ever lived or will live. His life is unlike anyone else's because he alone always lived God's way. And his death by execution on a cross was like no other because it brought life and forgiveness to the world. Jesus life and death was to show God's love for us:

> This is love: not that we loved God, but that he loved us and sent his Son as an atoning sacrifice for our sins. (1 John 4:10 NIV)

If you’d like to meet Jesus through the Bible, please come along to a [Sunday service](/sundays) or [one of our activities](/midweek). We'd love to help you [plan your visit](/plan-your-visit).

## More about Jesus
{{< youtube ZnpU6nUHzRc>}}

You can also dig deeper into who Jesus is and what Christians believe by visiting [Two Ways to Live](http://twowaystolive.com). You can also join one of our [Explore courses]({{< ref explore.md >}}).

## More about what we believe
As a Presbyterian Church, we hold to the [Westminster Confession of Faith](https://presbyterian.org.au/download/wcf-australian-version-2019/) read in the light of the [Declaratory Statement](https://presbyterian.org.au/the-declaratory-statement/).

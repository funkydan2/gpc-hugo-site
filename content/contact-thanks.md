---
date: 2019-05-28T13:01:06+10:00
title: Thanks for getting in touch!
images: ["/uploads/2015/11/lightstock_121230_medium_daniel_saunders-940x350.jpg"]
---
Thanks for getting in touch. One of our [church leaders](/leaders) will get back to you soon.

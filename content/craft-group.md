---
date: 2016-05-11 03:24:05+00:00
images: ["/uploads/2020/10/BibleAndCraft.jpg"]
title: Bible and Craft Group
---
Do you enjoy craft and company?

Wednesday mornings a friendly group meets together in our hall for craft, friendship, and Bible study. During our time together we do all sorts of crafty things. Sometimes we work on a larger project for the Gympie community, other times we learn new skills, though you're welcome to bring along your own project to work on.

The morning starts at 9:30 with a cuppa and light morning tea. Then we spend some time reading the Bible and discussing it, we pray for one another and then get into our craft project. We usually finish around 11:30, though you are free to leave when you need to (or stick around longer if you're on a roll!).

A donation of $2 to cover morning tea and any extra craft supplies is appreciated, but not required.

To find out more, please [contact us](/contact/).

![Lavendar Bags](/uploads/2016/05/Craft_1.jpg)
![Gift Jars](/uploads/2016/05/Craft_2.jpg)
![Christmas Gifts](/uploads/2016/05/Craft_3.jpg)

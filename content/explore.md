---
title: "Christianity Explored"
subtitle: "What's the best news you've ever heard?"
date: 2020-06-19T09:00:00+10:00
images: ["/uploads/2021/02/CEBanner.jpg"]
tags: ["Event"]
---
What's the best news you've ever heard?

[Christianity Explored](https://www.christianityexplored.org) is a short, seven-session series of discussions to discover what Jesus is all about..

During each session we'll watch a short video, read a portion of the Bible, and discuss what we've heard. There'll be opportunities for you to ask as many questions as you'd like and you won't be asked to pray, sing, or read out loud.

<a href="#upcoming-courses" class="button small">Next course <i class="fas fa-calendar"></i></a>
<a href="#tell-me-more" class="button alt small">Sign up now <i class="fas fa-edit"></i></a>

{{< youtube B5W-11Om8ME >}}

# Frequently Asked Questions
## Is there a cost?
There is no cost for participating in *Christianity Explored*.
## How long does the course run for?
There are seven sessions in the course, and each session goes for about 60-90 minutes.
## Do I need to attend the whole course?
We find people get the most out of the course if they can attend all seven sessions. If, however, you can't attend one we can arrange for you to watch the videos at another time.
## Why do you run *Christianity Explored*?
We run this course because our church is made up of all sorts of people who've explored life's questions and found Jesus' answers to be compelling, exciting, and true.

# Upcoming Courses
Our next *Christianity Explored* course will run in all our [Growth Groups]({{< relref "growth-groups.md" >}}) in Term 1 2021.

# Tell me more
If you'd like to join us or you're interested in finding out more, please fill out the form below.
<form name="explore" method="POST" action="contact-thanks" netlify>
  <div class="row gtr-50 gtr-uniform">
    <div class="col-12">
        <input type="text" name="name" aria-label="Your name" placeholder="Your name">
    </div>
    <div class="col-6 col-12-mobilep">
        <input type="text" name="email" aria-label="Your email" placeholder="Your email">
    </div>
    <div class="col-6 col-12-mobilep">
        <input type="text" name="phone" aria-label="Phone number" placeholder = "Phone number">
    </div>
    <div class="col-12">
      <textarea name="message" aria-label="Enter your message" placeholder="(Optional) Enter your message" rows="6"></textarea>
    </div>
    <div class="col-12">
      <ul class="actions special">
          <li>
          <input type="submit" value="Send" />
        </li>
      </ul>
    </div>
  </div>
</form>

We respect your privacy. Any details given through this form will only be used for Gympie Presbyterian Church running the *Christianity Explored* course. For more information see the [Presbyterian Church of Queensland privacy policy](https://www.pcq.org.au/pcq-policies-and-procedures.php).

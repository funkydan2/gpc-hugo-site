---
date: 2015-11-26 07:10:57+00:00
draft: false
title: I'm New
images: [uploads/2015/11/lightstock_5068_medium_daniel_saunders-940x350.jpg]
---

If you're new to Gympie or new to thinking about church, please let us introduce ourselves to you.

[About Jesus](/about-jesus/)

[About Us](/about/)

[FAQ](/frequently-asked-questions/)

[Join us this Sunday](/sundays/)

[Life Explored]({{< ref explore.md >}})

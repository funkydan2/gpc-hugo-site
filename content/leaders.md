---
date: 2020-02-11 17:20:55+10:00
lastmod: 2023-11-19T15:33:08+10:00
title: Leaders
---

# Minister - Rev. Daniel Saunders

![Saunders Family](/uploads/2015/11/24425429556_12bf8c3c16_m.jpg)]

Daniel and his family have been ministering in Gympie since 2016. Daniel loves to see people growing as followers of Jesus and is excited to see God at work in the Gympie Region. Daniel is the Gympie Ministers' Network's coordinator for Religious Instruction in the local state schools. During his time off he enjoys playing board games with his family, running (you'll see him most Saturday's at [parkrun](https://www.parkrun.com.au/victoryheightstrail/)), roasting and drinking coffee, and keeping track of the world of technology.

Prior to moving to Gympie, Daniel was the Assistant Minister at [Kenmore Presbyterian Church](http://kpc.org.au). Daniel studied for the ministry at [Moore Theological College](http://moore.edu.au) and the [Queensland Theological College](http://www.qtc.edu.au).

# Elders
Together with the minister, our church is lead by a team of elders.

Our elders are:
* Cliff Roberts
* Rev. Jono Buesnel (Assessor/Presbytery Elder) ([Bargara Presbyterian Church](https://www.bargarapresbyterian.org.au))
* Rev. Shane Wright (Assessor/Presbytery Elder) ([Palmview Church](https://www.palmview.church))—Session Clerk

# Committee of Management

Elected annually by the congregation

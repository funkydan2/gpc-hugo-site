---
title: Privacy Policy
date: 2022-04-08T09:50:09+10:00
---
# Your Privacy
Gympie Presbyterian Church respects your privacy. This ‘Privacy Statement’ summarises how we deal with and protect any private information we may acquire in our dealings with you.

## Browsing this website
Viewing the pages of this website does not involve the transmission or storage of personal information beyond the generic web browser details commonly logged by web servers. Such information may be used to improve and extend this site over time.

## Contact page
For us to respond to your website enquiry (via the [“Contact” page]({{<ref "contact.md">}}) page) certain information is required. This information will be used and possibly retained by us to respond to your enquiry. It will not be used for other purposes nor shared with third parties without your consent. Details entered on the website enquiry form are sent via email which is not secure and so no guarantee is given as to the confidentiality of information sent via this method.

## Visitor Feedback forms
Contact details gathered through the Visitor Feedback forms available at church events will be securely stored in our church management system, [Elvanto](https://www.elvanto.com). Your information will be accessible by church leaders (minister and elders) and used only for the ministry of Gympie Presbyterian Church.

We may use third-party systems (e.g. mailing lists) in our ministry. In these situations, minimal personal information will be disclosed (e.g. name and email address for mailing systems). We will use systems that respect your privacy (e.g. email systems which comply with anti-spam laws).

## Other forms of Contact
Be aware that responding to your enquiries may require us to request identifying information. The nature and extent of this information will vary depending on the type of interaction you have with us.

## Scope
This is a statement of the Gympie Presbyterian Church which covers this website only. It does not apply to external sites linked to on this site, including the sites of the Presbyterian Church of Queensland.

## PCQ Privacy Policy
The full PCQ Privacy Policy, which covers our church, is [available for download here](https://www.pcq.org.au/pcq_pdf/pcq-privacy-policy-02-21.pdf).

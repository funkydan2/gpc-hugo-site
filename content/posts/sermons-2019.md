---
date: 2019-01-01
title: Sermon Series in 2019
tags:
- Bible Study
- Sermon Series
images:
- "/uploads/2019/01/bible-and-plant.jpg"
---
In 2019, we're going to growing in [Knowing God]({{< relref "vision.md" >}}) as we study His word together. In the first three terms this year, we're excited to be partnering with churches around Australia in our teaching and preaching through [Church2Church](https://www.church2church.org.au).

# Term 1 - The Way Home (Part 3)
As we pick up the story of *Luke’s Gospel* in *chapters 10-14*, we will see Jesus journeying to Jerusalem. The cross is looming larger. And Jesus’ call to discipleship is growing louder. We will hear the one journeying to the cross calling us to take up our own cross – living life in the same shape as his.

![The Way Home 3](/uploads/2019/02/TheWayHome3.jpg)

[Sermon Recordings](/series/the-way-home-part-3/)

# Term 2 - Still Waiting for the True King
When we look at our world it’s easy to feel despair – there’s so much that is wrong. We long for a world of peace and rest, not conflict and anxiety. We long for a life of joy and blessing, not sorrow and pain. What would it take to put things right again?

The story of *2 Samuel* gives voice to these longings we have, but looks forward with hope to a world put right.

Jesus entered the world as God’s true and long-promised king. He fulfilled the promises of the Old Testament and now invites us into a new way of life lived under his loving rule. A life of true rest, peace, joy, and blessing.

![Still Waiting For The True King](/uploads/2019/04/SWFTTK.jpg)

[Sermon Recordings](/series/still-waiting-for-the-true-king/)

# Term 3 - The Way Home (Part 4)
As we pick up the story of *Luke’s Gospel* in *chapters 15-19*, we will see Jesus’ journey leading him to Jerusalem. As the cross draws nearer, God’s judgement is imminent, and Jesus’ call to urgent action to choose the right king grows louder. We will see what the worldly kingdoms offer and see that God’s kingdom is one of radical and lavish generosity. If we choose God’s kingdom, we will be challenged to show the same radical and lavish generosity God showed to us through Jesus to the people in need around us.

![The Way Home 4](/uploads/2019/07/TWH4.jpg)

[Sermon Recordings](/series/the-way-home-part-4/)
# Term 4 - Back to Basics
Whether you're kicking off in the Christian life, or you've been following Jesus for many years, you need a strong foundation. In this series we're going to put together the building blocks of the faith—the gospel, increasing in godliness, and growing in knowing God.

![Back To Basics](/uploads/2019/10/back-to-basics.jpg)

[Sermon Recordings](/series/back-to-basics/)

This series is based on the well known set of Bible studies ["Just For Starters"](https://www.matthiasmedia.com.au/just-for-starters).

---
title: "Philippians: Gospel Partners"
subtitle: "Growth Group Studies"
date: 2024-09-30T05:03:28.357Z
images: ["/uploads/2024/10/GospelPartners-Cover.jpg"]
tags: ["Growth Group"]
---
It's been said the Christian life is more like netball than gold, it's a team sport.

In term 4 we'll be studying Paul's letter to the Philippians where we see the beauty of partnership in the gospel and learn how to stop things that erode unity.

[Download the study booklet here](/uploads/2024/10/Philippians-GospelPartners.pdf) and [contact us]({{< ref "contact.md" >}}) if you want to joing a [Growth Group]({{<ref "growth-groups.md">}}).

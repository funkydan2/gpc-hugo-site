---
date: 2023-01-01
title: Sermon Series in 2023
tags:
- Bible Study
- Sermon Series
images:
- "/uploads/2019/01/bible-and-plant.jpg"
---
# Summer Holidays - Grow in Grace (2 Peter)
At a time when followers of Jesus were being mocked and led astray, Peter wrote to give believers confidence in the hope of Jesus' return and encourage them to grow in grace.

![Grow in Grace: 2 Peter. Image of a growing plant in soil](/uploads/2023/01/2Peter.jpg)

[Listen Here](/series/grow-in-grace/)

# Term 1 - Unstoppable Kingdom (Acts 15-28)
What can stop God’s plans? Can riots? Prison? Trials? Storms? Shipwreck? Join us this term as we’re reminded and encouraged by God’s unhindered kingdom.

![Unstoppable Kingdom: Acts 15-28. Image of globe held in a hand.](/uploads/2023/01/Acts15-28.jpg)

# Term 2 & 3 - Zechariah and Zepheniah
![Zephaniah: Seek the LORD.](/uploads/2023/04/zeph.jpg)

[Listen Here](/series/zephaniah/)

![Zechariah: Kingdom Come.](/uploads/2023/04/zechariah.jpg)

Jesus came announcing the arrival of the Kingdom of God. Hundreds of years earlier, when there was no king and no temple, the prophet Zechariah spoke. His message was a day would come when sin would be cleansed, God’s people restored, and the kingdom come. Join us this term as we hear what it means to be part of God’s kingdom.

[Listen Here](/series/kingdom-come/)

During this term our Growth Groups will be learning about [Caring for One Another](https://www.crossway.org/books/caring-for-one-another-tpb/)

# Term 3 - Faith Under Pressure (1 & 2 Thessalonians)
Ill health, financial problems, sinful temptations, outside pressure: Christians can feel their faith is under all kinds of pressure. In Terms 3 & 4, we’re going to be hearing from two early Christian letters to believers whose faith was under pressure.

![Faith Under Pressure Series Image](/uploads/2023/07/FUP-Sermons.jpg)

# Term 4 - Half Hearted (Malachi)
As the Old Testament closes, we see God's people becoming weary, half-hearted in their worship of God. Through the prophet Malachi, God reminds us of all he's done for his people, so we might live whole-heartedly for him.

![Half Hearted Series Image](/uploads/2023/11/HalfHearted.png)

During this term our Growth Groups will be exploring [God's Big Picture](//godsbigpicture.co.uk)--a Bible overview.

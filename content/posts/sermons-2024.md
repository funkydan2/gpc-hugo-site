---
date: 2024-01-01
lastmod: 2024-09-09T08:30:40+10:00
title: Sermon Series in 2024
tags:
- Bible Study
- Sermon Series
images:
- "/uploads/2019/01/bible-and-plant.jpg"
---
# Summer & Term 1 - Matthew 1-7
Starting over Christmas 2023 and going through to Easter 2024 we'll be meeting Jesus, the 'King of Heaven,' as we sit at the feet of Jesus and learn how to 'stay salty' as his apprentices.

![King of Heaven: Matthew 1-4. Image of a river in a desert with icon of Crown](/uploads/2023/12/KingOfHeaven.jpg)

[Listen here](/series/king-of-heaven/)

![Stay Salty: Matthew 5-7. Image of table salt](/uploads/2024/02/StaySalty.jpg)

[Listen here](/series/stay-salty)

During term 1 our [Growth Groups]({{<ref "growth-groups.md">}}) will be studing the [Sermon on the Mount (Matthew 5-7)]({{<ref "2024-01-22-stay-salty-growth-group.md">}}).

# Term 2 - 1 Kings
From what seems like good beginnings, the kings of God's people fail to live up to God's calling. But despite decline and disappointment God doesn't desert his people as we wait for the promised king.

![1 Kings: Decline and Despair](/uploads/2024/04/1Kings.png)

[Listen here](/series/1-kings)

During Term 2 in our Growth Groups we'll be learning to [Resolve Everyday Conflict](https://peacewise.org.au/store/product/resolving-everyday-conflict-kit/).

# Term 3 - Matthew 8-12
In term 3 we'll continue our series in Matthew at church ([listen to the sermons here](/series/seeing-the-kingdom/)) and [in growth groups]({{< ref "2024-07-16-seeing-the-kingdom-growth-group.md">}}).

![Matthew 8-12: Seeing the Kingdom](/uploads/2024/07/SeeingTheKingdom.jpg)

# Term 4 - Philippians
It’s been said following Jesus is more like netball than gold—it’s a team sport or a partnership. Through Paul’s letter to the believers in Philippi, we’re encouraged to live out our gospel partnership within our church and beyond.

![Philippians: Gospel Partners](/uploads/2024/10/GospelPartners.jpeg)

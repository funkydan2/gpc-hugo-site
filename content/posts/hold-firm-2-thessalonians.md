---
date: 2017-05-28 23:51:15+00:00
draft: false
title: Hold Firm - 2 Thessalonians
tags:
- Sermon Series
images: ["/uploads/2017/05/Hold-Firm-2-Thessalonians-420x190.jpg"]
---

For the final few weeks of term 2, we'll be having a short sermon series in 2 Thessalonians. 2 Thessalonians is a very short New Testament letter—only three chapters. It's written to believers who are being persecuted and are anxious about the future. It's written to encourage them (and us!) to hold firm to Jesus through these times.

It starts 9:30 am, June 4, at [Gympie Presbyterian Church](/sundays/).

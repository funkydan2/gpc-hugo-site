---
date: 2017-04-06 00:08:15+00:00
draft: false
title: Easter Services 2017
tags:
- Event
- Easter
---

This term we've been following Jesus on [his way to the cross](/series/the-way-to-the-cross/) as recorded in Luke's biography of Jesus. In this final week the King came to his capital, the Son of God came to his temple, but he faced rejection and betrayal.

Join us this Easter weekend as we reflect on the dramatic conclusion to this week and the astounding new beginning of the next.

8:30am Good Friday, 9:30am Easter Sunday, [11 Crown Road, Gympie](/sundays/).

![GoodFriday](/uploads/2016/03/GoodFriday.jpg)
![Easter](/uploads/2016/03/Easter.jpg)

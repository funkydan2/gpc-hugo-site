---
date: 2023-12-01
title: Christmas 2023
subtitle: "Immanuel: God With Us"
tags:
- Event
images: ["/uploads/2023/12/Christmas1.jpg"]
---
We can be surrounded by people but feel alone. Texting isn't the same as talking. Devices interrupt our dinner tables.

The Bible says that because of Jesus' birth we're not alone because *God is with us*.

We'd love you to join us as we celebrate Christmas with us!

![Christmas Service Details](/uploads/2023/12/Christmas2.jpg)

**Christmas Eve:** December 24, 9:30 am

**Christmas Morning**: December 25, 8:30 am

[One Mile State School Hall, McLeod Lane, Gympie](https://www.google.com.au/maps?hl=en&q=one+mile+state+school)

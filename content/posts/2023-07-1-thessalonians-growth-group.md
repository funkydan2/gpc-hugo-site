---
date: 2023-07-13
lastmod: 2023-09-13T11:30:17+10:00
title: '1 Thessalonians: Faith Under Pressure'
tags:
- Growth Group
images: ["/uploads/2023/07/FUP-Studies.jpg"]
---

In term 3 2023 we'll be studying 1 Thessalonians in our [Growth Groups]({{<ref "growth-groups.md">}}). Please [get in touch]({{< ref "contact.md">}}) if you'd like to join a group. You can [download the study booklets here.](/uploads/2023/07/FUP.pdf) (The images in this PDF are deliberately provided in low resolution. Full resolution versions will be printed and available at church and in Growth Groups.)

These studies accompany our sermons on 1 & 2 Thessalonians which will kick off later in term 3.

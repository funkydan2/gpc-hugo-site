---
title: Combined Youth Term 4
description: "Find out who Jesus is from John's gospel."
date: 2024-10-02T01:55:35.858Z
tags: ["News","Youth"]
---
In term 4 2024, Combined Youth continues fortnightly on Friday nights.

![Combined Youth Program](/uploads/2024/10/CombinedYouth-Term4Flyer.png)

[Drop us a line]({{<ref "contact.md">}}) to find out more.

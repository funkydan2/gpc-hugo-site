---
title: "Matthew 8-12: Seeing the Kingdom"
date: 2024-07-16T11:13:32+10:00
images: ["/uploads/2024/07/SeeingTheKingdom-GGBooklet.jpg"]
tags: ["Growth Group"]
---
In term 3 2024 we'll be studying Matthew 8-12 in our [Growth Groups]({{<ref "growth-groups.md">}}). Please [get in touch]({{< ref "contact.md">}}) if you'd like to join a group. You can [download the study booklets here.](/uploads/2024/07/SeeingTheKingdom.pdf) (The images in this PDF are deliberately provided in low resolution. Full resolution versions will be printed and available at church and in Growth Groups.)

These studies accompany our sermons on Matthew 8-12.

---
date: 2022-02-05T19:23:23+10:00
title: 'Acts 1-7: The Saving Word'
tags:
- Growth Group
images: ["/uploads/2022/01/Acts1-7.jpg"]
---

In term 1 2022 we're going to be studying Acts 1-7 in our [Bible Study groups]({{< relref "growth-groups.md" >}}). Please [get in touch]({{< ref "contact.md">}}) if you'd like to join a group. You can [download the study booklets here.](/uploads/2022/02/ActsPart1.pdf)

You may also find [this graphic of the *Gospel Progress in Acts*](https://visualunit.me/2011/03/05/gospel-progress-in-acts/) useful to accompany these studies.

These studies accompany our [sermons on Acts 1-7.](/series/the-saving-word/)

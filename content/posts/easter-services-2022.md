---
date: 2022-04-07T08:04:24+10:00
title: Easter Services 2022
#images: ["/uploads/2021/03/Easter2021.jpg"]
tags:
- Event
- Easter
---
> And when the centurion, who stood there in front of Jesus, saw how he died, he said, “Surely this man was the Son of God!” (Mark 15:39 NIV)

Who is Jesus? The answer of the soldier who witnessed his death is that he is the *Son of God*. But what does this mean? Why would the Son of God allow himself to die? And what are we to make of the events on the first day of the next week?

We'd love you to join us this Easter as we explore Jesus' death and resurrection and how it offers freedom and life for us.

Good Friday: 8:30 am || Resurrection Sunday: 9:30 am

[One Mile State School, Gympie](/sundays/)

![](/uploads/2021/03/GoodFriday.jpg)
![](/uploads/2021/03/EasterSunday.jpg)

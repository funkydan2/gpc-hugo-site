---
title: "Easter Services 2024"
subtitle: ""
date: 2024-03-22T17:04:17+10:00
tags: ["Event","Easter"]
---
We'd love you to join us this Easter as we explore the great exchange and living hope Jesus brings.

Good Friday: 8:30 am || Resurrection Sunday: 9:30 am

In the [One Mile State School Hall, Gympie](/sundays/)

![](/uploads/2024/03/GoodFriday.jpg)
![](/uploads/2024/03/EasterSunday.jpg)

---
date: 2024-01-22
title: 'Matthew 5-7: Stay Salty'
tags:
- Growth Group
images: ["/uploads/2024/01/StaySalty-Cover.jpg"]
---

In term 1 2024 we'll be studying Jesus' 'Sermon on the Mount' in our [Growth Groups]({{<ref "growth-groups.md">}}). Please [get in touch]({{< ref "contact.md">}}) if you'd like to join a group. You can [download the study booklets here.](/uploads/2024/01/StaySalty.pdf) (The images in this PDF are deliberately provided in low resolution. Full resolution versions will be printed and available at church and in Growth Groups.)

These studies accompany our sermons on Matthew 5-7.

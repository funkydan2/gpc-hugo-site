---
date: 2021-01-01
lastmod: 2021-09-01T07:36:56+10:00
title: Sermon Series in 2021
tags:
- Bible Study
- Sermon Series
images:
- "/uploads/2019/01/bible-and-plant.jpg"
---
# Term 1 - Jesus: Who Do You Say He Is? (Mark 1-8)
People have many ideas about Jesus. Some say he is a creation of fiction, others that he's a spiritual guide or teacher, but the Bible claims he is God's promised king (Messiah) and even God himself (the Son of God). In this series we're going to the beginning of the gospel, to discover who Jesus really is.

![Jesus: Who Do You Say He Is?](/uploads/2021/01/Mark1-8.png)

[Listen here](/series/jesus-who-do-you-say-he-is/)

# Term 2 - Micah: What Does the LORD require?
What does God require of you?

Micah spoke God's message at a time of great prosperity but also growing international political turmoil. His message is a serious wake-up call to God's people—they cannot continue to ignore God. Yet through all the warnings of judgment, Micah also speaks of hope - God will send a *shepherd* to bring peace.

![What Does the LORD Require? Micah. Image of path over hill. Scales of justice. Heart in hands. Footprints.](/uploads/2021/04/Micah.jpg)

[Listen here](/series/micah/)

# Term 2 & 3 - 1 Peter: Hope away from Home
The [2020 Lowy Institute Poll](https://poll.lowyinstitute.org/report/) reported its lowest level of optimism in Australians. According to the poll, we don't have much hope about the economy or the environment, we're concerned about safety at home and around the world.

1 Peter is written to Christians who have reason to be pessimistic—-they're suffering 'grief in all kinds of trials' (1 Peter 1:6). Yet they 'rejoice' (1 Peter 1:6) and they have 'hope' (1 Peter 3:15).

Join us from June to August as we hear how because of Jesus you can have *Hope Away From Home*.

![Hope Away From Home: 1 Peter. Image of Road, Location Icon, Sheep, Flower](/uploads/2021/06/HAFH.png)

# Term 4 - Mark 8-16
People were struck by Jesus’ teaching—“What is this? A new teaching—and with authority!” (Mark 1:27 NIV). Not only did Jesus’ teaching come with authority, but he also had authority over sickness, storms, and evil spirits. But as we continue to meet Jesus, we discover he is God’s king who “did not come to be served, but to serve, and to give his life as a ransom for many.” (Mark 10:34).

We invite you to join us to meet *Jesus: the Servant King* starting September.

![Jesus: The Servant King, image of Crown of Thorns](/uploads/2021/09/JTSK.jpg)

---
date: 2022-07-05
title: 'Acts 8-15: Salvation to the World'
tags:
- Growth Group
images: ["/uploads/2022/07/Acts8-15.jpg"]
---

In term 3 2022 we're going to be studying Acts 8-15 in our [Bible Study groups]({{<ref "growth-groups.md">}}). Please [get in touch]({{< ref "contact.md">}}) if you'd like to join a group. You can [download the study booklets here.](/uploads/2022/07/ActsPart2.pdf) (The images in this PDF are deliberately provided in low resolution. Full resolution versions will be printed and available at church and in Bible study groups.)

You may also find [this graphic of the *Gospel Progress in Acts*](https://visualunit.me/2011/03/05/gospel-progress-in-acts/) useful to accompany these studies.

These studies accompany our sermons on Acts 8-15.

---
date: 2017-05-31 02:26:52+00:00
draft: false
title: Sing to Him - June 4 2017
tags:
- Event
- Singing
images: ["/uploads/2017/05/Sing-to-Him.jpg"]
---

> Let the word of Christ dwell richly among you, in all wisdom teaching and admonishing one another through psalms, hymns, and spiritual songs, singing to God with gratitude in your hearts. (Colossians 3:16)


Join us for an afternoon of sharing in song and fellowship. **June 4, 2 pm** at [11 Crown Road, Gympie](/sundays/). Please bring a plate to share.

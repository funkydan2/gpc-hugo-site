---
date: 2024-12-01
title: Christmas 2024
subtitle: "A Mary Christmas?"
tags:
- Event
images: ["/uploads/2024/12/Christmas1.jpg"]
---
In the hustle and bustle of Christmas, how can we find peace or even joy?

This Christmas, we'd love you to join us as we explore what it means to have a *Mary* Christmas! We'll be looking at *two 'Marys'* in the Bible and how they teach us to find peace and joy, not only at Christmas but all year round.

![Christmas Service Details](/uploads/2024/12/Christmas2.jpg)

**Carols Services:** December 22, 9:30 am

**Christmas Morning**: December 25, 8:30 am

[One Mile State School Hall, McLeod Lane, Gympie](https://www.google.com.au/maps?hl=en&q=one+mile+state+school)

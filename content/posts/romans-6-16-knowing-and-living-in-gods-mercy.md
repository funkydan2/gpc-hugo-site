---
date: 2017-10-01 23:00:47+00:00
draft: false
title: 'Romans 6-16: Knowing and Living in God''s Mercy'
tags:
- Growth Group
- Sermon Series
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

God's salvation in Jesus is amazing. In the opening chapters of Romans, we read 'all have sinned and fall short of the glory of God, and all are justified freely by his grace'. This message is good news—free salvation, undeserved forgiveness, gracious righteousness. But what difference does it make?

In term 4 2017, at both Church and Bible studies, we're continuing our series in Romans, seeing what it means to _know_ and to _live_ in God's mercy.

Please join us, [9:30 am, Sundays at 11 Crown Road](/sundays/), Gympie. Or [contact us](/contact/) about joining a [Bible study group](/bible-study-groups/).

The Bible study booklet [can be downloaded here (pdf).](/uploads/2017/09/Romans-6-12-Studies-2017.pdf) And if you miss any of the sermons you can [listen online](/series/knowing-and-living-in-gods-mercy/).

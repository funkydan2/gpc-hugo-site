---
date: 2020-01-01
title: Sermon Series in 2020
tags:
- Bible Study
- Sermon Series
images:
- "/uploads/2019/01/bible-and-plant.jpg"
---
# Term 1 - The Way Home (Part 5)
Continuing our series in Luke [in 2019]({{<relref sermons-2019>}}) we will see Jesus, God's promised King, as he enters Jerusalem. As Jesus' conflict with the religious and political powers comes to a dramatic head, we see God's King crucified in shame and rise in glory in fulfilment of the Scriptures.

![The Way Home 5: Behold Your King](/uploads/2020/02/TheWayHome5.jpg)

In this series we'll be continuing our partnerships through [Church2Church](http://church2church.org.au) as we finish off our journey home with Jesus as recorded in Luke's Gospel.

[Listen here](/series/the-way-home-part-5/)

# Term 1-2 - Psalms of Comfort
![Psalms of Comfort](/uploads/2020/04/Psalms_Of_Comfort.jpg)

A journalist has called 2020 *'The Year God Forgot'* (and [there's a great article reflecting on this on the Gospel Coalition Australia website](https://au.thegospelcoalition.org/article/when-sorrows-come/)). As life feels out of control and God may even feel far away, we look to God and cry out to him for comfort.

[Listen here](/series/psalms-of-comfort)

# Term 2 - Open Our Eyes (Ephesians 1:1-4:16)
We live in a world where we see the devastating results of sin: hopelessness, death, broken relationships. In his life, death, and resurrection, Jesus has won a cosmic victory to reign over all creation. And he invites us to trust in him and to enjoy the benefits of his victory. God calls us to know that only through his presence on earth—in the person of Jesus, and his Spirit in us—he brings reconciliation and renewal to all things.

![Open The Eyes of Your Heart](/uploads/2020/05/Ephesians_OTEYH.jpg)

[Listen here](/series/open-the-eyes-of-your-heart/)

# Term 3 - Living in the Light
Having had our *eyes opened* to what Jesus has won for his people, how should we live?

![Living In The Light](/uploads/2020/07/Ephesians_LITL.jpg)

[Listen here](/series/living-in-the-light/)

# Term 4 - What We Believe, Obadiah, and Jonah
In the final term of 2020, we'll spend a few weeks hearing from the Old Testament prophets of Obadiah and Jonah. Our main series for the term will be a topical series based around the ancient '[Apostles' Creed](https://en.wikipedia.org/wiki/Apostles%27_Creed#English_translations)'.

![What We Believe](/uploads/2020/10/WWB.jpg)

[Listen here](/series/what-we-believe/)

---
date: 2016-11-18 21:13:07+00:00
draft: false
title: Christmas 2016
tags:
- Event
images: ["/uploads/2015/12/Christmas2016.jpg"]
---

At Christmas there's lots to celebrate: family, holidays, food, summer, presents.

On the first Christmas there was the greatest reason to celebrate. The birth of 'a saviour, who is Christ the Lord.' This new born child caused angels to sing and kings to tremble.

This Christmas we invite you to join us to celebrate the saviour, Jesus.


**Carols:** December 9, 6:30pm


**Christmas Morning**: December 25, 8:30am


11 Crown Road, Gympie

---

Around Gympie there are other Christmas events Gympie Presbyterian will be participating in. If you can't make it to our carols or Christmas Day service (or just want more!) then we encourage you to get along to these.

[Cooloola Christian College Carols/Fete/Fireworks - 26th November](http://www.cccgympie.qld.edu.au/2015-03-01-09-31-35/ccc-celebrates-25yrs)

[Gympie Carols in the Park - hosted by the Ministers' Network - 17th December](https://www.facebook.com/GympieCarolsInThePark/)

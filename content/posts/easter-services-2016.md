---
date: 2016-03-10 04:49:33+00:00
draft: false
title: Easter Services 2016
tags:
- Event
---

Good Friday and Easter Sunday are at the focus of what Jesus came to do, and the centre of Christianity.

One of the earliest summaries of the Christian message is


> “Christ died for our sins according to the Scriptures, that he was buried, that he was raised on the third day according to the Scriptures, and that he appeared" (1 Corinthians 15:3–5)


Join us this Easter to listen and reflect on the death and resurrection of Jesus as recorded in Luke's gospel.

8:30am Good Friday, 9:30am Easter Sunday, [11 Crown Road, Gympie](/sundays/).

![GoodFriday](/uploads/2016/03/GoodFriday.jpg)
![Easter](/uploads/2016/03/Easter.jpg)

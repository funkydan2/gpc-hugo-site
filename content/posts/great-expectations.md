---
date: 2016-11-18 21:07:06+00:00
draft: false
title: Great Expectations
tags:
- Sermon Series
- Christmas
images: ["/uploads/2015/12/Great-Expectations.jpg"]
---

Waiting for the birth of a child is a time full emotions—joy, worry, hope, concern. It's a time of _great expectation_ for what the future may hold. In the weeks leading into Christmas 2016, join us at Gympie Presbyterian as we wait in wonder for the birth of the one whose birth was announced by angels


> “He will be great and will be called the Son of the Most High. The Lord God will give him the throne of his father David, and he will reign over Jacob’s descendants forever; his kingdom will never end.” (Luke 1:32–33)

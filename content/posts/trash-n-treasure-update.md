---
date: 2016-06-14 02:03:30+00:00
draft: false
title: Trash 'n' Treasure - Update
tags:
- News
images: ["/uploads/2016/05/TrashNTreasure.png"]
---

A big thanks to everyone who supported our [Trash 'n' Treasure]({{< relref "trash-n-treasure.md" >}}) sale on June 11. We had a great day, cooking up sausages and selling loads of 'stuff'—collectables, bric-a-brac, clothing, plants, relishes...even a piano!

The total amount raised during the sale was around $800, and as church we 'rounded up' the total to $1000. So we've been able to donate $500 each to [Care Outreach](http://www.careoutreach.com.au/) and [Community Action Gympie](http://www.communityactiongympie.com.au/). In addition, we had loads of leftover items which have been donated to [Little Haven](http://littlehaven.org.au), so they can sell the items and raise funds for their services in palliative care and cancer support services.

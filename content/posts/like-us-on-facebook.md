---
date: 2016-11-13 02:21:48+00:00
draft: false
title: Like us on Facebook
---

We've now got a page on [Facebook](//facebook.com/gympiepresbyterian). Head over [there](//facebook.com/gympiepresbyterian) and give us a like—it's a great way to keep up to date with things happening around our church.

---
date: 2019-04-09 11:26:01+10:00
draft: false
title: Easter Services 2019
images: ["uploads/2019/04/EasterSearching.png"]
tags:
- Event
- Easter
---
What are you searching for? Last year ‘What is Good Friday good?’ was one of the most searched questions on Google. Join us this Easter as we hear Jesus' answer to what we are searching for.

Good Friday: 8:30 am || Easter Sunday: 9:30 am

[11 Crown Road, Gympie](/sundays/)

During the Good Friday service, we'll take up a collection for the [PresAID Easter Appeal](https://presaid.org.au). This year, funds from the appeal will go towards building an amenities block in Zambia and a clean water supply for a prison in Malawi. You can also give to this appeal on Easter Sunday through a marked envelope, or directly into our church bank account.

![](/uploads/2016/03/GoodFriday-300x225.jpg)
![](/uploads/2016/03/Easter-300x225.jpg)

---
date: 2016-01-30 05:19:08+00:00
draft: false
title: Commissioning a New Minister
tags:
- Event
- News
images: ["/uploads/2016/04/DSCF5250_web.jpg"]
---

On January 30 2016 the [Presbytery of Wide-Bay/Sunshine Coast](http://www.pcq.org.au/presbyterian-church-directory.php?PbyID=5) commissioned Rev. Daniel Saunders to minister with Gympie Presbyterian Church. Our church was encouraged to have a number of members of Presbytery and members of [Kenmore Presbyterian Church](http://kpc.org.au) (where Daniel had previously ministered) joining with us to pray, encourage, and support our church and new minister.

{{< figure src="/uploads/2016/04/DSCF5257_web.jpg" caption="Rev. Saunders being interviewed by Rev. Wal Brown (Bundaberg Presbyterian Church)" >}}

{{< figure src="/uploads/2016/04/DSCF5267_web.jpg" caption="Elder Daryl Brenton praying for Rev. Saunders." >}}

---
date: 2017-08-30 21:30:09+00:00
draft: false
title: "Listen to 'Jesus' Crucifixion: One Among Many?'"
tags:
- Event
- Cross
- History
---

In partnership with [Gympie Baptist](//gympiebaptist.org/), we recently hosted a team of students from [Queensland Theological College](//qtc.edu.au) for a week of mission and ministry training. As part of the week one of the QTC lecturers, Dr Wes Regen, spoke on the nature and purpose of crucifixion in the Roman Empire. During the lecture, he explained why, out of the hundreds of thousands, if not millions, of people crucified under Rome, is Jesus' crucifixion considered significant by Christians 2000 years later.

<audio controls>
  <source src="https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20170822_JesusCrucifixionOneAmongMany_WR.mp3" type="audio/mpeg"/>
</audio>

[Download MP3](https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20170822_JesusCrucifixionOneAmongMany_WR.mp3)

The recording starts a few minutes into the lecture and doesn't include the Q & A session at the end of the night. If you've got further questions, we'd love to hear from you in the comment section below.

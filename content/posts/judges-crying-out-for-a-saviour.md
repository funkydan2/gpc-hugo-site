---
date: 2017-07-11 10:22:06+00:00
draft: false
title: 'Judges: Crying out for a Saviour'
tags:
- Sermon Series
images: ["/uploads/2017/06/Judges-640x350.jpg"]
---

What do you do when things go from bad to worse? Where do you turn when you see a spotlight shone on the ugliness and evil in the world?

This term at Gympie Presbyterian we're hearing from the Old Testament book of Judges. It a book of graphic and sometimes barbaric events which show our desperate need, the reason we _cry out for a Saviour_.

[**Join us**, 9:30 am Sundays](/sundays/) as we hear from this brutally honest part of God's Word, and look to our only hope, the Saviour Jesus.

[Listen to the sermons in this series here](/series/crying-out-for-a-saviour/).

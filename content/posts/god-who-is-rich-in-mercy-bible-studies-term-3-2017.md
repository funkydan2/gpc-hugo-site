---
date: 2017-07-25 00:05:47+00:00
draft: false
title: God, Who Is Rich In Mercy - Bible Studies Term 3 2017
tags:
- Growth Group
---

This term our Bible Study groups are looking at the Old Testament book of Judges. You can grab printed copies of the study books from church or download a low-resolution version [here](/uploads/2017/07/Judges-Bible-Studies-2017.pdf).

You can find our more about our [Bible Study groups here](/bible-study-groups/).

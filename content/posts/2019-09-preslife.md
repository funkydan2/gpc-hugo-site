---
title: "PresLife Church Profile"
subtitle: "A snapshot of our church family"
date: 2019-09-13T09:00:00+10:00
images: ["/uploads/2019/09/PresLife.jpg"]
tags: ["News"]
---
Want to find out a bit more about our church family? Our *church profile* was featured in the September 2019 issue of the PresLife Magazine. Download and read [the full edition here](https://www.pcq.org.au/pcq_pdf_pres_life/pres-life-qld-i3-19.pdf). You'll find us on page 6.

---
date: 2017-08-16 03:36:42+00:00
draft: false
title: Gympie Times article about our upcoming event
tags:
- Event
- News
---

[![](/uploads/2017/08/IMG_0744-1024x322.jpg)
](/uploads/2017/08/IMG_0744.jpg)

Big thanks to the [Gympie Times](https://www.gympietimes.com.au/news/jesus-crucifixion-one-among-many/3212752/) for this article about our upcoming event, '[Jesus' Crucifixion: One Among Many?]({{< relref "jesus-crucifixion-one-among-many-free-public-lecture.md" >}})'. It's looking to be a stimulating and enjoyable night. If you're thinking of coming along, you can RSVP via [Facebook](https://www.facebook.com/events/340924169695293/?acontext=%7B%22ref%22%3A%222%22%2C%22ref_dashboard_filter%22%3A%22upcoming%22%2C%22action_history%22%3A%22[%7B%5C%22surface%5C%22%3A%5C%22dashboard%5C%22%2C%5C%22mechanism%5C%22%3A%5C%22main_list%5C%22%2C%5C%22extra_data%5C%22%3A[]%7D]%22%7D).

---
title: "Flood Update"
subtitle: "Changes to church on February 27 and March 6"
date: 2022-02-26T07:18:11+10:00
lastmod: 2022-03-18T06:56:42+10:00
images: ["/uploads/2022/03/20220306FloodUpdate.jpg"]
tags: ["News"]
---
Due to recent rainfall, the Bureau of Meteorology is expecting record flood levels to hit Gympie on the morning of Sunday, February 27. Due to flooding, we'll be gathering **online only** on February 27. Those on our email list will receive a link to a Zoom meeting and we'll also be live-streaming to our private Facebook Group. If you don't receive our emails and would like to connect with us please [get in touch]({{<ref "contact.md" >}}). Also, [get in touch]({{<ref "contact.md" >}}) if you need flood relief.

**Update:** Due to continued flood clean up at One Mile State School, we'll be church will meet at [11 Crown Road, Gympie](https://www.google.com/maps/place/Gympie+Presbyterian+Church/@-26.1943571,152.6690848,17z/data=!3m1!4b1!4m5!3m4!1s0x6b94a36d244bfc9b:0x1218e6e7242a2f06!8m2!3d-26.1943571!4d152.6712788) 9:30 am on Sunday, March 6.

It's expected we'll return to gathering at [One Mile State School]({{<ref "sundays.md">}}) as of Sunday, March 13.

For more flood updates, please follow our [Facebook Page](https://facebook.com/gympiepresbyterian).

---

From March 7 to 25, our [Crown Road building](https://www.google.com/maps/place/Gympie+Presbyterian+Church/@-26.1943571,152.6690848,17z/data=!3m1!4b1!4m5!3m4!1s0x6b94a36d244bfc9b:0x1218e6e7242a2f06!8m2!3d-26.1943571!4d152.6712788) is being used by local volunteers to receive and distribute donated items to those impacted by flooding. Pop in between 10 am and 4 pm on weekdays to receive:
* Cleaning supplies
* Personal hygiene items
* Non-perishable food
* Work clothes.

This flood relief centre is not an activity of Gympie Presbyterian Church.

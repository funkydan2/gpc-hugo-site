---
date: 2016-02-03 04:49:28+00:00
draft: false
title: Bible Study Book—Term 1 2016
tags:
- Growth Group
---

Who is Jesus? What was His mission? Why does it matter to us?

In our bible studies this term, we'll be answering these questions as we read Luke 9-19 together.

Want to find out more? You can [contact us](/contact/) to join a bible study group, or [download a copy of the study booklet](/uploads/2016/02/Luke-9-19-Bible-Studies-2015_Web.pdf) for yourself.

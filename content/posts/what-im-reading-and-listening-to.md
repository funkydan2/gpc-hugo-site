---
date: 2016-03-26 07:05:32+00:00
draft: false
title: What I'm Reading and Listening To
tags:
- News
---

On Easter Saturday the Gympie Times featured some local ministers in their _What I'm Reading and Listening To_ section. They were kind enough to interview our minister and you can read his contribution by clicking on the image below.

[![GympieTimes](/uploads/2016/04/GympieTimes-1024x696.jpeg)
](/uploads/2016/04/GympieTimes.jpeg)

The book he references _The Final Days of Jesus_ can be bought from [good bookstores](https://booko.com.au/9781433535109/The-Final-Days-of-Jesus) or [on Kindle](https://www.amazon.com.au/Final-Days-Jesus-Important-Person-ebook/dp/B00HDHUTZ4/ref=sr_1_1?ie=UTF8&qid=1459753783&sr=8-1&keywords=The+final+days+of+jesus).

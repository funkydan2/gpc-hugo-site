---
title: "Combined Youth Term 3"
subtitle: "Find out who Jesus is from John's gospel."
date: 2024-07-15T11:01:00+10:00
#images: [""]
tags: ["News","Youth"]
---
In term 3 2024, Combined Youth continues fortnightly on Friday nights and finishes with the [SC Engage Camp](https://www.pyk.org.au/scengage/) with youth from around our whole region!

![Combined Youth Program](/uploads/2024/07/CombinedYouth-Term3Flyer.png)

[Drop us a line]({{<ref "contact.md">}}) to find out more.

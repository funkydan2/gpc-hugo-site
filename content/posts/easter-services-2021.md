---
date: 2021-03-22T12:00:00+10:00
title: Easter Services 2021
images: ["/uploads/2021/03/Easter2021.jpg"]
tags:
- Event
- Easter
---
> “After he has suffered, he will see the light of life” Isaiah 53:11 (NIV)

800 years before the time of Jesus, a prophet spoke of someone who would suffer 'for us' and yet after he was crushed this one would again see 'the light of life'.

We'd love you to join us this Easter as we explore Jesus' death and resurrection and how it offers forgiveness and life for us.

Good Friday: 8:30 am || Resurrection Sunday: 9:30 am

[One Mile State School, Gympie](/sundays/)

![](/uploads/2021/03/GoodFriday.jpg)
![](/uploads/2021/03/EasterSunday.jpg)

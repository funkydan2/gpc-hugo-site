---
date: 2016-04-14 05:38:48+00:00
draft: false
title: Bible Study Book—Term 2 2016
tags:
- Growth Group
images: ["/uploads/2016/04/Romans1-5BibleStudies2016_WEB.jpg"]
---

> For I am not ashamed of the **gospel**, because it **is the power of God** that brings salvation to everyone who believes: first to the Jew, then to the Gentile. For in the gospel the **righteousness of God** is revealed—a righteousness that is by faith from first to last, just as it is written: “The righteous will live by faith.”
Romans 1:16–17 NIV


This term at church and in bible study groups we'll be hearing from the first five chapters of Paul's letter to the church in Rome. It's a letter that presents the gospel—the universal problem of sin, and how in His righteousness, God saves sinners.

Bible study books are available at church and for [download here](/uploads/2016/04/Romans1-5BibleStudies2016_WEB.pdf).

---
date: 2020-12-01
draft: false
title: Christmas 2020
subtitle: Darkness Ends
tags:
- Event
images: ["/uploads/2020/12/Christmas2020-1.jpg"]
---
Many have found 2020 to be a year of darkness, with fires, floods, lockdowns, and unemployment. So for many people, Christmas couldn't come sooner—the darkness might finally end.

In the Biblical account of the birth of Jesus, we read of a world cloaked in darkness, but with the coming of Jesus, the sun rises and light comes.

This Christmas, we invite you to hear the good news that darkness has finally ended in Jesus.

![Christmas Service Details](/uploads/2020/12/Christmas2020-2.jpg)

**Carols Service:** December 20, 9:30 am

**Christmas Morning**: December 25, 8:30 am

[One Mile State School Hall, McLeod Lane, Gympie](https://www.google.com.au/maps?hl=en&q=one+mile+state+school)

As with all our church gatherings, we'll be following the approved [COVID Safe protocols]({{< relref 2020-07-covid-19.md >}}).

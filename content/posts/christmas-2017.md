---
date: 2017-11-20 23:40:07+00:00
draft: false
title: Christmas 2017
tags:
- Event
- Christmas
images: ["uploads/2017/11/2017-Christmas-Web.jpg"]
---

There were many _strange things_ around the time of Jesus' birth—angelic appearances, shepherds announcing new kings, visitors from distant lands, and a maiden who became a mum. These strange things were all pointing to one momentous event—the baby in the manager who is 'God with Us,' the promised saviour and king—it's strange _but true_.

Please [join us](/sundays/) this Christmas as we reflect on _strange Christmas things_ and celebrate the coming of Jesus.

**Carols**: December 1. Free sausage sizzle from 5:45 pm, **_Carols from 6:30 pm_**. If the weather's fine we'll be outside, so please bring a rug or chair.

**Christmas Eve**: December 24, 9:30 am

**Christmas Day**: December 25, 8:30 am

[11 Crown Road, Gympie](/sundays/).

---
date: 2019-12-01
draft: false
title: Christmas 2019
subtitle: Filled Full//Fulfilled
tags:
- Event
images: ["/uploads/2019/12/Christmas2019.jpg"]
---
For many people, Christmas is a time of feasting and fullness. Whether it's a baked meal with all the trimmings or steaks on the BBQ, Christmas lunch is a big event on many families calendars. An event where we often enjoy things, without too much moderation!

According to the Bible, fulness is what Christmas is about. Jesus' birth *fulfilled* many of God's ancient promises. Jesus is the *fullness* of God.

![Christmas Service Details](/uploads/2019/12/Christmas2019-Back.jpg)

This Christmas, we invite you to be *filled full* by the hope of Jesus.

**Carols Service:** December 22, 9:30 am

**Christmas Morning**: December 25, 8:30 am


[11 Crown Road, Gympie](https://www.google.com.au/maps/place/11+Crown+Rd,+Gympie+QLD+4570/@-26.1943563,152.6690575,17z/data=!3m1!4b1!4m5!3m4!1s0x6b94a36d24e50275:0x51f3916ef5b3466c!8m2!3d-26.1943611!4d152.6712462)

---

Around Gympie there are other Christmas events Gympie Presbyterian will be participating in. If you can't make it to our Christmas events (or just want more!) then we encourage you to get along to these.

[Gympie Carols in the Park - hosted by the Ministers' Network - December 21](https://www.facebook.com/GympieCarolsInThePark/)

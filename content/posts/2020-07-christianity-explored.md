---
title: "Christianity Explored - July 2020"
subtitle: "What's the best news you've ever heard?"
date: 2020-07-09T14:43:45+10:00
images: ["/uploads/2020/06/ChristianityExplored.jpg"]
tags: ["Event"]
---
We're excited to be running the *Christianity Explored* course *online* starting Tuesday July 14 2020 at 7:30 pm. After the success of running [Life Explored]({{< ref 2020-04-life-explored.md >}}) via Zoom, we're going to be running this course online too.

{{< youtube LMl14NIqABs >}}

Christianity Explored is a seven-week course which introduces us to the best news you've ever heard. You don't need to have any prior Bible knowledge or religious experience.

You can find out more and sign up on our [Christianity Explored page]({{< ref explore.md >}})

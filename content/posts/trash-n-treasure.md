---
date: 2016-05-10 05:54:24+00:00
draft: false
title: Trash 'n' Treasure
tags:
- Event
images: ["/uploads/2016/05/TrashNTreasure.png"]
---

Gympie Presbyterian Church is holding a _Trash 'n' Treasure_ sale on 11 June, 7am-1pm at [11 Crown Road, Gympie](/sundays/). There'll be loads of bargains: household goods, clothing, bric-a-brac, handmade crafts, plants, and delicious food—who knows what you'll find.

During the day there'll be a sausage sizzle for only $2.

All proceeds will go to support people in need through the work of [Care Outreach](http://www.careoutreach.com.au) and [Community Action](http://www.communityactiongympie.com.au). Care Outreach works to _care for the physical, emotional, and spiritual well-being of people in rural and remote Australia_. Community Action provides housing and support for those with needs in the Gympie Community, including [_Swags for the Homeless_](http://www.communityactiongympie.com.au/swags-for-the-homeless/).


# Grab a bargain and help those in need!

![](/uploads/2016/05/DSCF5432.jpg)
![](/uploads/2016/05/DSCF5433.jpg)
![](/uploads/2016/05/DSCF5434.jpg)
![](/uploads/2016/05/DSCF5435.jpg)
![](/uploads/2016/05/DSCF5432.jpg)
![](/uploads/2016/05/DSCF5436.jpg)
![](/uploads/2016/05/DSCF5437.jpg)
![](/uploads/2016/05/DSCF5438.jpg)
![](/uploads/2016/05/DSCF5439.jpg)
![](/uploads/2016/05/DSCF5440.jpg)

_Edit: find out how [much we raised here]({{< relref "trash-n-treasure-update.md" >}})._

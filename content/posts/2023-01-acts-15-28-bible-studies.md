---
date: 2023-01-25
title: 'Acts 15-28: Mission Unhindered'
tags:
- Growth Group
images: ["/uploads/2023/01/Acts15-28.jpg"]
---

In term 1 2023 we'll be studying Acts 15-28 in our [Growth Groups]({{<ref "growth-groups.md">}}). Please [get in touch]({{< ref "contact.md">}}) if you'd like to join a group. You can [download the study booklets here.](/uploads/2023/01/ActsPart3.pdf) (The images in this PDF are deliberately provided in low resolution. Full resolution versions will be printed and available at church and in Bible study groups.)

You may also find [this graphic of the *Gospel Progress in Acts*](https://visualunit.me/2011/03/05/gospel-progress-in-acts/) useful to accompany these studies.

These studies accompany our sermons on Acts 15-28.

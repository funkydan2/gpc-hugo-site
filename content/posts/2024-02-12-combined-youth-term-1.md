---
title: "Combined Youth Term 1 2024"
subtitle: "Starting March 1"
date: 2024-02-12T15:24:47+10:00
images: ["/uploads/2024/02/CombinedYouth.png"]
tags: ["News","Youth"]
---
In partnership with Gympie Baptist Church, on March 1 we're starting a new youth group for High Schoolers in the Gympie region.

On Friday nights (6:30-8:30 pm) throughout the school term, we'll get together for games, Bible study, and hanging out.

Cost is $2 per child/night, max $5 for a family.

[Get in touch]({{< ref "contact.md" >}}) to find out more.

(All leaders hold current BlueCards (or equivalent) and have been approved and trained in either [PresSafe](https://pressafe.org.au) (Presbyterian) or ChildSafe (Baptist).)

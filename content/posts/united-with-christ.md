---
date: 2017-04-22 11:28:11+00:00
draft: false
title: United with Christ
tags:
- Sermon Series
images: ["/uploads/2017/04/United-with-Christ_web.jpg"]
---

> Blessed be the God and Father of our Lord Jesus Christ, who has blessed us **in Christ** with every spiritual blessing in the heavenly places (Ephesians 1:3)


How does something that happened to Jesus 2000 years ago impact us? This term at Gympie Presbyterian we're going to dive deep into how Jesus death and resurrection benefits us and means we can enjoy a relationship of love and fellowship with the God who made us.

23rd April - The Big Picture
30th April - Incarnation
7th May - Justification
14th May - Adoption
21st May - Church
28th May - Sanctification

If you missed a talk, you can [listen online](/series/united-with-christ/).

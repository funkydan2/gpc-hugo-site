---
date: 2022-01-01
lastmod: 2022-09-09T11:01:10+10:00
title: Sermon Series in 2022
tags:
- Bible Study
- Sermon Series
images:
- "/uploads/2019/01/bible-and-plant.jpg"
---
# Term 1 - The Saving Word (Acts 1-7)
What is God's plan? What is Jesus doing now? The story of Jesus doesn't finish with his death or even his resurrection. As we open the book of Acts, we see how the ascended Christ continues to save people, pouring out his Spirit, as the word (or message) of his salvation is announced.

![Acts: The Saving Word (Acts 1-7)](/uploads/2022/01/Acts1-7.jpg)

[Listen online here](/series/the-saving-word/)

# Term 2 - Proverbs and Minor Prophets
In term 2 we plan to study Nahum, Habakkuk and explore God's Wisdom in Proverbs.

## Nahum: Compassionate Justice

Is God's justice good news? What compassion can be found when God avenges wrongs?

![Nahum: Compassionate Justice](/uploads/2022/04/Nahum.png)

[Listen online here](/series/nahum/)

## Habakkuk: Questioning God

Does God listen? Will he rescue his people? Is he fair?

![Habakkuk: Questioning God](/uploads/2022/05/Habakkuk.jpg)

[Listen online here](/series/questioning-god/)

## Proverbs: The Voice of wisdom

There are lots of voices in our world, where do we learn wisdom?

![Proverbs: The Voice of Wisdom](/uploads/2022/05/Proverbs.png)

[Listen online here](/series/the-voice-of-wisdom/)

# Term 3 - Salvation to the World (Acts 8-15)
In Term 3 we'll return to Acts, and see how God's saving word grows throughout the world.

![Acts: Salvation to the World](/uploads/2022/07/Acts8-15.jpg)

[Listen online here](/series/salvation-to-the-world/)

# Term 4 - Galatians
Where can we find deep and lasting freedom? In Term 4 we’ll be hearing from Paul’s letter to the churches in Galatia and seeing that only in the gospel of Jesus can we find eternal freedom.

![Galatians: Freedom](/uploads/2022/09/Galatians.jpg)

[Listen online here](/series/freedom/)

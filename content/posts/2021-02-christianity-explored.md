---
title: "Christianity Explored - Term 1 2021"
subtitle: "What's the best news you've ever heard?"
date: 2021-02-01T14:49:25+10:00
images: ["/uploads/2021/02/CEBanner.jpg"]
tags: ["Event"]
---
We're excited to be running the *Christianity Explored* course in all our [Bible Study Groups]({{< relref "growth-groups.md" >}}) starting the first week of February 2021.

Christianity Explored is a seven-week course which introduces us to the best news you've ever heard. You don't need to have any prior Bible knowledge or religious experience.

You can find out more and sign up on our [Christianity Explored page]({{< ref explore.md >}})

---
date: 2025-01-01
title: Sermon Series in 2025
tags:
- Bible Study
- Sermon Series
images:
- "/uploads/2019/01/bible-and-plant.jpg"
---
# Summer & Term 1 - Why? Church & Matthew 13-20
As we begin 2025, we're going to be asking a most important question--_why?_. *Why* did the risen and reigning Lord Jesus create church? And how does the *why* help us know *how* and *what* we are to be his people?

![Why? Church](/uploads/2025/01/WhyChurch.png)

([Listen here](/series/why-church/))

Then in term 1, we’ll be seeing and hearing Jesus as he shows how his kingdom is ‘right side up’ (and we’re upside down).

![The Right Side Up Kingdom](/uploads/2025/02/Matthew13-20.jpg)

([Listen here](/series/the-right-side-up-kingdom/))

During term 1 our [Growth Groups]({{<ref "growth-groups.md">}}) will be studing Matthew 13-17.

# Term 2 - 2 Kings & Learn the Gospel
All good things come to an end. In 2 Kings we trace the decline and destruction of the kingdoms of Judah and Israel. But even through this, God's promises and hope remains.

While we're hearing from 2 Kings in our sermon series, in our [Growth Groups]({{<ref "growth-groups.md">}}) we're going to be refreshed as dig deep into [Learning the Gospel](https://matthiasmedia.com.au/products/learn-the-gospel).

# Term 3 - 1, 2, 3 John
In term 3 we'll listen to three letters from one of Jesus' closest eye-witnesses, the Apostle John

# Term 4 - Daniel
*Living in the Lion's Den*

---
date: 2023-03-20T12:00:00+10:00
title: Easter Services 2023
tags:
- Event
- Easter
---
Christians often talk about Jesus being *King*. In a world where power is often associated with domination and control, Jesus shows us a different way of being king. He came not to be served but to serve, giving his life to rescue and restore.

We'd love you to join us this Easter as we explore Jesus' crowning and rising to life again.

Good Friday: 8:30 am || Resurrection Sunday: 9:30 am

[One Mile State School, Gympie](/sundays/)

![](/uploads/2023/03/GoodFriday.jpg)
![](/uploads/2023/03/EasterSunday.jpg)

{{<youtube "TpBALCrB59k">}}

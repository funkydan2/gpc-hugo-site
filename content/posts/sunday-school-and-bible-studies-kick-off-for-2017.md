---
date: 2017-01-27 02:12:17+00:00
draft: false
title: Sunday School and Bible Studies kick off for 2017
tags:
- Growth Group
- News
---

School's back, Australia day's over and so things are kicking off again for 2017.

On Sunday (January 29) our Sunday morning children's teaching program will start. This term the children will be learning from John's gospel. Sunday School starts part way through the service—so our church family spends time hearing from God's word and praying together first, before the children head down to the hall for their own teaching.

Bible studies are also restarting after what feels like a very long summer break. This term we'll be studying the final couple of chapters of Luke's Gospel and the opening few chapters of Acts (Luke's second volume). Study books will be available this week (January 29) at church. [Click here to find out more](/bible-study-groups/).

---
title: "1 Peter Bible Studies"
subtitle: "Hope Away From Home"
date: 2021-07-08T11:45:00+10:00
images: ["/uploads/2021/06/HAFH.png"]
tags: ["Growth Group"]
---
In Term 3 2021 we're studying the letter of 1 Peter in our [Bible Studies]({{< relref growth-groups.md >}}) and [sermons at church]({{< relref sermons-2021.md >}}).

If you want to be part of a Bible study group, [get in touch]({{< relref "contact.md" >}}). You can [download the study booklets here (PDF)](/uploads/2021/07/HAFH_Studies.pdf).

### Copyright
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>&nbsp; These Bible studies are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

---
date: 2021-12-01
title: Christmas 2021
subtitle: Waiting for Christmas
tags:
- Event
images: ["/uploads/2021/12/2021ChristmasTheme.jpg"]
---
Christmas is a time of anticipation. For many of us, we can't wait for Christmas morning: the joy presents and family brings. For others, we're waiting with a bit more trepidation: for the disappoints and griefs around the day.

At the time of Jesus' birth, many people were *waiting*--waiting for God to make good on his promises.

We'd love you to join us as we celebrate that the *waiting* of Christmas is over.

![Christmas Service Details](/uploads/2021/12/2021ChristmasTimes.jpg)

**Carols Service:** December 19, 9:30 am

**Christmas Morning**: December 25, 8:30 am

[One Mile State School Hall, McLeod Lane, Gympie](https://www.google.com.au/maps?hl=en&q=one+mile+state+school)

As with all our church gatherings, we'll be following the approved [COVID Safe protocols]({{< relref 2020-07-covid-19.md >}}).

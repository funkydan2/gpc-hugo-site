---
date: 2018-12-01
draft: false
title: Christmas 2018
tags:
- Event
images: ["/uploads/2018/12/Christmas2018.jpg"]
---

At Christmas there are lots of things to celebrate: family, holidays, food, summer, presents..

On the first Christmas, there was the greatest reason to celebrate—the birth of 'a saviour, who is Christ the Lord.' This newborn child caused angels to sing and kings to tremble.

This Christmas we invite you to join us to celebrate the saviour, *Jesus*.


**Carols:** December 21. You're invited to a *free* sausage sizzle from 5 pm, and we'll some some great Christmas carols and hear the Christmas story together from 6 pm.


**Christmas Morning**: December 25, 8:30am


[11 Crown Road, Gympie](https://www.google.com.au/maps/place/11+Crown+Rd,+Gympie+QLD+4570/@-26.1943563,152.6690575,17z/data=!3m1!4b1!4m5!3m4!1s0x6b94a36d24e50275:0x51f3916ef5b3466c!8m2!3d-26.1943611!4d152.6712462)

---

Around Gympie there are other Christmas events Gympie Presbyterian will be participating in. If you can't make it to our carols or Christmas Day service (or just want more!) then we encourage you to get along to these.

[Gympie Carols in the Park - hosted by the Ministers' Network - 17th December](https://www.facebook.com/GympieCarolsInThePark/)

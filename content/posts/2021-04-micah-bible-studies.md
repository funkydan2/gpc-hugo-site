---
title: "Micah Bible Studies"
subtitle: "What Does God Require"
date: 2021-04-25T15:19:05+10:00
images: ["/uploads/2021/04/Micah-Banner.jpg"]
tags: ["Growth Group"]
---
In Term 2 2021 we're studying the Old Testament prophet Micah in our [Bible Studies]({{< relref growth-groups.md >}}) and [sermons at church]({{< relref sermons-2021.md >}}).

If you want to be part of a Bible study group, [get in touch]({{< relref "contact.md" >}}). You can [download the study booklets here (PDF)](/uploads/2021/04/Micah.pdf).

### Copyright
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>&nbsp; These Bible studies, apart from pages 2-4, is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>. Pages 2-4 copyright [Visual Unit](https://visualunit.me/2010/11/29/navigating-micah/).

![Micah Sermon Cover Art - Text 'What Does the Lord Require, Micah, Gympie Presbyterian Church - Image of path through field - icons of scales, heart in hands, feet](/uploads/2021/04/Micah.jpg)

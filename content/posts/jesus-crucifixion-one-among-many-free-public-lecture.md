---
date: 2017-08-10 03:09:34+00:00
draft: false
title: 'Jesus'' Crucifixion: One Among Many (Free Public Lecture)'
tags:
- Event
images: ["/uploads/2017/08/Gympie-Mission-Slide-1024x590.jpg"]
---

During the time of the Ancient Roman Empire, ten's, if not hundred's of thousands of people were executed by crucifixion. It was a painfully cruel method of execution reserved for the lowest of the low—slaves, pirates, and enemies of the state. However, out of all those crucified by Rome, there is **one** whose name is remembered and whose death is celebrated around the world—**Jesus of Nazareth**.

What makes Jesus' crucifixion different? Is it just _one among many_?

Gympie Presbyterian and [Gympie Baptist](http://www.gympie-baptist-church.com) Churches invite you to a _free public lecture_ at the Gympie Baptist Church, [133 Corella Road, Gympie](https://www.google.com/maps/place/133+Corella+Rd,+Araluen+QLD+4570,+Australia/@-26.165013,152.648388,16z/data=!4m5!3m4!1s0x6b94a4a8eb8e6ad3:0xd86e9b3ee297c1e8!8m2!3d-26.1650129!4d152.6483881?hl=en-AU) at  7 pm Tuesday, August 22. The lecture will be given by Rev Dr Wesley Regen. Dr Regen is senior New Testament lecturer at the [Queensland Theological College](http://qtc.edu.au) and specialist in the first-century Roman world.

You can register your interest and join in the discussion through this [Facebook event](http://www.facebook.com/events/340924169695293/).
[![](/uploads/2017/08/Gympie-Mission-Slide-1024x590.jpg)](/uploads/2017/08/Gympie-Mission-Slide.jpg)
This event is part of a week of ministry partnership between Gympie Presbyterian Church, Gympie Baptist Church and students from the Queensland Theological. Our two churches will be hosting a team of QTC students to involve them in the life and ministry of our churches. For one week they'll be putting down their books and getting their hands dirty (sometimes literally) with ministry to seniors, adults, families, youth, and children in the Gympie region.

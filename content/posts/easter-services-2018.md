---
date: 2018-03-20 11:26:01+00:00
draft: false
title: Easter Services 2018
images: ["uploads/2016/03/Easter-640x350.jpg"]
tags:
- Event
- Easter
---

Jesus said he came that people might have 'life, and have it to the full' (John 10:10). He said he came to give _eternal life_ (John 3:16), that he gives *living water* (John 4:10) and is the _bread of life_ (John 6:35).

Despite these claims, his own life was tragically cut short, executed through a conspiracy of religious leaders and a failure of political justice. Yet, it is through this darkness Jesus offers life.

Join us this Easter as we hear the great news of the human tragedy and divine comedy of Easter.

Good Friday: 8:30 am || Easter Sunday: 9:30 am

[11 Crown Road, Gympie](/sundays/)

![](/uploads/2016/03/GoodFriday-300x225.jpg)
![](/uploads/2016/03/Easter-300x225.jpg)

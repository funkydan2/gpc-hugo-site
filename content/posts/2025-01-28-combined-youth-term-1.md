---
title: Combined Youth Term 1
subtitle: "Exporing God's Attributes and Nature."
date: 2025-01-28T03:29:59.851Z
tags:
    - News
    - Youth
---
In term 1 2025, Combined Youth continues fortnightly on Friday nights starting February 7.

![Combined Youth Program](/uploads/2025/01/CombinedYouth.png)

[Drop us a line]({{<ref "contact.md">}}) to find out more.

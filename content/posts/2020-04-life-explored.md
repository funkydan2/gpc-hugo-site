---
title: "Life Explored - May 2020"
subtitle: "What's the best gift God could give you?"
date: 2020-04-28T14:43:45+10:00
images: ["/uploads/2020/04/LifeExplored.jpg"]
tags: ["Event"]
---
We're excited to be running the *Life Explored* course *online* starting Tuesday May 5 2020 at 7:30 pm.

{{< youtube pyao9x557X8 >}}

Life Explored is a seven-week course which asks the big questions about life, and through discussion groups explores the Bible's answers to these questions.

You can find out more and sign up on our [Life Explored page]({{< ref explore-life.md >}})

---
title: "Combined Youth Term 2"
subtitle: "Find out who Jesus is from John's gospel."
date: 2024-04-02T09:15:47+10:00
#images: [""]
tags: ["News","Youth"]
---
In term 2 2024, Combined Youth continues fortnightly (except for the Gympie Show holiday) on Friday nights.

![Combined Youth Program](/uploads/2024/04/CombinedYouth-Term2Flyer.png)

[Drop us a line]({{<ref "contact.md">}}) to find out more.

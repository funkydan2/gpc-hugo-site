---
date: 2022-12-01
title: Christmas 2022
subtitle: It's Christmas Time
tags:
- Event
images: ["/uploads/2022/12/Christmas1.jpg"]
---
We often don't feel there's enough time for Christmas. There are so many things to do, places to be, and people to see.

The Bible says that Jesus' birth happened at exactly the right moment, *when the set time had fully come*.

We'd love you to join us as we celebrate that Christmas time is here!

![Christmas Service Details](/uploads/2022/12/Christmas2.jpg)

**Carols Service:** December 18, 5:30 pm for Sausage Sizzle, 6:30 pm for carols

**Christmas Morning**: December 25, 8:30 am

[One Mile State School Hall, McLeod Lane, Gympie](https://www.google.com.au/maps?hl=en&q=one+mile+state+school)

---
title: "Combined Youth"
subtitle: "Games + Bible For Youth"
date: 2024-02-12T15:36:10+10:00
images: ["/uploads/2024/02/CombinedYouth.png"]
tags: ["Youth"]
---
In partnership with Gympie Baptist Church, fortnightly on Friday nights (6:30-8:30pm) we run a youth group for High Schoolers in the Gympie region.

Each fortnight we get together for games, Bible study, and hanging out.

You can find the latest program [here.](/tags/youth)

Cost is $2 per child/night, max $5 for a family.

[Get in touch]({{< ref "contact.md" >}}) to find out more.

(All leaders hold current BlueCards (or equivalent) and have been approved and trained in either [PresSafe](https://pressafe.org.au) (Presbyterian) or ChildSafe (Baptist).)

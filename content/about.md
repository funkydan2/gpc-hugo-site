---
date: 2015-11-26 06:45:25 +0000
lastmod: 2024-04-25T10:21:43+10:00
title: About Us
---
Who are we? We're a group of people whose lives have been changed by Jesus. We seek to follow Jesus as we listen to God's Word, the Bible, as we live together serving the community of Gympie.

We're a group of people from a diverse range of backgrounds and a variety of life stages. We range in age from toddlers to grandparents and everyone in between. We are stay-at-home mums, tradies, professionals, farmers, and retirees. And what brings us together is that Jesus has changed our lives.

We're a **Presbyterian Church**, part of the [Presbyterian Church of Queensland](http://pcq.org.au). What does it mean to be Presbyterian? It means that we give the Gospel of Jesus and the Bible the central place in everything we do and believe. Being Presbyterian means a [team of elders](/leaders/) leads our church.

# Want to know more?

Find out more about:

* [Our vision](/vision)
* [What we believe](/about-jesus/#more-about-what-we-believe)
* [Church on Sunday](/sundays)
* [Midweek ministries](/midweek)
* [FAQ](/frequently-asked-questions)
* [Giving](/giving/)

# Local Ministry Partners
Our church is one of four churches associated with [Cooloola Christian College](https://ccc.qld.edu.au/). Our pastor speaks regularly at student chapel and contributes in other ways to the spiritual life of the college. As an associated church, we have a role in the spiritual life and governance of the college.

{{< youtube hxLwrtlF6pU>}}

Our church is connected with other Christian churches through the Gympie Ministers' Network. Through the Network, we cooperate to provide [Christian RI](https://christianri.org.au/) in local state primary schools and run community events such as Carols in the Park.

# International Ministry Partners
Our church partners with [the Colyer family](https://cms.org.au/missionaries/bill-and-linda-colyer/) who are preparing to see the gospel grow amongst university students in the South Pacific. The Colyer's are part of team of workers and members of the [Church Missionary Society](//cms.org.au) to see a _World that Knows Jesus_.

{{< vimeo 887867100>}}

Our church supports the Colyers by prayer and financial support. To give to our international gospel partners, you can make a [deposit in our church bank account]({{< ref "giving.md" >}}) (put _Colyers_ as the transaction description) or give with cash into the marked box on the welcome table.

---
title: Bountiful Kingdom
passage: Matthew 14:13-36
date: 2025-03-02T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/RightSideUpKingdom/20250302_BountifulKingdom_DS.mp3
audio_duration: 25:29
audio_size: 6345953
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/02/Matthew13-20.jpg
series:
    - The Right Side Up Kingdom
tags:
    - Faith
    - Prayer
---
Does Jesus care? Will Jesus provide? As we consider two amazing miracles, we see Jesus, the king of the Bountiful Kingdom, in action.

# Outline
1. Jesus’ Feast (vv13-22)
2. Jesus’ Identity (vv23-46)

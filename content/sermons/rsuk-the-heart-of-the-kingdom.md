---
title: The Heart of the Kingdom
passage: Matthew 15:1-20
date: 2025-03-09T09:30:00+10:00
audio:
    - "https://f001.backblazeb2.com/file/GympieSermonAudio/RightSideUpKingdom/20250309_HeartKingdom_DS.mp3"
audio_duration: 26:28
audio_size: 6582112
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/02/Matthew13-20.jpg
series:
    - The Right Side Up Kingdom
tags:
    - Sin
    - Cleanliness
---
In the face of a serious problem, you need a serious solution. In this passage, Jesus tells us sin is a serious problem—it comes from our heart. Which means we need a serious saviour.

# Outline
1. Do you nullify God’s word? (vv1-9)
2. What really makes us unclean (vv10-19)
3. The issue of the heart

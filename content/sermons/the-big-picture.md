---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnitedWithChrist/20170423_TheBigPicture_DS.mp3"]
audio_duration: '25:16'
audio_size: 6292090
date: 2017-04-23 07:30:49+00:00
draft: false
passage: Ephesians 5:25-33
title: The Big Picture
preachers: ["Daniel Saunders"]
series:
- United with Christ
tags:
- Creation
- Marriage
- New Creation
- Union With Christ
images: ["/uploads/2017/04/United-with-Christ_web.jpg"]
---
1. The garden wedding that started it all (Genesis 2)
2. Marriage: A picture of union with Christ (Ephesians 5:25-33)
3. The wedding of eternity (Revelation 21:1-3)

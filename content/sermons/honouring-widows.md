+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180812_HonouringWidows_DS.mp3"]
audio_duration = "27:04"
audio_size = 6725271
date = "2018-08-12T10:01:12+10:00"
images = ["/uploads/2018/04/Gods-House-768x644.jpg"]
passage = "1 Timothy 5:1-16"
preachers = ["Daniel Saunders"]
series = ["God's House"]
tags = ["Church", "Family"]
title = "Honouring Widows"

+++
1. Relationships in God's Household (vv1-2)

2. Honour Widows (v3)

   1. In your family (vv4, 7-8, 16)

   2. In God's family (vv5-6, 9-10)

   3. ... but with a warning (vv11-15)

3. How are we honouring widows?

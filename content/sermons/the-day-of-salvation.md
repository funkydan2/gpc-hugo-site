---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joel/20170115_TheDayOfSalvation_DS.mp3"]
audio_duration: '26:42'
date: 2017-01-15 05:57:12+00:00
draft: false
passage: Joel 2:18-32
title: The Day of Salvation
preachers: ["Daniel Saunders"]
series:
- The Day of the LORD
tags:
- Pentecost
- Salvation
- Spirit
images: ["/uploads/2017/01/The-Day-of-the-LORD.jpg"]
---

1. God answers (vv18-19a)
2. and promises to restore
    1. ...the land (vv19b-27)
    2. ...and be with his people (vv28-32)
3. The day has begun! (Acts 2:14-24, 34, 38)

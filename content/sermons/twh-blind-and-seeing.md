---
title: "Blind and Seeing"
passage: "Luke 18:35-19:10"
date: 2019-09-08T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190908_BlindAndSeeing_DS.mp3"]
audio_duration: "24:01"
audio_size: 5993727
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Money", "Generosity"]
---
How sure are you that you’re seeing what’s real? Do you need glasses?Or do you just need to pay attention?

In this sermon, we see that Jesus came to seek and save the lost. He came to rescue us from blindness and to open our eyes to his generous mercy.

# Outline

1. Are you seeing Jesus truly?
2. A blind man sees (18:35-43)
3. A short man sees (19:1-10)
4. Jesus came to seek and save us!

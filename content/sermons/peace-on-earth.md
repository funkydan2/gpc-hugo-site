---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke1-4/20161225_PeaceOnEarth_DS.mp3"]
audio_duration: '18:04'
date: 2016-12-24 23:31:24+00:00
draft: false
passage: Luke 2:1-20
title: Peace on Earth?
preachers: ["Daniel Saunders"]
series:
- Great Expectations
tags:
- Christmas
- Peace
- Reconciliation
images: ["/uploads/2015/12/Great-Expectations.jpg"]
---
1. Jesus' Promise
2. Who's the promise for?
3. A Saviour?
4. Peace...with God

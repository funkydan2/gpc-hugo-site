---
title: "Preach Christ"
passage: "Philippians 1:12-25"
date: 2023-06-25T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20230625_PreachChrist_LW.mp3"]
audio_duration: "29:47"
audio_size: 7380345
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
---
What is your reason for life? Your number one motivation?

# Outline
1. The Gospel is Spread Two Ways
2. Two Motives for Preaching
3. Paul's Goal to Exalt Jesus
4. Paul's Choice of Life Rather than Death

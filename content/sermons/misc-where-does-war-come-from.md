---
title: "Where does war come from?"
passage: "James 4:1-10"
date: 2021-04-25T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20210425_WhereDoesWarComeFrom_DS.mp3"]
audio_duration: "22:49"
audio_size: 5706698
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/04/James4.jpg"]
series: ["Miscellaneous"]
tags: ["Peace", "Love"]
---
This Anzac Day we considered where war comes from and what is God's answer for peace.

# Outline
1. War among ‘you’ (vv1-3)
2. War with God! (vv4-6)
3. God’s way for peace (vv7-10)

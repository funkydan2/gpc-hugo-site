---
title: "Seek God"
passage: "Psalm 27"
date: 2020-05-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/PsalmsOfComfort/20200503_POC-SeekGod_DS.mp3"]
audio_duration: "26:35"
audio_size: 6609161
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/04/Psalms_Of_Comfort.jpg"]
series: ["Psalms of Comfort"]
tags: ["God", "Prayer", "Guidance"]
---

If you could ask God for one thing, if there was one good gift he could give you, what would you ask for?

# Outline

1. Confidence in God (vv1-3)

2. Seeking God’s House (vv4-6)

    Romans 8:31-32

3. Seeking God’s Face (vv7-12)
   1. Prayer (1 John 5:14)
   2. Acceptance (Exodus 33:20; Hebrews 4:14-16)
   3. Guidance (Galatians 5:16)
   4. Protection (Romans 8:33)

4. Confidence in God (vv13-14)

5. Seeking God’s Presence (Hebrews 10:19-22; Revelation 22:3-4)


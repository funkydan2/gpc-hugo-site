---
title: "Growing to Maturity"
passage: "Ephesians 4:7-16"
date: 2020-07-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200719_GrowingToMaturity_DS.mp3"]
audio_duration: "27:15"
audio_size: 6768864
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Church", "Grace", "Victory", "Ascension", "King", "False Teaching"]
---

What does a strong, healthy, mature Christian life and church look like?

This sermon was recorded during Gympie Presbyterian Church's first in-person gathering after 17 weeks of 'gathering' online.

# Outline

1. Christ’s generous victory (v7-10)
2. Christ’s gifts (v11)
3. To grow to maturity (vv12-16)

(In the introduction to this message, [this podcast episode](https://anchor.fm/reach-australia/episodes/Think-Theologically-Your-Churchs-Relaunch-Lionel-Windsor--Andrew-Heard--Mikey-Lynch-eggkfg) from Reach Australia was referenced.)

+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180909_DelayedGratification_DS.mp3"]
audio_duration = "22:45"
audio_size = 5685941
date = "2018-09-09T15:13:05+10:00"
images = ["/uploads/2018/04/Gods-House-768x644.jpg"]
passage = "1 Timothy 6:17-21"
preachers = ["Daniel Saunders"]
series = ["God's House"]
tags = ["Money", "False Teaching"]
title = "Delayed Gratification"

+++
1. Waiting for lasting wealth (vv17-19)

2. Waiting for lasting truth (vv20-21)

3. Will you wait for something better?

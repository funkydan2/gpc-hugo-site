---
title: "Crushed For Us"
passage: "Isaiah 53:1-9"
date: 2021-04-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20210402_CrushedForUs.mp3"]
audio_duration: "19:39"
audio_size: 4948029
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/03/Easter2021.jpg"]
series: ["Easter"]
tags: ["Good Friday", "Peace", "Atonement"]
---
Sometimes we need to own up to an uncomfortable truth. But it's worth it because on the cross, Jesus took the punishment for sin, that you might receive peace with God.

# Outline
1. Our Need
2. His substitution

+++
audio =  ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190210_LoveYourNeighbour_DS.mp3"]
audio_duration = "23:48"
audio_size = 5938469
date = "2019-02-10T09:30:00+10:00"
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 10:25-42"
preachers = ["Daniel Saunders"]
series = ["The Way Home (Part 3)"]
tags = []
title = "Love Your Neighbour"

+++
1. Why is my neighbour? (10:25-37)

2. Who is my disciple? (10:38-42)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180211_LivingWater_DS.mp3"]
audio_duration: '26:18'
date: 2018-02-11 06:57:02+00:00
audio_size: 6540779
draft: false
passage: John 4:4-42
title: Living Water
preachers: ["Daniel Saunders"]
series:
- Find Life
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. Jesus offers a Samaritan woman ... (4:1-9)

2. living water... (4:10-14)

3. to quench her thirst ... (4:15-18)

4. so she can truly worship God. (4:19-26)

5. Will you receive living water from Jesus?

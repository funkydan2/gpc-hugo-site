+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180805_TrainingForEternity_DS.mp3"]
audio_duration = "17:06"
audio_size = 4330612
date = "2018-08-05T10:10:23+10:00"
images = ["/uploads/2018/04/Gods-House-768x644.jpg"]
passage = "1 Timothy 4:6-16"
preachers = ["Daniel Saunders"]
series = ["God's House"]
tags = ["Gospel", "Godliness"]
title = "Training for Eternity"

+++
1\. Practicing for eternity (vv6-10)

2\. Persevering for others (vv11-16)

3\. Are you persevering and practicing godliness?

\(We had technical troubles, so this was recorded after the service.)

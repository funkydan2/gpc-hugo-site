---
title: "Jesus' Family"
passage: "Mark 3:7-35"
date: 2021-01-31T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210131_JesusFamily_DS.mp3"]
audio_duration: "26:47"
audio_size: 6660522
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Family"]
---
Not only was Jesus misunderstood by his opponents, who thought him a liar, but his family also didn't understand him or his mission, thinking he'd lost the plot. But, for those who listened to and received him, he called 'my family'.

# Outline
1. The crowd (vv7-12)
2. Jesus’ people (vv13-19)
3. Family concerns (vv20-21, 31-32)
4. Family business (vv22-30)
5. Jesus’ true family (vv31-35)

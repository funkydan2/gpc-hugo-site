---
title: "Wisdom Calls"
passage: "Proverbs 1:1-9; 8:1-11"
date: 2022-06-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheVoiceOfWisdom/20220612_WisdomCalls_DS.mp3"]
audio_duration: "29:55"
audio_size: 7409469
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Proverbs.png"]
series: ["The Voice of Wisdom"]
tags: ["Wisdom","Discipleship","Eternity"]
---
How do we measure wisdom? How do we assess whether a certain decision is wise? How can we know if we’re wise?

Where do we hear the voice of wisdom?


# Outline
1. To know God’s wisdom (Proverbs 1:1-9)
2. The voice of Wisdom (Proverbs 8:1-11)
3. Listen to Jesus’ Wisdom (Matthew 7:24-29)

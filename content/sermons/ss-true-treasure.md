---
title: "True Treasure"
passage: "Matthew 6:19-34"
date: 2024-03-24T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StaySalty/20240324_TrueTreasure_DS.mp3"]
audio_duration: "22:28"
audio_size: 5623975
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/02/StaySalty.jpg"]
series: ["Stay Salty"]
tags: ["Money","Anxiety"]
---
What do you worry about? Health, money, politics, culture, conflict?

In this message from Jesus, we hear how money can be a harsh master, but finding treasure in God leads to eternal reward.

# Outline
1. Where is your treasure? (vv19-21)
2. Who do you serve? (vv22-24)
3. Why do you worry? (vv25-34)

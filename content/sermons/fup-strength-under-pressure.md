---
title: "Strength Under Pressure"
passage: "2 Thessalonians 2:13-3:5"
date: 2023-08-20T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20230820_StrengthUnderPressure_DS.mp3"]
audio_duration: "24:19"
audio_size: 6068185
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Prayer","Endurance","Love"]
---
How do you cope with the pressures of life? If you're a Christian, how do you sometimes feel pressure to give up on trusting in Jesus?

In this message from 2 Thessalonians, we hear God's promise to strengthen the people he loves in Christ.

# Outline
1. Know who you are (2:13-14)
2. Pray to stand firm (2:15-17)
3. Pray for Gospel Progress (3:1-2)
4. Rest in the God who strengthens (3:3-5)

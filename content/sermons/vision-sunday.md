---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20180121_VisionSunday_DS.mp3"]
audio_duration: '26:36'
audio_size: 6609530
date: 2018-01-21 06:50:52+00:00
draft: false
passage: 1 Peter 2:4-12
title: Vision Sunday
preachers: ["Daniel Saunders"]
series:
- Miscellaneous
tags:
- Church
- Evangelism
images: ["/uploads/2018/01/Vision-Sunday-Slide-768x576.jpg"]
---

* Know God (vv4-8)
* Love One Another (vv5, 9-10)
*	Reach our Region (vv9, 11-12)

---
title: Family of the King
passage: Matthew 12:38-50
date: 2024-10-13T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20241013_FamilyoftheKing_DS.mp3
audio_duration: 0:23:04
audio_size: 5769057
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/07/SeeingTheKingdom.jpg
series:
    - Seeing the Kingdom
tags:
    - Family
    - Prayer
    - Church
---
Family can make us feel complex emotions: it can be both a source of joy and safety but also a cause of anguish, longing, or grief. As we finish this series in Matthew's gospel we hear Jesus rebuke of those who should be in God's family but have broken faith and also the precious promise of a place in the family of the king.

# Outline
1. Will you see the signs? (vv38-45)
2. Who’s Jesus’ family? (vv46-50)

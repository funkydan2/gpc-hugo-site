---
title: "God's Foolish Wisdom"
passage: "1 Corinthians 1:18-2:5"
date: 2022-05-29T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheVoiceOfWisdom/20220529_GodsFoolishWisdom_DS.mp3"]
audio_duration: "28:13"
audio_size: 7005613
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Proverbs.png"]
series: ["The Voice of Wisdom"]
tags: ["Cross","Wisdom","Power"]
---
Where do you think true wisdom and power are found?

As we begin our series on the voice of wisdom heard in Proverbs, we first look to the unexpected 'right-way-up' wisdom of the cross. It's _Christ crucified_ that is the centre of all God's wisdom.

# Outline
1. The world’s wisdom destroyed (1:18-25)
2. Shown in…
    1. who God saves (1:26-31)
    2. how God saves (2:1-5)
3. Hearing the voice of the cross

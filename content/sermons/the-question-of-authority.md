---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170212_TheQuestionOfAuthority_DS.mp3"]
audio_duration: '21:50'
date: 2017-02-12 07:48:39+00:00
draft: false
passage: Luke 20:1-20
title: The Question of Authority
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Authority
- King
- Messiah
- Parable
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---

1. Just who do you think you are? (vv1-8)
2. Just who do they think they are? (vv9-16)
3. The cost of rejecting Jesus (vv17-20)

    Psalm 118, Isaiah 8, Daniel 2

Just who do you think Jesus is?

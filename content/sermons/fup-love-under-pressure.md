---
title: "Love Under Pressure"
passage: "1 Thessalonians 2:17-3:13"
date: 2023-10-15T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20231015_LoveUnderPressure_DS.mp3"]
audio_duration: "23:55"
audio_size: 5968941
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Love","Church","Prayer"]
---
Through his death and resurrection, Jesus rescues people into a new family bound together by love.

# Outline
1. Prioritise Presence (2:17-3:5)
2. Find Joy in Others’ Faith (3:6-10)
3. Pray with Eternal Views (3:11-13)

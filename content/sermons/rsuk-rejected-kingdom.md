---
title: Rejected Kingdom
passage: Matthew 13:53-14:12
date: 2025-02-23T06:01:49.539Z
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/RightSideUpKingdom/20250223_RejectedKingdom_DS.mp3
audio_duration: 23:35
audio_size: 5889012
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/02/Matthew13-20.jpg
series:
    - The Right Side Up Kingdom
tags:
    - Kingdom
---
In this message we see two different ways people reject Jesus and the Kingdom of Heaven, which is a challenge for us to be humbly amazed by him.

# Outline
1. Is Jesus too familiar? (13:53-58)
2. Is Jesus a threat? (4:1-12)
3. Humbly amazed by Jesus

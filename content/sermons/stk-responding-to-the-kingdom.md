---
title: "Responding to the Kingdom"
passage: "Matthew 11:1-24"
date: 2024-09-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240901_RespondingToTheKingdom_DS.mp3"]
audio_duration: "23:18"
audio_size: 5821044
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/07/SeeingTheKingdom.jpg"]
series: ["Seeing the Kingdom"]
tags: ["Doubt","Judgment"]
---
What do you find yourself doubting? In this message from Matthew 11 we hear Jesus deal mercifully with those who doubt.

# Outline
1. Are you the coming one? (vv1-6)
2. Is he the messenger? (vv7-15)
3. How will you respond? (vv16-24)

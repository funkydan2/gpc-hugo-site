---
title: "Seeing Jesus' Authority"
passage: "Matthew 8:1-17"
date: 2024-07-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240721_SeeingJesusAuthority_DS.mp3"]
audio_duration: "24:45"
audio_size: 6172766
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/07/SeeingTheKingdom.jpg"]
series: ["Seeing the Kingdom"]
tags: ["Authority","Healing","Cleanliness","Kingdom"]
---
It's no good taking your concerns to someone who doesn't have the authority to change things. But Jesus is one with authority. 

# Outline
1. Jesus' authority to cleanse (vv1-4)
2. Jesus' authority for the world (vv5-13)
3. Jesus' authority and the cross (vv14-17)

(Note: the recording was started a few seconds into the sermon.)

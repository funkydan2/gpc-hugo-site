+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180819_HonouringElders_DS.mp3"]
audio_duration = "25:11"
audio_size = 6271242
date = "2018-08-19T10:00:17+10:00"
images = ["/uploads/2018/04/Gods-House-768x644.jpg"]
passage = "1 Timothy 5:17-25"
preachers = ["Daniel Saunders"]
series = ["God's House"]
tags = ["Elders", "Leaders"]
title = "Honouring Elders"

+++
1. Honour elders by

   1. Financial support (vv17-18)

   2. Taking sin seriously (vv19-21)

   3. Being slow to appoints (vv22-25)

2. Honouring our elders

(Unfortunately there was some distortion in the recording.)

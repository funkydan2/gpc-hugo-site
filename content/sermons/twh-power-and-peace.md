---
title: "Power and Peace"
passage: "Luke 19:11-28"
date: 2019-09-15T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190915_PowerAndPeace_DS.mp3"]
audio_duration: "26:57"
audio_size: 6696620
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Kingdom", "History", "Parable"]
---

The rulers and kingdoms of our world are all about power and profit, no matter the cost. But in Jesus we  Jesus a king who's kingdom brings true peace.

As Jesus heads up to Jerusalem, he asks us to *choose our kingdom*.

# Outline

1. A story of two kingdoms
2. A parable of an evil king
3. Which kingdom will you choose?

---
title: "Truth on Trial"
passage: "Mark 14:53-72"
date: 2021-12-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211212_TruthOnTrial_DS.mp3"]
audio_duration: "24:32"
audio_size: 6120514
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Anxiety", "Son of Man"]
---
When anxiety is high, truth is often low.

In the anxious events recorded during Jesus' trial, we see lies and denial, yet Jesus remains true. He knows and reveals his identity, which is means grace for us.

# Outline
1. …the sheep will be scattered (vv53-54)
2. Anxiety and Lies (vv55-65)
3. Anxiety and Denial (vv66-72)
4. Lies, Truth, and Grace

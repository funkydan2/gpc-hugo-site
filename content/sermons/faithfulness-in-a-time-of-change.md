+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20181209_FaithfulnessInATimeOfChange_DS.mp3"]
audio_duration = "24:48"
audio_size = 6180291
date = "2018-12-10T11:03:03+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 12"
preachers = ["Daniel Saunders"]
series = []
tags = ["Leadership", "Change"]
title = "Faithfulness in a Time of Change"

+++
1. History Repeats (12:1-12)

2. A gracious ultimatum (12:13-25)

3. Passing the Gospel Baton

    2 Timothy 2:2

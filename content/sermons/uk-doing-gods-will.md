---
title: "Doing God's Will"
passage: "Acts 21:1-26"
date: 2023-03-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230305_DoingGodsWill_DS.mp3"]
audio_duration: "31:01"
audio_size: 7676913
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Guidance","Temple","Sacrifice"]
---
As we live for Jesus, it's not always clear what God's will is. In this message from Acts 21, we see the Apostle Paul's approach to two difficult decisions—and how he lives boldly for Christ through them.

# Outline
1. Knowing God’s Will (vv1-16)
2. Doing God’s Will (vv17-27)

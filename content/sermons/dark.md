---
title: "Dark"
passage: "Genesis 1:1-5; Luke 23:44-46"
date: 2020-04-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20200410_GoodFriday_Dark_DS.mp3"]
audio_duration: "19:55"
audio_size: 5011008
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/04/GoodFriday.jpg"]
series: ["Easter"]
tags: ["Dark", "Cross", "Death", "Good Friday"]
---

Where is God when life is dark? This Good Friday we see how the darkness at the crucifixion shows that in the death of Jesus creation is undone as God’s judgment against sin is revealed.

# Outline

1. Facing the Void (Genesis 1:1-5)

2. The Darkness of…

   Evil (Luke 22:53)

   Uncreation (vv44-45a)

   Temple (vv45b-46)

3. Conclusion/Application

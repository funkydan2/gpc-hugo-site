---
title: "Second Chances"
passage: "Zechariah 1-2"
date: 2023-04-30T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230430_SecondChances_DS.mp3"]
audio_duration: "30:50"
audio_size: 7632985
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Repentance","Temple","Apocalyptic"]
---
We love second chances—but what about God? Does he give second chances?

In this message from the opening chapters of Zechariah we hear the good news of God's promise to give something better than a second chance.

# Outline
1. Context: no king and no temple (1:1)
2. ‘Return to the LORD’ (1:2-6)
3. Four Scenes of Overflowing Grace (1:7-2:13)
4. God’s borderless city!

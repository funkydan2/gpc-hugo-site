---
title: "Promise Keeper"
passage: "Galatians 3:15-29"
date: 2022-11-06T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221106_PromiseKeeper_DS.mp3"]
audio_duration: "28:42"
audio_size: 7120142
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Covenant","Law","Union With Christ"]
---
The good news of Jesus calls us to trust in him, but can God be trusted?

In this message we see how God's promises never change and answers why God gave a law through Moses after making his covenant with Abraham.

# Outline
1. God keeps his promises (3:15-18)
2. Why the Law? (3:19-25)
3. All one in Christ (3:26-29)
4. Trusting in God’s Promises

---
title: "The Mercy of God"
passage: "Psalm 103:1-18"
date: 2021-04-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20210411_TheMercyofGod_MD.mp3"]
audio_duration: "26:22"
audio_size: 6557306
preachers: ["Malcolm Diefenbach"]
images: ["/uploads/2021/04/Psalm103.jpg"]
series: ["Miscellaneous"]
tags: ["Mercy", "Grace"]
---
1. What is the difference between grace and mercy?
2. What is God the father of?
3. Where was it said that God used to dwell?
4. How great is God’s mercy?

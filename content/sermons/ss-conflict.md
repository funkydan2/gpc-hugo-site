---
title: "Conflict"
passage: "Matthew 5:21-26"
date: 2024-02-25T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StaySalty/20240225_Conflict_DS.mp3"]
audio_duration: "24:57"
audio_size: 6218108
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/02/StaySalty.jpg"]
series: ["Stay Salty"]
tags: ["Anger","Law"]
---
Jesus calls his followers to 'stay salty'—to wholeheartedly live as his people. In this message, we hear Jesus' call for his people to do anger and conflict differently.

# Outline
1. Is this for us?
2. Anger is abolished (vv21-22)
3. Insults are out of bounds (v22)
4. Peacemaking is the priority (vv23-26)
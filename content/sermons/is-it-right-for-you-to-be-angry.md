---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20161204_IsItRightForYouToBeAngry_SB.mp3"]
audio_duration: '28:09'
date: 2016-12-04 09:30:00+10:00
passage: Jonah
title: Is it right for you to be angry?
preachers: ["Steve Blencowe"]
images:
- "/uploads/2016/02/sermons.jpg"
series:
- Miscellaneous
---

1. Jonah, the reluctant yet 'successful' prophet, is deeply angry (Jonah 3:10-4:3)
2. And God correct Jonah through an object lesson involving a plant (Jonah 4:4-11)
3. What can we learn from this about God's heart and ours?

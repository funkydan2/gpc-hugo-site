---
title: "Faith Without Deeds is Dead"
passage: "James 2:14-26"
date: 2022-03-13T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20220313_FaithWithoutWorksIsDead_LW.mp3"]
audio_duration: "30:37"
audio_size: 7577100
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
tags: ["Justification", "Works"]
---
A sermon on James 2:14-26 by guest preacher, Leo Woodward

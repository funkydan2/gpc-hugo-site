---
title: "The Sun Rises"
passage: "Luke 1:68-79"
date: 2020-12-20T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20201220_TheSunRises_DS.mp3"]
audio_duration: "18:19"
audio_size: 4627000
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/12/Christmas2020-1.jpg"]
series: ["Christmas"]
tags: ["Christmas", "Dark", "Light"]
---
When life feels dark, as it has for many of us this year, where is there hope? Is it found in making it 'bigger than Christmas? In this message from our Carols service, discover how the coming of Jesus brings light into our darkness.

# Outline
1. Jesus Came Into a Dark World
2. In Jesus the Sun Rises

Before the sermon, we watched this Christmas advertisement:

{{<youtube B5NmhtqGg04>}}

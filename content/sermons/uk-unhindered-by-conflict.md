---
title: "Unhindered by Conflict"
passage: "Acts 15:46-16:10"
date: 2023-01-29T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230128_UnhinderedByConflict_DS.mp3"]
audio_duration: "27:52"
audio_size: 6918464
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Forgiveness","Mission"]
---
What difference does Jesus make when there is disagreement and conflict? As we return to Acts, the record of what Jesus *continued* to do through the earliest believers, we see how conflict doesn't stop Christ's mission but is an opportunity to glorify God.

# Outline
1. A Sharp Dispute! (15:36-41)
2. Cutting Compromise? (16:1-5)

(Note: the first minute wasn't recorded during the service and has been re-recorded afterwards.)

---
title: "True Faithfulness"
passage: "Malachi 2:10-16"
date: 2023-11-26T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Malachi/20231126_TrueFaithfulness_DS.mp3"]
audio_duration: "25:02"
audio_size: 6239127
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/11/HalfHearted.png"]
series: ["Half Hearted"]
tags: ["Covenant","Marriage","Promises","Faithfulness"]
---
Broken promises break relationships. In this message from Malachi 2 we hear that God takes faithfulness seriously because he is a faithful God.

# Outline
1. Unfaithful People (v10)
2. who marry unfaithfully (vv11-12)
3. who are unfaithful in marriage (vv13-16)
4. Our Faithful God (Ephesians 5:31-33)

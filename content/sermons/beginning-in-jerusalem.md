---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170409_BeginningInJerusalem_DS.mp3"]
audio_duration: '30:12'
audio_size: 7478408
date: 2017-04-09 02:39:53+00:00
draft: false
passage: Luke 24:36-52
title: Beginning in Jerusalem
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Evangelism
- Mission
- Old Testament
- Spirit
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---

1. The end of Act 1
2. It is written and must be fulfilled (vv44-49)
    1. ... to suffer
    2. ... to rise
    3. to preach
3. Preaching to the Nations

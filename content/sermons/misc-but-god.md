---
title: "But God..."
passage: "Ephesians 2:1-10"
date: 2023-04-23T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20230423_ButGod_DS.mp3"]
audio_duration: "26:08"
audio_size: 6502737
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/ephesians2.jpg"]
series: ["Miscellaneous"]
tags: ["Sin","Grace","Life","Death","Union With Christ"]
---
In the back of our minds we have the gnawing thought *things are quite what they should be*.

In Ephesians 2 we hear God's diagnosis, but the even better good news of grace (undeserved gift) in Jesus.

# Outline
1. Diagnosis: Walking Dead (vv1-3)
2. But God… (v4)
    1. made alive (v5)
    2. raised (vv6-7)
3. It’s all a gift! (vv8-10)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnitedWithChrist/20170514_Adoption_DS.mp3"]
audio_duration: '30:06'
audio_size: 7454727
date: 2017-05-14 05:39:35+00:00
draft: false
passage: Galatians 3:26-4:7
title: Adoption
preachers: ["Daniel Saunders"]
series:
- United with Christ
tags:
- Adoption
- Spirit
- Union With Christ
images: ["/uploads/2017/04/United-with-Christ_web.jpg"]
---

1. Why do we need to be adopted?
2. How can we be adopted?
3. What comes from our adoption?

    Calling God *Father*

    > 'What is a Christian? [...] The richest answer I know is that a Christian is one who has God for his Father.' (J.I. Packer, Knowing God)

    Disciplined as children (Hebrews 12:7-8)

    Heirs of God

    Secure in God's love

Join the family!

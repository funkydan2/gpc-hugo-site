---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20170827_OurRefugeInAnAgeOfAnxiety_SW.mp3"]
audio_duration: '24:58'
date: 2017-08-27 07:17:30+00:00
draft: false
passage: Psalm 91
title: Our Refuge in an Age of Anxiety
preachers: ["Shane Wright"]
series:
- Miscellaneous
images: ["/uploads/2017/08/Ps91-a-420x190.jpg"]
---

1. The God who is our refuge (vv1-2)

2. How God is our refuge (vv3-13)

3. God's promise of refuge (vv14-16)

---
title: "From Fake Fasting to True Feasing"
passage: "Zechariah 7-8"
date: 2023-06-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230611_FromFakeFastingToTrueFeasting_DS.mp3"]
audio_duration: "26:30"
audio_size: 6590411
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Religion","Love","Promises"]
---
What does fake 'religion,' fake 'devotion' to God look like? And how do we see genuine love of God?

In this message from Zechariah 7-8, we hear God's stinging rebuke and yet astounding promises to his people.

# Outline
1. Is sadness no more? (7:1-3)
2. Pure religion (7:4-14)
3. Sadness no more! (Chapter 8)

---
title: "Rejecting God's Shepherd"
passage: "Zechariah 11"
date: 2023-07-16T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230716_RejectingGodsShepherd_DS.mp3"]
audio_duration: "23:55"
audio_size: 5972236
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Leadership"]
---
Can you imagine rejecting good leaders and following bad? It's actually not all that uncommon.

In this message from Zechariah 11, we hear the danger of rejecting Jesus, God's good shepherd.

# Outline
1. The abused flock (10:2; 11:4-5)
2. … who detest the good shepherd (11:6-14)
3. The foolish shepherd (11:15-17)
4. The rejected Shepherd (Matthew 26:14-16; 27:3-10)

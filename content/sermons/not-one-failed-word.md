---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joshua/20161120_NotOneFailedWord_DS.mp3"]
audio_duration: '22:29'
date: 2016-11-20 03:22:47+00:00
draft: false
passage: Joshua 23-24
title: Not One Failed Word
preachers: ["Daniel Saunders"]
series:
- Fear and Serve the Lord
tags:
- Faithfulness
- Promises
images: ["/uploads/2015/12/Joshua.jpg"]
---

1. Last Words

2. Joshua's last words (Joshua 23:1-2)

    1. What the LORD has done (23:3-5; 24:1-13)

    2. What his people are to do (24:14-24)

3. Our last words

4. Not one word has failed...in Jesus

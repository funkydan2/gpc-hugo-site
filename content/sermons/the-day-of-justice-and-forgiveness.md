---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joel/20170122_TheDayOfJusticeandForgiveness_DS.mp3"]
audio_duration: '27:21'
date: 2017-01-22 10:03:57+00:00
draft: false
passage: Joel 3
title: The Day of Justice and Forgiveness
preachers: ["Daniel Saunders"]
series:
- The Day of the LORD
tags:
- Forgiveness
- Judgment
images: ["/uploads/2017/01/The-Day-of-the-LORD.jpg"]
---
1. The day of justice (vv1-16a)
    1. God will judge the nations
    2. ...for their sin
    3. So prepare to meet the LORD! (Revelation 14:14-20)
2. The day of forgiveness (vv16b-21) (Revelation 21:27)
3. Are you prepared for this day?

---
title: "Help My Unbelief"
passage: "Mark 9:14-32"
date: 2021-09-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20210912_HelpMyUnbelief_DS.mp3"]
audio_duration: "25:59"
audio_size: 6468055
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Doubt", "Faith", "Mercy"]
---
Where do you go with questions, fears, or doubts?

As we continue journeying with Jesus, in this message we see Jesus mercifully welcomes people who desperately come to him with questions and struggles.

# Outline
1. Descending to earth (vv14-18)
2. If you are able (vv19-24)
3. Rising again (vv25-32)
4. Help our unbelief

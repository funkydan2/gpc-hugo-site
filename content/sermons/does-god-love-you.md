---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160612_DoesGodLoveYou_DS.mp3"]
audio_duration: '26:19'
date: 2016-06-12 07:43:39+00:00
draft: false
passage: Romans 5:1-11
title: Does God Love You?
preachers: ["Daniel Saunders"]
series:
- The Verdict
tags:
- Cross
- Love
- Reconciliation
- Suffering
- Assurance
images: ["/uploads/2015/12/Romans-TheVerdict.jpg"]
---

1. You can know God's love
    1. because of certain hope (5:1-5)
    2. because of what He did and when He did it (5:6-8)
2. Living in God's love
    1. Confidence (5:9-11)
    2. Loving like God (12:14-21)

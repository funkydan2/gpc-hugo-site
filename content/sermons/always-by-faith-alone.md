---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160605_AlwaysByFaithAlone_DS.mp3"]
audio_duration: '29:32'
date: 2016-06-05 07:35:34+00:00
draft: false
passage: Romans 4
title: Always by Faith Alone
preachers: ["Daniel Saunders"]
series:
- The Verdict
tags:
- Faith
- Justification
- Assurance
images: ["/uploads/2015/12/Romans-TheVerdict.jpg"]
---
1. Justification has always been by faith (4:1-15)
2. ...but what is faith?
3. And it's the same for us! (4:23-25)
4. Always and only by faith

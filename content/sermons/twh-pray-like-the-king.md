---
title: "Pray Like the King"
passage: "Luke 22:39-53"
date: 2020-03-08T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200308_PrayLikeTheKing_DS.mp3"]
audio_duration: "24:10"
audio_size: 6031236
preachers: ["Daniel Saunders"]
images: ["uploads/2020/02/TheWayHome5.jpg"]
series: ["The Way Home (Part 5)"]
tags: ["Prayer"]
---

What do you do when things get tough?

In this sermon as the cross draws ever closer, we see how, through Christ, we can turn to God in humbly confident prayer.

# Outline

1. The need for prayer (vv39-40)
2. Jesus’ confidently humble prayer (vv41-44)

    1. Place
    2. Posture
    3. Pain
3. The Power of Darkness (vv45-53)


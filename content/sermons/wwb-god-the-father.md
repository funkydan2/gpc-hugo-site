---
title: "God the Father"
passage: "John 5:16-27; Revelation 4:9-11"
date: 2020-10-18T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhatWeBelieve/20201018_GodTheFather_DS.mp3"]
audio_duration: "29:00"
audio_size: 7193906
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/10/WWB.jpg"]
series: ["What We Believe"]
tags: ["Creation", "Adoption", "Prayer"]
---
Christians call God 'Father', but why do we do this? What does it mean to know someone as almighty as God in such a close, personal way? This sermon is the first in our series, 'What We Believe' based on the Apostles' Creed.

'I believe in God, the Father almighty, creator of heaven and earth'

# Outline
1. I believe…what???
2. The Eternal Father (John 5:16-27)
3. The Creating Father (Revelation 4:9-11)
4. My Father and Your Father (John 8:42; 20:17; Romans 8:15)
5. Knowing God as Father (Luke 11:9-13)


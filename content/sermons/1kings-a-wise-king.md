---
title: "A Wise King"
passage: "1 Kings 3-4"
date: 2024-04-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/1Kings/20240421_AWiseKing_DS.mp3"]
audio_duration: "25:26"
audio_size: 6335641
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/04/1Kings.png"]
series: ["1 Kings"]
tags: ["Biblical Theology","Wisdom","Promises","Perseverance","Suffering","King"]
---
As Solomon's kingdom is established we see God's promised blessings fulfilled—though cracks show, and so we look to the greater wise king, Jesus.

# Outline
1. The story so far
2. God’s promises fulfilled through a wise king
3. though, cracks are beginning to show
4. Becoming wise through our wise king

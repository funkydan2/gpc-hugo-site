---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160410_FearOrFaith_DS.mp3"]
audio_duration: '26:41'
date: 2016-04-10 02:54:18+00:00
draft: false
passage: Luke 8:22-56
title: Fear or Faith?
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---

1. Jesus' power over the destructive powers of...
    1. Nature (vv22-25)
    2. Demons (vv26-38)
    3. Sickness (vv40-48)
    4. Death (vv49-56)
2. This is power to be feared!
3. But is fear the only response?

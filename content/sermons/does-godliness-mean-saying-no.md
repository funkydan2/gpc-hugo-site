+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180729_GodlinessMeanSayingNo_DS.mp3"]
audio_duration = "25:25"
audio_size = 6327223
date = "2018-07-29T16:15:04+10:00"
images = ["/uploads/2018/04/Gods-House-768x644.jpg"]
passage = "1 Timothy 4:1-5"
preachers = ["Daniel Saunders"]
series = ["God's House"]
tags = ["Thankfulness", "Creation", "Salvation"]
title = "Does godliness mean saying 'no'?"
+++
1. What's the time? (v1)

2. False forbidding (vv1-3a)

3. True thankfulness (vv3b-5)

4. Saying 'yes' with thankfulness

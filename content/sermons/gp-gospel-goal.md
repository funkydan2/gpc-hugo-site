---
title: Gospel Goal
passage: Philippians 3:12-4:1
date: 2024-12-01T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/GospelPartners/20241201_GospelGoal_DS.mp3
audio_duration: 24:34
audio_size: 6127906
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/10/GospelPartners.jpeg
series:
    - Gospel Partners
tags:
    - Endurance
    - Resurrection
    - Perseverance
---
What's your goal or purpose for life? In this message, we hear Paul's encouragement to follow his example and press on to the goal of knowing Christ.

# Outline
1. Press on to the goal (3:12-16)
2. Follow the right examples (3:17-21)
3. Press on and Stand Firm (4:1)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180520_GodlinessInGodsHousehold_DS.mp3"]
audio_duration: '26:33'
audio_size: 6599494
date: 2018-05-20 06:10:57+00:00
draft: false
passage: 1 Timothy 2:8-15
title: Godliness in God's Household
preachers: ["Daniel Saunders"]
series:
- God's House
tags:
- Godliness
images: ["/uploads/2018/04/Gods-House-768x644.jpg"]
---

1. Godly Men Pray (v8)

2. Godly Women are Modest (vv9-10)

3. Godly Women Learn (vv11-15)

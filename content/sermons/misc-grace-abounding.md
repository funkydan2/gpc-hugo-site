---
title: "Grace Abounding"
passage: "Mark 5:21-43"
date: 2024-07-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20240714_GraceAbounding_PB.mp3"]
audio_duration: "28:21"
audio_size: 7032851
preachers: ["Peter Bloomfield"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
---
Message on Christ's grace to sinners from guest preacher, Peter Bloomfield.

# Outline
1. Grace for the bleeding woman
2. Grace for Jairus
3. Christ still performs his ministry of grace!

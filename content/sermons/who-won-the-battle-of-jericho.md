---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joshua/20161023_WhoFoughtTheBattleOfJericho_DS.mp3"]
audio_duration: '25:33'
date: 2016-10-23 03:53:15+00:00
draft: false
passage: Joshua 5:13-6:27
title: Who Won the Battle of Jericho?
preachers: ["Daniel Saunders"]
series:
- Fear and Serve the Lord
tags:
- Death
- Resurrection
- Sin
- Victory
images: ["/uploads/2015/12/Joshua.jpg"]
---

1. A mysterious man with a strange plan (6:13-6:5)
2. God wins (6:6-27)
3. Jesus wins for us (1 Corinthians 15:50-57)

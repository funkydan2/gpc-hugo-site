---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170326_DenyingJesus_DS.mp3"]
audio_duration: '28:21'
date: 2017-03-26 09:50:25+00:00
draft: false
passage: Luke 22:31-65
title: Denying Jesus
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Forgiveness
- Grace
- Sin
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---

1. A further threat... (vv31-34)

2. ...so be prepared (vv35-38)

3. Jesus' final preparation

    The Cup (Jeremiah 25:15-38)

4. Twice betrayed (vv47- 65)

Good news for deniers!

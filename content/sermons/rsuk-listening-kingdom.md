---
title: Listening Kingdom
passage: Matthew 13:1-23
date: 2025-02-02T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/RightSideUpKingdom/20250202_ListeningKingdom_DS.mp3
audio_duration: 24:58
audio_size: 6221089
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/02/Matthew13-20.jpg
series:
    - The Right Side Up Kingdom
tags:
    - Evangelism
    - Parable
    - Election
---
Do you (or someone you know) have selective hearing? What about with God? In this message, Jesus calls us to have ears to hear his message.

# Outline
1. Nice story (vv1-9)
2. Why stories? (vv10-17)
3. Hear the story (vv18-23)

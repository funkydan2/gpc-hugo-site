---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20180107_LovingDiversity_DS.mp3"]
audio_duration: '28:16'
audio_size: 7014099
date: 2018-01-10 09:20:38+00:00
draft: false
passage: Romans 14:1-15:13
title: Loving Diversity
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. Accept the weak (14:1-3a)

2. Because ...

    1. ...you're not their Lord (14:3b-12)

    2. ...of the 'law of love' (14:13-23)

    3. ...it's Jesus-like (15:1-13)

3. Loving diversity in our church

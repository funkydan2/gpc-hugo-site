---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180311_LifeResurrected_DS.mp3"]
audio_duration: '22:46'
audio_size: 5695565
date: 2018-03-11 06:28:08+00:00
draft: false
passage: John 11
title: Life Resurrected
preachers: ["Daniel Saunders"]
series:
- Find Life
tags:
- Resurrection
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. The death of a friend (11:1-16)

    1. reveals God's glory bcause

    2. Jesus is the resurrection and the life (11:17-37)

    3. Jesus raises the dead (11:38-44)

*Will Jesus be at your funeral?*

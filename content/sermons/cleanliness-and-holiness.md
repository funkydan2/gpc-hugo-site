+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnholyPeopleHolyGod/20181028_CleanlinessAndHoliness_DS.mp3"]
audio_duration = "29:20"
audio_size = 7268715
date = "2018-10-28T16:21:47+10:00"
images = ["/uploads/2018/10/Unholy-People-Holy-God.jpg"]
passage = "Leviticus 11-15"
preachers = ["Daniel Saunders"]
series = ["Unholy People Holy God"]
tags = ["Purity", "Cleanliness"]
title = "Cleanliness and Holiness"

+++
1. What is cleanliness?

    Leviticus 11:45

2. Unclean ...

    1. Foods (Leviticus 11)

    2. Bodily Fluids (Leviticus 12, 15)

    3. Rashes and Moulds (Leviticus 13-14)

3. Jesus and cleanliness

    Mark 1:40; 5:25-35; 7:14-20

4. Are you clean?

    Hebrews 9:13-14

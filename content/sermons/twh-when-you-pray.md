+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190217_WhenYouPray_DS.mp3"]
audio_duration = "26:34"
audio_size = 6604368
date = "2019-02-17T09:30:00+10:00"
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 11:1-13"
preachers = ["Daniel Saunders"]
series = ["The Way Home (Part 3)"]
tags = ["Prayer", "Forgiveness", "Spirit"]
title = "When You Pray"

+++
1. Teach us to pray (v1)

2. ... for the greatest gift (vv2-4)

3. ...which honours God's name (vv5-8)

4. ...as he gives good gifts (vv9-13)

5. Jesus is the answer to his own prayer

    Luke 3:22; Acts 2:38

6. So it can be answered in us!

(Unfortunately there was some distortion in the recording of this sermon.)

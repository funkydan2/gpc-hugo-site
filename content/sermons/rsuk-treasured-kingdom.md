---
title: Treasured Kingdom
passage: Matthew 13:44-52
date: 2025-02-16T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/RightSideUpKingdom/20250216_TreasuredKingdom_DS.mp3
audio_duration: 25:07
audio_size: 6258004
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/02/Matthew13-20.jpg
series:
    - The Right Side Up Kingdom
tags:
    - Eternity
    - Judgment
    - Kingdom
    - Parable
    - Forgiveness
---
What is your greatest treasure? In this message we explore reasons why the Kingdom of Heaven should be treasured about all else.

# Outline
1. Priceless treasure (vv44-46)
2. Why treasure? (vv47-49)
3. Treasures new and old (vv51-52)

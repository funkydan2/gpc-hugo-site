---
title: "A Wise King?"
passage: "1 Kings 10-11"
date: 2024-05-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/1Kings/20240505_AWiseKing_DS.mp3"]
audio_duration: "26:20"
audio_size: 6551394
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/04/1Kings.png"]
series: ["1 Kings"]
tags: ["Wisdom","Repentance","Idolatry","King"]
---
Does God's glory take your breath away?

As Solomon's life comes to an end, we see the breathtaking goodness of God in Solomon's wisdom, but also the sad way his life ends. It's an encouragement and warning to us—because in Jesus, one greater than Solomon is here.

# Outline
1. Breathtaking Wisdom (1 Kings 10)
2. Head-shaking Disobedience (1 Kings 11)
3. Learning and Seeing True Wisdom (Matthew 12:42; 1 Corinthians 1:21-25)

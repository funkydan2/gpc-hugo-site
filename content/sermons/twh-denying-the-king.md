---
title: "Denying the King"
passage: "Luke 22:54-23:25"
date: 2020-03-15T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200315_DenyingTheKing_DS.mp3"]
audio_duration: "25:37"
audio_size: 6375846
preachers: ["Daniel Saunders"]
images: ["uploads/2020/02/TheWayHome5.jpg"]
series: ["The Way Home (Part 5)"]
tags: []
---

When the stakes were high, even one of Jesus' closest followers denied ever knowing him. This is a tragic moment of abandonment as Jesus goes to the cross alone. 

In this message, as we continue following Jesus on his Way Home, we'll be considering how we fail to recognise Jesus as King and how God offers forgiveness—even to deniers.

# Outline

1. Denied by his disciple (22:54-62)
2. Denied by the religious leaders (22:63-71)
3. Denied by the world’s leaders (23:1-16)
4. Denied by everyone (23:18-25)


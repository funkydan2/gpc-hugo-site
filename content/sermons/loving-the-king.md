+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20190113_LovingTheKing_DS.mp3"]
audio_duration = "26:39"
audio_size = 6622816
date = "2019-01-13T10:00:00+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 18-20"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = ["Friendship", "Love"]
title = "Loving the King"

+++
(Note: The name of one of our gospel partners, who works in a secure location, was mentioned towards the end of this recording. For the sake of privacy and security their name has been removed from this recording.)

# Sermon Outline

1. Two reactions to God's chosen king
   1. Jonathan's covenant-love for David
   2. Saul's hatred of David
2. Do you love God's king, Jesus?

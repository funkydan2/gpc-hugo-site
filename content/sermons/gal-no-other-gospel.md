---
title: "No Other Gospel"
passage: "Galatians 1:1-10"
date: 2022-09-18T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20220918_NoOtherGospel_DS.mp3"]
audio_duration: "28:16"
audio_size: 7013260
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Gospel"]
---
The best way to spot a fake is to know the real thing through and through. The best way to spot a non-gospel is to know the true gospel (good news) of Jesus.

# Outline
1. A letter to Galatia (vv1-5)
2. Another gospel (vv6-10)
3. No other gospel

---
title: "Feeding His Sheep"
passage: "Mark 6:30-56"
date: 2021-03-07T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210307_FeedingHisSheep_DS.mp3"]
audio_duration: "23:18"
audio_size: 5823623
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Glory"]
---
When someone asks 'so, who are you,' often we answer by talking about what we do. In this message, as we see Jesus feed a crowd in the wilderness, and 'pass by' by walking on the lake, Jesus is revealed by what he does.

# Outline
1. Feeding sheep (6:3-46)
2. Passing by (6:47-56)
3. Who is Jesus?

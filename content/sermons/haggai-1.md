---
title: "Haggai 1"
date: 2022-01-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Haggai/20220102_Haggai1_RP.mp3"]
audio_duration: "30:12"
audio_size: 7479248
preachers: ["Rayk Platzek"]
images: ["/uploads/2016/02/podcast_cover.jpg"]
series: ["Haggai"]
tags: ["Temple", "Judgment"]
---
Where is God, and the things of God, in your priorities?

# Outline
1. Historical background (Hag. 1:1,15; Ezra 4:4-5,24)
2. Wrong priorities (Hag. 1:2-6, 7, 9)
3. Punishment (Hag. 1:6, 10-11)
4. Right priorities (Hag. 1:12-14, 8)

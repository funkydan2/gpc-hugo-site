---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170219_TheQuestionOfLoyalty_DS.mp3"]
audio_duration: '26:43'
date: 2017-02-19 04:03:53+00:00
draft: false
passage: Luke 20:20-26
title: The Question of Loyalty
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Caesar
- Gospel
- Government
- Jesus
- Messiah
- Politics
- State
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---
1. The trap is set (vv20-22)
2. but Jesus springs the trap (vv23-26)
3. Give to Caesar
4. Give to God

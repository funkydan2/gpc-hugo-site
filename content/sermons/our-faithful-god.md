---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joshua/20161009_OurFaithfulGod_DS.mp3"]
audio_duration: '22:29'
date: 2016-10-09 07:10:56+00:00
draft: false
passage: Joshua 1
title: Our Faithful God
preachers: ["Daniel Saunders"]
series:
- Fear and Serve the Lord
tags:
- Faithfulness
- God
- Promises
images: ["/uploads/2015/12/Joshua.jpg"]
---

1. Joshua's Commission (vv1-9)

    1. I will not leave you

    2. Be Strong and of Good Courage

2. Joshua's Command (vv10-18)

3. Our Faithful God

    Matthew 28:20 & Acts 1:8

    Ephesians 6:10

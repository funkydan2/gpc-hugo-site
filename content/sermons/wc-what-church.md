---
title: What? Church
passage: Acts 2:42-47
date: 2025-01-26T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/WhyChurch/20250126_WhatChurch_DS.mp3
audio_duration: 25:36
audio_size: 6376541
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/01/whychurch.png
series:
    - Why? Church
tags:
    - Church
    - Lord's Supper
    - Prayer
    - Scripture
    - Evangelism
---
If Christ is building his church so that all creation will see the wisdom of God, what should churches do when we gather?

# Outline
1. A model church
2. Apostles’ Teaching (Acts 2:42; Ephesians 4:11-13)
3. Sharing (Acts 2:42-46; 1 Corinthians 11:17-35)
4. Prayer (and song) (Acts 2:42, 47; Colossians 3:16-17)
5. Adding (Acts 2:47; 1 Corinthians 14:24-25)
6. Christ’s Church, Christ’s Way

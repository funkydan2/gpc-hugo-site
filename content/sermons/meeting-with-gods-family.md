---
title: "Meeting With Gods Family"
passage: "Hebrews 10:19-25"
date: 2019-11-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/BackToBasics/20191117_MeetingWithGodsFamily_DS.mp3"]
audio_duration: "27:05"
audio_size: 6729434
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/10/back-to-basics.jpg"]
series: ["Back To Basics"]
tags: ["Church"]
---

Does *going to church* make someone a Christian? Do you have to be part of a church to be a Christian?

In this message, we explore why Christians meet with God's family.

# Outline

1. Jesus is our access to God (vv19-21)
2. So draw near to God (vv22-23)
3. And encourage each other (vv24-25)

(During this sermon some people were mentioned by name. For privacy reasons that section of the sermon has been edited.)
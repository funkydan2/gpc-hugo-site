---
title: How to be blessed like Mary this Christmas
passage: Luke 1:39-45
date: 2024-12-25T08:30:00+10:00
audio:
    - "https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20241225_BlessedLikeMary_DS.mp3"
audio_duration: 17:23
audio_size: 4402079
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/12/Christmas1.jpg
series:
    - Christmas
tags:
    - Blessing
    - Christmas
    - King
---
Christmas 2025 message.

# Outline
1. Why do we need God’s blessing?
2. Why is Mary blessed?
3. How you can be blessed too.
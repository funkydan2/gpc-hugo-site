---
title: "Spirit and Boldness"
passage: "Acts 4:1-31"
date: 2022-02-27T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220227_SpiritAndBoldness_MH.mp3"]
audio_duration: "25:15"
audio_size: 6291192
preachers: ["Mitchell Harte"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Persecution", "Evangelism", "Spirit"]
---
When opposition comes, due to the Holy Spirit's power, God's people are able to respond with boldness.

This sermon was recorded as part of our online gathering due to the February 2022 floods in Gympie.

---
title: "Wisdom and Work"
passage: "Proverbs"
date: 2022-06-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheVoiceOfWisdom/20220619_WisdomandWork_DS.mp3"]
audio_duration: "29:51"
audio_size: 7393114
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Proverbs.png"]
series: ["The Voice of Wisdom"]
tags: ["Work","Wisdom"]
---
How can we work wisely?

In this message, we hear how the Proverbs encourage hard, honest work.

# Outline
1. Hard work
2. Honest work
3. … for the Lord (Colossians 3:15-16)

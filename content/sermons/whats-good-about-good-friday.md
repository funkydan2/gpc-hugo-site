---
title: "What's Good About Good Friday?"
passage: "Luke 23:32-43"
date: 2019-04-19T8:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20190419_WhatsGoodAboutGoodFriday_DS.mp3"]
audio_duration: "19:13"
audio_size: 4840650
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/04/EasterSearching.png"]
series: ["Easter"]
tags: []
---
1. Big questions

2. What's *not good* about Good Friday?

3. What's *good* about Good Friday?

    1. Forgiveness

    2. Paradise

4. What are you searching for?

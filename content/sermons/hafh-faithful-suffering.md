---
title: "Faithful Suffering"
passage: "1 Peter 4:12-19"
date: 2021-08-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210822_FaithfulSuffering_DS.mp3"]
audio_duration: "24:43"
audio_size: 6165176
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Suffering", "Union With Christ", "Judgment"]
---
What does God have to say about suffering, especially the suffering of his people? In this message from 1 Peter we hear how not to be surprised by suffering.

# Outline
1. Suffering with Christ (vv12-14)
2. Suffering as a Christian (vv15-16)
3. Suffering and the will of God (vv17-19)

A pre-recorded video of this sermon is also available
{{< youtube NooY45AZJk0 >}}

---
title: "The Reign of God’s Word"
passage: "1 Kings 12-16"
date: 2024-05-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/1Kings/20240512_TheReignOfGodsWord_DS.mp3"]
audio_duration: "24:03"
audio_size: 6002509
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/04/1Kings.png"]
series: ["1 Kings"]
tags: ["King","Covenant","Idolatry"]
---
Why do things (good and bad) happen? As God's people are thrown into chaos as they are ruled by faithless and foolish kings, we see the God's Word remains in control.

# Outline
1. Foolish and Faithless Kings
2. But God’s Word Reigns

---
title: "Counted Worthy"
passage: "Acts 5:12-42"
date: 2022-03-20T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220320_CountedWorthy_DS.mp3"]
audio_duration: "29:22"
audio_size: 7278518
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Persecution","Mission"]
---
What do you think is the future for Christianity? Should persecution or gospel growth be expected?

In this message from Acts, we hear how the earliest church continued to grow and rejoiced to be *counted worthy* of suffering for Jesus.

# Outline
1. Signs of Life (vv12-16)
2. Obeying God or People? (vv17-33)
3. Opposing God? (vv34-39)
4. Counted Worthy (vv40-42)

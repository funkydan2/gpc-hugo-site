---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171112_DoYouHaveBeautifulFeet_DS.mp3"]
audio_duration: '27:58'
audio_size: 6941274
date: 2017-11-12 06:14:24+00:00
draft: false
passage: Romans 10
title: Do You Have Beautiful Feet?
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Evangelism
- Gospel
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. Religion will not save you (vv1-4)

2. Only Jesus saves (vv5-13)

3. Therefore ... tell the world! (vv14-15)

4. ...and be prepared for the message to be rejected (vv16-21)

5. A church with beautiful feet

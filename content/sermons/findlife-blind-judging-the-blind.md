---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180225_BlindJudgingTheBlind_DS.mp3"]
audio_duration: '22:43'
audio_size: 5680096
date: 2018-02-25 05:59:04+00:00
draft: false
passage: John 9
title: Blind Judging the Blind
preachers: ["Daniel Saunders"]
series:
- Find Life
tags:
- Blindness
- Hypocrisy
- Judgment
- Sight
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. Judging a blind man (9:1-5)

2. ...who is given sight (9:6-13)

3. ...reveals true blindness (9:14-41)

    Tradition and Religion

    Fear of people

    Pride

4. Has judgment made you blind?

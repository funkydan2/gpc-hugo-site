---
title: "Real Reward"
passage: "Matthew 6:1-12"
date: 2024-03-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StaySalty/20240317_RealReward_DS.mp3"]
audio_duration: "25:12"
audio_size: 6277516
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/02/StaySalty.jpg"]
series: ["Stay Salty"]
tags: ["Prayer","Generosity","Money","Treasure","Hypocrisy"]
---
As we continue our series listening to Jesus' Sermon on the Mount we hear his call for sincere spirituality, righteousness for God's reward.

# Outline
1. Where is your reward? What is your treasure? (vv1, 19-21)
2. Secret Generosity. Secret Fasting (vv2-4, 16-18)
3. Secret and Simple Prayer (vv5-15)
4. Is your ‘righteousness’ for real reward?

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20171224_AreWeAlone_DS.mp3"]
audio_duration: '23:25'
audio_size: 5847822
date: 2017-12-24 09:46:30+00:00
draft: false
passage: Matthew 1:18-25
title: Are We Alone?
preachers: ["Daniel Saunders"]
series:
- Strange Christmas Things
tags:
- Christmas
- Incarnation
images: ["/uploads/2017/11/Strange-Christmas-Things.jpg"]
---

1. A strange dream (vv18-20)

    1. Unexpected news

    2. Strange command

2. Two strange names (vv21-25)

    1. Saviour

    2. God with us

3. Jesus, still the saviour who is God with us (Matthew 28:16-20)

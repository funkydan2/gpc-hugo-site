---
title: "Serving God With a Pure Heart"
passage: "Haggai 2:10-19"
date: 2022-01-16T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Haggai/20220116_Haggai2v10-19_RP.mp3"]
audio_duration: "28:21"
audio_size: 7032442
preachers: ["Rayk Platzek"]
images: ["/uploads/2016/02/podcast_cover.jpg"]
series: ["Haggai"]
tags: ["Faith", "Works", "Holiness", "Temple"]
---
1. Why our works _cannot_ please God
2. Why our works _can_ please God

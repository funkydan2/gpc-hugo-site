---
title: "Marriage"
passage: "1 Peter 3:1-7"
date: 2021-08-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210801_Marriage_DS.mp3"]
audio_duration: "29:06"
audio_size: 7213404
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Marriage"]
---
In movies and fairy tales, the wedding is the start of 'happily ever after'. But we know the reality is quite different. What does Jesus have to say to us in our imperfect 'now but not yet' relationships?

# Outline
1. True beauty (vv1-6)
2. Deep respect (v7)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20170129_TheWorshipGodDeserves_DS.mp3"]
audio_duration: '29:49'
date: 2017-01-29 09:30:00+00:00
passage: Hebrews 12:18-13:19
title: The Worship God Desires
preachers: ["Daniel Saunders"]
images:
- "/uploads/2016/02/sermons.jpg"
series:
- Miscellaneous
tags:
- Church
- Worship
---

1. A tale of two mountains (Hebrews 12:18-24)
    1. The mountain where we're not...
    2. The mountain we've come to.
2. Therefore listen and serve/worship (12:25-29)

3. The worship God deserves (13:1-19)

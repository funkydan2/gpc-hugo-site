---
title: "Seek the LORD"
passage: "Zephaniah"
date: 2023-04-16T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zephaniah/20230416_SeekTheLORD_DS.mp3"]
audio_duration: "28:22"
audio_size: 7039251
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zeph.jpg"]
series: ["Zephaniah"]
tags: ["Judgment","Salvation","Joy"]
---
Through Zephaniah, God warns the world of the judgment that will come. This is serious, but the good news is, God loves to save!

# Outline
1. Context (1:1)
2. God’s sweeping judgment (1:2-7)
    1. because of his people’s sin (1:8-2:3)
    2. because of the world’s sin (2:4-3:8)
3. Singing for God’s Salvation! (3:9-20)

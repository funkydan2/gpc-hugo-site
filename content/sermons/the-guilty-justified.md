---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160529_TheGuiltyJustified_DS.mp3"]
audio_duration: '26:48'
date: 2016-05-29 04:35:35+00:00
draft: false
passage: Romans 3:21-31
title: The Guilty Justified
preachers: ["Daniel Saunders"]
series:
- The Verdict
tags:
- Atonement
- Gospel
- Justification
- Mission
- Propitiation
- Redemption
- Righteousness
images: ["/uploads/2015/12/Romans-TheVerdict.jpg"]
---

1. The Verdict: Righteous (3:21-23)
2. But how? (3:24-26)
    1. Justified
    2. Redeemed
    3. Propitiated
    4. ...and God is still just
3. So it's got nothing to do with us (3:27-31)
4. Trust in Christ!

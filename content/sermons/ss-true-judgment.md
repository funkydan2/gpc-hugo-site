---
title: "True Judgment"
passage: "Matthew 7"
date: 2024-04-07T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StaySalty/20240407_TrueJudgement_DS.mp3"]
audio_duration: "24:33"
audio_size: 6123232
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/02/StaySalty.jpg"]
series: ["Stay Salty"]
tags: ["Hypocrisy","Judgment"]
---
# Outline
1. Don’t judge hypocritically (vv1-5)
2. Don’t judge poorly (v6)
3. Judge God correctly (vv7-11)
4. Fulfilling the law and the prophets (v12)
5. Judge the 
    1. true path (vv13-14)
    2. true fruit (vv15-23)
    3. true foundation (vv24-29)

+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20181230_SeeingTheKing_DS.mp3"]
audio_duration = "28:47"
audio_size = 7136780
date = "2018-12-30T09:30:00+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 16"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = []
title = "Seeing the King"

+++
1. Seeing God's way (vv1-13)
2. Seeing the true king (vv14-23)
3. Seeing beyond appearances (2 Corinthians 5:16-21)

---
title: "The Living Cross"
passage: "1 Corinthians 15"
date: 2019-04-14T9:30:38+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20190414_TheLivingCross_DS.mp3"]
audio_duration: "24:41"
audio_size: 6152540
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/04/TheLivingCross.jpg"]
series: ["Easter"]
tags: ["Resurrection"]
---
1. Good news (15:1-11)
2. So ...
    1. the dead will be raised (15:12-23)
    2. Jesus is the living king (15:24-28)
    3. it's worth living for Jesus (15:29-34)
3. But how are the dead raised? (15:35-54)
4. Praise God and live for Jesus (15:55-58)

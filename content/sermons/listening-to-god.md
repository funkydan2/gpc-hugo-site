---
title: "Listening to God"
passage: "2 Timothy 3:16-17; 2 Peter 1:20-21; Hebrews 4:12-13"
date: 2019-11-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/BackToBasics/20191103_ListeningToGod_DS.mp3"]
audio_duration: "27:22"
audio_size: 6796224
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/10/back-to-basics.jpg"]
series: ["Back To Basics"]
tags: ["Scripture","Spirit"]
---

If God spoke to you, would you listen?  In this sermon, we continue our [Back to Basics](/series/back-to-basics) series by finding out how we can *Listen to God*.

# Outline

1. God’s *breathed-out* word (2 Timothy 3:16, 2 Peter 1:20-21)
2. God’s useful word (2 Timothy 3:16-17, Hebrews 4:12-13)
3. Are you listening?

---
title: Refuge
passage: Psalm 46
date: 2020-04-26T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/PsalmsOfComfort/20200426_POC_Refuge_DS.mp3
audio_duration: 19:55
audio_size: 5009488
preachers:
  - Daniel Saunders
images:
  - /uploads/2020/04/Psalms_Of_Comfort.jpg
series:
  - Psalms of Comfort
tags:
  - Anxiety
  - Eternity
  - Judgment
  - New Creation
  - Victory
---

What is your source of comfort?  Where do you find refuge and safety when it feels like the world is crumbling?

In Psalm 46, we're given the great news that God is refuge and strength for all who turn to him.

# Outline

1. God our refuge (vv1-3)
2. The refuge of God’s presence (vv4-7)
3. The refuge of God’s victory (vv8-11)
4. Resting in God’s refuge


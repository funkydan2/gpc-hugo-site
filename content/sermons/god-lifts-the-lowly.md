---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke1-4/20161211_GodLiftsTheLowly_DS.mp3"]
audio_duration: '23:05'
date: 2016-12-11 07:37:24+00:00
draft: false
passage: Luke 1:39-56
title: God Lifts the Lowly
preachers: ["Daniel Saunders"]
series:
- Great Expectations
tags:
- Grace
- Humility
- Incarnation
- Jesus
- Mary
images: ["/uploads/2015/12/Great-Expectations.jpg"]
---

1. Elizabeth's Prophecy (1:39-45)
2. Mary's Song/Poem (1:46-56)
3. God still lifts the lowly

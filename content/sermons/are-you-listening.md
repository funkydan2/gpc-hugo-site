---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160403_AreYouListening_DS.mp3"]
audio_duration: '24:22'
date: 2016-04-03 06:09:44+00:00
draft: false
passage: Luke 8:1-21
title: Are You Listening?
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
tags:
- Parable
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---

1. Women who listened (vv1-3)
2. Soil that listens (vv4-15)
3. Watch how you listen (vv16-18)
4. A family who listens (vv19-21)

Are you listening to Jesus?

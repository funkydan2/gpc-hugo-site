---
title: "Reconciled by Christ"
passage: "Ephesians 2:11-22"
date: 2020-06-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200614_ReconciledByChrist_DS.mp3"]
audio_duration: "22:28"
audio_size: 5621445
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/05/Ephesians_OTEYH.jpg"]
series: ["Open The Eyes Of Your Heart"]
tags: ["Reconciliation", "Peace"]
---

Where can we find peace?

In this message, we hear the great news that Jesus came to tear down division and bring us peace.

# Outline

1. Alienation (vv11-13)
2. Peace (vv14-18)
3. Together in the Lord (vv19-22)


---
title: "Changed by Christ"
passage: "Ephesians 4:17-24"
date: 2020-07-26T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200726_ChangeInChrist_DS.mp3"]
audio_duration: "22:13"
audio_size: 5563337
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Repentance", "Change"]
---

Is it possible to change?

In Jesus, believers have put on the *new self* in Christ, created to be like God, so we're not to live as if still enslaved under the power of the *old self*.

# Outline

1. The old life (vv17-19)
2. The new life (vv20-24)
3. Jesus changes lives!

During the introduction we watched the first section of [this Youtube Clip](https://www.youtube.com/watch?v=mA1wqjaeKj0)


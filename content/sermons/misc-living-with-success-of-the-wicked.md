---
title: "Living with the Success of the Wicked"
passage: "Psalm 37"
date: 2022-09-25T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20220925_LivingWithTheSuccessOfTheWicked_AB.mp3"]
audio_duration: "34:08"
audio_size: 8423342
preachers: ["Andrew Brown"]
images: ["/uploads/2022/09/Psalm37.jpg"]
series: ["Miscellaneous"]
tags: ["Wisdom","Suffering","Patience"]
---
We live in a world where it often seems that unjust, wicked people succeed. How does this fit with the Bible's claim that God is a loving and powerful Father?

In Psalm 37 we hear the 'A-Z' of how to not fret by instead trusting in God's goodness, trusting in his just judgment, and patiently waiting upon him.

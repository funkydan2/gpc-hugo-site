---
title: "Sent for the Kingdom"
passage: "Matthew 9:35-10:15"
date: 2024-08-18T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240818_SentForTheKingdom_DS.mp3"]
audio_duration: "24:26"
audio_size: 6094379
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/07/SeeingTheKingdom.jpg"]
series: ["Seeing the Kingdom"]
tags: ["Prayer","Mission","Evangelism"]
---
How do you respond when you see the 'mess' of our world? In this message, we hear how Jesus responded and what he calls his followers to do.

# Outline
1. Jesus' Compassion (9:35-38)
2. Jesus' Call (10:1-4)
3. Jesus' Command (10:5-15)

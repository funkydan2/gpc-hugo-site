---
title: "I AM the Bread of Life"
passage: "John 6:30-59"
date: 2022-10-16T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20221016_IAmTheBreadOfLife_MD.mp3"]
audio_duration: "30:12"
audio_size: 7480033
preachers: ["Malcolm Diefenbach"]
images: ["/uploads/2022/10/John6.jpg"]
series: ["Miscellaneous"]
tags: ["Jesus","Christ"]
---
A sermon on who Jesus is, his divinity and eternity, from John 6.

---
title: "The Extra Mile"
passage: "Matthew 5:38-48"
date: 2022-11-13T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20221113_TheExtraMile_SW.mp3"]
audio_duration: "25:35"
audio_size: 6371493
preachers: ["Shane Wright"]
images: ["/uploads/2022/11/MILE-InTheBibleBG2.jpg"]
series: ["Miscellaneous"]
tags: ["Persecution","Love","Jesus"]
---
What does it really mean to 'go the extra mile' for someone? In this message, we hear how this saying from Jesus doesn't match up with what we might think the saying means.

(Due to a power outage this sermon was taken from our live stream.)

# Outline
1. Going the extra mile. (Mat. 5:41)
2. How to live in the second mile.
    1. Going the extra mile by turning the other cheek. (Mat. 5:39)
    2. Going the extra mile by giving more than required. (Mat. 5:40)
    3. Going the extra mile by living generously. (Mat. 5:42)
3. Why we live in the second mile?
    1. It keeps us from taking justice into our own hands. (Rom 13:1-5)
    2. Going the extra mile in love is a better strategy. (Rom. 12:19-21)
    3. Jesus went the extra mile… all the way to the cross. (1 Peter 2:21-24)

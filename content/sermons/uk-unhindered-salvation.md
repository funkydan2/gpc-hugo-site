---
title: "Unhindered Salvation"
passage: "Acts 16:11-40"
date: 2023-02-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230205_UnhinderedSalvation_DS.mp3"]
audio_duration: "26:34"
audio_size: 6605685
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Salvation","Baptism"]
---
One of the most important questions to ask is 'what must I do to be saved'. In this message from Acts 16, we see the difference discovering the answer to this question makes.

# Outline
1. Lydia’s household saved (vv11-15)
2. An enslaved girl saved (vv16-24)
3. A jailer’s household saved (vv25-40)

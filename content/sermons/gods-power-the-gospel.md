---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160501_GodsPowerTheGospel_DS.mp3"]
audio_duration: '25:50'
date: 2016-05-01 06:54:13+00:00
draft: false
passage: Romans 1:1-17
title: 'God''s Power: The Gospel'
preachers: ["Daniel Saunders"]
series:
- The Verdict
tags:
- Gospel
images: ["/uploads/2015/12/Romans-TheVerdict.jpg"]
---

1. A letter
    1. From Paul, a slave of the gospel (vv1, 5, 11-15)
    2. To believers in Rome (vv6-8)
2. About the gospel
    1. of Jesus (vv2-4)
    2. of salvation (vv16-17)
3. Do you trust in God's gospel power?

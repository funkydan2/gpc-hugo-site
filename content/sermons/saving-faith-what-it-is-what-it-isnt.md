---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20180610_SavingFaith_DB.mp3"]
audio_duration: '23:03'
audio_size: 5759342
date: 2018-06-10 07:06:05+00:00
draft: false
title: 'Saving Faith: What it is, What it isn''t'
preachers: ["Daryl Brenton"]
series:
- Miscellaneous
tags:
- Faith
images: ["/uploads/2018/06/Saving-Faith-768x644.jpg"]
---

1. Not all kinds of faith save

2. Belief and Unbelief

3. Unbelief

    1. Adam

    2. Israelites

4. Saving Faith

    1. Its source

    2. Its qualities

5. Let us then examine ourselves

(Unfortunately, the recording was quiet and the occasional word was inaudible and was lost during editing.)

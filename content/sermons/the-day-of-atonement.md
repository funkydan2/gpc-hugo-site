+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnholyPeopleHolyGod/20181104_TheDayOfAtonement_DS.mp3"]
audio_duration = "25:49"
audio_size = 6424118
date = "2018-11-04T10:00:00+10:00"
images = ["/uploads/2018/10/Unholy-People-Holy-God.jpg"]
passage = "Leviticus 16"
preachers = ["Daniel Saunders"]
series = ["Unholy People Holy God"]
tags = ["Forgiveness", "Atonement", "Sacrifice"]
title = "The Day Of Atonement"

+++
1. Entering God's presence

2. Jesus and the Day of Atonement

    Hebrews 9:23-28

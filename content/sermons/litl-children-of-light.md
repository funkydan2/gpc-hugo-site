---
title: "Children of Light"
passage: "Ephesians 5:1-14"
date: 2020-08-23T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200823_ChildrenOfLight_DS.mp3"]
audio_duration: "27:11"
audio_size: 6753155
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Light", "Greed", "Money", "Thankfulness"]
---

Jesus calls his people to love sacrificially, and when they do this they'll shine as Children of Light.

# Outline

1. Not even a hint (vv3-4)
2. Because
    1. No inheritance (v5)
    2. You are light (vv6-10)
    3. … which enlightens (vv11-14)
3. Living as Light


---
title: "The King's House"
passage: "1 Kings 5-9"
date: 2024-04-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/1Kings/20240428_TheKingsHouse_DS.mp3"]
audio_duration: "27:10"
audio_size: 6751766
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/04/1Kings.png"]
series: ["1 Kings"]
tags: ["Biblical Theology","Temple","Church","Glory"]
---
Where is God? Where does he live?

As God's temple is built in Jerusalem, we see God dwelling among his people. But even this is only a shadow of the true temple, Christ, and how God dwells now with his gathered people by the Spirit.


# Outline
1. God’s Place
2. Building God’s House (1 Kings 5-7)
3. God’s Presence (1 Kings 8-9)
4. Building God’s Spiritual (and Permanent) House (John 2:19-21, Ephesians 2:19-22)

---
title: "The Exiled King"
passage: "2 Samuel 14-16"
date: 2019-06-09T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190609_TheExiledKing_DS.mp3"]
audio_duration: "29:47"
audio_size: 7376716
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/04/SWFTTK.jpg"]
series: ["Still Waiting For The True King"]
tags: []
---
We long for a true home because we are in exile from God. In Jesus, the king who weeps on the Mount of Olives, and enters into exile to bring us home, we find the answer to this longing and the fulfilment of God’s promises.

# Outline
1. Absalom’s exile and return (2 Samuel 14)
2. Absalom’s coup (2 Samuel 15:1-13)
3. David’s exile (2 Samuel 15:14-16:23)
4. Jesus’ exile for us (John 18:1; Luke 22:39-42)
5. Life as exiles (1 Peter 2:11-12)

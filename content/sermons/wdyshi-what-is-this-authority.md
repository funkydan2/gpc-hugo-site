---
title: "What is this? Authority!"
passage: "Mark 1:14-45"
date: 2021-01-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210110_WhatIsThisAuthority_DS.mp3"]
audio_duration: "23:59"
audio_size: 5988297
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Authority", "Healing", "Mission", "Discipleship"]
---
Jesus stood out because of his unique and unquestionable authority. As we see Jesus' authority in his words and actions, it raises the question of how we respond to him.

# Outline
1. It’s time (vv14-15)
2. Jesus has authority…
    1. to call (vv16-20)
    2. in his teaching (vv21-22)
    3. over unclean spirits (vv23-28)
    4. over sickness (vv29-39)
    5. over uncleanness (vv40-45)
3. Life under Jesus’ authority

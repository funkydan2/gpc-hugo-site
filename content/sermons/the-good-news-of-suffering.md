---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171029_GoodNewsOfSuffering_DS.mp3"]
audio_duration: '26:11'
audio_size: 6515250
date: 2017-10-29 06:26:06+00:00
draft: false
passage: Romans 8:18-39
title: The Good News of Suffering
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Creation
- Gospel
- Hope
- Perseverance
- Suffering
images: ["/uploads/2017/09/Romans6-16.jpg"]
---
1. Suffering - but with hope (vv18-25)

2. Suffering - but not alone (vv26-27)

3. Suffering - but with security (vv28-39)

4. Suffering as a Christian

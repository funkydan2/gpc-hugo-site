---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170226_TheQuestionOfResurrection_DS.mp3"]
audio_duration: '23:27'
date: 2017-02-26 04:42:50+00:00
draft: false
passage: Luke 20:27-40
title: The Question of Resurrection
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Resurrection
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---

1. The riddle of resurrection (vv27-33)
2. Jesus' answer
    1. Marriage at the resurrection (vv34-36)
    2. Resurrection and God (vv37-40)
3. Our hope for resurrection

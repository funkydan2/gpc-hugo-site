---
title: "Lord of the Sabbath"
passage: "Luke 2:23-3:6"
date: 2021-01-24T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210124_LordOfTheSabbath_DS.mp3"]
audio_duration: "24:19"
audio_size: 6065514
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Sabbath", "Rest", "Salvation"]
---
Do you need rest and re-creation? Jesus shows us how humanity has the ability to take what is made for joy and make it a burden. But in this message from Mark, we hear Jesus came to give rest and life.

# Outline
1. Remember the Sabbath (Exodus 20:8-11; Deuteronomy 5:12-15)
2. Lord of the Sabbath (Mark 2:23-38)
3. Saviour of the Sabbath (Mark 3:1-6)
4. Resting in Christ

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160417_IsJesusWorthTheCost_DS.mp3"]
audio_duration: '24:50'
date: 2016-04-17 04:03:01+00:00
draft: false
passage: Luke 9:1-27
title: Is Jesus Worth the Cost?
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---
1. The Cost of Following Jesus (9:23)
2. Following Jesus' Mission (9:1-6)
3. Why it's worth the cost:
    1. Jesus is God's Christ (9:7-9, 18-20)
    2. ... who provides for His people (9:10-17)
    3. ... who will suffer for His people (9:21-27)

Is following Jesus worth the cost?

---
title: "Healthy Relationships"
passage: "Ephesians 2:25-32"
date: 2020-08-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200802_HealthyRelationships_DS.mp3"]
audio_duration: "29:35"
audio_size: 7330884
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Work", "Wrath", "Glory", "Parenting", "Godliness", "Money"]
---

How can we have healthy relationships?  In this message, we hear how Jesus enables us to 'live in the light', particularly in the way we speak, work, and respond to anger.

# Outline

1. Healthy Words (vv25, 29-32)
2. Healthy Work (v28)
3. Healthy Wrath (vv26-27, 31-32)
       Psalm 4:1-5
4. Changed for relational health


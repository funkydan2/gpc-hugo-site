---
title: "Building Life with God"
passage: "Psalm 127"
date: 2024-06-09T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20240609_BuildingLifeWithGod_MH.mp3"]
audio_duration: "23:43"
audio_size: 5923428
preachers: ["Mitchell Harte"]
images: ["/uploads/2024/06/Psalm127.jpg"]
series: ["Miscellaneous"]
tags: ["Work","Children"]
---
1. Building buildings and work with God (vv1-2)
2. Building families with God (vv3-5)

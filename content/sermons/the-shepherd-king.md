---
title: The Shepherd King
passage: 2 Samuel 12
date: 2019-05-26T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190526_TheShepherdKing_DS.mp3
audio_duration: 31:53
audio_size: 7882101
preachers:
  - Daniel Saunders
images:
  - /uploads/2019/04/SWFTTK.jpg
series:
  - Still Waiting For The True King
tags:
  - Forgiveness
  - Repentance
---
Just like Saul before him, David has fallen as the shepherd-king and done evil in the eyes of the LORD. His house will now experience curse rather than blessing—the people have fallen with their king.

# Outline
1. Secret Sin … Revealed (vv1-12)
2. Grace … and consequences (vv13-25)
3. Has David changed? (vv26-31)
4. Finding grace in the Good Shepherd

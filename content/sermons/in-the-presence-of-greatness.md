---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160313_InThePresenceOfGreatness_DS.mp3"]
audio_duration: '21:41'
date: 2016-03-12 23:43:14+00:00
draft: false
passage: Luke 7:1-17
title: In The Presence of Greatness
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
tags:
- Faith
- Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---
How to respond to greatness

1. Humble Faith (vv1-10)
    ...in Jesus' authority
2. Awe at Jesus' Power (vv11-17)

How do *you* respond to Jesus' greatness?

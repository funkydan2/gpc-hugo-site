---
title: Kingdom of Rest
passage: Matthew 11:25-30
date: 2024-09-15T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240915_KingdomOfRest_DS.mp3
audio_duration: 25:37
audio_size: 6379428
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/07/SeeingTheKingdom.jpg
series:
    - Seeing the Kingdom
tags:
    - Election
    - Evangelism
    - Rest
---
Are you weary or burdened? In this message, we hear Jesus invitation to those who are.

# Outline
1. What pleases God? (vv25-27)
2. What is Christ’s heart? (vv28-30)
3. Find Rest in Christ

During the sermon two books by Dane Ortland were mentioned:
1. [Gentle and Lowly](https://reformers.com.au/products/9781433566134-gentle-and-lowly-the-heart-of-christ-for-sinners-and-sufferers-ortlund-dane)
2. [The Heart of Jesus](https://reformers.com.au/products/9781433593734-the-heart-of-jesus-how-he-really-feels-about-you-dane-ortlund)

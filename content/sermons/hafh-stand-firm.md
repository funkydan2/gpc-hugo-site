---
title: "Stand Firm"
passage: "1 Peter 5"
date: 2021-08-29T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210829_StandFirm_DS.mp3"]
audio_duration: "24:38"
audio_size: 6142539
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Leadership", "Authority", "Church", "Perseverance", "Prayer","Assurance"]
---
Many of us find it a challenge to stand firm to our principles and things we value - especially when they're challenged.

In this final talk in our series 'Hope Away from Home' be encouraged by what God's done for his people so we can stand firm in the truth.

# Outline
1. Elders and Youngers (vv1-5)
2. Humble prayers (vv5-7)
3. Stand firm (vv8-14)

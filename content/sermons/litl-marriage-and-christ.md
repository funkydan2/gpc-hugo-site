---
title: "Marriage and Christ"
passage: "Ephesians 5:21-33"
date: 2020-09-06T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200906_MarriageAndChrist_DS.mp3"]
audio_duration: "29:53"
audio_size: 7399290
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Marriage", "Family", "Love"]
---

The new life Jesus gives is reflected in all our relationships. In this message, we hear the difference Jesus makes to marriage, and how marriage is a picture of the gospel.

# Outline

1. Wives (vv22-24)
2. Husbands (vv25-31)
3. Christ and the Church (vv32-33)


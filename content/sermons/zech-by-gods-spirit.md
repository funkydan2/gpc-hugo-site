---
title: "By God's Spirit"
passage: "Zechariah 4:1-6:8"
date: 2023-05-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230521_ByGodsSpirit_DS.mp3"]
audio_duration: "29:08"
audio_size: 7222282
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Temple","Spirit","Priest","King","Sin"]
---
Do you feel things are hopeless and you're helpless? That the mountains of obstacles only ever look larger and harder?

In this message from Zechariah's vision, we hear a message of hope in God's Spirit in difficult times.

# Outline
1. Scene 6: God’s strengthening Spirit (Chapter 4)
2. Scenes 7-9: God removes sin (5:1-6:8)
3. By my Spirit! (Acts 2:32-33)

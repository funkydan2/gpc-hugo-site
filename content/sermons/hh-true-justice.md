---
title: "True Justice"
passage: "Malachi 2:17-3:5"
date: 2023-12-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Malachi/20231203_TrueJustice_DS.mp3"]
audio_duration: "26:49"
audio_size: 6667623
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/11/HalfHearted.png"]
series: ["Half Hearted"]
tags: ["Temple","Sin","Justice","Covenant"]
---
When we make a mistake, we often try to cover it up, hoping no one notices. Our sin is always seen—we can't hide it from the God of Justice. But there's good news—in Jesus, God has come to purify and refine his people, so we can be acceptable to him.


# Outline
1. Where is the God of justice? (2:17; 3:5)
2. An unwanted messenger (3:1-4)
3. When the Lord comes (Mark 11:15-17; 15:38; 1 Peter 2:4-5)

---
title: "Out of the Shadows"
passage: "Colossians 2:16-23"
date: 2019-12-29T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20191229_OutOfTheShadows_DS.mp3"]
audio_duration: "23:36"
audio_size: 5891764
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/09/ChristOverAll.jpg"]
series: ["Christ Over All"]
tags: ["Law", "False Teaching", "Religion", "Union With Christ"]
---

Saying 'no' can feel spiritual - I can't eat that, I can't do that today - but does it help us know God in Christ?

# Outline

1. Don’t be judged

   1. …because of *shadows* (vv16-17)
   2. …because of false religion (vv18-19)

2. Since Christ brings life (vv20-23)

   Mark 7
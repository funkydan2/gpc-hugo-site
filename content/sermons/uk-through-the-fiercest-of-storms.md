---
title: "Through the Fiercest of Storms"
passage: "Acts 27:1-28:10"
date: 2023-03-26T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230326_ThroughTheFiercestOfStrorms_MH.mp3"]
audio_duration: "21:55"
audio_size: 5488773
preachers: ["Mitchell Harte"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Faith","Perseverance"]
---
How can we persevere when life's fiercest storms come?

In the message from Acts 27-28, we see how God's promises to Paul enable him to remain confident in God—even through fear.

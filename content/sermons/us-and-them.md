---
title: "Us and Them"
passage: "Ephesians 1:1-14"
date: 2020-05-24T09:20:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200524_UsAndThem_DS.mp3"]
audio_duration: "26:16"
audio_size: 6531452
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/05/Ephesians_OTEYH.jpg"]
series: ["Open The Eyes Of Your Heart"]
tags: ["Israel", "Salvation", "Union With Christ", "Redemption", "Forgiveness"]
---

What makes you want to praise or celebrate someone?

In Ephesians 1:1-14, we hear so many reasons to praise God for his glorious grace.

# Outline

1. Who are you? (vv1-2, 11-14)
2. God’s blessed people (vv3-10)
3. …going to the nations (vv11-14)
4. To the praise of God’s glory!


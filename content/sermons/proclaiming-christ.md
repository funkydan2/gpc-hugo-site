---
title: "Proclaiming Christ"
passage: "Colossians 1:24-2:5"
date: 2019-12-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20191201_ProclaimingChrist_DS.mp3"]
audio_duration: "23:10"
audio_size: 5787163
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/09/ChristOverAll.jpg"]
series: ["Christ Over All"]
tags: ["Evangelism"]
---

Do you have a life motto or a life goal?  

In this sermon, we return to our series in Colossians, [Christ Over All](/series/christ-over-all), and we're considering what a Christian's life goal should be.

# Outline

1. Suffering for Christ and his church (1:24, 29; 2:1)
2. To present everyone fully mature (1:25-28; 2:2-3)
3. And not deceived (2:4-5)
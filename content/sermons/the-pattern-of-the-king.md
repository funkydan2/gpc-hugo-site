---
title: The Pattern of the King
passage: 2 Samuel 13
date: 2019-06-01T23:30:00.000+00:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190602_ThePatternOfTheKing_DS.mp3
audio_duration: 29:44
audio_size: "7363808"
preachers:
  - Daniel Saunders
images:
  - /uploads/2019/04/SWFTTK.jpg
series:
  - Still Waiting For The True King
tags:
  - Revenge
  - Sin
  - justice
---
In David's sons we see the dreadful spiral and spread of evil as they repeat the sin of their father. It's another series of gut-wrenching events, which shows our need for the true Son of God who will spread blessing and not curse.

Just before the sermon we watched this video {{<youtube yiXvAo6o0HY>}}

Please be aware this sermon and Bible passage addresses a situation of abuse and violence.

# Outline
1. The pattern repeats
    1. Twisted desires (vv1-19)
    2. Passive ‘protectors’ (vv20-23)
    3. Deadly revenge (vv23-39)
2. Jesus breaks the pattern (Romans 12:1-2)

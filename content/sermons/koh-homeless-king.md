---
title: "Homeless King"
passage: "Matthew 2:13-23"
date: 2024-01-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/KingOfHeaven/20240114_HomelessKing_DS.mp3"]
audio_duration: "25:11"
audio_size: 6275217
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/12/KingOfHeaven.jpg"]
series: ["King of Heaven"]
tags: ["Victory","Anger"]
---
Jesus begins his life despised and rejected, fleeing the fury of kings. As he 'fills up' Old Testament prophecy, even in these events we see the beginning of Jesus' victory.

# Outline
1. No place to lay his head (vv13-15)
2. The fury of kings (vv16-18)
3. The Nazarene (vv19-23)
4. The end of Rachel’s tears (Jeremiah 31, Revelation 21)

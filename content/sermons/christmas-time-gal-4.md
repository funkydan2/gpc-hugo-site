---
title: "Christmas Time"
passage: "Galatians 4:4-7"
date: 2022-12-25T08:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20221225_ChristmasTime_DS.mp3"]
audio_duration: "16:37"
audio_size: 4216605
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/12/Christmas1.jpg"]
series: ["Christmas"]
tags: ["Christmas","Incarnation"]
---
At Christmas, we celebrate two *sendings* of God—the sending of the Son and the Spirit 'when the set time had fully come'.

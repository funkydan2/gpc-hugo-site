---
title: "Trusting Jesus"
passage: "Mark 5:21-43"
date: 2021-02-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210221_TrustingJesus_DS.mp3"]
audio_duration: "26:27"
audio_size: 6580390
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Faith", "Power"]
---
Why should you trust Jesus? As we see Jesus heal a chronically ill woman and raise a little girl to life, we see that Jesus can be trusted not only because of his power and authority but also because of his compassionate heart.

# Outline
1. Desperate Faith (vv21-24)
2. Hidden faith (vv25-34)
3. Faith for life (vv35-43)
4. Trusting Jesus

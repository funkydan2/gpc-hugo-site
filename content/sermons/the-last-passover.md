---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170319_TheLastPassover_DS.mp3"]
audio_duration: '20:15'
date: 2017-03-19 02:15:38+00:00
draft: false
passage: Luke 22:1-30
title: The Last Passover
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Gospel
- Jesus
- Lord's Supper
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---

1. Conspiracies inside and out (vv1-6)

2. Preparing the Passover (vv7-13)

3. The Last Passover (vv14-20)

  Remember
  'for you'

  New Covenant

4. Why did Jesus die? (vv21-23)

5. Celebrating the Passover

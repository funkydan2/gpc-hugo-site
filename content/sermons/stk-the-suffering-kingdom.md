---
title: "The Suffering Kingdom"
passage: "Matthew 10:16-42"
date: 2024-08-25T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240825_TheSufferingKingdom_DS.mp3"]
audio_duration: "25:43"
audio_size: 6402540
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/07/SeeingTheKingdom.jpg"]
series: ["Seeing the Kingdom"]
tags: ["Mission","Evangelism","Persecution"]
---
What is worth suffering rejection and opposition? In this message, we hear Jesus speak of how his eternal kingdom is even worth enduring persecution.

# Outline
1. Be on Guard
2. Why persecution?
3. Why endure it?

---
title: "Witnesses to the Resurrection"
passage: "Acts 1"
date: 2022-01-30T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220130_WitnessesOfTheResurrection_DS.mp3"]
audio_duration: "24:39"
audio_size: 6146826
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Resurrection", "Mission", "Evangelism", "Ascension", "Kingdom"]
---
Where is Jesus now? What is he doing?

In this message, which begins our new series in Acts 1-7, we hear the encouraging answer to these questions.

# Outline
1. What Jesus *began* to do (vv1-5)
2. Power for witness (vv6-8)
3. What Jesus continues to do (vv9-11)
4. What the witnesses continue to do (vv12-26)

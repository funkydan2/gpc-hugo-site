---
title: "The Son Revealed"
passage: "Mark 13"
date: 2021-11-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211121_TheSonRevealed_DS.mp3"]
audio_duration: "0:31:58"
audio_size: 7901422
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Cross", "Resurrection", "Ascension", "Kingdom", "Mission", "Apocalyptic", "Son of Man"]
---
In this message, we hear some of Jesus most troubling words, as he prepares his followers for the greatest distress of history—and the mission that will follow.

# Outline
1. Mountain Top View (vv1-4)
2. Signs and Suffering (vv5-13)
3. Greatest distress (vv14-23)
4. The Son Comes (vv24-32)
5. Watch (vv33-37)
6. The Mission of the Son Revealed

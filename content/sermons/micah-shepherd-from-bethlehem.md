---
title: "The Shepherd from Bethlehem"
passage: "Micah 4:9-5:15"
date: 2021-05-31T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Micah/20210530_TheShepherdFromBethlehem_DS.mp3"]
audio_duration: "28:21"
audio_size: 7034417
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/04/Micah.jpg"]
series: ["Micah"]
tags: ["Suffering", "Jesus", "Holiness"]
---
Where do you see God at work? Our instinct is often to look for 'big' things or 'good' things. But as God spoke to his people through Micah, he teaches them that he's at work through good and difficult times, in unexpected places, and as he grows them in holiness.

# Outline
1. Present pain (4:9-5:1)
2. Coming shepherd (5:2-8)
3. The end of evil (5:9-15)
4. Seeing God at work

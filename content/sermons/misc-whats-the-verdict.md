---
title: "What's the Verdict"
passage: "John 5:31-47"
date: 2020-10-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20201011_TheVerdict_LW.mp3"]
audio_duration: "32:22"
audio_size: 7999252
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
tags: ["Jesus"]
---
# Outline

* Witness 1: Jesus
* Witness 2: God the Father
* Witness 3: John the Baptist
* Witness 4: Jesus' Works
* Witness 5: The Scriptures
* Witness 6: Moses

* Jesus exposes the plaintiffs (the religious leaders)


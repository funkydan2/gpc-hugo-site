---
title: "Unstoppable Salvation"
passage: "Acts 8:1-25"
date: 2022-07-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220717_UnstoppableSalvation_DS.mp3"]
audio_duration: "28:21"
audio_size: 7035154
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Mission","Persecution","Sin","Spirit"]
---
Does God ever find things not going to his plan?

In Acts 8 we see that persecution, sorcery, slowness, and sin cannot stop God's plan to bring salvation.

# Outline
1. Scattered but not stopped (vv1-4)
2. Receiving the Gospel and the Spirit (vv5-17)
3. Repentance (vv18-25)

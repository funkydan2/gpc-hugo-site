---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Judges/20170903_DeliveranceThroughDeath_DS.mp3"]
audio_duration: '27:18'
audio_size: 6780748
date: 2017-09-03 05:29:21+00:00
draft: false
passage: Judges 13-16
title: Deliverance Through Death
preachers: ["Daniel Saunders"]
series:
- Crying Out For A Saviour
images: ["/uploads/2017/06/Judges.jpg"]
---
1. The deliverer no-one wanted (13:1-5)

2. The deliverer who wants to be like everyone else (16:4-21)

3. The deliverer who's victorious in death (16:22-31)

4. A better deliverer!

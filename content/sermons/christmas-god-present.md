---
title: "God Present"
passage: "Matthew 1:18-25"
date: 2023-12-24T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20231224_GodPresent_DS.mp3"]
audio_duration: "17:32"
audio_size: 4436786
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/12/Christmas1.jpg"]
series: ["Christmas 2023"]
tags: ["Christmas","Incarnation"]
---
A Christmas Eve message on Immanuel: God's presence.

# Outline
1. In Jesus, God is Present
2. but is this good or bad news?

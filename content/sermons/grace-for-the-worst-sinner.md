---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180429_GraceForTheWorstSinner_DS.mp3"]
audio_duration: '21:01'
audio_size: 5272066
date: 2018-04-29 07:55:46+00:00
draft: false
passage: 1 Timothy 1:12-17
title: Grace for the Worst Sinner
preachers: ["Daniel Saunders"]
series:
- God's House
tags:
- Grace
- Mercy
- Salvation
- Sin
images: ["/uploads/2018/04/Gods-House-768x644.jpg"]
---

1. A trustworthy saying (vv12-16)

2. about a glorious God (v17)

3. Do you know God's grace?

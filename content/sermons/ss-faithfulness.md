---
title: "Faithfulness"
passage: "Matthew 5:27-32"
date: 2024-03-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StaySalty/20240303_Faithfulness_DS.mp3"]
audio_duration: "26:30"
audio_size: 6589999
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/02/StaySalty.jpg"]
series: ["Stay Salty"]
tags: ["Marriage","Faithfulness","Grace"]
---
Jesus calls his followers to ‘stay salty’—to wholeheartedly live as his people. In this message, we hear Jesus’ call for his people to do marriage and relationships differently.

# Outline
1. Faithful eyes (vv27-30)
2. Faithful promises (vv31-32)
3. The Faithful Husband (Hebrews 13:4-5; Ephesians 5:31-32)

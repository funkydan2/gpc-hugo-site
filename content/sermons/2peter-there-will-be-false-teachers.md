---
title: "There Will Be False Teachers"
passage: "2 Peter 2"
date: 2023-01-15T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/2Peter/20230115_ThereWillBeFalseProphets_DS.mp3"]
audio_duration: "26:34"
audio_size: 6607199
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/2Peter.jpg"]
series: ["Grow in Grace"]
tags: ["Salvation","Judgment"]
---
How can we know true or false teachers? In this message from 2 Peter 2, we hear the warning of false teachers and are encouraged to be discerning.

# Outline
1. A Perennial Problem (vv1-3)
2. God will judge and save (vv4-10)
3. Seeing the Truth  (vv10-22)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joshua/20161016_BroughtIn_DS.mp3"]
audio_duration: '23:08'
date: 2016-10-16 06:45:01+00:00
draft: false
passage: Joshua 2
title: Brought In
preachers: ["Daniel Saunders"]
series:
- Fear and Serve the Lord
tags:
- Grace
- Judgment
- Mercy
images: ["/uploads/2015/12/Joshua.jpg"]
---

(Just before the sermon began we watched this video clip https://www.youtube.com/watch?v=ul16jUU8-Sc)

1. Two sneaky men and a shady lady (2:1-7)
2. God's reputation (2:8-11)
3. Striking a deal (2:12-21)
4. The spies report (2:22-23)
5. Rahab brought in (6:22-25; Matthew 1:5-6)
6. God brings us in (Ephesians 2:12-13)

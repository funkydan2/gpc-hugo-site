---
title: Compassionate Justice
passage: Nahum
date: 2022-04-24T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/Nahum/20220424_CompassionateJustice_DS.mp3
audio_duration: 33:10
audio_size: 8191929
preachers:
  - Daniel Saunders
images:
  - /uploads/2022/04/Nahum.png
series:
  - Nahum
tags:
  - Judgment
  - Justice
---
Is God's justice good news? What compassion can be found when God avenges wrongs?

In this message from the little known prophet, Nahum, we hear the good news of God's compassionate justice.

# Outline
1. Setting the Scene
2. An avenging refuge (1:1-8)
3. Judgment against Nineveh (2:3-10)
4. Joy in Justice (1:15; 3:18-19)
5. Compassionate Justice at the Cross (Colossians 2:13-15)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171119_GodsUnendingMercy_DS.mp3"]
audio_duration: '30:16'
audio_size: 7492529
date: 2017-11-19 08:40:33+00:00
draft: false
passage: Romans 11
title: God's Unending Mercy
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Israel
- Mercy
images: ["/uploads/2017/09/Romans6-16.jpg"]
---
1. Not rejected but a remnant (vv 1-10)

2. Not by genetics but by grafting (vv 11-24)

3. Not 'Israel' but 'All Israel' (vv 25-32)

4. God's glorious mercy (vv 33-36)

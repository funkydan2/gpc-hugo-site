---
title: "New Creation"
passage: "Galatians 6:11-18"
date: 2022-12-18T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221218_NewCreation_DS.mp3"]
audio_duration: "21:53"
audio_size: 5484081
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Sin","Law","Persecution"]
---
What is *legalism* a problem?

# Outline
1. What causes false teaching? (vv11-13)
2. What keeps us true to Christ? (vv14-18)

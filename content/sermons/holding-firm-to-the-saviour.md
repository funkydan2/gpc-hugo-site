---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/2Thessalonians/20170625_HoldingFirmToTheSaviour_DS.mp3"]
audio_duration: '25:19'
audio_size: 6305934
date: 2017-06-25 06:49:17+00:00
draft: false
passage: 2 Thessalonians 2:13-17
title: Holding Firm to the Saviour
preachers: ["Daniel Saunders"]
series:
- Hold Firm
tags:
- Gospel
- Holiness
- Salvation
- Spirit
images: ["/uploads/2017/05/Hold-Firm-2-Thessalonians.jpg"]
---

1. Why do we need to be saved? (v13)
2. How can we be saved? (vv13-14)
3. How do we hold firm? (vv15-17)

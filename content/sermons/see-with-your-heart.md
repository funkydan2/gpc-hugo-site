---
title: "See With Your Heart"
passage: "Ephesians 1:15-23"
date: 2020-05-31T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200531_SeeWithYourHeart_DS.mp3"]
audio_duration: "25:49"
audio_size: 6426982
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/05/Ephesians_OTEYH.jpg"]
series: ["Open The Eyes Of Your Heart"]
tags: ["Prayer", "Salvation"]
---

If you could ask God for anything, what would it be?

Having heard of God's lavish blessings, in Ephesians 1:15-23 we see how the gospel shapes the priorities of prayer.

# Outline

1. Blessing and thanks (vv15-16)
2. Asking God to (v17)
    1. know hope (v18)
    2. know riches (v18)
    3. know resurrection power (vv19-23)
3. Praying for spiritual sight


+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180902_HowAreYouTeaching_DS.mp3"]
audio_duration = "22:07"
audio_size = 5535369
date = "2018-09-02T10:06:29+10:00"
images = ["/uploads/2018/04/Gods-House-768x644.jpg"]
passage = "1 Timothy 6:3-16"
preachers = ["Daniel Saunders"]
series = ["God's House"]
tags = ["Money"]
title = "How are you teaching?"

+++
1. The failings of false teaching:

     1. Fighting (vv3-5)

     2. Finance (vv6-10)

2. Flee and fight (vv11-16)

3. How are you teaching?

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180527_LeadershipInGodsHousehold_DS.mp3"]
audio_duration: '27:33'
audio_size: 6841136
date: 2018-05-27 05:51:27+00:00
draft: false
passage: 1 Timothy 3:1-13
title: Leadership in God's Household
preachers: ["Daniel Saunders"]
series:
- God's House
tags:
- Elders
- Character
- Godliness
- Leadership
images: ["/uploads/2018/04/Gods-House-768x644.jpg"]
---

1. Who are they?

2. Requirements for Overseers (vv1-7)

3. Requirements for Deacons (vv8-13)

4. Godly Servant-Leaders in Gympie

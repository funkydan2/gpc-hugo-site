---
title: God the Holy Spirit
passage: John 14:15-26; 16:4-15
date: 2020-11-08T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/WhatWeBelieve/20201108_GodTheHolySpirit_DS.mp3
audio_duration: 0:27:45
audio_size: 6893282
preachers:
  - Daniel Saunders
images:
  - /uploads/2020/10/WWB.jpg
series:
  - What We Believe
tags:
  - Judgment
  - Righteousness
  - Spirit
  - Trinity
---
Who is the Holy Spirit, what is he doing in believers and in the world? In this message, as we continue our series *What We Believe,* we listen to what Jesus taught about the Spirit of Truth.


# Outline
1. Another Advocate (John 14:16)
2. The Spirit and Believers (John 14:17-21, 26; 16:6-7, 12-15)
3. The Spirit and the World (John 16:8-11)
4. Believing in the Holy Spirit


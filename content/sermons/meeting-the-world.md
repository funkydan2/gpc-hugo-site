---
title: "Meeting the World"
passage: "Colossians 2:4-6; 1 Peter 3:8-18"
date: 2019-11-24T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/BackToBasics/20191124_MeetingTheWorld_DS.mp3"]
audio_duration: "26:51"
audio_size: 6672380
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/10/back-to-basics.jpg"]
series: ["Back To Basics"]
tags: ["Evangelism"]
---

We speak about things which are most important to us. If you're a Christian - are you prepared to speak about your hope in Jesus?

In this sermon, we're finishing up our [Back to Basics series](/series/back-to-basics) by reflecting on the amazing role God gives his people to tell others about Jesus.

# Outline

1. God’s mission and ours (Matthew 28:19; Colossians 1:20)
2. Salty Speech (Colossians 4:3-6)
3. Being prepared (1 Peter 3:14-16)
4. Deep Desire (Acts 2:47)

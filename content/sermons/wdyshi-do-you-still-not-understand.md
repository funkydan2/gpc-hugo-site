---
title: "Do You Still Not Understand?"
passage: "Luke 8:1-21"
date: 2021-03-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210328_DoYouStillNotUnderstand_DS.mp3"]
audio_duration: "23:44"
audio_size: 5925702
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
---
Even after seeing the amazing things Jesus did, his disciples still did not understand. But Jesus was patient with them, teaching and showing them who he is.

# Outline
1. Feeding (other) sheep (vv1-10)
2. They won’t see the signs (vv11-13)
3. Do you still not understand? (vv14-21)?

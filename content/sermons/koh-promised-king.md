---
title: "Promised King"
passage: "Matthew 1:1-17"
date: 2023-12-31T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/KingOfHeaven/20231231__PromisedKing_DS.mp3"]
audio_duration: "25:24"
audio_size: 6328743
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/12/KingOfHeaven.jpg"]
series: ["King of Heaven"]
tags: ["Faithfulness","Biblical Theology","Covenant","Promises"]
---
Is there a place for you in Jesus' family?

Matthew's biography of Jesus begins with a summary of the back story. It shows Jesus is the 'yes' to God's great promises to David and Abraham. He is God's king for the whole world...and for us.

# Outline
1. God keeps his promises (v1)
2. Will God really keep promises? (vv2-16)
3. Is there a place for me? (vv3, 5, 6)
4. Promises kept in King Jesus (v17)

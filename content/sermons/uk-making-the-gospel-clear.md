---
title: "Making the Gospel Clear"
passage: "Acts 18:1-19:10"
date: 2023-02-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230219_MakingTheGospelClear_DS.mp3"]
audio_duration: "29:36"
audio_size: 7334182
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Gospel","Spirit","Mission"]
---
Do you know the good news of Jesus *clearly*?

As God's kingdom continues to grow in Corinth and Ephesus, we're reminded of the importance of gospel partnership, persevering through opposition, and being clear on the gospel.

# Outline
1. Gospel Partnership in Corinth and Ephesus (18:1-23)
2. Gospel Clarity in Ephesus (18:24-19:10)
3. Partnership, Perseverance, Clarity

---
title: "Against God?"
passage: "Acts 6:8-7:60"
date: 2022-04-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220403_AgainstGod_DS.mp3"]
audio_duration: "27:44"
audio_size: 6885973
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Biblical Theology"]
---
What do we do with the Old Testament?

In this, our final message in this series, we hear Stephen's final words as he defends the accusation that followers of Jesus are against 'this Holy Place' and against 'God's law'. And doing so we see how Jesus is present and promised in the whole Bible.

# Outline
1. Falsely accused (6:8-15)
2. This Holy Place, the Law, and Jesus
    1. Abraham (7:1-8)
    2. Joseph (7:9-16)
    3. Moses (7:17-43)
    4. David/Solomon (7:44-50)
    5. Jesus (7:51-60)

This sermon was prerecorded.

{{<youtube Sg4awNHYHuQ>}}

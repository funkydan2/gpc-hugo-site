---
title: "Oppressive Leaders"
passage: "Micah 3"
date: 2021-05-16T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Micah/20210516_OppressiveLeaders_DS.mp3"]
audio_duration: "22:19"
audio_size: 5589433
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/04/Micah.jpg"]
series: ["Micah"]
tags: ["Leadership", "Church", "False Teaching"]
---
Too often we hear stories of corrupt, abusive, and oppressive leaders. People who use power in evil and destructive ways. Does God care?

In this message from Micah 3 we hear what God will do about oppressive leaders—and the good news of a different kind of leader.

# Outline
1. Devouring justice (vv1-4)
2. Disgrace prophets (vv5-8)
3. Justice from God (vv9-12)
4. Serving under-shepherd (1 Peter 5:1-4)

---
title: "Continue in Christ"
passage: "Colossians 2:6-15"
date: 2019-12-15T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20191215_ContinueInChrist_DS.mp3"]
audio_duration: "26:09"
audio_size: 6503408
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/09/ChristOverAll.jpg"]
series: ["Christ Over All"]
tags: ["Perseverance", "Forgiveness", "Victory"]
---

Even when it's tough, Jesus is worth receiving and continuing in because only in him can we know God, new life, and forgiveness.

# Outline

1. Continue in Christ (vv6-7)
2. Don’t be taken captive (v8)
3. Because in Christ
   1. …is the fullness of God (vv9-10)
   2. …is new life (vv11-13)
   3. …is victory (vv14-15)
4. Continue, and watch out!

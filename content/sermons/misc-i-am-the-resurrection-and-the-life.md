---
title: "I AM the Resurrection and the Life"
passage: "John 11:1-44"
date: 2023-05-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20230514_IAmTheResurrectionAndTheLife_MD.mp3"]
audio_duration: "26:27"
audio_size: 6579155
preachers: ["Malcolm Diefenbach"]
images: ["/uploads/2023/05/John11.jpg"]
series: ["Miscellaneous"]
tags: ["Resurrection"]
---
Our hope is in the living, risen Lord Jesus, who reconciles us with God, so we can share in his resurrection.

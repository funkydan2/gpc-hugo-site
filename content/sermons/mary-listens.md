+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20181225_MaryListens_DS.mp3"]
audio_duration = "15:23"
audio_size = 3920694
date = "2018-12-25T08:30:00+10:00"
images = ["/uploads/2018/12/Christmas2018.jpg"]
passage = "Luke 2:22-40"
preachers = ["Daniel Saunders"]
series = ["Christmas"]
tags = []
title = "Mary Listens"

+++
1. God's salvation is here

2. How will you respond?

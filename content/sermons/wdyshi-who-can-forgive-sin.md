---
title: "Who Can Forgive Sin?"
passage: "Mark 2:1-22"
date: 2021-01-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210117_WhoCanForgiveSin_DS.mp3"]
audio_duration: "26:54"
audio_size: 6684960
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Forgiveness", "Authority"]
---
The 'righteous' were appalled by the company Jesus kept. But his welcome of people who are normally left out is good news for us!

#Outline
1. Authority to forgive sins (vv1-12)
2. Audacity to call ‘sinners’ (vv13-17)
3. Announcing the new (vv18-22)

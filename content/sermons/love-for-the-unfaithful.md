---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Hosea/20160828_LoveForTheUnfaithful_DS.mp3"]
audio_duration: '25:05'
date: 2016-08-28 10:24:53+00:00
draft: false
passage: Hosea 1-3
title: Love for the Unfaithful
preachers: ["Daniel Saunders"]
series:
- Unbreakable Love
tags:
- Idolatry
- Love
images: ["/uploads/2015/12/Hosea.jpg"]
---

1. The pain of infidelity (1:1-9; 2:2-5)
2. The punishment of unfaithful Israel (2:9-13)
3. Loving the unfaithful (1:10-2:1; 2:14-3:5)
4. This is God's love for us (1 Peter 2:10)

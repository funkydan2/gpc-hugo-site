+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnholyPeopleHolyGod/20181021_ConsumingGlory_DS.mp3"]
audio_duration = "26:28"
audio_size = 6578232
date = "2018-10-21T15:55:40+10:00"
images = ["/uploads/2018/10/Unholy-People-Holy-God.jpg"]
passage = "Leviticus 8-10"
preachers = ["Daniel Saunders"]
series = ["Unholy People Holy God"]
tags = ["Sacrifice", "Priest"]
title = "Consuming Glory"

+++
1. An obedient priesthood (Leviticus 8-9)

2. Disobedient priests (Leviticus 10)

3. Leviticus and us...

    1. Our God is a consuming fire (Hebrews 12:28-29)

    2. Jesus is our priest (Hebrews 10:11-14)

---
title: Where Do I Find the Good Life?
passage: Luke 24:1-12
date: 2019-04-20 23:30:00 +0000
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20190421_WhereDoIFindTheGoodLife_DS.mp3"]
audio_duration: '23:18'
audio_size: 5818755
preachers: ["Daniel Saunders"]
images:
- "/uploads/2019/04/EasterSearching.png"
series:
- Easter
tags:
- Resurrection

---
1. What are you searching for?

2. Looking for the living?

3. Finding the good life in the perfect man

---
title: "John 5:16-30"
passage: "John 5:16-30"
date: 2021-10-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20211003_John5_LW.mp3"]
audio_duration: "32:06"
audio_size: 7932841
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
---
Sermon on John 5:16-30 by guest preacher Leo Woodward.

There's, unfortunately, some distortion in this recording

+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20181202_TheKingWeAskedFor_DS.mp3"]
audio_duration = "23:30"
audio_size = 5868371
date = "2018-12-02T10:09:42+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 9-11"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = ["Kingdom"]
title = "The King We Asked For"
+++
1. The story so far

2. Looking for a leader (1 Samuel 9)

3. ... chosen by God (1 Samuel 10)

4. ... to bring victory (1 Samuel 11)

5. Saul and Us

    1. What does this say about God?

    2. What does this say about how God works in Jesus?

    3. What does this say about us?

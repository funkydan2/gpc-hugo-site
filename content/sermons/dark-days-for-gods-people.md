---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Judges/20170910_DarkDaysForGodsPeople_DS.mp3"]
audio_duration: '28:59'
audio_size: 7183986
date: 2017-09-10 07:57:27+00:00
draft: false
passage: Judges 19-21
title: Dark Days for God's People
preachers: ["Daniel Saunders"]
series:
- Crying Out For A Saviour
images: ["/uploads/2017/06/Judges.jpg"]
---

1. Everyone did what was right in their own eyes
    1. Wrongdoing (Judges 19)
    2. Revenge (Judges 20)
    3. Regret (Judges 21)
2. In those days there was no king in Israel

---
title: "Wisdom and Wedlock"
passage: "Proverbs"
date: 2022-07-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheVoiceOfWisdom/20220703_WisdomAndWedlock_DS.mp3"]
audio_duration: "30:16"
audio_size: 7493082
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Proverbs.png"]
series: ["The Voice of Wisdom"]
tags: ["Wisdom","Marriage"]
---
What does the voice of wisdom say for marriage or singleness?

# Outline
1. Danger (Proverbs 5:1-14; 1 Corinthians 6:15-20)
2. Delight (Proverbs 5:15-23, Proverbs 21:19)
3. but I’m talking about… (Ephesians 5:31-33)

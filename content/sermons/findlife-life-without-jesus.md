---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180318_LifeWithoutJesus_DS.mp3"]
audio_duration: '25:19'
audio_size: 6302629
date: 2018-03-18 05:50:51+00:00
draft: false
passage: John 14
title: Life without Jesus
preachers: ["Daniel Saunders"]
series:
- Find Life
tags:
- Anxiety
- Cross
- Eternity
- Spirit
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. Jesus is going...

    1. to prepare a place (vv1-7)

    2. so greater works will be done (vv8-14)

    3. to send the Spirit (vv15-26)

    4. so his disciples will have peace (vv27-31)

*Life 'without' Jesus*

---
title: "Behold Our King"
passage: "Luke 24"
date: 2020-03-29T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200329_BeholdOurKing_DS.mp3"]
audio_duration: "27:21"
audio_size: 6795837
preachers: ["Daniel Saunders"]
images: ["uploads/2020/02/TheWayHome5.jpg"]
series: ["The Way Home (Part 5)"]
tags: ["Resurrection", "Hope", "Ascension"]
---

In uncertain times, the resurrection of Jesus gives his followers joy and hope and propels them into a changed life on mission for him.

N.b. this sermon was recorded during an 'online service' due to restrictions in place [in response to COVID-19]({{< relref 2020-03-covid-19.md >}}).

# Outline

1. The shock of an empty tomb (vv1-12)
2. The joy of resurrection
   1. …on the road (vv13-32)
   2. …in Jerusalem (vv33-49)
   3. …at the temple (vv50-53)
3. In shock and joy—on mission for our king!


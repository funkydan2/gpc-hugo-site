---
title: "Worse than you thought!"
passage: "Mark 7:1-23"
date: 2021-03-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210314_WorseThanYouThought_DS.mp3"]
audio_duration: "24:27"
audio_size: 6099554
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Sin", "Hypocrisy"]
---
What if the problem is worse than you thought?

In this message, we hear Jesus tell us the news that our situation before God is worse than we thought. But the good news is Jesus' came to bring a solution that's better than we could ever imagine.

# Outline
1. Breaking their rules (vv1-5)
2. You hypocrites! (vv6-13)
3. The problem is worse than you think (vv14-23)
4. Jesus' solution is better than we could imagine (1 Corinthians 6:9-11)

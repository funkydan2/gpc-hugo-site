---
title: Gospel Peace
passage: Philippians 4:2-9
date: 2024-12-08T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/GospelPartners/20241208_GospelPeace_DS.mp3
audio_duration: 25:32
audio_size: 6358802
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/10/GospelPartners.jpeg
series:
    - Gospel Partners
tags:
    - Conflict
    - Relationships
    - Reconciliation
---
How should we deal when we conflict or tension with others? In this message from Philippians, we hear how the gospel gives us the resources we need to deal with conflict and know God's peace.

# Outline
1. Be united (vv2-3)
2. Guard your heart and mind (vv4-9)

---
title: "Fulfilled"
passage: "Matthew 5:17-20"
date: 2024-02-18T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StaySalty/20240218_Fulfilled_DS.mp3"]
audio_duration: "25:10"
audio_size: 6272679
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/02/StaySalty.jpg"]
series: ["Stay Salty"]
tags: ["Old Testament","Law","Biblical Theology"]
---
Do Christians just 'pick and choose' the rules in the Bible they want to follow?

In this message, we hear Jesus talk about how his coming fulfils the Law and the Prophets—and how this shapes how we read the whole Bible.

# Outline
1. The things not said. (v17)
2. Not abolish but fulfil (vv17-18)
3. Greater Righteousness (vv19-20)
4. The law, the prophets, and us

---
title: "Crowning the King"
passage: "Matthew 20 & 27"
date: 2023-04-07T08:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20230407_CrowningTheKing_DS.mp3"]
audio_duration: "20:37"
audio_size: 5177514
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/03/GoodFriday.jpg"]
series: ["Easter"]
tags: ["Cross","Good Friday","King","Humility"]
---
In this Good Friday message, we hear from Matthew's gospel what it means for Jesus to be a king who serves by giving his life as a ransom for many.

# Outline
1. The king is crowned
2. The king’s kingdom
3. The king’s ransom

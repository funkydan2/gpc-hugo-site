---
title: Why? Church
passage: Matthew 16:13-20; Ephesians 2-4
date: 2025-01-12T09:30:00+10:00
audio:
    - "https://f001.backblazeb2.com/file/GympieSermonAudio/WhyChurch/20250112_WhyChurch_DS.mp3"
audio_duration: 26:46
audio_size: 6654557
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/01/whychurch.png
series:
    - Why? Church
tags:
    - Church
    - Glory
---
Why has the risen and ascended Lord Jesus made 'the church'?

# Outline
1. The church Jesus builds (Matthew 16:16-18; 28:18-20)
2. Called to be… (Ephesians 2:11-22; 3:8; 4:1-6)
3. So that… (Ephesians 3:7-12; 4:11-16)

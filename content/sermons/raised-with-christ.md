---
title: "Raised With Christ"
passage: "Ephesians 2:1-10"
date: 2020-06-07T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200607_RaisedWithChrist_DS.mp3"]
audio_duration: "23:42"
audio_size: 5917049
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/05/Ephesians_OTEYH.jpg"]
series: ["Open The Eyes Of Your Heart"]
tags: ["Faith", "Grace", "Salvation", "Sin", "Death"]
---

Who are you? What's your *story*?

In Ephesians 2, we hear the good news of God's gift of salvation—that those who receive Christ receive his story of death to life.

How will you respond to God's gift?

# Outline

1. Previously: Dead and Mislead (vv1-3)
2. But God (v4)

    1. made alive together (v5)
    2. raised and seated together in the heavenly realms (vv6-7)
    3. because of his grace (vv8-9)

3. Which changes us (v10)

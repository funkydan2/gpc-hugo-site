---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnitedWithChrist/20170528_Sanctification_DB.mp3"]
audio_duration: '27:47'
audio_size: 6899286
date: 2017-05-28 09:08:55+00:00
draft: false
passage: Romans 5:12-21
title: The Eternal Covenant and Sanctification
preachers: ["Daryl Brenton"]
series:
- United with Christ
tags:
- Covenant
- Law
- Union With Christ
images: ["/uploads/2017/04/United-with-Christ_web.jpg"]
---

1. The Eternal Covenant

    Hebrews 13:20-21

    Hebrews 8:6-13

    John 6:37

2. What is a Covenant?

3. Covenant Representatives

    Romans 5:12-21

4. What is Sanctification?

    1. The cure for lawlessness (2 Corinthians 5:17)

    2. Sanctification (Philippians 2:12-13; Romans 13:8-10; Romans 5:5)

5. Summary

>Holiness or Sanctification makes what God did in Justification real in the heart of the believer.

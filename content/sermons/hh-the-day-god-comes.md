---
title: "The Day God Comes"
passage: "Malachi 3:16-4:6"
date: 2023-12-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Malachi/20231217_TheDayGodComes_DS.mp3"]
audio_duration: "25:00"
audio_size: 6229302
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/11/HalfHearted.png"]
series: ["Half Hearted"]
tags: ["Salvation","Joy","Judgment","Eschatology"]
---
Serious risks deserve serious warnings—which is what we hear as God's message through Malachi ends. God warns of judgment and promises joy.

# Outline
1. A Day of Distinction (3:16-18)
2. A Day of Fire and Joy (4:3)
3. Be ready for that Day (4:4-6)
4. The Day is Now 

    Matthew 11:7-15

    Matthew 17:10-13
    
    2 Peter 3:8-9

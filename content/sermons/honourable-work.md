+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180826_HonourableWork_DS.mp3"]
audio_duration = "23:17"
audio_size = 5814500
date = "2018-08-26T16:19:15+10:00"
images = ["/uploads/2018/04/Gods-House-768x644.jpg"]
passage = "1 Timothy 6:1-2"
preachers = ["Daniel Saunders"]
series = ["God's House"]
tags = ["Work"]
title = "Honourable Work"

+++
1. Slaves ...

    1 Corinthians 7:21; Colossians 4:1; Galatians 3:28

2. Work for God's honour (v1)

3. Work because of gospel love (v2)

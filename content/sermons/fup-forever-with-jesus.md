---
title: "Forever with Jesus"
passage: "1 Thessalonians 4:13-5:11"
date: 2023-10-29T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20231029_ForeverWithJesus_DS.mp3"]
audio_duration: "23:42"
audio_size: 5919543
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Resurrection","Death","Eschatology"]
---
In the face of death is there any hope, any comfort, any encouragement? In this message from 1 Thessalonians, we hear the encouraging, sure, hope that Jesus gives—being physically, eternally, present with him in resurrection

# Outline
1. Grieve with hope (4:13)
    1. because believers will be resurrected (4:14)
    2. because believers will be forever with Jesus (4:15-18)
2. But don’t guess when (5:1-3)
3. Instead, live in the light of that day! (5:4-11)

---
title: How? Church
passage: Hebrews 10:19-25; Romans 12
date: 2025-01-19T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/WhyChurch/20250119_HowChurch_DS.mp3
audio_duration: 25:09
audio_size: 6266153
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/01/whychurch.png
series:
    - Why? Church
tags:
    - Biblical Theology
    - Church
    - Love
    - Elders
---
How does 'church' bring glory to God?

# Outline
1. By gathering (Deuteronomy 4:10; Hebrews 12:22-24)
2. to encourage (Hebrews 10:19-25)
3. one another (Romans 12)
4. (A word about elders)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160221_DoYouNeedJesus_DS.mp3"]
audio_duration: '22:42'
date: 2016-02-21 01:21:16+00:00
draft: false
passage: Luke 5:1-32
title: Do You Need Jesus?
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---
1. Sinners (vv1-11)
2. The Unclean (vv12-16)
3. The Desperate (vv17-26)
4. The Sin-sick (vv27-32)

Do you need Jesus?

---
title: "Walking by the Spirit"
passage: "Galatians 5:16-26"
date: 2022-12-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221204_WalkingByTheSpirit_DS.mp3"]
audio_duration: "24:34"
audio_size: 6126849
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Spirit","Sin","Change"]
---
How do people change?

# Outline
1. Led to change (vv16-18)
2. Don’t walk by the Flesh (vv19-21)
3. Walk by the Spirit (vv22-26)

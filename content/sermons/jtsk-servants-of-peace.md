---
title: "Servants of Peace"
passage: "Mark 9:33-50"
date: 2021-09-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20210919_ServantsOfPeace_DS.mp3"]
audio_duration: "28:10"
audio_size: 6993541
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Peace", "Hell"]
---
The source of many of our conflicts is wanting to be 'first', to win, to be 'great'. In this message, we hear Jesus' teaching about true greatness and how to be at peace.

# Outline
1. Who’s greater? (vv33-37)
2. Whose side? (vv38-41)
3. Don’t stumble (vv42-49)
4. Be at peace (v50)

## Reflection questions
1. What relationships, or what are the contexts where you want to be first rather than last? How could you be a servant of all in that relationship this week?
2. Who do you find hard to welcome? Who do you welcome, because they can benefit you? Who do you need to welcome that you’ve excluded?
3. What do you need to cut off so you won’t stumble?
4. Do you have any relationships that have lost their salt? How could you bring peace?

(Apologies for the recording quality - there's a fair bit of distortion.)

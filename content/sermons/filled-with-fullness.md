---
title: "Filled With Fullness"
passage: "Ephesians 3:14-21"
date: 2020-06-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200628_FilledWithFullness_DS.mp3"]
audio_duration: "23:04"
audio_size: 5767695
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/05/Ephesians_OTEYH.jpg"]
series: ["Open The Eyes Of Your Heart"]
tags: ["Prayer", "Trinity"]
---

Believers in Christ have been given freedom and confidence before God. He invites us to ask Him for more than we can image. So what should we pray, especially when we're discouraged?

# Outline

1. Confidently coming to the Father (v14-51)
2. To ask…
   1. for the Spirit’s strength (v16)
   2. for Christ’s presence (v17)
   3. for sure footing (v17)
3. Filled by knowing Love (vv18-19)
4. To God be Glory (vv20-21)


---
title: "When the King Comes"
passage: "Zechariah 14"
date: 2023-07-30T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230730_WhentheKingComes_DS.mp3"]
audio_duration: "27:08"
audio_size: 6742784
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Eschatology","Forgiveness","Judgment","Victory"]
---
Will history always keep repeating? In this message from Zechariah 14 we hear the great, but stern, promise of the end of history—which is all about Jesus.

# Outline
1. From War to Worship, Hostility to Holiness
    1. War (vv1-2)
    2. Rescue (vv3-5)
    3. New Creation (vv6-11)
    4. Judgment (vv12-15)
    5. Worship (v16)
    6. and Judgment (vv17-19)
    7. Holiness (vv20-21)
2. The Day God Stood on the Mountain
    John 7:37-39; 8:12
    Mark 11:22-25
    Revelation 22:1-5

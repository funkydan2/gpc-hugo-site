---
title: "Behold the King"
passage: "Luke 23:26-56"
date: 2020-03-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200322_BeholdTheKing_DS.mp3"]
audio_duration: "21:16"
audio_size: 5333107
preachers: ["Daniel Saunders"]
images: ["uploads/2020/02/TheWayHome5.jpg"]
series: ["The Way Home (Part 5)"]
tags: ["Cross"]
---

As Jesus is crucified, the sign of the charge against him read 'This is the King of the Jews'. In this message, as Jesus is crucified, we'll consider how we respond to the momentous event of the cross.

N.b. this sermon was recorded during an 'online service' due to restrictions in place [in response to COVID-19]({{< relref 2020-03-covid-19.md >}}).

# Outline

1. Responses to the cross:
   1. Carrying (v26)
   2. Mourning (vv27-34)
   3. Mocking (vv35-38)
   4. Seeing (vv39-47)
   5. Watching (vv48-49)
   6. Honouring (vv50-56)
2. How do you respond to the crucified king?

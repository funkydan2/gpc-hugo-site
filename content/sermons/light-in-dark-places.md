---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke1-4/20161218_LightInDarkPlaces_DS.mp3"]
audio_duration: '23:29'
date: 2016-12-18 06:14:59+00:00
draft: false
passage: Luke 1:57-80
title: Light in Dark Places
preachers: ["Daniel Saunders"]
series:
- Great Expectations
tags:
- Christmas
- David
- Incarnation
images: ["/uploads/2015/12/Great-Expectations.jpg"]
---

1. John's birth (1:57-66)

2. ...makes Zacharias prophesy

    1. ...about God's salvation (1:68-74)

        Psalm 132

    2. ...and God's prophet (1:76-77)

        Isaiah 40:3 (Malachi 3)

    3. ...and the coming of Light! (1:78-79)

        Isaiah 9:1-2

3. The Light in our darkness

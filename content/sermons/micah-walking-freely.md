---
title: "Walking Freely"
passage: "Micah 6:1-8"
date: 2021-06-06T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Micah/20210606_WalkingFreely_MH.mp3"]
audio_duration: "25:26"
audio_size: 6336588
preachers: ["Mitchell Harte"]
images: ["/uploads/2021/04/Micah.jpg"]
series: ["Micah"]
tags: ["Justice", "Mercy", "Humility", "Religion"]
---
What does God require?

Some feel that God burdens people with oppressive obligations. In this message from Micah, we hear that knowing God frees us to live justly, mercifully, and humbly, *with God*.

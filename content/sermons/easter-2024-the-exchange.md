---
title: "The Exchange"
passage: "1 Peter 2:22-25; 3:18"
date: 2024-03-29T08:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20240329_TheExchange_DS.mp3"]
audio_duration: "15:47"
audio_size: 4019472
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/03/GoodFriday.jpg"]
series: ["Easter"]
tags: ["Good Friday","Atonement"]
---
Our Good Friday message.

# Outline
1. We are far from God
2. The exchange

---
title: Hope is a Dangerous Thing
passage: Psalm 33
date: 2024-10-27T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20241027_HopeIsADangerousThing_SW.mp3
audio_duration: 26:32
audio_size: 6598045
preachers:
    - Shane Wright
images:
    - /uploads/2024/10/Psalm33.jpg
series:
    - Miscellaneous
tags:
    - Hope
    - Power
---
So often our hopes become disappointments. But what about hope in God?

# Outline
1. Let us rejoice
    1. In God’s character.
    2. In God’s power.
    3. In God’s providence. 
    4. In God’s care.
2. Hope is found.
3. Hope is a dangerous thing.

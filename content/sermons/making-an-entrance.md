---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170205_MakingAnEntrance_DS.mp3"]
audio_duration: '20:55'
date: 2017-02-05 06:41:43+00:00
draft: false
passage: Luke 19:28-48
title: Making an Entrance
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Christ
- King
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---

1. The entrance of the king (19:28-38)
2. But not everyone's welcoming...
    1. The Pharisees (vv39-40)
    2. The Capital (vv41-44)
    3. The Priests (vv45-48)
3. Will you welcome the king?
4. Will you welcome this king?

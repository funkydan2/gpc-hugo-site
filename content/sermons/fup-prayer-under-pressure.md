---
title: "Prayer Under Pressure"
passage: "2 Thessalonians 1"
date: 2023-08-06T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20230806_PrayerUnderPressure_DS.mp3"]
audio_duration: "25:45"
audio_size: 6412871
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Persecution","Judgment","Prayer"]
---
What do you say to someone suffering for following Jesus? What do you pray for them?

In this message from 2 Thessalonians 1, we hear how Paul, through his prayers, encourages a church facing persecution.

# Outline
1. A Church Born in a Hurry (2 Thess 1:1-2; Acts 17)
2. What do you pray for (part 1)? (vv3-4)
3. Is God’s judgment right? (vv5-10)
4. What do you pray for (part 2)? (vv11-12)

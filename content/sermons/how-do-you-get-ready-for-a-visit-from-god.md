---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160131_HowDoYouGetReadyForAVisitFromGod_DS.mp3"]
audio_duration: '21:53'
date: 2016-01-31 02:16:21+00:00
draft: false
passage: Luke 3:1-13
title: How Do You Get Ready For A Visit ... From God?
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---
John's Message:

1. Repent! (vv7-14)

2. The saviour and judge is coming (vv15-18)

God has come...are you ready?

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20180401_TragedyOrComedy_DS.mp3"]
audio_duration: '18:49'
audio_size: 4744727
date: 2018-04-01 04:26:24+00:00
draft: false
passage: John 20
title: Tragedy or Comedy
preachers: ["Daniel Saunders"]
series:
- Easter
tags:
- Easter
- Forgiveness
- Resurrection
- Spirit
images: ["/uploads/2016/03/Easter.jpg"]
---

1. A tragic morning that turns to joy

2. So we can find life

    Romans 8:11

3. Is your life a tragedy or a comedy

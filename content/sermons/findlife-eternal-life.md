---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180325_EternalLife_DS.mp3"]
audio_duration: '25:16'
audio_size: 6292187
date: 2018-03-25 07:55:20+00:00
draft: false
passage: John 17
title: Eternal Life
preachers: ["Daniel Saunders"]
series:
- Find Life
tags:
- Church
- Eternal Life
- Gospel
- Love
- Prayer
- Union With Christ
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. Knowing God (vv1-12, 24-26)

2. Sanctified from the world. Sent into the world. (vv13-19)

3. United in Christ (vv20-23)

4. Do you have eternal life?

---
title: "Who Can You Trust?"
passage: "2 Peter 1:16-21"
date: 2023-01-08T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/2Peter/20230108_WhoCanYouTrust_DS.mp3"]
audio_duration: "25:09"
audio_size: 6268317
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/2Peter.jpg"]
series: ["Grow in Grace"]
tags: ["Scripture"]
---
Can we trust the Bible?

# Outline
1. Trust the eyewitnesses (vv16-18)
2. Trust God’s Word (vv19-21)

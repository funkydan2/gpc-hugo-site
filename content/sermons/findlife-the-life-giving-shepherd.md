---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180304_TheLifeGivingShepherd_DS.mp3"]
audio_duration: '22:32'
date: 2018-03-04 06:08:40+00:00
draft: false
audio_size: 5637307
passage: John 10:1-21, 25-30
title: The Life-Giving Shepherd
preachers: ["Daniel Saunders"]
series:
- Find Life
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. A story about sheep (vv1-6)

2. Jesus is the ...

    1. Gate (vv7-10)

    2. Good shepherd (vv11-13)

    3. ...who knows his sheep (vv14-21)

    4. ...and won't let them go (vv25-30)

3. Have you heard his voice?

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Judges/20170723_AScandalousAndOffensiveSaviour_SB.mp3"]
audio_duration: '21:29'
audio_size: 5387242
date: 2017-07-23 11:08:12+00:00
draft: false
passage: Judges 3:7-4:3
title: A Scandalous and Offensive Saviour!
preachers: ["Steve Blencowe"]
series:
- Crying Out For A Saviour
images: ["/uploads/2017/06/Judges.jpg"]
---

1. Time after time...
2. Ehud - an 'unlikely' saviour (3:12-30)
3. Jesus - an 'unlikely' saviour (1 Corinthians 1:18-2:5)
4. Judges and us...
    1. What does it show about people?
    2. What does it show about God?
    3. What does it show about how God works in Jesus?

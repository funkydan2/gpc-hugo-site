---
title: "God’s Deep Compassion"
passage: "Jonah 3-4"
date: 2020-12-27T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Jonah/20201227_GodsDeepCompassion_DS.mp3"]
audio_duration: "0:26:56"
audio_size: 6694574
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/12/Jonah.jpg"]
series: ["God's Abounding Grace"]
tags: ["Mercy", "Justice"]
---
Although we love to receive mercy, we find it hard to give it to others. Our motto can be 'mercy for me, justice for you'. In this message, we hear the conclusion to Jonah's story and see the compassionate heart of Christ.

# Outline
1. Nineveh Repents(Jonah 3:1-9)
2. God Relents (Jonah 3:10)
3. Jonah Resents (Jonah 4)
4. One Greater than Jonah (Luke 19:41; 5:30-32)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160124_KnowForCertain_DS.mp3"]
audio_duration: '21:03'
date: 2016-01-24 09:39:31+00:00
draft: false
passage: Luke 1:1-4
title: Know for Certain
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---
1. The historical truth of Jesus
2. The world-changing truth of Jesus
3. For God-Lovers...to Know for Certain

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnitedWithChrist/20170507_Justification_DS.mp3"]
audio_duration: '26:41'
audio_size: 6635360
date: 2017-05-07 05:44:04+00:00
draft: false
passage: Philippians 3:7-11
title: Justification
preachers: ["Daniel Saunders"]
series:
- United with Christ
tags:
- Justification
- Righteousness
- Union With Christ
images: ["/uploads/2017/04/United-with-Christ_web.jpg"]
---

1. What is Justification? (Romans 3:23-24)
2. What is Union with Christ? (Ephesians 1:3-14)
3. How are we Justified in Christ? (

    2 Corinthians 5:21

    Philippians 3:7-11

4. Justification, Union, and You

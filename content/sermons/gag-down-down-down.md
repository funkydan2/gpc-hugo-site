---
title: "Down, Down, Down"
passage: "Jonah 1:1-16"
date: 2020-12-06T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Jonah/20201206_DownDownDown_DS.mp3"]
audio_duration: "23:58"
audio_size: 5981212
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/12/Jonah.jpg"]
series: ["God's Abounding Grace"]
tags: ["Sin", "Evangelism", "Mission"]
---
Are you running from God?  In this message, we hear about a bloke who tried running from God and the unexpected way God's grace showed up.

# Outline
1. Setting the scene (vv1-2)
2. Man on the run (v3)
3. Stormy weather (vv4-15)
4. Fearing the Lord (v16)
5. Greater than Jonah (Matthew 12:41; Mark 4:35-41)


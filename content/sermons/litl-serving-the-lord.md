---
title: "Serving the Lord"
passage: "Ephesians 6:5-9"
date: 2020-09-20T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200920_ServingTheLord_DS.mp3"]
audio_duration: "28:43"
audio_size: 7123205
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Work"]
---
Serving Jesus impacts all our relationships and actions, including how we 'work'.  In this message, we hear what the Bible says about serving the Lord.

# Outline

1. Slavery, the World, and the Bible
2. Slaves: serve the Lord (vv5-8)
3. Masters: remember the Lord (v9)
4. Working and ‘mastering’ as to the Lord

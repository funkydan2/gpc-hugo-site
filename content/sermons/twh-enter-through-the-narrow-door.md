+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190324_EnterThroughTheNarrowDoor_DS.mp3"]
audio_duration = "27:45"
audio_size = 6887053
date = "2019-03-24T09:30:00+10:00"
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 13"
preachers = ["Daniel Saunders"]
series = ["The Way Home (Part 3)"]
tags = []
title = "Enter Through the Narrow Door"
+++
1. Foxes in the coop (vv33-35, 1-5)

2. Judgement is coming (vv6-9)

3. ...because of the predators (vv10-17)

4. The kingdom of judgement and safety (vv18-35)

5. So repent, or you too will perish!

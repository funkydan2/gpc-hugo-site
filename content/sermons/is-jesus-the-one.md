---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160320_IsJesusTheOne_DS.mp3"]
date: 2016-03-20 02:11:39+00:00
draft: false
passage: Luke 7:18-50
title: Is Jesus 'The One'?
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
tags:
- Doubt
- Faith
- Religion
- Sin
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---

1. The Complaint: not playing by our rules (vv31-35)
2. John's Doubt (vv18-23)
3. The Crowd's Doubt (vv24-30)
4. The Pharisee's Rejection? (vv36-50)

Is Jesus 'The One'?

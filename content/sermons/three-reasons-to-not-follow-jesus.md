---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160228_ThreeReasonsToNotFollowJesus_DS.mp3"]
audio_duration: '26:21'
date: 2016-02-28 01:20:25+00:00
draft: false
passage: Luke 5:33-6:11
title: Three Reasons to NOT Follow Jesus
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---
1. He doesn't fast (5:33-35)

2. He doesn't keep the Sabbath

    1. ... by 'working' (6:1-5)

    2. ...by healing (6:6-11)

3. The heart of the issue (5:36-39)

4. Some things never change!

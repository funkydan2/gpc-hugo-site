---
title: "Accepted by God"
passage: "Acts 9:32-11:18"
date: 2022-08-07T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220807_AcceptedByGod_DS.mp3"]
audio_duration: "29:31"
audio_size: 7314684
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Hospitality","Cleanliness","Holy","Holiness","Salvation","Mission","Evangelism"]
---
Are we willing to accept, to show hospitality, to those who are different? In this message from Acts 10, we see that in Christ, God makes the unclean to be holy, which challenges the boundaries of our acceptance.

# Outline
1. What God makes clean (10:1-23)
2. What God makes holy (10:24-48)
3. No Favouritism

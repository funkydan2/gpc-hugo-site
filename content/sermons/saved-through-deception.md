---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joshua/20161106_SavedThroughDeception_DS.mp3"]
audio_duration: '23:09'
date: 2016-11-06 03:30:17+00:00
draft: false
passage: Joshua 9
title: Saved Through Deception
preachers: ["Daniel Saunders"]
series:
- Fear and Serve the Lord
tags:
- Grace
- Guidance
- Mercy
images: ["/uploads/2015/12/Joshua.jpg"]
---

1. The Gibeonite Deception

    * A Crafty Plan (vv1-15)

    * ...that's caugty out (vv16-18)

    * ...but it works! (vv19-27)

2. The Gibeonites and Us

    * The Israelite's Failure (v14)

    James 1:5

    * We don't need to deceive God!

    Romans 5:8

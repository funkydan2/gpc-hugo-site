---
title: "How to live a 'things above' life"
passage: "Colossians 3:12-17"
date: 2021-09-26T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20210926_HowToLiveAThingsAboveLife_LW.mp3"]
audio_duration: "35:43"
audio_size: 8800748
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
---
Sermon on Colossians 3:12-17 by guest preacher Leo Woodward.

There's, unfortunately, some distortion in this recording

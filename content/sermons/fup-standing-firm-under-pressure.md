---
title: "Standing Firm Under Pressure"
passage: "2 Thessalonians 2"
date: 2023-08-13T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20230813_StandingFirmUnderPressure_DS.mp3"]
audio_duration: "23:59"
audio_size: 5985867
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Eschatology","Perseverance"]
---
The rebellion. The Man of Lawlessness. Is the end near?

In this message from 2 Thessalonians 2, we hear how the Bible's teaching about the Day of the Lord is given to comfort and strengthen those who belong to Jesus.

# Outline
1. Don’t be shaken (vv1-2)
2. The End of Lawlessness (vv2-12)
3. So stand firm (vv13-16)

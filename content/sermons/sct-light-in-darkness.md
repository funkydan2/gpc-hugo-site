---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20171217_LightInDarkness_DS.mp3"]
audio_duration: '27:28'
audio_size: 6822233
date: 2017-12-17 05:47:09+00:00
passage: Isaiah 9:1-7
title: Light In Darkness
preachers: ["Daniel Saunders"]
series:
- Strange Christmas Things
tags:
- Advent
- Christmas
images: ["/uploads/2017/11/Strange-Christmas-Things.jpg"]
---

1. Darkness in Israel and Judah (Isaiah 7-8)
2. Light in Darkness (Isaiah 9:1-5)
3. Light of the Messiah (Isaiah 9:6-7)
4. A Son is Born (Matthew 4:12-14)

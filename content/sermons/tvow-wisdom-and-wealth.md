---
title: "Wisdom and Wealth"
passage: "Proverbs"
date: 2022-07-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheVoiceOfWisdom/20220710_WisdomAndWealth_DS.mp3"]
audio_duration: "29:01"
audio_size: 7196891
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Proverbs.png"]
series: ["The Voice of Wisdom"]
tags: ["Wisdom","Money","Generosity"]
---
What's the wise thing to do with your money? Should you save or spend? Invest to leave an inheritance?

In this, the final sermon in our Proverbs series, we consider wisdom and wealth.

# Outline
1. The Blessing of Wealth (Proverbs 3:9-18)
2. The wisdom of financial caution (Proverbs 22:7, 26-27)
3. The wisdom of generosity (Proverbs 22:9, 16, 22-23, 28)

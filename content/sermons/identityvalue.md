---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/MoneyMatters/20160807_IdentityValue_DS.mp3"]
audio_duration: '24:05'
date: 2016-08-07 04:25:56+00:00
draft: false
passage: 1 Timothy 6:17-21
title: Identity/Value
preachers: ["Daniel Saunders"]
series:
- Money Matters
tags:
- Gospel
- Identity
- Money
images: ["/uploads/2015/12/Money-Matters.jpg"]
---
1. You are what you earn

2. The Gospel and Identity

    1. ...a gift from our Creator

    2. ...a gift from our Saviour

        1 Corinthians 1:26-29

        Romans 6:1-4

Who are you?

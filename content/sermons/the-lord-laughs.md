---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20180330_TheLordLaughs_DS.mp3"]
audio_duration: '19:53'
audio_size: 4999259
date: 2018-03-30 02:57:54+00:00
draft: false
passage: John 18-19
title: The Lord Laughs
preachers: ["Daniel Saunders"]
series:
- Easter
images: ["/uploads/2016/03/GoodFriday.jpg"]
---

1. Bumbling soldiers and leaders ... reveal who is in control

2. People who say more than they realise

    1. Jesus is king

    2. We have no king

    3. It's better than one dies

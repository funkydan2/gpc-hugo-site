---
title: "Why Did Jesus Rise?"
passage: "Isaiah 52:13-14; 53:10-12"
date: 2021-04-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20210404_WhyDidJesusRise_DS.mp3"]
audio_duration: "20:49"
audio_size: 5226466
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/03/Easter2021.jpg"]
series: ["Easter"]
tags: ["Resurrection", "Justification", "Victory", "Glory"]
---
Why did Jesus rise to life again? What does it reveal about him, and what difference does it make?

# Outline
1. From suffering to glory
2. For his offspring
3. For justification
4. For victory

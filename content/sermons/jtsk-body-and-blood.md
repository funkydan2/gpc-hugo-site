---
title: "Body and Blood"
passage: "Mark 14:1-25"
date: 2021-11-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211128_BodyAndBlood_DS.mp3"]
audio_duration: "27:59"
audio_size: 6948103
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Covenant", "Lord's Supper"]
---
Christians throughout history and around the world celebrate the Lord's Supper. Where does this come from?

In this message, we go back to the days leading up to Jesus' crucifixion and see two meals that explain the cross and resurrection.

# Outline
1. Preparing the Body (vv1-11)
2. Body and Blood (vv12-25)
3. Receiving Christ

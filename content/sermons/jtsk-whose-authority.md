---
title: "Whose Authority?"
passage: "Mark 11:27-12:27"
date: 2021-11-07T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211107_WhoseAuthority_DS.mp3"]
audio_duration: "30:47"
audio_size: 7620974
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Authority", "Government", "Resurrection"]
---
'Who do you think you are?'

'Where do you get your authority from?'

As we continue to follow Jesus during his final week in Jerusalem, we see his authority as he debates and teaches the so-called religious leaders in the temple.

# Outline
1. Test 1: Authority (11:27-33)
2. The rejected cornerstone (12:1-11, Isaiah 5:1-2, 7)
3. Test 2: Politics (12:12-17)
4. Test 3: Resurrection (12:18-27)
5. Don’t be deceived!

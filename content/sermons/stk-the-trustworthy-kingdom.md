---
title: "The Trustworthy Kingdom"
passage: "Matthew 9:18-34"
date: 2024-08-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240811_TheTrustworthyKingdom_DS.mp3"]
audio_duration: "22:28"
audio_size: 5624385
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/07/SeeingTheKingdom.jpg"]
series: ["Seeing the Kingdom"]
tags: ["Faith"]
---
Who can you trust? In this message we hear that Jesus is trustworthy.

# Outline
1. Trusting Jesus
2. Rejecting Jesus
3. Will you trust the king?

During this sermon, an earlier message on Mark 5 is mentioned. You can listen to that sermon [here](https://gympiepresbyterian.org.au/sermons/misc-grace-abounding/).

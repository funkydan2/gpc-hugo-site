---
title: "Beloved King"
passage: "Matthew 3"
date: 2024-01-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/KingOfHeaven/20240121_BelovedKing_DS.mp3"]
audio_duration: "24:07"
audio_size: 6019531
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/12/KingOfHeaven.jpg"]
series: ["King of Heaven"]
tags: ["Repentance","Trinity"]
---
Listen as we hear the good (and serious) news of the coming king, Jesus Spirit-empowered beloved Son of the Father.


# Outline
1. Listen to the preparing voice (vv1-12)
2. Listen to the declaring voice (vv13-17)

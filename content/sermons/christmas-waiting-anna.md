---
title: "Waiting for Christmas (Anna)"
passage: "Luke 2:21-24, 36-38"
date: 2021-12-25T08:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20211225_WaitingForChristmas_DS.mp3"]
audio_duration: "14:42"
audio_size: 3757980
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/12/2021ChristmasTheme.jpg"]
series: ["Christmas 2021"]
tags: ["Christmas","Redemption"]
---
Christmas means the wait is over, and we're still waiting.

Our Christmas day message for 2021.

+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190224_TheKingdomHasCome_DS.mp3"]
audio_duration = "22:36"
audio_size = 5651252
date = "2019-02-24T09:30:00+10:00"
draft = false
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 11:14-36"
preachers = ["Daniel Saunders"]
series = ["The Way Home (Part 3)"]
tags = []
title = "The Kingdom Has Come"

+++
1. Israel's Unhealthy Eyes (vv14-26)

2. Those with eyes to see (vv27-36)

3. What fills your eyes?

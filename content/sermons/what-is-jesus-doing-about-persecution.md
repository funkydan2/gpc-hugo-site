---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/2Thessalonians/20170604_WhatIsJesusDoingAboutPersecution_DS.mp3"]
audio_duration: '25:29'
audio_size: 6345370
date: 2017-06-04 03:23:27+00:00
draft: false
passage: 2 Thessalonians 1
title: What is Jesus Doing About Persecution?
preachers: ["Daniel Saunders"]
series:
- Hold Firm
tags:
- Hell
- Judgment
- Persecution
- Suffering
images: ["/uploads/2017/05/Hold-Firm-2-Thessalonians.jpg"]
---
1. Paul and the Thessalonians (vv1-2)

    Acts 17

2. A Church Growing in Faith and Love (v3)
3. A Church that is Persecuted (vv4-10)

    Isaiah 2:19

    Romans 12:9

4. A Church Working for Jesus' Glory (vv11-22)
5. What is Jesus doing?

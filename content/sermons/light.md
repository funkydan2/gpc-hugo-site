---
title: "Light"
passage: "Luke 24:1-8; 44-49"
date: 2020-04-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20200412_EasterSunday_Light_DS.mp3"]
audio_duration: "24:02"
audio_size: 5999517 
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/04/Easter.jpg"]
series: ["Easter"]
tags: ["Resurrection", "Hope", "Light"]
---

What is the *light* and *hope* Jesus’ resurrection brings?

In this message, as we look at Luke's record of the *first day of the week* we see the *light* of Jesus’ resurrection is that new creation has broken into the world and so forgiveness of sins is now possible.

# Outline 

1. New Creation (Luke 24:1-8)
2. Repentance and Forgiveness (Luke 24:44-49)
3. Mission and Message


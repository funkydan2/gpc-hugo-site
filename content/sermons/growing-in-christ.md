---
title: "Growing in Christ"
passage: "Colossians 1:1-14"
date: 2019-09-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20190922_GrowingInChrist_DS.mp3"]
audio_duration: "30:45"
audio_size: 7609667
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/09/ChristOverAll.jpg"]
series: ["Christ Over All"]
tags: ["Prayer", "Mission", "Evangelism"]
---

How does someone grow as a follower of Jesus?

# Outline

1. Paul and Colossae (vv 1-2)
2. Faith, Hope, and Love (vv 3-8)
3. Living worthy of the God we know (vv 9-13)
4. Are we growing in Christ?

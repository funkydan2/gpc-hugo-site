---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180603_WhyChurch_DS.mp3"]
audio_duration: '22:09'
audio_size: 5543078
date: 2018-06-03 06:21:24 +0000
passage: 1 Timothy 3:14-16
title: Why Church?
preachers: ["Daniel Saunders"]
series:
- God's House
tags:
- Church
- Jesus
images: ["/uploads/2018/04/Gods-House-768x644.jpg"]
---

1. Knowing how to conduct ourselves (vv14-15)

2. in church (v15)

3. because of Jesus (v16)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180218_FeastOnChrist_DS.mp3"]
audio_duration: '24:36'
audio_size: 6134262
date: 2018-02-18 06:41:57+00:00
draft: false
passage: John 6
title: Feast on Christ
preachers: ["Daniel Saunders"]
series:
- Find Life
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. We want food! (6:25-27)

2. Jesus is the bread of life (6:28-40)

3. Two objections (6:41-66)

4. The words of eternal life (6:67-71)

5. Will you feast on Christ?

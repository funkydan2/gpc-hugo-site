---
title: "Jesus the Son"
passage: "John 1:1-18; Matthew 1:18-25"
date: 2020-10-25T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhatWeBelieve/20201025_JesusTheSon_DS.mp3"]
audio_duration: "0:25:40"
audio_size: 6390318
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/10/WWB.jpg"]
series: ["What We Believe"]
tags: ["Jesus", "Incarnation", "Trinity", "Humility"]
---
Jesus once asked his closest followers, 'who do you say I am?' This is the most significant question anyone can answer.

In this message, we see what the Bible says about who Jesus is, and explore questions like 'why does the virgin birth matter,' 'how can we really know God,' and 'what difference does the eternal Word becoming human make to us'?

# Outline
1. In the Beginning (John 1:1-3)
2. Born of a Virgin

    Matthew 1:18-25
    
    John 1:14-18
    
    Philippians 2:1-6

3. Is Jesus Your Lord?


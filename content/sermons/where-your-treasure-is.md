+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190310_WhereYourTreasureIs_DS.mp3"]
audio_duration = "28:45"
audio_size = 7127123
date = "2019-03-10T09:30:00+10:00"
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 12:13-34"
preachers = ["Daniel Saunders"]
series = []
tags = ["Money","Anxiety"]
title = "Where Your Treasure Is"

+++
1. There's more to life (vv13-21)
2. ... so don't worry (vv22-29)
3. ... and seek eternal treasure (vv30-34)

The article referenced during the sermon is [Don't be anxious about anything! Is that all God's got to say?](https://bit.ly/2TCVmf7 "Links to Moore Theological College's blog") by Paul Grimmond

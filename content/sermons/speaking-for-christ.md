---
title: "Speaking for Christ"
passage: "Colossians 2:4-6"
date: 2020-01-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20200112_SpeakingForChrist_DS.mp3"]
audio_duration: "27:13"
audio_size: 6762090
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/09/ChristOverAll.jpg"]
series: ["Christ Over All"]
tags: ["Evangelism", "Prayer"]
---

Why do many (most?) Christians find it hard to speak about Jesus to non-believers?

# Outline

1. Prayerful partnership (vv2-4)
2. Seasoned speech (vv5-6)

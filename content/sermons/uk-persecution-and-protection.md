---
title: "Persecution and Protection"
passage: "Acts 21:27-23:30"
date: 2023-03-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230312_PersecutionAndProtection_DS.mp3"]
audio_duration: "30:05"
audio_size: 7451621
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Persecution","Resurrection","Perseverance"]
---
When we struggle or suffer, does this mean God is distant, that he's not in control? In this message, we see how Jesus strengthens and protects his people through persecution with the hope of resurrection.

# Outline
1. Persecution and Plots
2. Protected by God

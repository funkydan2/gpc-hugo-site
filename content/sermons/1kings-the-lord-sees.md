---
title: "The LORD Sees"
passage: "1 Kings 21-22"
date: 2024-05-26T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/1Kings/20240526_TheLORDSees_DS.mp3"]
audio_duration: "24:16"
audio_size: 6054384
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/04/1Kings.png"]
series: ["1 Kings"]
tags: ["Repentance","Sin","Punishment"]
---
It looked like the 'perfect crime'. But the LORD God saw the sin of Israel's most vile king, Ahab, sin and brought justice.

In this message from 1 Kings, we hear the sobering reality that God sees everything, but also his amazing grace, that this is the same God who gives forgiveness and new life to all who trust in Jesus.

# Outline
1. A Perfect Crime (21:1-16)
2. Found out! (21:17-26)
3. Repentance (21:27-28) or Regret (Chapter 22)

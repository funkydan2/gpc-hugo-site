---
title: "The Greatest"
passage: "Mark 10:32-52"
date: 2021-10-24T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211024_TheGreatest_TD.mp3"]
audio_duration: "18:25"
audio_size: 4649398
preachers: ["Thomas Dean"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Greatness"]
---
What would you give to be the greatest?

As we continue our journey with Jesus to Jerusalem, we hear his radical call to true greatness as he himself prepares to serve by giving his life.

{{< youtube 1xmTEcO3Ttc >}}

(This sermon was pre-recorded.)

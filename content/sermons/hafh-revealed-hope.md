---
title: "Revealed Hope"
passage: "1 Peter 1:10-12"
date: 2021-07-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210706_RevealedHope_DS.mp3"]
audio_duration: "25:56"
audio_size: 6456452
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Scripture","Old Testament","Spirit"]
---
What do you expect to find in the Bible?

In this message, we hear how the whole Bible is about Jesus—his suffering and glories.

# Outline
1. This salvation
2. Revealed to the prophets (vv10-11)
3. Revealed to us (v12)
4. …but not to angels (v12)
5. Search the scriptures

---
title: "Three Kings"
passage: "Matthew 2:1-12"
date: 2023-12-25T08:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20231225_ThreeKings_DS.mp3"]
audio_duration: "14:55"
audio_size: 3810007
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/12/Christmas1.jpg"]
series: ["Christmas 2023"]
tags: ["Christmas","Power","Peace","King"]
---
A Christmas message considering the three kings of Matthew 2--not the three kings you might expect, but two responses to the shepherd-king, Jesus.

# Outline
1. Herod: The Anxious King
2. Magi: Honouring the True King
3. Jesus: The Shepherd-King

+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnholyPeopleHolyGod/20181118_HolyPriesthood_DS.mp3"]
audio_duration = "25:24"
audio_size = 6322344
date = "2018-11-18T16:14:56+10:00"
images = ["/uploads/2018/10/Unholy-People-Holy-God.jpg"]
passage = "Leciticus 21-22"
preachers = ["Daniel Saunders"]
series = ["Unholy People Holy God"]
tags = ["Priest", "Holy", "Sacrifice"]
title = "Holy Priesthood"

+++
1. Priestly Holiness

2. Unholy Priests

    Numbers 12 & 20; 1 Samuel 1-2; Jeremiah 23:11; 26:8; Matthew 26:59

3. Jesus, our holy priest

    Hebrews 4:14-16

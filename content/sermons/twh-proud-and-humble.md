---
title: "Proud and Humble"
passage: "Luke 18:1-14"
date: 2019-08-25T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190825_ProudAndHumble_DS.mp3"]
audio_duration: "26:21"
audio_size: 6551617
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Prayer"]
---

When was the last time you prayed? What did you say or do? How did you approach God?

In this sermon, we listen to Jesus' teaching on prayer—how we should approach God when we know Jesus is king.

# Outline

1. Faithful persistence (vv 1-8)
2. Humble contrition (vv 9-14)
3. Pray—humbly and persistently!

---
title: "Our Gospel Hope"
passage: "1 Corinthians 15"
date: 2020-11-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhatWeBelieve/20201122_OurGospelHope_DS.mp3"]
audio_duration: "32:28"
audio_size: 8024001
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/10/WWB.jpg"]
series: ["What We Believe"]
tags: ["Resurrection", "Hope", "Gospel", "Work", "Discipleship"]
---
'You only live once.' But what if that's not true?  

In this message, we finishing our series 'What We Believe' and hear what the Bible says about resurrection and life everlasting and how that gives us hope and changes our now.


# Outline
1. The good news of Jesus (1 Corinthians 15:1-11)
2. The good news of resurrection (15:12-28)
3. Why resurrection matters (15:29-34)
4. All things new (15:35-53)
5. Life everlasting…and now (15:54-58)
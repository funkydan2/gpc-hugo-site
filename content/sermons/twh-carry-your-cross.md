+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190331_CarryYourCross_DS.mp3"]
audio_duration = "27:11"
audio_size = 6751718
date = "2019-03-31T09:30:00+10:00"
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 14"
preachers = ["Daniel Saunders"]
series = ["The Way Home (Part 3)"]
tags = ["Hospitality", "Generosity"]
title = "Carry Your Cross"

+++
1. Who needs healing? (vv1-6)

2. Generous humility (vv7-11)

3. Generous invitation (vv12-24)

4. Count the cost (vv25-35)

---
title: "Truth and Love"
passage: "Matthew 5:33-48"
date: 2024-03-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StaySalty/20240310_TruthAndLove_DS.mp3"]
audio_duration: "21:52"
audio_size: 5480036
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/02/StaySalty.jpg"]
series: ["Stay Salty"]
tags: ["Promises","Persecution","Love"]
---
As we continue listening to Jesus' teaching his followers, he calls them to be people who speak truth, refuse to retaliate, and love enemies. It's a huge call, but it's good news as we see it lived out by Jesus.


# Outline
1. Speak the truth (vv33-37)
2. Do not resist (vv38-42)
3. Love Enemies (vv43-47)
4. Because this is our God (v48)

`Note: this sermon was pre-recorded.`

{{< youtube 0hIRS-va2hc >}}

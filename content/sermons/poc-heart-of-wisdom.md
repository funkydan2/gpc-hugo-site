---
title: "A Heart of Wisdom"
passage: "Psalm 90"
date: 2020-04-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/PsalmsOfComfort/20200405_POC-Wisdom_DS.mp3"]
audio_duration: "22:18"
audio_size: 5581429
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/04/Psalms_Of_Comfort.jpg"]
series: ["Psalms of Comfort"]
tags: ["Eternity", "Wisdom"]
---

What are we to learn from God in this unsettled time of uncertainty?

This Sunday we begin a short series hearing some Psalms of Comfort, as together we draw near to God.

# Outline

1. God’s Eternity (vv1-3)

    Deuteronomy 33:27

2. Our Frailty (vv4-6)

3. Our Failing (vv7-11)

4. Ask God for Wisdom and Comfort (vv12-17)

    1 Corinthians 1:22-25


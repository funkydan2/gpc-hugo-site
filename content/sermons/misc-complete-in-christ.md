---
title: "Complete in Christ"
passage: "Romans 8:33-37"
date: 2023-07-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20230702_CompleteInChrist_PB.mp3"]
audio_duration: "24:45"
audio_size: 6169608
preachers: ["Peter Bloomfield"]
images: ["/uploads/2023/07/Romans8.jpg"]
series: ["Miscellaneous"]
---
Without Christ we have nothing but in Christ we have everything.

# Outline
1. Because of Christ crucified
2. Because of Christ risen
3. Because of Christ exalted
4. Because of Christ interceding

---
title: Gospel Humility
passage: Philippians 2:1-11
date: 2024-11-10T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/GospelPartners/20241110_GospelHumility_DS.mp3
audio_duration: 25:06
audio_size: 6253921
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/10/GospelPartners.jpeg
series:
    - Gospel Partners
tags:
    - Humility
    - Trinity
    - Union With Christ
    - Christ
---
Jesus - both by his example but, even more, as we're united to him, teaches the humility needed for unity.

# Outline
1. A Call to Unity and Humility (vv1-4)
2. United to the Humble Christ (vv5-8)
3. United to the Exalted Christ (vv9-11)
4. Living Gospel Humility

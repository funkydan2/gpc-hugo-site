---
title: "A Dancing King"
passage: "1 Samuel 5-6"
date: 2019-05-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190505_ADancingKing_DS.mp3"]
audio_duration: "30:02"
audio_size: 7438516
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/04/SWFTTK.jpg"]
series: ["Still Waiting For The True King"]
tags: ["Joy", "Singing", "Temple"]
---
In the rest King David brings to God’s people we see glimpses of a priest-king like Adam in Eden or Melchizedek in (Jeru)Salem. David rejoices in God’s presence as we see the fulfilment of God's promises to Abraham.

# Outline
1. The danger of God in a box (6:1-11)
2. Dancing in God’s presence (6:12-20)
3. Despising the delight of God (6:21-23)
4. Jesus, the dancing priest-king
5. Dancing before God

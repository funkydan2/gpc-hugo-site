+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20181216_TheKingsDemise_DS.mp3"]
audio_duration = "25:38"
audio_size = 6378977
date = "2018-12-16T13:47:31+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 13-15"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = []
title = "The King's Demise"

+++
1. The King's Victory? (13:1-5)

2. The King's Folly (1 Samuel 13, 15)

3. Saul's Sin and Us

    Mark 7:13

4. We need a wise, obedient king

    John 9:38; Mark 9:7; Philippians 2:9-11

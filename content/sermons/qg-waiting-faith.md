---
title: "Waiting Faith"
passage: "Habakkuk 2:2-20"
date: 2022-05-08T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Habakkuk/20220508_WaitingFaith_DS.mp3"]
audio_duration: "34:04"
audio_size: 8406481
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Habakkuk.jpg"]
series: ["Questioning God"]
tags: ["Suffering","Justice","Faith"]
---
How do we 'live by faith' in the midst of suffering and oppression? In this message, we hear God's confronting and comforting response to this question.

# Outline
1. Two ways to live (vv2-5)
2. Five Woes (vv6-20)
3. Living by Faith

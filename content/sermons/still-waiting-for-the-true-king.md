+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20190127_StillWaitingForTheTrueKing_DS.mp3"]
audio_duration = "25:15"
audio_size = 6286602
date = "2019-01-27T10:00:00+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 27-31"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = ["Death", "Good News", "Evangelism"]
title = "Still Waiting for the True King"

+++
1. David's Doubting Deception (1 Samuel 27)
2. Saul's Defiant Disobedience (1 Samuel 28)
3. The King is Dead... (1 Samuel 31)
4. ...Long Live the King (Matthew 28:16-20)

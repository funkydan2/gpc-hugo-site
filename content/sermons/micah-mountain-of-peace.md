---
title: "The Mountain of Peace"
passage: "Micah 4:1-8"
date: 2021-05-23T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Micah/20210523_MountainOfPeace_DS.mp3"]
audio_duration: "28:53"
audio_size: 7163585
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/04/Micah.jpg"]
series: ["Micah"]
tags: ["Eschatology", "Peace"]
---
In a world full of corruption, violence, and revenge where can we get peace? In this message from the prophet Micah, we hear God's wonderful vision and promise for peace which is fulfilled in Jesus!

# Outline
1. Walking in God’s Word (vv1-2)
2. Dwelling in Peace (vv3-5)
3. Restoring the Lame (vv6-8)
4. Jerusalem, City of our God

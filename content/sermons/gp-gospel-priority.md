---
title: Gospel Priority
passage: Philippians 1:12-30
date: 2024-11-03T09:30:00+10:00
audio:
    - "https://f001.backblazeb2.com/file/GympieSermonAudio/GospelPartners/20241103_GospelPriority_DS.mp3"
audio_duration: 24:49
audio_size: 6187894
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/10/GospelPartners.jpeg
series:
    - Gospel Partners
tags:
    - Suffering
    - Gospel
---
What's your number one priority in life? What gets you out of bed and going? What sustains you when things are tough?

In this message from Philippians 1,  we hear that the gospel and glory of Christ is to be the number one priority for Christians.

# Outline
1. Gospel priority over prison (vv12-18)
2. Gospel priority over life or death (vv19-26)
3. Gospel priority over identity (vv27-30)
4. Is the gospel our priority?

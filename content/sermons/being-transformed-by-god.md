---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171126_BeingTransformedByGod_DS.mp3"]
audio_duration: '28:50'
audio_size: 7149332
date: 2017-11-26 08:38:36+00:00
draft: false
passage: Romans 12
title: Being Transformed By God
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Gifts
- Humility
- Love
- Worship
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. Worshiping our merciful God (vv1-2)

2. ... in humble service (vv3-8)

3. ... in genuine love (vv9-13, 15-16)

4. ... by showing mercy to enemies (vv14, 17-21)

5. Living in God's Mercy

---
title: "Mocking the King"
passage: "Mark 15:1-32"
date: 2022-04-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20220410_MockingTheKing_DS.mp3"]
audio_duration: "24:53"
audio_size: 6201805
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["King", "Temple", "Salvation", "Cross"]
---
Why did Jesus die? As Jesus is mocked by soldiers and leaders, they unwittingly speak the truth—that through his death, Jesus saves.

# Outline
1. The King or the Terrorist (vv1-16)
2. Crowning the King (vv16-20)
3. The King who Saves (vv21-32)

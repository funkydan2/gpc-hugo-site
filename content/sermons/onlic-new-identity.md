---
title: "New Identity"
passage: "Colossians 3:1-11"
date: 2019-06-30T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20190630_NewIdentity_DS.mp3"]
audio_duration: "27:29"
audio_size: 6821609
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/06/ONLIC.jpg"]
series: ["Our New Life in Christ"]
tags: ["Identity", "Union With Christ"]
---

Who are you?

Jesus graciously offers a new, true identity, through dying and rising with him.

# Outline
1. You have died. You have been raised.
2. Set your heart. Set your mind.
3. Christ is all.

---
title: "The Good Life (in Bad Days)"
passage: "1 Peter 3:8-22"
date: 2021-08-08T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210808_TheGoodLife_DS.mp3"]
audio_duration: "25:46"
audio_size: 6414433
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Suffering", "Love", "Blessing", "Resurrection"]
---
What do you think the 'good life' is, and where do you find it?

In 1 Peter 3, God says the good life is found in Jesus, and this is something even suffering and persecution can't take away. This passage also has some hard to understand bits in it - so have a listen to understand how something about 'imprisoned spirits' gives confidence and hope even in bad days.


# Outline
1. How to see the good life (vv8-12)
2. Even in bad days (vv13-17)
3. Because of the Victory of Christ (vv18-22)

---
title: "A Model Church (Part 2)"
passage: "Acts 4:32-5:11"
date: 2022-03-06T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220306_AModelChurch-Part2_DS.mp3"]
audio_duration: "27:56"
audio_size: 6934824
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Generosity", "Money", "Godliness", "Church", "Hospitality", "Love"]
---
Are you faking it? In this message, we see an encouraging example of generosity and a severe warning to those who fake godliness.

# Outline
1. God’s true people (4:32-37)
2. Pretend Piety (5:1-11)

Due to disruptions caused by flooding, this message was pre-recorded. You can watch the video recording [on YouTube](https://youtu.be/WF4MfMmji8Y)

{{< youtube  WF4MfMmji8Y >}}

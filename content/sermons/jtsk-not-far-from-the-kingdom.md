---
title: "Not Far From the Kingdom"
passage: "Mark 12:28-44"
date: 2021-11-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211114_NotFarFromTheKingdom_DS.mp3"]
audio_duration: "30:54"
audio_size: 7649141
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Love", "Law"]
---
What's most important to God? What's valued in Jesus' kingdom?

In this message, we hear Jesus' answer about the most important commandment, and see the devastating impact of ignoring what Jesus says.

# Outline
1. The most important commandment (vv28-34)
2. David’s Son (vv35-37, Psalm 110)
3. Devouring Widows (vv39-44)
4. Loving God and Neighbour

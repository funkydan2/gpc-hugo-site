+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190317_BeReady_DS.mp3"]
audio_duration = "28:32"
audio_size = 7127123
date = "2019-03-17T09:30:00+10:00"
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 12:35-59"
preachers = ["Daniel Saunders"]
series = ["The Way Home (Part 3)"]
tags = []
title = "Be Ready"
+++
1. God's clock is ticking

    Malachi 3:1; Luke 7:27; Luke 9:51

2. Are you ready? (vv35-40)

3. ...especially the leaders (vv41-48)

4. ...because Jesus brings judgment and division (vv49-53)

5. So understand the times and be wise (vv54-59)

6. Are you ready?

    Acts 1:5-8

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20160325_ForgivingTheUnforgivable_GoodFriday_DS.mp3"]
audio_duration: '18:19'
date: 2016-03-25 02:53:55+00:00
draft: false
passage: Luke 23:32-43
title: Forgiving the Unforgivable
preachers: ["Daniel Saunders"]
series:
- Easter
tags:
- Cross
- Easter
- Forgiveness
- Good Friday
images: ["/uploads/2016/03/GoodFriday.jpg"]
---
Good Friday Sermon, 2016

1. The unforgiving mockers (vv34-38)
2. The unforgivable men (vv32-33; 39-43)
3. Finding forgiveness

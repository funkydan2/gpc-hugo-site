---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnitedWithChrist/20170521_Church_DS.mp3"]
audio_duration: '28:23'
audio_size: 7042762
date: 2017-05-21 10:51:56+00:00
draft: false
passage: Ephesians 4:1-16
title: Church
preachers: ["Daniel Saunders"]
series:
- United with Christ
tags:
- Union With Christ
- Church
images: ["/uploads/2017/04/United-with-Christ_web.jpg"]
---
1. United with Christ, the Head
2. United in One Body
3. Growing and Building in Unity

Living in Unity

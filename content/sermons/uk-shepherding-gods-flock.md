---
title: "Shepherding God's Flock"
passage: "Acts 19:11-20:38"
date: 2023-02-26T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230226_SheperdingGodsFlock_DS.mp3"]
audio_duration: "26:14"
audio_size: 6528574
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Leadership","Atonement","Repentance"]
---
What should Christian leadership (elders) look like?

In this message, we hear Paul's final words of instruction and encouragement to some church elders.

# Outline
1. A model of Christian ministry (20:17-27)
2. What elders must do (20:28-35)

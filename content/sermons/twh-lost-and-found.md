---
title: "Lost and Found"
passage: "Luke 15"
date: 2019-07-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190721_LostAndFound_DS.mp3"]
audio_duration: "25:07"
audio_size: 6258037
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Parable"]
---

Are you lost?

Jesus told some stories to show there are two ways to be lost. You can be lost far away or lost right at home. The good news is Jesus came to show us God’s heart for all lost people—wanting to bring them from death to life.

# Outline

1. Two stories of finding joy (Luke 15:1-10)
2. A story of two lost sons…
3. and their prodigal father. (Luke 15:11-31)
4. Feast with the Father
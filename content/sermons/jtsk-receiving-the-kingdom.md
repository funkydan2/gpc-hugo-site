---
title: "Receiving the Kingdom"
passage: "Mark 10:13-31"
date: 2021-10-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211017_ReceivingTheKingdom_DS.mp3"]
audio_duration: "25:51"
audio_size: 6436216
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Kingdom", "Children", "Money"]
---
Who would you expect Jesus to welcome? Who would God want in his kingdom?

In this message from Mark's Gospel we see two events that show us who receives God's kingdom.

# Outline
1. Welcoming in (vv13-16)
2. Walking away (vv17-22)
3. All things are possible (vv23-31)
4. Disciples of the Kingdom

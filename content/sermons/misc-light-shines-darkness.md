---
title: "The Light Shine in the Darkness"
passage: "John 8:12-30"
date: 2023-06-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20230604_TheLightShinesInTheDarkness_PS.mp3"]
audio_duration: "20:25"
audio_size: 5132163
preachers: ["Phil Stolk"]
images: ["/uploads/2023/06/John8.jpg"]
series: ["Miscellaneous"]
tags: ["Light","Dark","Jesus","Temple"]
---
Where is there light in our dark world?

In this message from John 8, we hear Jesus' astounding claim that he is *the* light of the world. Hope isn't found in 'religion' or success, but in looking at Jesus, the light.

# Outline
1. The Light of the world (v 12)
2. The Darkness of the world (v 13-30)
    1. Those living in darkness judge Jesus by human standards
    2. Those living in darkness do not know God
    3. Those living in darkness will die in darkness 
3. Where we see the light

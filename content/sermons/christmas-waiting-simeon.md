---
title: "Waiting for Christmas (Simeon)"
passage: "Luke 2:21-33"
date: 2021-12-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20211219_WaitingForChristmas_DS.mp3"]
audio_duration: "13:37"
audio_size: 3499061
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/12/2021ChristmasTheme.jpg"]
series: ["Christmas 2021"]
tags: ["Christmas","Light","Salvation","Glory"]
---
Message from our 2021 Christmas Carols service

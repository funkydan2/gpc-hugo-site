---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160306_HowFirmAreYourFoundations_DS.mp3"]
audio_duration: '26:02'
date: 2016-03-06 00:42:44+00:00
draft: false
passage: Luke 6:17-49
title: How Firm Are Your Foundations?
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
tags:
- Discipleship
- Faith
- Works
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---

1. How firm are your foundations? (vv17-20, 46-49)
2. Following Jesus changes...
    1. Who we think is blessed (vv20-26)
    2. How we love others (vv27-36)
    3. How we view others (vv37-42)
3. People will see (vv43-45)

How firm are your foundations?

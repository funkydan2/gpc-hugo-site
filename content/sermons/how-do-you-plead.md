---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160508_HowDoYouPlead_DS.mp3"]
audio_duration: '26:28'
date: 2016-05-08 11:26:29+00:00
draft: false
passage: Romans 1:18-32
title: How Do You Plead?
preachers: ["Daniel Saunders"]
series:
- The Verdict
tags:
- Gospel
- Judgment
images: ["/uploads/2015/12/Romans-TheVerdict.jpg"]
---

1. Our biggest problem (1:18)
2. Ignorance is no defence (1:19-21)
3. Our 'rap sheet' (1:22-32)
4. It's a dark picture, but there's hope (1:16-17, 3:25-26)

How do you plead?

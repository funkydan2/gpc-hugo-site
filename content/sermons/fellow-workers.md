---
title: "Fellow Workers"
passage: "Colossians 4:7-18"
date: 2020-01-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20200119_FellowWorkers_DS.mp3"]
audio_duration: "29:07"
audio_size: 7216625
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/09/ChristOverAll.jpg"]
series: ["Christ Over All"]
tags: ["Prayer", "Mission"]
---

Is the Christian life more like a solo or a team sport? In this message, we hear from the often-overlooked 'final words' of the letter to the Colossians which gives us insight into what life is like co-labouring in God's service.

# Outline

1. Messengers of Encouragement (vv7-9)
2. Fellow workers (vv10-14)
3. Churches of the Word (vv15-18)

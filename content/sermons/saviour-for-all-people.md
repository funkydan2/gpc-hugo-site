---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180513_SaviourForAllPeople_DS.mp3"]
audio_duration: '25:45'
audio_size: 6407114
date: 2018-05-13 06:05:02+00:00
draft: false
passage: 1 Timothy 2:1-7
title: Saviour For All People
preachers: ["Daniel Saunders"]
series:
- God's House
tags:
- Mission
- Salvation
images: ["/uploads/2018/04/Gods-House-768x644.jpg"]
---

1. Pray for all people (vv1-2)

2. because there is one God and one Mediator (vv3-7)

3. Pray ourselves onto mission

[Slides](https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180513_1+Timothy+2v1-7_Slides.pdf)

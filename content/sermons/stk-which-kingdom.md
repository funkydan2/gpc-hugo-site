---
title: Which Kingdom?
passage: Matthew 12:22-37
date: 2024-10-06T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20241006_WhichKingdom_DS.mp3
audio_duration: 24:08
audio_size: 6022609
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/07/SeeingTheKingdom.jpg
series:
    - Seeing the Kingdom
tags:
    - Forgiveness
    - Kingdom
    - Victory
---
In this message we hear Jesus serious warning for those who think they can stay on the fence with him.

# Outline
1. David or the Devil? (vv22-27)
2. Satan’s Kingdom Plundered (vv28-29)
3. So Find Forgiveness (vv30-37)

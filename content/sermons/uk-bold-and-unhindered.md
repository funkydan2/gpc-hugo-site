---
title: "Bold and Unhindered"
passage: "Acts 28:11-31"
date: 2023-04-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230402_BoldlyWithoutHinderance_DS.mp3"]
audio_duration: "30:31"
audio_size: 7554205
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Mission","Evangelism","Kingdom"]
---
Can anything stop God's kingdom? Often followers of Jesus feel weak and broken—but in this final message from the book of Acts, we see how when we are weak, God is strong.

# Outline
1. Strength in Weakness (vv11-16)
2. Persevering despite hard hearts (vv17-28)
3. Proclaiming Boldly and without Hindrance (vv30-31)

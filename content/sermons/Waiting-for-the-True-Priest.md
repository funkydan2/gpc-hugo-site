+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20180923_WaitingForTheTruePriest_DS.mp3"]
audio_duration = "26:56"
audio_size = 6723537
date = "2018-09-23T13:29:12+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 2:12-3:21"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = ["Priest", "Sacrifice", "Guilt"]
title = "Waiting for the True Priest"

+++
1. Eli's Worthless Sons (2:12-17)
2. Eli's Weak Rebuke (2:22-25)
3. God's Word to Eli (2:26-36; 3:11-18)
4. A New House Rises? (2:18, 26; 3:19-21)
5. God's True Priest

     Hebrews 9:11-14

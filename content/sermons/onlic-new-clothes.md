---
title: "New Clothes"
passage: "Colossians 3:12-17"
date: 2019-07-07T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20190707_NewClothes_DS.mp3"]
audio_duration: "21:34"
audio_size: 5402493
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/06/ONLIC.jpg"]
series: ["Our New Life in Christ"]
tags: ["Union With Christ", "Holiness", "Singing"]
---

How does trusting in Jesus change us?

The new life Jesus gives means we take off what is opposed to Christ and put on a new way of living and speaking.

# Outline

1. Take off the old self (vv 5, 8)
2. Put on the new self (vv12-15)
3. Be indwelled by Christ’s word (vv16 -17)

(Note: the recording didn't work during church and so this audio was recorded afterwards.)

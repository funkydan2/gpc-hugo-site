---
title: "Rich and Poor"
passage: "Luke 16:19-31"
date: 2019-08-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190804_RichAndPoor_DS.mp3"]
audio_duration: "20:41"
audio_size: 5672305
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Parable", "Money", "Heaven", "Hell", "Eternity"]
---

Would you be convinced if someone were to rise from the dead?

In this message, we listen to another story Jesus told. A story about a rich man who loves money, not God, and who shows this by overlooking the poor.

# Outline

1. A parable
   1. … of two men (vv19-21)
   2. …and the tables are turned (vv22-26)
   3. …because they wouldn’t listen. (vv27-31)
2. Jesus has crossed the chasm
3. …will you?

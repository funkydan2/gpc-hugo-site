---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20180114_PartnersInGodsMission_DS.mp3"]
audio_duration: '29:33'
audio_size: 7319981
date: 2018-01-14 05:32:01+00:00
draft: false
passage: Romans 15:14-16:27
title: Partners in God's Mission
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Mission
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

(Note, during this sermon some of our gospel partners were mentioned by name. Their names have been removed from this recording in the interests of security and privacy.)

1. God's Mission (15:14-22; 16:25-27)

2. which we are partners in

    1. by giving (15:23-29)

    2. by praying (15:30-33)

    3. by working (16:1-23)

3. What's your part in God's mission?

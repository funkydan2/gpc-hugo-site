---
title: "Clean Clothes"
passage: "Zechariah 3"
date: 2023-05-07T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230507_CleanClothes_DS.mp3"]
audio_duration: "21:28"
audio_size: 5381659
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Temple","Priest"]
---
Have you ever found yourself underdressed for the occasion? As Zechariah's vision continues, we're confronted with the problem of filthy clothes—but God has an amazing solution and promise.

# Outline
1. Dirty clothes in God’s courtroom (vv1-5)
2. Promises of things to come (vv6-10)
3. Our great high priest! (Hebrews 4:14-16)

This sermon was prerecorded. Watch the video:

{{< youtube oAits7sdbu8 >}}

---
title: "Gospel Partnership"
passage: "Acts 11:19-30"
date: 2022-08-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220814_GospelPartnership_DS.mp3"]
audio_duration: "28:11"
audio_size: 6997095
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Mission","Elders","Leadership","Generosity"]
---
Is following Jesus a solo-survival mission?

As we continue our journey through Acts we see how the gospel of Jesus leads to partnerships between churches.

# Outline
1. God’s Mission (vv19-21)
2. Partnership (1): From Jerusalem (vv22-24)
3. Partnership (2): With Saul (vv25-26)
4. Partnership (3): To Jerusalem (vv27-30)

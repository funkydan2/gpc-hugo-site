---
title: "Wisdom From the Spirit"
passage: "Ephesians 5:15-21"
date: 2020-08-30T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200830_WisdomFromTheSpirit_DS.mp3"]
audio_duration: "24:03"
audio_size: 6001551
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Singing", "Wisdom", "Dark", "Light"]
---

How can you respond wisely in a rapidly changing world?  In this message, we hear what God says about walking in wisdom in evil days.

# Outline

1. Live wisely in evil days (vv15-17)
2. Don’t be filled with wine (v18)
3. Be filled by the Spirit
   1. Singing to each other (v19)
   2. Singing to the Lord (v20)
   3. Submitting to one another (v21)
4. The days are evil. Are you living wisely?


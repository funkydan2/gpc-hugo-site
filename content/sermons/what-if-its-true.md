---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170416_WhatIfItsTrue_DS.mp3"]
audio_duration: '19:50'
audio_size: 4991308
date: 2017-04-16 08:12:13+00:00
draft: false
passage: Luke 24:1-12
title: What if it's true?
preachers: ["Daniel Saunders"]
series:
- Easter
tags:
- Resurrection
images: ["/uploads/2016/03/Easter.jpg"]
---

1. Looking among the dead

    1. The women

    2. The disciples and Peter

2. What if it's true?

    3. Jesus is the king

    4. Jesus brings life

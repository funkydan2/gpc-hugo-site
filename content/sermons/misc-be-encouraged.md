---
title: "Be Encouraged"
passage: "Galatians 5:13-26"
date: 2020-09-27T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20200927_BeEncouraged_LW.mp3"]
audio_duration: "25:53"
audio_size: 6443462
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
tags: ["Freedom"]
---

# Outline

1. Christian freedom
2. Our moment-by-moment civil war
3. What is the fruit of the flesh
4. What is the fruit of the Holy Spirit
5. Keep killing the flesh
6. Be encouraged


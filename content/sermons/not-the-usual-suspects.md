---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160515_NotTheUsualSuspects_DS.mp3"]
audio_duration: '28:07'
date: 2016-05-15 08:56:33+00:00
draft: false
passage: Romans 2
title: Not The Usual Suspects
preachers: ["Daniel Saunders"]
series:
- The Verdict
tags:
- Hypocrisy
- Judgment
- Sin
images: ["/uploads/2015/12/Romans-TheVerdict.jpg"]
---

1. The Charge: Hypocrisy (2:1-4)
2. The Sentence: Judged (2:5-16)
3. But!
    1. We've got the Law (2:17-24)
    2. We're the circumcised (2:25-29)
4. God's word to non-usual suspects

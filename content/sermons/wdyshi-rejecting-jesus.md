---
title: "Rejecting Jesus"
passage: "Mark 6:1-30"
date: 2021-02-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210228_RejectingJesus_DS.mp3"]
audio_duration: "25:26"
audio_size: 6336717
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Mission", "Persecution"]
---
Jesus faced rejection from the who knew him, those he didn't, and those in power. What do you think of him?

# Outline
1. Rejected by his people (6:1-6)
2. Preparing for rejection (6:6-13)
3. Rejected by the powerful (6:14-29)
4. Prepare for rejection
5. Rejecting Jesus

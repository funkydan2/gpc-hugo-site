+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnholyPeopleHolyGod/20181125_HolyRest_DS.mp3"]
audio_duration = "25:19"
audio_size = 6305054
date = "2018-11-25T16:15:24+10:00"
images = ["/uploads/2018/10/Unholy-People-Holy-God.jpg"]
passage = "Leviticus 23-27"
preachers = ["Daniel Saunders"]
series = ["Unholy People Holy God"]
tags = ["Sabbath", "Rest"]
title = "Holy Rest"

+++
1. Holy times (Leviticus 23)

2. for holy rest

3. in God's presence (Leviticus 24:1-9)

4. Resting in God's Presence

    Colossians 2:16-17; Matthew 11:28-30; Psalm 127:2

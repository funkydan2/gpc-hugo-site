---
title: "The Patience of God"
passage: "2 Peter 3"
date: 2023-01-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/2Peter/20230122_ThePatienceOfGod_DS.mp3"]
audio_duration: "27:13"
audio_size: 6761256
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/2Peter.jpg"]
series: ["Grow in Grace"]
tags: ["Eschatology","Repentance","Godliness","Salvation"]
---
It's been a long time since Jesus died and rose again—is he ever coming back?

In this message from 2 Peter 3 we hear the good news that God isn't slow, but patient—and how this changes everything.

# Outline
1. The Scoffers (vv1-4)
2. ...have forgotten (vv5-10)
3. How then should we live? (vv11-18)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171105_GodsFaithfulMercy_DS.mp3"]
audio_duration: '28:36'
audio_size: 7095600
date: 2017-11-05 08:12:54+00:00
draft: false
passage: Romans 9
title: God's Faithful Mercy
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Election
- Israel
- Promises
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. The dilemma: Can God be trusted? (vv1-5)

2. The answer: not every Israelite is 'true Israel' (vv6-13)

3. The objections

    1. Is God unjust? (vv14-18)

    2. Is God unfair? (vv19-30)

4. Conclusion: it's all about Jesus (vv31-33)

5. Trusting our merciful God

---
title: "A Dragon in the Nativity"
passage: "Revelation 12"
date: 2021-12-26T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20211226_ADragonInTheNativity_DS.mp3"]
audio_duration: "26:24"
audio_size: 6568024
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/12/DragonNativity.jpg"]
series: ["Christmas 2021"]
tags: ["Christmas", "Apocalyptic", "Cross"]
---
Revelation 12 is the Nativity story you probably didn't know was there. In the birth of Jesus, Satan (the dragon) is defeated...and the battle is won for those who trust in Jesus.

# Outline
1. The Revelation to John
2. Signs in Heaven (vv1-6)
3. War in heaven (vv7-12)
4. War on Earth (vv13-17)
5. War is over?

# Video
Following the sermon we watched this video from [SpeakLife](https://speaklife.org.uk)
{{<youtube DyC5RHKNBm4>}}

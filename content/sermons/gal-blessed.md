---
title: "Blessed"
passage: "Galatians 3:1-14"
date: 2022-10-30T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221030_Blessed_DS.mp3"]
audio_duration: "29:19"
audio_size: 7269391
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Blessing","Faith","Righteousness","Justification","Spirit"]
---
What does it mean to be blessed? How do we receive God's blessing?

In this message from Galatians 3 we hear the good news that God graciously pours out his promised blessing of the Spirit and righteousness through faith, not works.

# Outline
1. Fooled! (v1)
2. The blessing of the Spirit (vv2-5)
3. Blessed with Abraham (vv6-9)
4. Redeemed from the Curse (vv10-14)
5. Being blessed by God

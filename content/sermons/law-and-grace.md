+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20180624_LawAndGrace_DB.mp3"]
audio_duration = "31:50"
audio_size = 7865090
date = "2018-06-24T15:06:53+10:00"
images = ["/uploads/2018/06/Saving-Faith-768x644.jpg"]
passage = ""
preachers = ["Daryl Brenton"]
series = ["Miscellaneous"]
tags = ["Law", "Grace", "Covenant"]
title = "Law and Grace"
+++
1. Law
2. Grace
3. Law, Grace, and Covenant
4. Saving Faith

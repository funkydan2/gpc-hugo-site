---
title: "Who Is Greater? I AM"
passage: "John 4"
date: 2023-09-24T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20230924_WhoIsGreaterIAM_MD.mp3"]
audio_duration: "39:59"
audio_size: 9827868
preachers: ["Malcolm Diefenbach"]
images: ["/uploads/2023/09/John4.jpg"]
series: ["Miscellaneous"]
---
To a Samaritan woman, Jesus gives living water—and she meets the one greater than Jacob...the great I Am!

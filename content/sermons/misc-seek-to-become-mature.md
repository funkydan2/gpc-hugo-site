---
title: "Seek to Become a Mature Christian"
passage: "James 5:7-20"
date: 2022-10-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20221002_SeekToBecomeMature_LW.mp3"]
audio_duration: "28:12"
audio_size: 6999882
preachers: ["Leo Woodward"]
images: ["/uploads/2022/10/James5.jpg"]
series: ["Miscellaneous"]
tags: ["Prayer","Patience"]
---
A message from James 5 about how God makes his people mature.

# Outline
1. Be Patient in Suffering
2. Make no frivolous oaths
3. Pray for physical and spiritual healing
4. Help restore the rebellious believer

(There is a significant change in audio quality after the first 12 minutes.)

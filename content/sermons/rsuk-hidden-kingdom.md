---
title: Hidden Kingdom
passage: Matthew 13:24-43
date: 2025-02-09T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/RightSideUpKingdom/20250209_HiddenKingdom_DS.mp3
audio_duration: 25:05
audio_size: 6251427
preachers:
    - Daniel Saunders
images:
    - /uploads/2025/02/Matthew13-20.jpg
series:
    - The Right Side Up Kingdom
tags:
    - Judgment
    - Salvation
    - Parable
---
Have you ever had what you thought were unmet expectations? In this message we hear Jesus tell three stories that help us have right expectations for life following him now as we wait patiently for the complete coming of the Kingdom of Heaven.

# Outline
1. Hidden stories (vv24-33)
2. Prophecy fulfilled (vv34-35)
3. Kingdom revealed (vv36-43)

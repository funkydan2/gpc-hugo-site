---
title: "Living Under Pressure"
passage: "1 Thessalonians 4:1-12"
date: 2023-10-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20231022_LivingUnderPressure_DS.mp3"]
audio_duration: "24:07"
audio_size: 6021063
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Holiness","Marriage","Gospel"]
---
It can be hard to stick out—we often feel pressure to conform. In this message from 1 Thessalonians 4, God's people are encouraged to live for Jesus in our private and public relationahips.

# Outline
1. Avoid Sexual Immorality (vv1-8)
2. Live a Quiet Life (vv9-12)

---
title: The Right Side of History
passage: Psalm 2
date: 2024-12-29T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20241229_TheRightSideOfHistory_DS.mp3
audio_duration: 25:17
audio_size: 6298602
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/12/Psalm2.jpg
series:
    - Miscellaneous
tags:
    - Christ
    - King
    - Messiah
    - Persecution
    - Ascension
---
When there is opposition or persectuion, followers of Jesus may think they're on the 'wrong side of history'. But Psalm 2 explains why believers in Jesus can have bold confidence especially when 'the nations rage'.

# Outline
1. Listen to the Song (Psalm 2)
2. Why do the Nations (Still) Rage?
    Acts 13:26-33
    Acts 4:23-31
3. Will you ‘kiss the Son’?

(Note: the first few seconds have been re-recorded due to technical problems.)

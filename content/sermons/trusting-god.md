---
title: "Trusting God"
passage: "Ephesians 2:8-9"
date: 2019-10-20T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/BackToBasics/20191020_TrustingGod_DS.mp3"]
audio_duration: "24:24"
audio_size: 6083326
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/10/back-to-basics.jpg"]
series: ["Back To Basics"]
tags: ["Faith", "Salvation", "Grace"]
---

What do you think 'faith' is?  In this sermon, we continue our *Back to Basics* series by considering what it means to trust in God for life now and forever.

# Outline

1. Without grace we are… (vv 1-3)
2. Saved by grace (v 8)
3. Saved through faith (v 8)
4. Not by works, no boasting (v 9)
5. Are you trusting God?

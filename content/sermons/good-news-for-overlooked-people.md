---
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/Judges/20170813_GoodNewsForOverlookedPeople_DS.mp3
audio_duration: 27:26
audio_size: 6812964
date: 2017-08-13 06:12:48+00:00
draft: false
passage: Judges 9
title: Good News for Overlooked People
preachers:
  - Daniel Saunders
series:
  - Crying Out For A Saviour
tags:
  - Healing
  - Hope
  - Judgment
  - Revenge
images:
  - /uploads/2017/06/Judges.jpg
---

1. Abimelech: the man you want as king? (8:33-9:6)

2. Jotham: the voice of judgement (9:7-21)

3. Fire from Abimelech. Fire from Shechem. (9:22-57)

4. Good news for Abimelechs

    2 Corinthians 5:10

    1 Corinthians 1:26-31

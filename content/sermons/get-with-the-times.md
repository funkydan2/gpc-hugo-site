---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171210_GetWithTheTimes_DS.mp3"]
audio_duration: '24:59'
audio_size: 6224403
date: 2017-12-10 02:41:00+00:00
draft: false
passage: Romans 13:8-14
title: Get With the Times!
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Light
- Love
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. What's the time (vv 11-12)

2. Time for love (vv 8-10)

3. Time for light (vv 11-14)

4. Are you with the times?

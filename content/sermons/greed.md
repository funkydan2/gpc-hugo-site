---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/MoneyMatters/20160731_Greed_DS.mp3"]
audio_duration: '28:02'
date: 2016-07-31 07:45:43+00:00
draft: false
passage: 1 Timothy 6:6-19
title: Greed
preachers: ["Daniel Saunders"]
series:
- Money Matters
tags:
- Gambling
- Gospel
- Greed
- Idolatry
- Money
images: ["/uploads/2015/12/Money-Matters.jpg"]
---

1. A Root of all kinds of evil...

    1. What is greed? (1 Timothy 6:9; 3:3)

    2. Our money problem (1 Timothy 6:9-10, 17)

        Luke 12:13-15

        Luke 16:13

        Colossians 3:5

2. How to be really rich (Revelation 3:17-18; 1 Timothy 6:11)

    1. Flee Greed

    2. Pursue God

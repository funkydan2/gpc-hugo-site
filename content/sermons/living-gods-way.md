---
title: "Living God's Way"
passage: "Titus 2:11-13"
date: 2019-10-27T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/BackToBasics/20191027_LivingGodsWay_DS.mp3"]
audio_duration: "27:07"
audio_size: 6737466
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/10/back-to-basics.jpg"]
series: ["Back To Basics"]
tags: ["Grace", "Works", "Eschatology", "Trinity", "Discipleship", "Law", "Godliness"]
---
If God's forgiveness is a free gift, should Christians lives be changed?

# Outline

1. Two reasons for *good works*
   1. The grace of God (vv11-12, 14)
   2. The return of Jesus (v13)
2. Teaching one another (vv1-10)

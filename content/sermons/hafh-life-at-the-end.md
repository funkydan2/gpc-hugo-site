---
title: "Life At The End"
passage: "1 Peter 4:1-11"
date: 2021-08-15T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210815_LifeAt+TheEnd_DS.mp3"]
audio_duration: "23:49"
audio_size: 5946877
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Suffering", "Forgiveness", "Eschatology"]
---
How would knowing you only had a short time change your life?

In this message from 1 Peter we hear how knowing Jesus and his suffering shapes how we live in light of 'the end'.

# Outline
1. Not time for sin (vv1-6)
2. Time for prayer (v7)
3. Time for loving service (vv8-11)

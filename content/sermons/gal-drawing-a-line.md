---
title: "Drawing a Line"
passage: "Galatians 2:11-21"
date: 2022-10-23T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221023_DrawingALine_DS.mp3"]
audio_duration: "31:12"
audio_size: 7718043
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Law","Gospel","Union With Christ"]
---
Legalism is out of line with the gospel. Legalism is about what you have to do to get in line with God. But the gospel proclaims what Jesus has done to justify sinners.

# Outline
1. Out of line (vv11-14)
2. Because…
    1. Faith in Jesus, not Law (vv15-16)
    2. Faith in Jesus doesn’t make sinners (vv17-18)
    3. Faith unites to Christ (vv19-20)
    4. Because we need Jesus’ death! (v21)
3. Keeping in line with the gospel

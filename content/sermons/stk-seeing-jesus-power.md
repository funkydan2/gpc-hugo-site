---
title: "Seeing Jesus' Power"
passage: "Matthew 8:18-34"
date: 2024-07-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240728_SeeingJesusPower_DS.mp3"]
audio_duration: "22:25"
audio_size: 5609698
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/07/SeeingTheKingdom.jpg"]
series: ["Seeing the Kingdom"]
tags: ["Power","Discipleship"]
---
Jesus has power over physical and spiritual chaos—will you follow him?

# Outline
1. Jesus' power over the physical world (vv23-27)
2. Jesus' power over the spiritual world (vv28-34)
3. Following the powerful Son (vv18-22)

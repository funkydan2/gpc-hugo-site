---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170414_UnjustlyPunished_DS.mp3"]
audio_duration: '25:05'
audio_size: 6249607
date: 2017-04-14 04:47:53+00:00
draft: false
passage: Luke 23:44-56
title: Unjustly Punished
preachers: ["Daniel Saunders"]
series:
- Easter
tags:
- Atonement
- Cross
- Easter
- Gospel
images: ["/uploads/2016/03/GoodFriday.jpg"]
---
1. The death of a righteous man (vv44-47)
    1. The sun turns dark
    2. The temple curtain splits
    3. Jesus cries his last words
2. Why this unjust death is 'good' (1 Peter 3:18)
3. Three responses to Jesus (vv48-56)
    1. Recognise
    2. Respect
    3. Repent
4. How will you respond to Jesus?

---
title: The Return of the King
passage: 2 Samuel 17-20
date: 2019-06-16T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190617_TheReturnOfTheKing_DS.mp3
audio_duration: 31:54
audio_size: 7886256
preachers:
  - Daniel Saunders
images:
  - /uploads/2019/04/SWFTTK.jpg
series:
  - Still Waiting For The True King
tags:
  - Forgiveness
---
How do you respond when you are attacked or feel wronged?

As we continue our series in 2 Samuel, we see King David’s love and mercy towards his enemies. His mercy is a shadow of Jesus who loves his enemies and mercifully offers us forgiveness.

# Outline
1. Joab’s Revenge

2. David’s grace

3. Jesus’ way of the cross

    Matthew 26:52

    Matthew 18:23-35

    Romans 13:4

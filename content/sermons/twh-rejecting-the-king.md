---
title: Rejecting the King
passage: Luke 20:1-19
date: 2020-02-09T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200209_RejectingTheKing_DS.mp3
audio_duration: 24:14
audio_size: 6045772
preachers:
  - Daniel Saunders
images:
  - uploads/2020/02/TheWayHome5.jpg
series:
  - The Way Home (Part 5)
tags:
  - Judgment
  - Parable
  - Punishment
---

When Jesus spoke, people saw he had authority. But this was divisive and difficult, especially for religious people. In this passage, we see the final result of rejecting Jesus' kingly authority.

# Outline

1. Jesus’ authority questioned (vv1-8)
2. Jesus’ authority rejected (vv9-15)
3. Christ’s crushing authority? (vv16-19)
4. Will you receive or reject Jesus? (1 Peter 2:4-6)


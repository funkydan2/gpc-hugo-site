---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/MoneyMatters/20160724_Contentment_DS.mp3"]
audio_duration: '27:27'
date: 2016-07-24 09:36:11+00:00
draft: false
passage: 1 Timothy 6:6-10
title: Contentment
preachers: ["Daniel Saunders"]
series:
- Money Matters
tags:
- Contentment
- Gospel
- Money
- Sin
images: ["/uploads/2015/12/Money-Matters.jpg"]
---

1. Why is contentment so hard?

    Genesis 3:4-6

2. Contentment and God

    Matthew 7:7-11

3. How to Learn Contentment

    Philippians 4:10-13

    1 Timothy 6:6-10

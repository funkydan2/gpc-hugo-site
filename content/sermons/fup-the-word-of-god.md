---
title: "The Word of God"
passage: "1 Thessalonians 2:13-16"
date: 2023-10-08T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20231008_TheWordOfGod_DS.mp3"]
audio_duration: "22:12"
audio_size: 5557647
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Gospel","Scripture"]
---
Can you love God but not the Bible? Can you love the Bible and not God?

When the gospel first came to Thessalonica, some received as the Word of God, others rejected it as merely human words. What about you?

# Outline
1. The Rejected Word (vv14-16)
    (Acts 17:1-5; John 5:39-40, 46-47)
2. The Received Word (v13)
3. God’s Word at Work (vv13-14)

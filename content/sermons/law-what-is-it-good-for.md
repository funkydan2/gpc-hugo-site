---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171015_LawWhatIsItGoodFor_DS.mp3"]
date: 2017-10-15 11:26:15+00:00
audio_size: 6786022
draft: false
passage: Romans 7
title: Law, what is it good for?
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Grace
- Law
- Sin
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. Died to the law, alive to God (vv1-6)

2. Though the law isn't the problem, sin is (vv7-25)

3. The law and you

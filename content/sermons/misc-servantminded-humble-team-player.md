---
title: "I Can Be A Servant-minded, Humble, Team Player"
passage: "Philippians 1:27-2:11"
date: 2024-01-07T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20240107_Phil1-2_LW.mp3"]
audio_duration: "31:03"
audio_size: 7681935
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
---
A message from visiting preacher, Leo Woodward.

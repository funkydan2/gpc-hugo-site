---
title: "A Model Church?"
passage: "Acts 2:42-47"
date: 2022-02-13T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220213_AModelChurch_DS.mp3"]
audio_duration: "26:30"
audio_size: 6592990
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Church", "Scripture", "Money", "Generosity", "Hospitality", "Love"]
---
What should be at the centre of a Christian community?

In this message we go back to the original, earliest followers of Jesus, looking at their life together, and how it should shape us.

# Outline
1. Devoted to… (v42)
2. Awe (v43)
3. Love (vv44-46)
4. Gladness (v46)
5. Gospel Growth (v47)

---
title: How to listen like (another) Mary this Christmas
passage: Luke 10:38-42
date: 2024-12-22T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20241222_ListenLikeMary.mp3
audio_duration: 17:33
audio_size: 4444191
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/12/Christmas1.jpg
series:
    - Christmas
tags:
    - Advent
    - Rest
    - Mary
---
A message from our carols service.

# Outline
1. Martha’s complaint
2. Mary’s better choice

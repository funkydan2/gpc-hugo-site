---
title: Gospel Work
passage: Philippians 2:12-30
date: 2024-11-17T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/GospelPartners/20241117_GospelWork_DS.mp3
audio_duration: 27:03
audio_size: 6721998
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/10/GospelPartners.jpeg
series:
    - Gospel Partners
tags:
    - Salvation
    - Works
    - Union With Christ
---
In this message we hear how Jesus powerfully changes his people, so we might shine brightly.

# Outline
1. The gospel and work (vv12-13)
2. Gospel light (vv14-18)
3. The example of gospel-workers (vv19-30)

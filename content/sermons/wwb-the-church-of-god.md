---
title: "The Church of God"
passage: "Hebrews 12:18-29; Exodus 19:1-13"
date: 2020-11-15T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhatWeBelieve/20201115_TheChurchOfGod_DS.mp3"]
audio_duration: "28:20"
audio_size: 7030738
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/10/WWB.jpg"]
series: ["What We Believe"]
tags: ["Church", "Spirit"]
---
Can you know God without having anything to do with a Christian church?

In this message, we consider what it means to say 'I believe...the church.'

# Outline
1. I believe…the church? (Genesis 12:1-3; Ezekiel 37:24-27; 1 Peter 2:4-5)
2. The church
    1. is catholic (Hebrews 12:22-26)
    2. and the communion of saints (1 Corinthians 16:2-3)

> And let us consider how we may spur one another on toward love and good deeds, not giving up meeting together, as some are in the habit of doing, but encouraging one another—and all the more as you see the Day approaching. (Hebrews 10:24–25 NIV)


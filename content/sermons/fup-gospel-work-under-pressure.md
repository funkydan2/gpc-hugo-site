---
title: "Gospel Work Under Pressure"
passage: "1 Thessalonians 2:1-12"
date: 2023-09-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20230917_GospelWorkUnderPressure_DS.mp3"]
audio_duration: "24:17"
audio_size: 6057731
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
#tags: [""]
---
In this message from 1 Thessalonians, we are encouraged to follow the example of those who keep going with gospel work even under pressure.

# Outline
1. Down but not out (vv1-2)
2. Motives Matter (vv3-4)
3. Actions Matter (vv5-8)
4. Examples Matter (vv9-12)

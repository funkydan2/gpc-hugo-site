---
title: "Meal With the King"
passage: "Luke 22:1-38"
date: 2020-03-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200301_MealWithTheKing_DS.mp3"]
audio_duration: "25:04"
audio_size: 6247603
preachers: ["Daniel Saunders"]
images: ["uploads/2020/02/TheWayHome5.jpg"]
series: ["The Way Home (Part 5)"]
tags: ["Lord's Supper"]
---

As Jesus was heading towards the cross he shared with his followers a significant meal. It wasn't just significant because it was their last meal together, but through it, Jesus shows that by dying a criminal's death, he fulfils God's plans to bring in a radically new kingdom.

# Outline

1. The preparations (vv1-13)
2. …for fulfilment (vv14-23)
3. Reversals (vv24-38)
4. Remembering the King

(Note: there is a slight ringing sound occasionally throughout this recording.)
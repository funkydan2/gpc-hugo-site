---
title: "Living Hope"
passage: "1 Peter 1:3-9"
date: 2024-03-31T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20240331_LivingHope_DS.mp3"]
audio_duration: "16:27"
audio_size: 4179732
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/03/EasterSunday.jpg"]
series: ["Easter"]
tags: ["Resurrection","Hope","Suffering"]
---
Our Resurrection Sunday message.

# Outline
1. Have you got living hope? (v3)
2. Do you have a new future? (vv4-5)
3. Even in suffering? (vv6-7)
4. A certain, living, hope.

(Due to technical issues the first few seconds of this message were re-recorded.)

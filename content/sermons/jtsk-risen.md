---
title: "Risen"
passage: "Mark 16"
date: 2022-04-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20220417_Risen_DS.mp3"]
audio_duration: "22:15"
audio_size: 5571755
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Easter", "Resurrection"]
---
As light dawns on the third day, the future breaks into the present with fear and alarm, but because Jesus is alive we need not be afraid.

# Outline
1. Light dawns (vv1-3)
2. An alarming sight! (vv4-7)
3. Trembling and Afraid (v8)

This sermon covers Mark 16:1-8. In the message there is a brief mention of the question of the ending of Mark. [This is a short introduction to the questions around Mark's ending](https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/MarkEnding.pdf)

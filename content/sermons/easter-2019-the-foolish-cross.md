+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20190407_TheFoolishCross_DS.mp3"]
audio_duration = "26:56"
audio_size = 6692073
date = "2019-04-07T09:30:00+10:00"
images = ["/uploads/2019/04/TheFoolishCross.jpg"]
passage = "1 Corinthians 1:18-2:5"
preachers = ["Daniel Saunders"]
series = ["Easter"]
tags = ["Cross", "Easter"]
title = "The Foolish Cross"

+++
In the lead-up to Easter, we're reflecting on the foolish cross and the empty tomb from 1 Corinthians 1 and 15.

## Talk Outline

1. The cross is foolish and weak? (1:18-25)

2. The church is foolish and weak? (1:26-31)

3. So stick with the foolish and weak message (2:1-5)

4. Knowing Christ Crucified

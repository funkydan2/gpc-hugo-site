---
title: "What is a Christian?"
passage: "Galatians 4"
date: 2022-11-20T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221120_WhatIsAChristian_PS.mp3"]
audio_duration: "27:50"
audio_size: 6912270
preachers: ["Phil Stolk"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Adoption","Grace"]
---
In grace, God invites us into his family to receive the blessing of being in his family. It's not due to anything we do (this leads to slavery) but in receiving God's gift.

# Outline
1. Christians are redeemed slaves
2. Christians are adopted children
3. Christians are heirs of God

---
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/Judges/20170716_TheDownwardSpiral_DS.mp3
audio_duration: 27:28
audio_size: 6820822
date: 2017-07-16 06:02:43+00:00
draft: false
passage: Judges 1:1-3:6
title: The Downward Spiral
preachers:
  - Daniel Saunders
series:
  - Crying Out For A Saviour
tags:
  - Grace
  - Judgment
images:
  - /uploads/2017/06/Judges.jpg
---

1. The story so far (2:1-2a)
2. God continues to be faithful (1:1-18)
3. ... but God's people fail (1:19-34)
4. What went wrong?
    1. Israel didn't pass the baton (2:6-10)
    2. The downward spiral (2:11-19)
5. Judges and us
    1. What does it show about people?
    2. What does it show about God?
    3. What does it show about how God works in Jesus?

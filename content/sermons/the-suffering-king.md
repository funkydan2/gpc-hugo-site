+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20190120_TheSufferingKing_DS.mp3"]
audio_duration = "25:21"
audio_size = 6312892
date = "2019-01-20T10:00:00+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 21-26"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = ["Suffering"]
title = "The Suffering King"

+++
1. In the cave (1 Samuel 21-23)

2. Entrusting to God (1 Samuel 24, 26)

3. Jesus, God's suffering King (Luke 24:26; 4:5-8)

4. God's suffering people (1 Peter 2:21-24)

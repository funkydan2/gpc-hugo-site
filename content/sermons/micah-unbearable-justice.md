---
title: "Unbearable Justice"
passage: "Micah 1"
date: 2021-05-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Micah/20210502_UnbearableJustice_DS.mp3"]
audio_duration: "27:18"
audio_size: 6784996
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/04/Micah.jpg"]
series: ["Micah"]
tags: ["Punishment", "Justice", "Love", "Lament"]
---
How does God's justice make you feel?  In this message, we hear God's strong justice and the surprising way God feels about it.

# Outline
1. Micah of Moresheth (v1)
2. God comes down (vv2-4)
    1. … against Samaria (vv5-7)
    2. …and Jerusalem (vv9-16)
3. Justice through tears (v8)

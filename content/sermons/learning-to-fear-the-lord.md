---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joshua/20161030_LearningToFearTheLord_DS.mp3"]
audio_duration: '29:25'
date: 2016-10-30 03:41:23+00:00
draft: false
passage: Joshua 7
title: Learning to Fear the LORD
preachers: ["Daniel Saunders"]
series:
- Fear and Serve the Lord
tags:
- Punishment
- Sin
images: ["/uploads/2015/12/Joshua.jpg"]
---

1. Achan's hidden sin (7:1)

2. ...has devastating results for Israel (7:2-15)

3. ...and Achan's family (7:16-26)

4. Learning to fear the LORD

    1. Sin is Serious (Acts 5; 1 Corinthinas 11:27-30; 2 Corinthians 11:24; 2 Timothy 3:12)

    2. Grace is Enormous

> Repent, then, and turn to God, so that your sins may be wiped out (Acts 3:19a NIV)

---
title: "Talking to God"
passage: "1 Thessalonians 5:16-18; Philippians 4:4-7"
date: 2019-11-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/BackToBasics/20191110_TalkingToGod_DS.mp3"]
audio_duration: "24:16"
audio_size: 6051386
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/10/back-to-basics.jpg"]
series: ["Back To Basics"]
tags: ["Prayer", "Anxiety"]
---

In his gracious kindness, God invites believers to speak to him. This is a great privilege, but one many Christians find hard. In this message, we look at two Bible passages about *Talking to God*.

# Outline

1. Pray…continually
2. Why is prayer hard?
3. How to talk to God
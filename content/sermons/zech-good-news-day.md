---
title: "Good News Day?"
passage: "Zechariah 12-13"
date: 2023-07-23T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230723_GoodNewsDay_DS.mp3"]
audio_duration: "24:17"
audio_size: 6058738
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Atonement","Cross","Eschatology"]
---
Do you want the good news or the bad news? In this message from the final prophecy in Zechariah we hear the good news of God's salvation coming through suffering.

# Outline
1. The Day of Suffering and Salvation
2. The Day of Piercing and Striking (12:9-13:1; 13:7-9)
3. Looking on the one who was pierced (Mark 14:27; John 19:37; Revelation 1:7)

---
title: "Gospel Logic"
passage: "1 Corinthians 1:18-25"
date: 2023-10-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20231001_GospelLogic_PB.mp3"]
audio_duration: "23:22"
audio_size: 5840280
preachers: ["Peter Bloomfield"]
images: ["/uploads/2023/09/1Cor1.jpg"]
series: ["Miscellaneous"]
tags: ["Atonement","Cross"]
---
History records the facts of the cross—that Jesus of Nazareth was executed by the authority of the Roman governor. But why? Why do Christians care? Why should I care? What is the logic of the gospel?

# Outline
1. The bare facts
2. The interpreted facts

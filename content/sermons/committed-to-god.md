---
title: "Committed to God"
passage: "Acts 18-20"
date: 2020-05-17T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200517_CommittedToGod_DS.mp3"]
audio_duration: "21:27"
audio_size: 5376758
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/05/Ephesians_OTEYH.jpg"]
series: ["Open The Eyes Of Your Heart"]
tags: ["Church", "Mission"]
---

What, or who, is the *church of God*? As we begin a series in Ephesians, we're looking at the *back story* of how the gospel came to Ephesus and the church was planted.

# Outline

1. A life worth watching (20:17-21; also 18:19-26; 19:1-10)
2. Shepherding the *church* of God (20:28; also 19:32, 41)
3. …to safety and growth (20:29-32)


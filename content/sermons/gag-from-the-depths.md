---
title: "From the Depths"
passage: "Jonah 1:17-2:10"
date: 2020-12-13T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Jonah/20201213_FromTheDepths_DS.mp3"]
audio_duration: "26:13"
audio_size: 6523955
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/12/Jonah.jpg"]
series: ["God's Abounding Grace"]
tags: ["Prayer", "Repentance", "Salvation", "Grace"]
---
As we hear Jonah's prayer from the belly of a big fish, Jonah is a mirror for us and shows that God's grace *precedes* and *exceeds* our repentance.

# Outline
1. God’s Deep Salvation (1:17)
2. Jonah’s Shallow Prayer (2:1-9)
3. God’s Response (2:10)
4. Salvation Comes From the LORD
5. The Sign of Jonah (Matthew 12:39-40)
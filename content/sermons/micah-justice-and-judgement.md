---
title: "Justice and Judgment"
passage: "Micah 6:9-16"
date: 2021-06-13T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Micah/20210613_JudgementAndJustice_DS.mp3"]
audio_duration: "28:09"
audio_size: 6986188
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/04/Micah.jpg"]
series: ["Micah"]
tags: ["Wisdom", "Judgment", "Greed"]
---
Where do you think real wisdom is found?

In this message from Micah, we hear how true wisdom is found, not in getting away with lies and deception, but fearing God.

# Outline
1. Wise fear (v9)
2. Injustice and Unfaithfulness (vv10-12)
3. Frustrated to futility (vv13-15)
4. Walking away from God (v16)
5. Fearing the God of Judgment and Justice

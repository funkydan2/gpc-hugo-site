---
title: "What Do You Expect?"
passage: "Acts 23:31–26:32"
date: 2023-03-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230319_WhatDoYouExpect_DS.mp3"]
audio_duration: "25:48"
audio_size: 6423827
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Persecution"]
---
What should followers of Jesus expect out of life? In this message we see Paul continuing to speak of Jesus as he stands trial—taking up his cross as he follows Jesus.

# Outline
1. Not troublemakers (Acts 24)
2. Making the most (Acts 26)
3. Take up your cross

---
title: "Transforming Hope"
passage: "1 Peter 1:13-2:3"
date: 2021-07-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210711_TransformingHope_DS.mp3"]
audio_duration: "27:31"
audio_size: 6835868
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Change", "Love", "Holy", "Holiness"]
---
How does Jesus transform lives?

In this message from 1 Peter, we hear how our holy God changes his people to love like him.

# Outline
1. Transforming hope (1:13-16)
2. Two truths for transformation (1:17-21)
3. Transformed to love (1:22-25)
4. Transformed taste (2:1-3)

---
title: "Good Different"
passage: "Matthew 5:1-16"
date: 2024-02-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StaySalty/20240211_GoodDifferent_DS.mp3"]
audio_duration: "24:32"
audio_size: 6118041
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/02/StaySalty.jpg"]
series: ["Stay Salty"]
tags: ["Blessing","Discipleship","Persecution","Mission"]
---
In this message we sit at Jesus' feet as he teaches us what the 'good life' is like in his kingdom, how his people will be 'good different'.

# Outline
1. Sitting at Jesus’ feet (vv1-2)
2. The blessed life (vv3-12)
3. Who is the blessed one?
4. Standing out (vv13-16)
5. Staying Salty

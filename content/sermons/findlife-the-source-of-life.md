---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180128_SourceOfLife_DS.mp3"]
audio_duration: '25:02'
audio_size: 6236096
date: 2018-01-28 10:15:33+00:00
draft: false
passage: John 1:1-18
title: The Source of Life
preachers: ["Daniel Saunders"]
series:
- Find Life
tags:
- Christ
- Creation
- Jesus
- Life
- Trinity
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. The source of everything (vv 1-5)

2. who is rejected and received (vv 6-13)

3. when he comes to us (vv 14-18)

4. Jesus, the source of your life

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20171225_ChristmasForStrangers_DS.mp3"]
audio_duration: '16:40'
audio_size: 4229323
date: 2017-12-25 02:29:34+00:00
draft: false
passage: Matthew 2:1-12
title: Christmas for Strangers
preachers: ["Daniel Saunders"]
series:
- Strange Christmas Things
tags:
- Christmas
images: ["/uploads/2017/11/Strange-Christmas-Things.jpg"]
---

1. A strange expedition (vv 1-10)

2. ... who came to worship (vv 11-12)

3. Come let us worship, Jesus the King

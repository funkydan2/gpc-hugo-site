---
title: "Fruitful Faith"
passage: "2 Peter 1:1-15"
date: 2023-01-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/2Peter/20230101_FruitfulFaith_DS.mp3"]
audio_duration: "24:13"
audio_size: 6043222
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/2Peter.jpg"]
series: ["Grow in Grace"]
tags: ["Godliness","Grace"]
---
Are you coasting in the Christian life?

In this message from 2 Peter, we hear the Apostle urge believers to *make every effort* to have fruitful faith.

# Outline
1. Peter’s Last Words (vv1-2, 12-15)
2. Making Every Effort (vv3-11)

---
title: "Welcomed In"
passage: "Acts 8:26-40"
date: 2022-07-24T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220724_WelcomedIn_DS.mp3"]
audio_duration: "30:25"
audio_size: 7529032
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Salvation","Baptism"]
---
Do you sometimes feel excluded or unwelcome?

In this message, we hear how, through Jesus, God welcomes those who would otherwise be excluded.

# Outline
1. An Outsider (vv26-29)
2. Who is God’s lamb? (vv30-35)
3. Who can the Lamb save? (vv36-40)

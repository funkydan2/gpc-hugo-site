---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20180408_LoveOneAnother_DS.mp3"]
audio_duration: '25:04'
audio_size: 6244002
date: 2018-04-08 09:30:00+10:00
passage: John 13
title: Love One-Another
preachers: ["Daniel Saunders"]
images:
- "/uploads/2016/02/sermons.jpg"
series:
- Miscellaneous
tags:
- Church
- Love
- Vision
---
1. Is our church loving?

2. Why love one-another?

    John 13:33-35

    John 15:12-17

    1 John 4:19-21

3. How to love one-another

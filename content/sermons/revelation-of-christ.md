---
title: "Revelation of Christ"
passage: "Ephesians 3:1-13"
date: 2020-06-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200621_RevelationOfChrist_DS.mp3"]
audio_duration: "21:16"
audio_size: 5333097
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/05/Ephesians_OTEYH.jpg"]
series: ["Open The Eyes Of Your Heart"]
tags: ["Evangelism", "Mission", "Union With Christ"]
---

Where do you go to uncover the mystery of God?

In this message, we hear how God's glorious plan, now revealed in Christ Jesus, gives hope when we're discouraged.

# Outline

1. What is God doing? (v1)
2. God is revealing the mystery (vv2-5)
3. of Jew and Gentile together in Christ (v6)
4. through gospel preaching (vv7-9)
5. for the sake of glory (vv10-13)


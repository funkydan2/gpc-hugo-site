---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/2Thessalonians/20170618_WhatIsOurWork_DS.mp3"]
audio_duration: '29:30'
audio_size: 7309637
date: 2017-06-18 03:49:18+00:00
draft: false
passage: 2 Thessalonians 3
title: What is our work?
preachers: ["Daniel Saunders"]
series:
- Hold Firm
tags:
- Prayer
- Work
images: ["/uploads/2017/05/Hold-Firm-2-Thessalonians.jpg"]
---

1. God is at work
    1. as the gospel progresses (vv1-2)
    2. as believers are established (vv3-5, 16- 18)
2. Believers are to work (vv6-15)
3. Working for Jesus

---
title: "Life's Most Important Decision"
passage: "John 1:1-18"
date: 2024-09-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20240922_LifesMostImportantDecision_LW.mp3"]
audio_duration: "0:27:54"
audio_size: 6928653
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
---
A sermon on John 1:1-18 by guest preacher, Leo Woodward.

# Outline
1. Introducing the Word (Jesus)
2. The Role of John the Baptist
3. The Word Becomes Flesh
4. Responses to the Word
5. Life's Most Important Decision

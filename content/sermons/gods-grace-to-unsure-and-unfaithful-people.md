---
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/Judges/20170806_GodsGraceToUnsureAndUnfaithfulPeople_DS.mp3
audio_size: 7788004
audio_duration: 31:29
date: 2017-08-06 07:06:38+00:00
draft: false
passage: Judges 6-8
title: God's Grace to Unsure and Unfaithful People
preachers:
  - Daniel Saunders
series:
  - Crying Out For A Saviour
tags:
  - Grace
  - Guidance
  - Idolatry
  - Judgment
  - Patience
images:
  - /uploads/2017/06/Judges.jpg
---

1. Another time around (6:1-10)

2. Gideon

    1. an unsure leader (6:11-16, 36-40; 7:9-15a)

    2. a spiteful leader (8:4-21)

    3. a faithless leader (8:21-32)

3. God is

    1. a mighty warrior (7:1-8, 17-23)

    2. gracious and patient beyond measure!

4. God's gracious patience means salvation

    2 Peter 3:3-9

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/2Thessalonians/20170611_WhenIsJesusReturning_DS.mp3"]
audio_duration: '22:58'
audio_size: 5743676
date: 2017-06-11 06:37:53+00:00
draft: false
passage: 2 Thessalonians 2:1-12
title: When is Jesus Returning?
preachers: ["Daniel Saunders"]
series:
- Hold Firm
images: ["/uploads/2017/05/Hold-Firm-2-Thessalonians.jpg"]
---

1. Keep Calm (vv1-2)
2. Don't be Deceived (vv3-5)
3. Christ is in Control (vv6-12)

    Restrainer

    Destroyer

    Deceiving miracles...

    ...are God's doing!

4. Keep Calm, Christ is in Control

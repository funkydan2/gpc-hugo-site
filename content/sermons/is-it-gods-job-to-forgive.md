---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Hosea/20160911_IsItGodsJobToForgive_DS.mp3"]
audio_duration: '25:25'
date: 2016-09-11 04:39:03+00:00
draft: false
passage: Hosea 8:1-11:11
title: Is it God's job to forgive?
preachers: ["Daniel Saunders"]
series:
- Unbreakable Love
tags:
- Forgiveness
- Love
- Wrath
images: ["/uploads/2015/12/Hosea.jpg"]
---
1. More accusations against Israel (Hosea 8-10)
2. ...but God won't give Israel up (Hosea 11:1-11)
3. Where anger and sympathy meet (1 Peter 3:18)

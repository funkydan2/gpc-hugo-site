---
title: "Knowing Genuine Faith"
passage: "1 Thessalonians 1"
date: 2023-09-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20230910_KnowingGenuineFaith_DS.mp3"]
audio_duration: "24:29"
audio_size: 6108590
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Thankfulness","Idolatry"]
---
What does genuine Christian faith look like?

In this message from 1 Thessalonians 1, we see the marks of genuine faith which were shown by the believers in Thessalonica.

# Outline
1. Genuine Thanksgiving (vv1-3)
2. Genuine Reception of the Gospel (vv4-8)
3. Genuine Conversion (vv9-10)
4. Genuine Christianity

---
title: "Seeing God's Glory"
passage: "Haggai 2:1-9"
date: 2022-01-09T10:00:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Haggai/20220109_Haggai2v1-9_RP.mp3"]
audio_duration: "32:04"
audio_size: 7927180
preachers: ["Rayk Platzek"]
images: ["/uploads/2016/02/podcast_cover.jpg"]
series: ["Haggai"]
tags: ["Temple", "Glory"]
---
How do you see God's glory?

# Outline
1. God's glory in the past (2:1-3)
2. God's glory in the present (2:4-5)
3. God's glory in the future (2:6-9)

Due to flooding and road closures this sermon was recorded over Zoom. The final few minutes were re-recorded due to technical issues.

---
title: "Stand Firm"
passage: "Ephesians 6:10-24"
date: 2020-10-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20201004_StandFirm_DS.mp3"]
audio_duration: "27:59"
audio_size: 6948192
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Gospel", "Prayer"]
---
If Jesus is the risen and ruling king of all creation, why is living in his new life a struggle? In this message, we consider Christ's victory and our struggle.

# Outline

1. Christ’s Victory (v10)
2. Our Struggle (vv11-12)
3. God’s Gospel Armour (vv13-17)
4. Gospel Action (vv18-20)
5. Gospel Encouragement (vv21-23)

---
title: "Wisdom and Words"
passage: "Proverbs"
date: 2022-06-26T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheVoiceOfWisdom/20220626_WisdomandWords_DS.mp3"]
audio_duration: "28:06"
audio_size: 6975844
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Proverbs.png"]
series: ["The Voice of Wisdom"]
tags: ["Wisdom"]
---
Small words can have a big impact, and all too often they can lead to hurt and trouble.

In this message from Proverbs we heard about the most dangerous part of our bodies—the tongue.

# Outline
1. A word in time (Proverbs 26:4-7)
2. The trouble with words (Proverbs 26:17-28)
3. Speaking from the heart (Luke 6:43-45)

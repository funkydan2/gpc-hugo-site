---
title: Filled Full
passage: Colossians 1:19
date: 2019-12-25T08:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20191225_FilledFull_DS.mp3"]
audio_duration: '15:48'
audio_size: "4018635"
preachers:
- Daniel Saunders
images:
- "/uploads/2019/12/Christmas2019.jpg"
series:
- Christmas 2019
tags:
- Incarnation
- Reconciliation
- Peace
- Christmas

---
At Christmas we celebrate that the *fullness of God* was in Christ, born to bring us peace with God.

This is our 2019 Christmas Day message.

# Outline

1. Jesus is the fullness of God
2. Who came to bring peace

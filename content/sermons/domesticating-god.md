+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20180930_DomesticatingGod_DS.mp3"]
audio_duration = "27:04"
audio_size = 6723537
date = "2018-09-30T15:37:03+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 4-6"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = []
title = "Domesticating God"

+++
1. God on a leash? (1 Samuel 4)

2. God in a cage? (1 Samuel 5)

3. Who can stand? (1 Samuel 6)

4. Domesticating God?

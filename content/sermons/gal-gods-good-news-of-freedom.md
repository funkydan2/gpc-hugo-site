---
title: "God's Good News of Freedom"
passage: "Galatians 1:11-2:10"
date: 2022-10-09T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221009_GodsGoodNewsOfFreedom_DS.mp3"]
audio_duration: "26:15"
audio_size: 6532177
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Gospel","Good News","Leaders"]
---
How do we know if the gospel we hear is the true gospel?

# Outline
1. The gospel (good news) is God’s (1:11-24)
2. The gospel is for freedom (2:1-5)
3. The gospel is for the world (2:6-10)

# Paul's Biography
In this passage, Paul retells a section of his life story which intersects with what we read in Acts. The table below shows how Galatians and Acts fits together:

|          Event         |    Acts reference    | Galatians reference |
|:----------------------:|:--------------------:|:-------------------:|
|       Conversion       |      Acts 9:1-19     |  Galatians 1:15-16  |
|  Three years in Arabia |    after Acts 9:25   |    Galatians 1:17   |
|  First Jerusalem visit |     Acts 9:26-31     |  Galatians 1:18-19  |
| Second Jerusalem visit | Acts 11:27-30; 12:25 |   Galatians 2:1-10  |

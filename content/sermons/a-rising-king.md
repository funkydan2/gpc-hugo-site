---
title: A Rising King
passage: 2 Samuel 1-4
date: 2019-04-28T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190428_ARisingKing_DS.mp3
audio_duration: 29:12
audio_size: 7238237
preachers:
  - Daniel Saunders
images:
  - /uploads/2019/04/SWFTTK.jpg
series:
  - Still Waiting For The True King
tags:
  - Biblical Theology
  - Leadership
---
Proud King Saul is dead and David has been anointed as king. Is he the king we’ve been waiting for?

# Outline
1. Dying by the sword (2 Samuel 2:1)

2. The backstory

3. Saul Falls, David Rises

    2 Samuel 1:1-2

    2 Samuel 1:15-16

    2 Samuel 2:1

    2 Samuel 1:19

    2 Samuel 9:7, 11

4. Kingdom Habits

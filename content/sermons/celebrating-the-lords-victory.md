---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joshua/20161113_CelebratingTheLordsVictory_DS.mp3"]
audio_duration: '30:11'
date: 2016-11-13 05:06:39+00:00
draft: false
passage: Joshua 10-11
title: Celebrating the LORD's Victory
preachers: ["Daniel Saunders"]
series:
- Fear and Serve the Lord
tags:
- Conquest
- Idolatry
- Judgment
- Punishment
images: ["/uploads/2015/12/Joshua.jpg"]
---

1. The LORD Wins!

2. God and Genocide

    * War on Idolatry

        Deuteronomy 20:16-18

        Colossians 3:5-10

3. War and Punishment

    Genesis 15:13-16

    2 Corinthians 5:21

    Revelation 19:11-16

    2 Peter 3:9

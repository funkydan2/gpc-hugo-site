---
title: "True Sacrifice and True Priest"
passage: "Malachi 1:6-2:9"
date: 2023-11-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Malachi/20231119_TrueSacrificeAndTruePriest_DS.mp3"]
audio_duration: "24:02"
audio_size: 6000406
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/11/HalfHearted.png"]
series: ["Half Hearted"]
tags: ["Sacrifice","Priest"]
---
Will God accept second-best from his people and priests?

In this message, we hear God's response to his people's disdain for him—ultimately a response of grace through Jesus, our great high priest and perfect sacrifice.

# Outline
1. Giving Your Worst (1:6-14)
2. Teaching the Worst (2:1-9)
3. The Best Priest and Perfect Sacrifice (Hebrews 7:23-28)

---
title: "The Crucified and Risen Son"
passage: "Acts 3:11-26"
date: 2020-11-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhatWeBelieve/20201101_TheCrucifiedAndRisenSon_DS.mp3"]
audio_duration: "0:29:04"
audio_size: 7208096
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/10/WWB.jpg"]
series: ["What We Believe"]
tags: ["Resurrection", "Cross", "King"]
---
For 2000 years, Christians have said that Jesus was crucified, yet rose again, is now ruling, and will return. This is a dangerous truth as it means our life has meaning and will be called to account. This is a dangerous truth because it gives hope and meaning in Christ.

# Outline
1. Crucified (Acts 3:13-15, 1 Peter 3:18)
2. Risen (Acts 3:15)
3. Ruling (Ephesians 1:19-23; Philippians 2:6-11; Acts 3:16-19)
4. Returning (Acts 3:20-23)
5. A dangerous truth? (Acts 4:1-4)

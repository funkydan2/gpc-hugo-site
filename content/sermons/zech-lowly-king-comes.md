---
title: "The Lowly King Comes"
passage: "Zechariah 9-10"
date: 2023-07-09T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230709_TheLowlyKingComes_DS.mp3"]
audio_duration: "24:26"
audio_size: 6095437
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Cross","Victory","Glory"]
---
When Jesus enters Jerusalem on a 'young donkey,' what do you see? When Jesus is on the cross, what do you see?

In Zechariah 9-10, we're taken *behind the scene* to see how these seemingly unimpressive moments reveal the glory of Christ.

# Outline
1. Look at this man on a donkey (John 12:12-14)
2. Look at this man on a cross (John 19)
3. Seeing behind the scene (Zechariah 9-10)
4. Look to Jesus!

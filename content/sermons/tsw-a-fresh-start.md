---
title: "A Fresh Start"
passage: "Acts 3"
date: 2022-02-20T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220220_AFreshStart_DS.mp3"]
audio_duration: "25:49"
audio_size: 6426223
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Repentance", "Forgiveness", "Healing"]
---
There are some things we wish we could do again—have a 'fresh start'—where our mistakes and failings are gone and we get to start again.

In this message on Acts 3 we see and hear of God's power to give a fresh start.

# Outline
1. The sign (vv1-10)
2. Whose power? (vv11-16)
3. The promise (vv17-21)
4. Fulfilled (vv22-26)

+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190303_AgainstHypocrisy_DS.mp3"]
audio_duration = "28:24"
audio_size = 7043289
date = "2019-03-03T09:30:00+10:00"
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 11:37-12:12"
preachers = ["Daniel Saunders"]
series = ["The Way Home (Part 3)"]
tags = []
title = "Against Hypocrisy"

+++
1. Dirty on the inside (11:37-54)
2. and it's everyone's problem (12:1-12)
3. Good news for hypocrites!

(Unfortunately there is some distortion in this recording.)

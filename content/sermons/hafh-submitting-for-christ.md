---
title: "Submitting for Christ"
passage: "1 Peter 2:11-25"
date: 2021-07-25T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210725_SubmittingForChrist_DS.mp3"]
audio_duration: "28:04"
audio_size: 6968753
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Righteousness", "Authority", "Government", "King", "Work", "Persecution", "Suffering"]
---
How should followers of Jesus relate to government and other 'authorities'? If Jesus is the eternal king, how does that change things?

This question is big at the moment, as we've all experienced new and regularly changing rules and instructions from those who govern us. What direction does the Bible give for us today?

# Outline
1. Living to God’s glory (vv11-12)
2. Submitting to government (vv13-17)
3. Submitting to ‘masters’ (vv18-20)
4. Because Christ is our suffering shepherd (vv21-25)

(In this message [this sermon, 'Serving the Lord']({{< ref "litl-serving-the-lord.md" >}}) is referenced.)

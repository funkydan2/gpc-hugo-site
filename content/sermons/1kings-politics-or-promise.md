---
title: "Politics or Promise?"
passage: "1 Kings 1-2"
date: 2024-04-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/1Kings/20240414_PoliticsOrPromise_DS.mp3"]
audio_duration: "25:32"
audio_size: 6361182
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/04/1Kings.png"]
series: ["1 Kings"]
tags: ["Promises","King"]
---
As the book of 1 Kings begins, we meet a king on his deathbed--and things go down from there. In a time of decline and despair, when people are making political alliances, how do we see God at work? What's really in control--politics or God's promises?

# Outline
1. Introducing Kings
2. Who Will Be King? (1 Kings 1)
3. The Kingdom Established (1 Kings 2)
4. Politics or Promise?
5. The Established Kingdom of David’s Son (Romans 1:1-4)

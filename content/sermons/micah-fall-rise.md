---
title: "Though I Fall, I Will Rise"
passage: "Micah 7"
date: 2021-06-20T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Micah/20210620_FallRise_DS.mp3"]
audio_duration: "28:17"
audio_size: 7019883
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/04/Micah.jpg"]
series: ["Micah"]
tags: ["Judgment", "Lament", "Forgiveness"]
---
God's big story is of *death and resurrection*, falling and rising. This means that even at times of distress, in God there is hope.

This talk is the final message in our series '[What Does the LORD Require?](/series/micah/)'.

# Outline
1. The miserable situation (vv1-6)
2. so, I wait for the Lord (vv7-13)
3. for restoration (vv14-17)
4. Who is like our God‽ (vv18-20)

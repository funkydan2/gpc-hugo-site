---
title: "New Relationships"
passage: "Colossians 3:18-4:1"
date: 2019-07-14T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20190714_NewRelationships_DS.mp3"]
audio_duration: "29:23"
audio_size: 7280872
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/06/ONLIC.jpg"]
series: ["Our New Life in Christ"]
tags: ["Union With Christ", "Marriage", "Family", "Parenting", "Work"]
---

When we have new life in Christ, all our relationships are changed by submitting to our loving Lord.

# Outline

1. Marriage
2. Parenting
3. Slavery
4. Our New Life in Christ

(The recording at church stopped before the end. The final few minutes were re-recorded later. Unfortunately, this means they sound quite different.)

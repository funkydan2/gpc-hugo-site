---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20171231_WeepingInBethlehem_DS.mp3"]
audio_duration: '24:23'
audio_size: 6081035
date: 2017-12-31 04:28:50+00:00
draft: false
passage: Matthew 2:13-23
title: Weeping in Bethlehem
preachers: ["Daniel Saunders"]
series:
- Strange Christmas Things
tags:
- Christmas
- Persecution
- Refugee
images: ["/uploads/2017/11/Strange-Christmas-Things.jpg"]
---

1. The rescue of God's Son (vv 13-15)
2. from Herod's fearful fury (vv 16-23)
3. 'Herods' are still furious

    Matthew 5:11-12; 24:9

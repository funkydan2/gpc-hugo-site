---
title: "The Good, Gentle, Greater Kingdom"
passage: "Matthew 12:1-21"
date: 2024-09-29T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240929_GGGKingdom_DS.mp3"]
audio_duration: "25:36"
audio_size: 6373798
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/07/SeeingTheKingdom.jpg"]
series: ["Seeing the Kingdom"]
tags: ["Rest","Sabbath","Temple","King"]
---
Where can we find true rest?

When Jesus is confronted by the Pharisees for not 'keeping' the Sabbath, he responds by showing that he is the greater Sabbath, King, and Temple.

# Outline
1. Jesus is Greater (vv1-8)
2. Jesus is Good (vv9-14)
3. Jesus is Gentle and Just (vv15-21)

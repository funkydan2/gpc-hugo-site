---
title: "Recognising the King"
passage: "Luke 19:28-48"
date: 2020-02-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200202_RecognisingJesus_DS.mp3"]
audio_duration: "22:33"
audio_size: 5642504
preachers: ["Daniel Saunders"]
images: ["uploads/2020/02/TheWayHome5.jpg"]
series: ["The Way Home (Part 5)"]
tags: ["King", "Peace", "Temple"]
---

Where do you look for peace?

In this message we see Jesus, the only hope for peace, humbly entering the royal city. But as he comes, the question is will we receive him and his peace?

# Outline

1. Prepare the way (vv28-35)
2. Three responses to Jesus (vv36-40)
3. The tears of the king (vv41-44)
4. Cleaning God’s house (vv45-48)
5. Will you recognise Jesus as king?

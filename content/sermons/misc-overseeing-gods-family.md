---
title: "Overseeing God's Family"
passage: "Titus 1"
date: 2023-09-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20230903_OverseeingGodsFamily_DS.mp3"]
audio_duration: "26:22"
audio_size: 6558020
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/09/Titus1.jpg"]
series: ["Miscellaneous"]
tags: ["Elders","Leadership","Gospel","Character","Godliness"]
---
How does Christ oversee his family, his church? 

In this message from Titus 1 we hear God's requirements for elders in his church.

# Outline
1. Elders are Examples
2. Elders Manage God’s Household
3. Elders Protect the Truth

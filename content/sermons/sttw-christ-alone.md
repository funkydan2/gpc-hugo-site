---
title: "Christ Alone"
passage: "Acts 15:1-35"
date: 2022-09-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220911_ChristAlone_DS.mp3"]
audio_duration: "29:52"
audio_size: 7451929
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Gospel","Law","Faith"]
---
When you add something to faith in Jesus it's really subtraction.

In this final message from this series in Acts, we see how the earliest churches respond when the good news of faith alone is under threat from legalistic false teachings.

# Outline
1. Christ Plus… (vv1-5)
2. Remembering what God’s done and promised (vv6-18)
3. Christ Alone (vv19-35)

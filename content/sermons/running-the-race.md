---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20160821_RunningTheRace_DS.mp3"]
audio_duration: '29:56'
date: 2016-08-21 09:06:09+00:00
draft: false
passage: Hebrews 12:1-17
title: Running the Race
preachers: ["Daniel Saunders"]
series:
- Miscellaneous
tags:
- Endurance
- Perseverance
images: ["/uploads/2015/12/Hebrews12.jpg"]
---
1. Head Up (vv1-2)
2. Train Hard (vv3-11)
3. Get Fit (vv12-17)
4. Keep Running

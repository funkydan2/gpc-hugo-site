---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20170917_AlwaysReforming_DS.mp3"]
audio_duration: '28:43'
audio_size: 7121270
date: 2017-09-17 09:30:00+00:00
passage: 2 Timothy 3:14-17; Mark 7:1-13
title: Always Reforming!
preachers: ["Daniel Saunders"]
series:
- Miscellaneous
tags:
- Scripture
images:
- "/uploads/2016/02/sermons.jpg"
---
1. Scripture is sufficient (2 Timothy 3:14-16)
    1. ... because God speaks
    2. ... because God speaks about Jesus
    3. ... for knowing Jesus and living for him
2. Scripture + ?

3. Reformed by the Scriptures

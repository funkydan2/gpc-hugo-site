---
title: "Long Live the King"
passage: "Matthew 28"
date: 2023-04-09T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20230409_LongLiveTheKing_DS.mp3"]
audio_duration: "20:13"
audio_size: 5083558
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/03/EasterSunday.jpg"]
series: ["Easter"]
tags: ["Resurrection"]
---
In this Resurrection Sunday message we hear what it means for Jesus to be the king who invites us to know him by name, calls us 'brothers and sisters,' and promises to be with us always.

# Outline
1. King Jesus is risen (vv1-10)
2. Mocking the king (vv11-15)
3. King Jesus for the nations (vv16-20)

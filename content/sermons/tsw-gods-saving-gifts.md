---
title: "God's Saving Gifts"
passage: "Acts 2:1-41"
date: 2022-02-06T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220206_GodsSavingGifts_DS.mp3"]
audio_duration: "29:05"
audio_size: 7211707
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Pentecost", "Spirit", "Baptism", "Mission"]
---
What's the best gift you've ever received?

In this message, we hear of God's amazing gifts of forgiveness and his Spirit, given because Jesus is risen and reigning.

# Outline
1. Beginning in Jerusalem (vv1-13)
2. The Spirit is poured out (vv14-21)
3. because the crucified Christ is ascended (vv22-36)
4. Everyone who calls upon his name… (vv37-41)

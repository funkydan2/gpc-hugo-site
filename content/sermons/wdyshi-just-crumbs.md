---
title: "Just Crumbs"
passage: "Mark 7:24-37"
date: 2021-03-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210321_JustCrumbs_MH.mp3"]
audio_duration: "22:58"
audio_size: 5742553
preachers: ["Mitchell Harte"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
---
'If these miracles are *just crumbs*, what's it like to be seated at God's table?'

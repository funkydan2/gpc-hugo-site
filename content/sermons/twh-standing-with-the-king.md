---
title: "Standing With the King"
passage: "Luke 21:5-38"
date: 2020-02-23T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200223_StandingWithTheKing_DS.mp3"]
audio_duration: "30:10"
audio_size: 7468376
preachers: ["Daniel Saunders"]
images: ["uploads/2020/02/TheWayHome5.jpg"]
series: ["The Way Home (Part 5)"]
tags: ["Anxiety", "King", "Apocalyptic", "Judgment"]
---

Whether it's the 'big things' in the news or the small things in our own life there are many things which can make us anxious. What does Jesus have to offer our anxious hearts?

# Outline

1. Do not be deceived (vv5-9)
2. Stand firm (vv10-28)
3. Be alert (vv29-38)


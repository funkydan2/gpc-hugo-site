---
title: "All One in Christ Jesus"
passage: "Acts 6:1-7"
date: 2022-03-27T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts1-7/20220327_AllOneInChristJesus_DS.mp3"]
audio_duration: "25:42"
audio_size: 6398725
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/01/Acts1-7.jpg"]
series: ["The Saving Word"]
tags: ["Leadership"]
---
How should Christians respond to challenges and tensions to unity?

In this message, we hear how the earliest followers of Jesus responded with unjust care for the vulnerable.

# Outline
1. Growing Pains (v1)
2. Delegated Authority (vv2-4)
3. They chose… (vv5-6)
4. The Word Grows (v7)

---
This sermon was prerecorded. A video of the recorded is availabe:

{{<youtube "Ay1pgp_2Db0">}}

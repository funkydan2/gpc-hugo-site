+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20181223_MarySings_DS.mp3"]
audio_duration = "25:07"
audio_size = 6256452
date = "2018-12-23T10:00:00+10:00"
images = ["/uploads/2018/12/Christmas2018.jpg"]
passage = "Luke 1:46-56"
preachers = ["Daniel Saunders"]
series = ["Christmas"]
tags = []
title = "Mary Sings"

+++
1. Mary's humble joy (vv46-58)
2. because God is turning the world right-side-up (vv49-53)
3. just as he promised (vv54-55)

During the sermon Hannah's Song (1 Samuel 2) is mentioned. You can [listen to the sermon on 1 Samuel from earlier in 2018 here]({{< relref "the-lord-lifts-up.md" >}}))

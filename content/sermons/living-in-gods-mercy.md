---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160626_LivingInGodsMercy_DS.mp3"]
audio_duration: '24:25'
date: 2016-06-26 09:30:00+10:00
passage: Romans 12:1-8
title: Living In God's Mercy
preachers: ["Daniel Saunders"]
series:
- Miscellaneous
images:
- "/uploads/2016/02/sermons.jpg"
tags:
- Gifts
- Grace
- Worship
---
1. Living in God's Mercy... (12:1)
2. ...changes us (12:2)
3. ...to live for others (12:3-8)

How does God's mercy change you?

---
title: "The Priest-King"
passage: "Zechariah 6:9-15"
date: 2023-05-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Zechariah/20230528_ThePriestKing_DS.mp3"]
audio_duration: "25:16"
audio_size: 6293817
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/04/zechariah.jpg"]
series: ["Kingdom Come"]
tags: ["Priest","King","Temple","Christ","Messiah"]
---
What does mean to say 'Jesus is the Christ'?

In this message from Zechariah 6, we dig deep into the good news that Jesus is the Priest-King.

# Outline
1. The Crowning of the Priest‽ 
2. Jesus Our Priest-King (Hebrews 1:1-3)
3. The Temple Builder
4. God’s Living Temple (1 Peter 2:4-10)

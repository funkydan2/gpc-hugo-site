---
title: "Marriage Test"
passage: "Mark 10:1-12"
date: 2021-10-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211010_MarriageTest_DS.mp3"]
audio_duration: "28:38"
audio_size: 7103048
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Marriage", "Law"]
---
“Therefore what God has joined together, let no one separate.”

What does Jesus think about marriage?

In this message, we hear how Jesus answers a trick question about marriage and divorce.

# Outline
1. It’s a trap! (vv1-2)
2. The vision and a concession (vv3-9)
3. The heart of the matter (vv10-12)

There's, unfortunately, some distortion in this recording

---
title: "The Sinning King"
passage: "2 Samuel 11"
date: 2019-05-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190519_TheSinningKing_DS.mp3"]
audio_duration: "27:56"
audio_size: 6934093
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/04/SWFTTK.jpg"]
series: ["Still Waiting For The True King"]
tags: ["Sin", "Guilt"]
---
We know deep down there’s a problem with us that needs fixing. Our attempts to deal with sin without Jesus will only escalate this, bringing anxiety and chaos rather than peace and rest.

In this sermon we're looking at a dark chapter in the life of Israel's great king...it's revealing of our own hearts, and also of refreshing cleansing Jesus brings.

# Outline
1. The seeds of sin (11:1)
2. David falls (11:2-6)
3. The cover-up (11:7-20)
4. Jesus can wash you clean

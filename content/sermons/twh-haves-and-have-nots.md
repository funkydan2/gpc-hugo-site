---
title: "Haves and Have-Nots"
passage: "Luke 18:15-34"
date: 2019-09-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190901_HavesAndHaveNots_DS.mp3"]
audio_duration: "25:21"
audio_size: 6311451
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Money", "Children", "Kingdom", "Eternal Life", "Law"]
---

What do you need to do to enter God's kingdom?

A bloke who seemed to have everything going for him, asked Jesus this question. But Jesus' surprising answer shows how God's kingdom conflicts with our world's values of status and power. Greedy-hearted humans can't enter God’s Kingdom, but God has made it possible for those who like a child receive his generosity in Jesus.

# Outline

1. Empty Hands (vv15-17)
2. Not what you offer (vv18-30)
3. The suffering and rising king (vv31-34)
4. Will you enter God’s kingdom?

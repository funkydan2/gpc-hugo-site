---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Easter/20160327_CanYouHopeInJesus_EasterSunday_DS.mp3"]
audio_duration: '19:05'
date: 2016-03-27 01:32:16+00:00
draft: false
passage: Luke 24:13-32
title: Can You Hope in Jesus?
preachers: ["Daniel Saunders"]
series:
- Easter
tags:
- Easter
- Resurrection
images: ["/uploads/2016/03/Easter.jpg"]
---
1. A Sorrowful Meeting (vv13-18)
    ... because hope has been lost (vv19-24)
2. The Story Explained (vv25-29)
3. Hope is Raised to Life (vv30-32)

Can you hope in Jesus?

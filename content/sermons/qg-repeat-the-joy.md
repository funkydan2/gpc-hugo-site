---
title: "Repeat the Joy"
passage: "Habakkuk 3"
date: 2022-05-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Habakkuk/20220522_RepeatTheJoy_DS.mp3"]
audio_duration: "27:45"
audio_size: 6891838
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Habakkuk.jpg"]
series: ["Questioning God"]
tags: ["Prayer","Scripture"]
---
What 'big prayers' do you pray? Why do or don't you pray 'big prayers'? In this message, we hear how knowing what God has done not only changes our prayers for the future but gives joy now.

# Outline
1. Good News on Repeat (vv1-2)
2. What our God has done (vv3-16)
3. Rejoicing in the Lord (vv17-19)
    Philippians 4:4-6

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171008_HowGraceChangesLives_DS.mp3"]
audio_duration: '26:06'
audio_size: 6493509
date: 2017-10-08 07:15:27+00:00
draft: false
passage: Romans 6
title: How God's Grace Changes Lives
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Freedom
- Grace
- Law
- Sin
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. The story so far ... (Romans 1-5)

2. How grace changes lives:

    1. it gives new life (6:2-11)

    2. it gives a new king (6:12-14)

    3. it gives a new master (6:15-23)

3. Has God's grace changed you?

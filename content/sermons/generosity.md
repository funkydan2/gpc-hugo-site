---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/MoneyMatters/20160814_Generosity_DS.mp3"]
audio_duration: '32:17'
date: 2016-08-14 05:50:54+00:00
draft: false
passage: 2 Corinthians 8-9
title: Generosity
preachers: ["Daniel Saunders"]
series:
- Money Matters
tags:
- Creation
- Generosity
- Giving
- Gospel
- Money
- Tithing
images: ["/uploads/2015/12/Money-Matters.jpg"]
---
1. God's Generosity
    1. Creation (Job 41:11b)
    2. Salvation (2 Corinthians 8:9)
2. Being Generous
    1. How generous?
1. Tithing? (Numbers 18; Deuteronomy 14)
2. Or...?
    1. Who to be generous to?

        Galatians 6:10

        Galatians 6:6; 1 Timothy 5:17-18

    2. How to be generous?

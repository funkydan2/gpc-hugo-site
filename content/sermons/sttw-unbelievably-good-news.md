---
title: "Unbelieveably Good News"
passage: "Acts 13"
date: 2022-08-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220828_UnbelievabilyGoodNews_DS.mp3"]
audio_duration: "31:20"
audio_size: 7749912
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Gospel","Good News","Biblical Theology","Promises","Evangelism","Mission"]
---
What makes Jesus unbelievably good news?

In this message we see how the death, resurrection, and reign of Jesus is the answer to God's promises and the only way we can receive forgiveness and freedom.

# Outline
1. Promised Saviour (vv16-25)
2. Risen Saviour (vv26-37)
3. Receiving Salvation (vv38-41)
4. Unbelievable Salvation (vv42-52)

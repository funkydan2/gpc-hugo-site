---
title: "Unhindered by 'Wisdom'"
passage: "Acts 17"
date: 2023-02-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts15-28/20230212_UnhinderedByWisdom_DS.mp3"]
audio_duration: "26:19"
audio_size: 6546150
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/01/Acts15-28.jpg"]
series: ["Unhindered Kingdom"]
tags: ["Resurrection","Mission","Gospel","Evangelism"]
---
Why is Jesus good news for you and for the world? As we hear the gospel proclaimed in a synagogue in Thessalonica and the Aeropagus in Athens, we hear how good Jesus is.

# Outline
1. Jesus is the Messiah (vv1-15)
2. Jesus is the Judge of the World (vv16-34)
3. The God who Saves

---
title: "A Living Home"
passage: "1 Peter 2:4-10"
date: 2021-07-18T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210718_ALivingHome_DS.mp3"]
audio_duration: "28:07"
audio_size: 6980741
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Temple", "Identity", "Priest", "Biblical Theology"]
---
Who are you? For followers of Jesus, God gives a purpose and a new identity as 'living stones' in his new temple.

# Outline
1. Building God’s place (vv4-8)
2. Being God’s people (vv9-10)
3. Living into identity

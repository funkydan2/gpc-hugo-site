---
title: "What You Do When You Hear Matters"
passage: "Matthew 13:1-23"
date: 2020-07-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20200705_HearingMatters_JB.mp3"]
audio_duration: "22:03"
audio_size: 5297990
preachers: ["Jono Buesnel"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
tags: ["Parable"]
---

Have you ears to hear? In the Parable of the Soils, Jesus calls us be good, fruitful hearers.

This sermon was brought to us during lock-down by Jono Buesnel from [Bargara Presbyterian Church](http://www.bargarapresbyterian.org.au).


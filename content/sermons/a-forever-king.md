---
title: "A Forever King"
passage: "2 Samuel 7"
date: 2019-05-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190512_AForeverKing_DS.mp3"]
audio_duration: "22:58"
audio_size: 5743324
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/04/SWFTTK.jpg"]
series: ["Still Waiting For The True King"]
tags: ["Leadership", "Biblical Theology", "Covenant", "Kingdom", "Temple"]
---
Do you want to build something which lasts?

David desires to build a house for God and secure his peace and blessing, but God reminds him that he is the one who builds things that last. God promises David's son will build his house and God himself will make a kingdom that will last forever.

# Outline
1. David has plans (vv1-2)
2. But so does God (v4)
    1. … because God hasn’t asked (vv5-7, 1 Kings 5:3)
    2. … because **God** will build (vv8-17)
3. David’s humble praise (vv18-29)
4. Where is this forever king?
5. What are you building?

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke1-4/20161127_NothingIsImpossibleWithGod_DS.mp3"]
audio_duration: '25:20'
date: 2016-11-27 04:11:55+00:00
draft: false
passage: Luke 1:5-38
title: Nothing Will Be Impossible with God
preachers: ["Daniel Saunders"]
series:
- Great Expectations
tags:
- Advent
- Angel
- Christ
- Christmas
- Messiah
images: ["/uploads/2015/12/Great-Expectations.jpg"]
---

1. Gabriel's two announcements:

    1. John will prepare the way (1:5-25)

    2. Jesus, the son of David (1:26-38)

        Isaiah 11

2. Nothing will be impossible with God!

3. God's prepared people

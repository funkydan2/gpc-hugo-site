---
title: "Who Is - I AM"
passage: "John 15:1-11"
date: 2022-06-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20220605_WhoIs-IAm_MD.mp3"]
audio_duration: "28:20"
audio_size: 7029156
preachers: ["Malcolm Diefenbach"]
images: ["/uploads/2022/06/John15.jpg"]
series: ["Miscellaneous"]
tags: ["Jesus","Discipleship"]
---
Who is Jesus, and what does it mean to be connected to him?

# Outline
1. Who is – I AM
2. Fruit Trees
3. Abiding
4. Keys

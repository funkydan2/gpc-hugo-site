---
title: The Sacrificing King
passage: 2 Samuel 21-24
date: 2019-06-23T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/StillWaitingForTheTrueKing/20190623_TheSacrificingKing_DS.mp3
audio_duration: 27:23
audio_size: 6799037
preachers:
  - Daniel Saunders
images:
  - /uploads/2019/04/SWFTTK.jpg
series:
  - Still Waiting For The True King
tags:
  - Judgment
  - Sacrifice
  - mercy
---
Forgiveness always costs.

As 2 Samuel ends, we visit the site of the future temple where King David makes a sacrifice to save his people from judgement. This sacrifice is a shadow of Jesus who sacrifices himself to rescue us from judgement. King David’s willingness to lay down his life for Israel, his sheep, prepares us for Jesus, the good shepherd who dies for his sheep.

# Outline

1. David’s grasping census (24:1-10)
2. Consequences and compassion (24:11-25)
3. Jesus, the self-sacrificing king

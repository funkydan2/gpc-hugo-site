---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Hosea/20160918_GodsLoveForLiars_DS.mp3"]
audio_duration: '26:30'
date: 2016-09-18 03:41:50+00:00
draft: false
passage: Hosea 11:12-14:9
title: God's Love for Liars
preachers: ["Daniel Saunders"]
series:
- Unbreakable Love
tags:
- Forgiveness
- Love
- Sin
images: ["/uploads/2015/12/Hosea.jpg"]
---

1. Israel are deceitful
2. yet God's given them every chance
3. but they've blown it
4. and still, God's love is unbreakable

We need God's unbreakable love (1 Corinthians 15:55-56)

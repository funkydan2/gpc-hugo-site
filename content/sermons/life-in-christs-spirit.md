---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171022_LifeInChristsSpirit_DS.mp3"]
audio_duration: '25:11'
audio_size: 6273546
date: 2017-10-22 09:41:06+00:00
draft: false
passage: Romans 8:1-17
title: Life in Christ's Spirit
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Adoption
- Spirit
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. No Condemnation (vv1-4)

2. Because believers have God's Spirit (vv5-13)

3. Who makes us God's children (sons) (vv14-17)

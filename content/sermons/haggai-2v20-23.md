---
title: "An Assurance From God"
passage: "Haggai 2:20-23"
date: 2022-01-23T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Haggai/20220123_Haggai2v20-23_RP.mp3"]
audio_duration: "28:12"
audio_size: 7000634
preachers: ["Rayk Platzek"]
images: ["/uploads/2016/02/podcast_cover.jpg"]
series: ["Haggai"]
tags: ["Justice","Promises","King"]
---
'God's past faithfulness gives assurance for his future promises.'

# Outline
1. Why is it needed?
2. Assurance through God's justice and protection
3. Assurance through God's faithfulness

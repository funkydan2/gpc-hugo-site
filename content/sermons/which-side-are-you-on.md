---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160619_WhichSideAreYouOn_DS.mp3"]
audio_duration: '23:14'
date: 2016-06-19 03:27:03+00:00
draft: false
passage: Romans 5:12-21
title: Which Side Are You On?
preachers: ["Daniel Saunders"]
series:
- The Verdict
tags:
- Gospel
- Grace
- Sin
images: ["/uploads/2015/12/Romans-TheVerdict.jpg"]
---

1. Through one man's sin... (5:12-14)
2. Through one man's righteousness... (5:15-21)

Which side are you on?

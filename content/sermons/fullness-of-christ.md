---
title: "The Fullness of Christ"
passage: "Colossians 1:15-23"
date: 2019-09-29T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/ChristOverAll/20190929_FullnessOfChrist_DB.mp3"]
audio_duration: "26:31"
audio_size: 6592896
preachers: ["Daryl Brenton"]
images: ["/uploads/2019/09/ChristOverAll.jpg"]
series: ["Christ Over All"]
tags: []
---

A sermon from our elder, Daryl Brenton, on Colossians 1:9-23 entitled 'The Fullness of Christ'.
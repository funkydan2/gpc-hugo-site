---
title: "The Beginning"
passage: "Mark 1:1-13"
date: 2021-01-03T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210103_TheBeginning_DS.mp3"]
audio_duration: "19:42"
audio_size: 4957890
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Gospel", "Repentance", "Baptism", "Spirit"]
---
How is Jesus' life good news now?

# Outline
1. The beginning (v1)
2. Preparation (vv2-8)
3. Declaration (vv9-11)
4. Conflict (vv12-13)
5. The good news continues

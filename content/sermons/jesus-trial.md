---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170402_JesusTrial_DS.mp3"]
audio_duration: '25:08'
audio_size: 6261906
date: 2017-04-02 05:00:33+00:00
draft: false
passage: Luke 22:66-23:31
title: Jesus' Trial
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Judgment
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---

1. The Trial
    1. Religious leaders (22:66-71)
    2. Pilate - part 1 (23:1-7)
    3. Herod (23:8-12)
    4. Pilate - part 2 (23:13-24)
2. Following Jesus to the cross
    1. The other Simon (23:26)
    2. The women (23:27-31)
3. Judging the judge

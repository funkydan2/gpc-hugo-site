---
title: "Feasting in the Kingdom"
passage: "Matthew 9:1-17"
date: 2024-08-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/SeeingTheKingdom/20240804_FeastingInTheKingdom_DS.mp3"]
audio_duration: "24:06"
audio_size: 6013991
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/07/SeeingTheKingdom.jpg"]
series: ["Seeing the Kingdom"]
tags: ["Forgiveness","Sin","Righteousness"]
---
Who does Jesus call to his table? Those who know they're sinners. Do you know Jesus welcome and will you extend it to others?

# Outline
1. Jesus’ forgiveness (vv1-8)
2. Jesus’ Company (vv9-13)
3. Celebrating Jesus (vv14-17)

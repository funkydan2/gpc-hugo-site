---
title: "But Now He Sees"
passage: "Acts 9:1-31"
date: 2022-07-31T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220731_ButNowHeSees_DS.mp3"]
audio_duration: "27:17"
audio_size: 6780330
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Salvation","Mission"]
---
As God opens Saul's eyes, we see his power and patience.

# Outline
1. The Blind See (vv1-19)
2. Speaking and Suffering (vv20-31)
3. Seeing God’s Immense Patience (1 Timothy 1:15-16)

---
title: "You Are the Messiah"
passage: "Mark 8:22-9:13"
date: 2021-09-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20210905_YouAreTheMessiah_DS.mp3"]
audio_duration: "26:44"
audio_size: 6646923
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Jesus", "Messiah", "Son of Man", "Discipleship", "Glory", "Identity"]
---
Who do you say Jesus is? This is *the* question.

As we return to Mark's gospel, this is the question Jesus asks his followers. But the answer he gives is surprising.

# Outline
1. Almost seeing (8:22-33)
2. Follow Me (8:34-9:1)
3. Sight! (9:2-13)
4. Follow Him

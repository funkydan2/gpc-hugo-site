---
title: "The Best King"
passage: "Psalm 72"
date: 2024-06-16T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20240616_TheBestKing_DS.mp3"]
audio_duration: "27:13"
audio_size: 6761443
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/06/Psalm72.jpg"]
series: ["Miscellaneous"]
tags: ["Biblical Theology"]
---
What would it take to be the best king or leader?

Psalm 72 is a prayer for kings, which in Jesus becomes a song of praise, because he's the best king.

# Outline
1. Reading Psalms
2. How Israel 'sings' this Psalm
3. How Jesus ‘sings’ this Psalm
4. How we ‘sing’ this Psalm in Christ

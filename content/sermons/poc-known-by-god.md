---
title: "Known by God"
passage: "Psalm 139"
date: 2020-05-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/PsalmsOfComfort/20200510_POC_KnownByGod_DS.mp3"]
audio_duration: "23:19"
audio_size: 5825911
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/04/Psalms_Of_Comfort.jpg"]
series: ["Psalms of Comfort"]
tags: ["Love", "Life"]
---

Psalm 139 is a precious Psalm for lots of people because it shows how the big truth about God knowing everything, being everywhere, and being able to do anything, is deeply personal for us.

# Outline

1. God knows everything (vv1-6)
2. God is everywhere (vv7-12)
3. The creator can do everything (vv13-18)
4. Being known changes us (vv19-24)

## Sermon resources:

* During the sermon, we watched the first minute of [this debate between Christopher and Peter Hitchens](https://www.youtube.com/watch?v=IPD1YGghtDk). The audio from the video isn't included in the podcast recording but can be [watched on YouTube](https://www.youtube.com/watch?v=IPD1YGghtDk). (N.B. there is explicit language after the first minute.)

* The Lydia Project podcast episode can be found on the [Gospel Coalition Australia website](https://au.thegospelcoalition.org/podcasts/the-lydia-project/ep-44-karola-emmeline-williams/).

---
title: Right Confidence
passage: Philippians 3:1-11
date: 2024-11-24T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/GospelPartners/20241124_RightConfidence_DS.mp3
audio_duration: 26:57
audio_size: 6697174
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/10/GospelPartners.jpeg
series:
    - Gospel Partners
tags:
    - Faith
    - Justification
    - Righteousness
    - Union With Christ
    - Assurance
---
Can you be confident before God? In this message, we hear the great news that, through faith in Christ, we can be 100% confident in our standing before God.

# Outline
1. Another example (vv1-3)
2. Confidence in ‘the flesh’ (vv4-6)
3. Confidence in Christ (vv7-11)

---
title: "True Profit"
passage: "Malachi 3:6-18"
date: 2023-12-10T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Malachi/20231210_TrueProfit_DS.mp3"]
audio_duration: "25:31"
audio_size: 6355237
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/11/HalfHearted.png"]
series: ["Half Hearted"]
tags: ["Generosity","Money"]
---
Have you ever missed out on a great business or investment opportunity?

In this message from Malachi, we hear how God's generosity in the gospel enables us to be generous.

# Outline
1. A generous God (vv6-7, 17-18)
2. who is robbed (vv8-11)
3. and spoken against (vv13-15)
4. Gospel generosity (2 Corinthians 8)

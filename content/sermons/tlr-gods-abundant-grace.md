+++
audio = [
  "https://f001.backblazeb2.com/file/GympieSermonAudio/Amos/20180722_GodsAbundantGrace_DS.mp3"
]
audio_duration = "27:21"
audio_size = 6_791_291
date = "2018-07-22T15:33:34+10:00"
images = [ "/uploads/2018/06/The-Lord-Roars-768x576.jpg" ]
passage = "Amos 7-9"
preachers = [ "Daniel Saunders" ]
series = [ "The Lord Roars" ]
tags = [ "Judgment", "Mercy" ]
title = "God's Abundant Grace"
+++
1. Visions of the end (7:1-9:10)

   1. God has been patient (7:1-6)

   2. But it won't last forever (7:7-9; 8:1-9:10)

   3. Because God's Word is rejected (7:10-17)

2. But even so, God will restore (9:11-15)

3. Has God been gracious (Acts 15:16-18)?
   Revelation 22:1-3

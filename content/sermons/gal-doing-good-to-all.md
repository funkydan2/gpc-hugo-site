---
title: "Doing Good to All"
passage: "Galatians 6:1-10"
date: 2022-12-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221211_DoingGoodToAll_DS.mp3"]
audio_duration: "25:34"
audio_size: 6365139
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Love","Sin","Church"]
---
The good news of Jesus brings freedom; freedom to 'do good to all'.

# Outline
1. Doing good to the person caught in sin (vv1-5)
2. Doing good to teachers (v6)
3. Do good, without giving up (vv7-10)

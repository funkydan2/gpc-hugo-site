---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/MoneyMatters/20160717_Money_Gift_DS.mp3"]
audio_duration: '35:18'
date: 2016-07-17 08:09:55+00:00
draft: false
passage: 1 Timothy 6:17-19
title: 'Money Matters: Gift'
preachers: ["Daniel Saunders"]
series:
- Money Matters
tags:
- Biblical Theology
- Blessing
- Gifts
- Money
- Treasure
images: ["/uploads/2015/12/Money-Matters.jpg"]
---

1. What is money?
2. The Bible's Big Story
    1. Creation (Genesis 2:10-16)
    2. Fall (Genesis 3:17-19)
    3. Promise (Genesis 12:1-3; Exodus 3:16-17)
    4. Fulfilment (?) (1 Kings 4:20-28)
    5. Exile (Jeremiah 31:10-14)
    6. Fulfilment (Luke 4:18-19; 6:20, 24; Hebrews 11:8-10, 16)
3. God's gift and us (1 Timothy 6:17-19)

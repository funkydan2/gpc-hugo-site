---
title: "God Forsaken"
passage: "Mark 15:33-47"
date: 2022-04-15T08:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20220415_GodForsaken_DS.mp3"]
audio_duration: "24:10"
audio_size: 6032174
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Easter", "Good Friday", "Cross", "Temple"]
---
Where do we find God? In this Good Friday message, we hear how in the darkness and God-forsakenness of the Cross, we meet the one who is the Son of God.

# Outline
1. Darkness
2. Cry
3. Curtain
4. Confession

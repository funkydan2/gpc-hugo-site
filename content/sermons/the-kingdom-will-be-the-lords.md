---
title: The Kingdom will be the Lord’s
passage: Obadiah
date: 2020-11-29T09:30:00+10:00
audio:
  - https://f001.backblazeb2.com/file/GympieSermonAudio/Obadiah/20201129_Obadiah_DS.mp3
audio_duration: 29:30
audio_size: 7309765
preachers:
  - Daniel Saunders
images:
  - /uploads/2020/11/Obadiah.jpg
series:
  - Obadiah
tags:
  - Judgment
  - Salvation
  - Suffering
---
When you suffer injustice, does God care?

In this sermon we hear from Obadiah, the shortest book in the Old Testament, who reveals God's heart to gloriously judge and save.

# Outline
1. Setting the scene
2. The King against the proud (vv1-9)
3. The King against enemies (vv10-16)
4. The King for his people (vv17-21)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Joel/20170108_ADayOfRepentance_DS.mp3"]
date: 2017-01-08 01:25:24+00:00
draft: false
passage: Joel 1:1-2:17
title: A Day of Repentance
preachers: ["Daniel Saunders"]
series:
- The Day of the LORD
tags:
- Judgment
- Repentance
images: ["/uploads/2017/01/The-Day-of-the-LORD.jpg"]
---

1. The prophet Joel (Joel 1:1)
2. A plague of biblical proportions
3. ... is a call to true repentance
4. True repentance

    (2 Corinthians 7:2-12)

(During the sermon we watched this news report of a locust plague in Madagascar - https://www.youtube.com/watch?v=MTOgUjMcQWk)

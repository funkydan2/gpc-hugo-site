---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnitedWithChrist/20170430_Incarnation_DS.mp3"]
audio_duration: '22:53'
audio_size: 5723194
date: 2017-04-30 06:57:41+00:00
draft: false
passage: John 1:1-18
title: Incarnation
preachers: ["Daniel Saunders"]
series:
- United with Christ
tags:
- Incarnation
- Jesus
- Prayer
- Union With Christ
images: ["/uploads/2017/04/United-with-Christ_web.jpg"]
---

1. The Word who

    1. ...is with God and who is God (John 1:1-3)

    2. ...became flesh (John 1:14)

    3. ...ascended and intercedes (Hebrews 4:14-16)

2. Knowing the God who's one with us

---
title: "Interrogating the King"
passage: "Luke 20:20-21:4"
date: 2020-02-16T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20200216_InterrogatingTheKing_DS.mp3"]
audio_duration: "25:24"
audio_size: 6325558
preachers: ["Daniel Saunders"]
images: ["uploads/2020/02/TheWayHome5.jpg"]
series: ["The Way Home (Part 5)"]
tags: ["Resurrection", "Marriage", "Money", "Government", "Temple"]
---

Often we ask questions with no intention of listening, but just to prove ourselves right. And it can be the same with the questions we ask of Jesus.

In this message we see some people meet Jesus with an agenda, hoping to trap him. But instead, the truth is revealed.

# Outline

1. Whose image? (20:20-26)
2. Whose wife? (20:27-40)
3. Whose son? (20:41-44)
4. Whose example? (20:45-21:4)

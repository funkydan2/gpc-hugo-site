---
title: "The Mission of the King"
passage: "Matthew 4:12-25"
date: 2024-02-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/KingOfHeaven/20240204_TheMissionOfTheKing_DS.mp3"]
audio_duration: "23:21"
audio_size: 5834231
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/12/KingOfHeaven.jpg"]
series: ["King of Heaven"]
tags: ["Evangelism","Mission"]
---
What is Jesus' mission and how do we get to partner in it?

# Outline
1. To bring light and life (vv12-17)
2. Joining the mission (vv18-22)
3. The kingdom comes (vv23-25)
4. Join the mission

(Due to technical issues, the audio for the first 1 1/2 minutes was recorded separately.)

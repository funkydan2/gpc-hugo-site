---
title: "The LORD is God"
passage: "1 Kings 17-20"
date: 2024-05-19T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/1Kings/20240519_TheLORDisGod_DS.mp3"]
audio_duration: "28:44"
audio_size: 7129992
preachers: ["Daniel Saunders"]
images: ["/uploads/2024/04/1Kings.png"]
series: ["1 Kings"]
tags: ["Idolatry","Victory"]
---
When it feels like things are on the decline we can easily fall into despair. In 1 Kings we see this with the prophet Elijah, even after God shows his power. In this message, we're encouraged from 1 Kings to remember that the LORD is God.

# Outline
1. The LORD lives—in Israel and beyond (chp 17)
2. The LORD is God—but his prophet despairs (chps 18-19)
3. Knowing the LORD—but the king sulks (chp 20)

In this message, the podcast [The Surprising Rebirth of Belief in God is recommended. You can listen to it here.](https://justinbrierley.com/surprisingrebirth/)

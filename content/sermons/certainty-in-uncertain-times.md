---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170312_CertaintyInUncertainTimes_DS.mp3"]
audio_duration: '29:16'
date: 2017-03-12 07:07:12+00:00
draft: false
passage: Luke 21:5-38
title: Certainty in Uncertain Times
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
images: ["/uploads/2017/02/TheWayToTheCross.png"]
tags:
- Apocalyptic
- Kingdom
- Son of Man
- Cross
---

1. Safe as houses?
2. You will live in uncertain times
    1. People will lead astray
    2. The earth will shake
    3. You'll be hated
3. But the Son of Man reigns
    1. He is with us
    2. He is reigning
    3. And he'll bring salvation!
4. Signs of uncertain times
5. ...remind us to watch
6. Where is your certainty?

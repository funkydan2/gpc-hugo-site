---
title: "Persecution and Prayer"
passage: "Acts 12"
date: 2022-08-21T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220821_PersecutionAndPrayer_DS.mp3"]
audio_duration: "29:47"
audio_size: 7377999
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Persecution","Prayer"]
---
If God is king, why are his people persecuted?

In Acts 12 we see persecution against God's church intensify, yet through God's powerful rescue, we see God is lovingly in control as his people live out the way of Christ.

# Outline
1. Herod Plans (but the church prays!) (12:1-5)
2. God’s power…
    1. Rescuing Peter (12:6-19)
    2. Herod’s Demise (12:20-23)
    3. Gospel growth (12:24-25)
3. Where is God?

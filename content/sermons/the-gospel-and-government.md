---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans6-16/20171203_GospelAndGovernment_DS.mp3"]
audio_duration: '27:25'
audio_size: 6808376
date: 2017-12-03 06:21:10+00:00
draft: false
passage: Romans 13:1-7
title: The Gospel and Government
preachers: ["Daniel Saunders"]
series:
- Knowing and Living in God's Mercy
tags:
- Gospel
- Government
- Politics
images: ["/uploads/2017/09/Romans6-16.jpg"]
---

1. Government is under God (v1)

2. Christians and the Government

    1. ... obey (vv2-5)

    2. ... pay (vv6-7)

3. Because Jesus is Lord

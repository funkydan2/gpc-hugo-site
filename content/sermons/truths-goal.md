---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180422_TruthsGoal_DS.mp3"]
date: 2018-04-23 02:15:37+00:00
audio_duration: "22:48"
audio_size: 5698066
draft: false
passage: 1 Timothy 1:1-11
title: Truth's Goal
preachers: ["Daniel Saunders"]
series:
- God's House
tags:
- False Teaching
- Law
- Love
images: ["/uploads/2018/04/Gods-House-768x644.jpg"]
---

1. The gospel in Ephesus (vv1-3)

    Acts 18-20

2. The problem in Ephesus (vv4-11)

    1. Myths and Genealogies

    2. Unlawful use of the law

3. Speaking hard-truths in love

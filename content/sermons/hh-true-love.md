---
title: "True Love"
passage: "Malachi 1:1-5"
date: 2023-11-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Malachi/20231112_TrueLove_DS.mp3"]
audio_duration: "24:35"
audio_size: 6129995
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/11/HalfHearted.png"]
series: ["Half Hearted"]
tags: ["Love","Salvation","Election"]
---
Do you think God loves you? Do you feel God's love?

In this message from Malachi 1, we see how God's love is known not in our situation but in God's salvation.

# Outline
1. Meet Malachi (v1)
2. How have you loved us? (vv2-5, Genesis 25)
3. God’s choosing love (Romans 9)
4. Do you know God’s love?

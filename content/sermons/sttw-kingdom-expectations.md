---
title: "Kingdom Expectations"
passage: "Acts 14"
date: 2022-09-04T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Acts8-15/20220904_KingdomExpectations_DS.mp3"]
audio_duration: "27:20"
audio_size: 6789609
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/07/Acts8-15.jpg"]
series: ["Salvation to the World"]
tags: ["Kingdom","Suffering","Eschatology","Persecution"]
---
If Jesus is risen and reigning, what expectations should we have for life in his kingdom?

# Outline
1. The Kingdom Comes (vv1-20)
2. Strengthened and Encouraged (vv21-28)
3. Strengthened for Hardship

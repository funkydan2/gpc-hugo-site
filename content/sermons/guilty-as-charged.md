---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Romans1-5/20160522_GuiltyAsCharged_DS.mp3"]
audio_duration: '23:45'
date: 2016-05-22 05:14:33+00:00
draft: false
passage: Romans 3:1-20
title: Guilty as Charged
preachers: ["Daniel Saunders"]
series:
- The Verdict
tags:
- Judgment
- Sin
images: ["/uploads/2015/12/Romans-TheVerdict.jpg"]
---

1. Two objections:
    1. Can God be trusted? (3:1-4)
    2. Is God fair? (3:5-8)
2. Our Big Problem (3:9-18)
3. So what's the law good for? (3:19-20)
4. The verdict is in...

---
title: "Dealing with Disruption"
passage: "2 Thessalonians 3:6-18"
date: 2023-08-27T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20230827_DealingWithDisruption_DS.mp3"]
audio_duration: "22:18"
audio_size: 5582828
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Discipleship","Work","Church","Leadership"]
---
One of the things God is doing in churches is forming a community shaped by peace, grace, and love. So how does a church deal with disruptive and destructive behaviour?

# Outline
1. What’s the problem? (vv6, 11, 16-18)
2. Be an Example (vv7-9)
3. Teach How to Live (vv10-13)
4. Discipline to Disciple (vv6, 14-15)

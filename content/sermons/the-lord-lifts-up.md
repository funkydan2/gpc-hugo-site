+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20180916_TheLordLiftsUp_DS.mp3"]
audio_duration = "25:17"
audio_size = 6295119
date = "2018-09-16T10:00:16+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 1:1-2:11"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = ["Prayer", "Grace"]
title = "The LORD Lifts Up"

+++
1. Hannah's Problem (1:1-8)
2. Hannah's Promise (1:9-18)
3. God's Provision (1:19-28)
4. Hannah's Praise (2:1-11)
5. 1 Samuel and Us
   1. What does this show us about God?
   2. What does this show us about how God works in Jesus?
   Philippians 2:6-11
   3. What does this show us about people?
   1 Corinthians 1:26-29

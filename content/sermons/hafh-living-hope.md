---
title: "Living Hope"
passage: "1 Peter 1:1-9"
date: 2021-06-27T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/HopeAwayFromHome/20210627_LivingHope_DS.mp3"]
audio_duration: "27:17"
audio_size: 6778163
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/06/HAFH.png"]
series: ["Hope Away From Home"]
tags: ["Hope","Joy","Suffering","Resurrection","Life","Trinity"]
---
Is it possible to face suffering and trials with joy or hope? As we begin this series in 1 Peter, *Hope Away From Home*, we hear how Jesus gives us everything we need to have a true, **living hope**.

# Outline
1. To those far from home (vv1-2)
2. What God has done (vv3-5)
3. Joy in trials (vv6-9)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke19-24/20170305_WhenGodIsSmall_DS.mp3"]
audio_duration: '26:45'
date: 2017-03-05 05:00:01+00:00
draft: false
passage: Luke 20:41-21:4
title: When God Is Small
preachers: ["Daniel Saunders"]
series:
- The Way to the Cross
tags:
- Christ
- Messiah
- Money
- Trinity
images: ["/uploads/2017/02/TheWayToTheCross.png"]
---

1. Jesus against the religious leaders who...
    1. made God small (20:41-44) (Psalm 110:1)
    2. and made themselves big (20:45-47)
2. But when God is big...(21:1-4)

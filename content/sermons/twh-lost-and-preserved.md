---
title: "Lost and Preserved"
passage: "Luke 17:20-37"
date: 2019-08-18T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190818_LostAndPreserved_DS.mp3"]
audio_duration: "23:00"
audio_size: 5748996
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Cross", "Resurrection", "Kingdom", "Apocalyptic", "Son of Man"]
---

When you hear a warning of something concerning coming your way, do you stay or do you go?

In this passage, as we continue our journey with Jesus, we will hear him talking about the cross, the turning point of history. It brings judgement on the world and ushers in the Kingdom of God. Jesus calls us to escape the world and find refuge in him.

# Outline

1. The Kingdom of God (Daniel 2:44, 7:13-14)
   1. … is among you (Luke 17:20-21)
   2. …comes through suffering (17:22-25)
   3. …comes with sudden judgment (17:26-37)
2. Living now in the Kingdom

+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/Amos/20180715_GodHatesReligion_DS.mp3"]
audio_duration = "28:32"
audio_size = 7078995
date = "2018-07-15T16:36:07+10:00"
images = ["/uploads/2018/06/The-Lord-Roars-768x576.jpg"]
passage = "Amos 4-6"
preachers = ["Daniel Saunders"]
series = ["The Lord Roars"]
tags = ["Justice", "Poor", "Sin", "Religion"]
title = "God Hates Religion"
+++
1. Israel's corruption (4:1; 5:10-12; 6:1-6)

2. means God hates their religion (4:4-5; 5:21-26)

3. Let justice and righteousness roll (5:24; 14-15)

---
title: "Called to Unity"
passage: "Ephesians 4:1-6"
date: 2020-07-12T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200712_CalledToUnity_DS.mp3"]
audio_duration: "26:33"
audio_size: 6602385
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Church", "Virtue", "Trinity"]
---

We live in a world which is looking and hoping for *togetherness* and *unity*. But where can it be found?

In Ephesians 4:1-6, we find that Christian unity is only found in the one Triune God as believers live out our calling in Christ.

# Outline

1. Called (v1)
2. Virtues for Unity (v2)
3. Unity in the Triune God (vv3-6)

---
title: Coming, Ready or Not!
passage: Matthew 25:1-13
date: 2025-01-05T09:30:00+10:00
audio:
    - "https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20250105_ComingReadyOrNot_PB.mp3"
audio_duration: 23:16
audio_size: 5812848
preachers:
    - Peter Bloomfield
images:
    - /uploads/2025/01/Matthew25.jpg
series:
    - Miscellaneous
tags:
    - Eternity
    - Election
---
# Outline
1. The critical hour
2. The critical failure
3. The critical question

---
title: "Forgiveness"
passage: "Ephesians 4:32-5:2"
date: 2020-08-16T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200816_Forgiveness_DS.mp3"]
audio_duration: "25:58"
audio_size: 6464015
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Forgiveness", "Love", "Virtue"]
---

What difference would kindness, compassion, forgiveness, and love make to your family, your workplace, your church? In this message, we hear how believers, as God's children, are to imitate his kindness, compassion, forgiveness, and love.

# Outline

1. Kind (4:32)
   (See also Luke 6:35; Matthew 5:45)

2. Compassionate (4:32)
   (See also Mark 6:34; James 1:19)
3. Forgiving (4:32)
   (See also Acts 2:38; Hebrews 9:22; Matthew 18:15-35)
4. Loving (5:1-2)
   (See also 1 Peter 4:8)
5. Imitators of Christ


---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20170101_NewYear_NewYou_DS.mp3"]
audio_duration: '27:02'
date: 2017-01-01 03:41:22+00:00
draft: false
passage: 2 Corinthians 5:11-21
title: New Year, New You!
preachers: ["Daniel Saunders"]
tags:
- Gospel
- New Creation
- Reconciliation
- Union With Christ
images: ["/uploads/2017/01/New-Year-New-You.jpg"]
---

1. The problem in Corinth (vv11-13)
2. God's Gospel Solution
    1. New motivation (vv14-15)
    2. New vision (v16)
    3. New creation (v17)
    4. New relationship (vv18-21)
3. New Year, New You!

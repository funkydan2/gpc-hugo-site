---
title: Joyful Partners
passage: Philippians 4:10-23
date: 2024-12-15T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/GospelPartners/20241215_JoyfulPartners_DS.mp3
audio_duration: 25:54
audio_size: 6447698
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/10/GospelPartners.jpeg
series:
    - Gospel Partners
tags:
    - Generosity
    - Mission
    - Money
    - Prayer
---
In this message from Philippians 4 we hear of the joy of generous, costly, partnership in the gospel.

# Outline
1. In Christ we learn contentment (vv10-13)
2. In Christ we learn generous partnership (vv14-16)
3. In Christ we benefit from partnership (vv17-20)
4. In Christ our ‘circles’ grow (vv21-23)

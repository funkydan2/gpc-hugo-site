+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnholyPeopleHolyGod/20181111_HolyPeopleHolyGod_DS.mp3"]
audio_duration = "27:45"
audio_size = 6887812
date = "2018-11-11T10:01:56+10:00"
images = ["/uploads/2018/10/Unholy-People-Holy-God.jpg"]
passage = "Leviticus 17-20"
preachers = ["Daniel Saunders"]
series = ["Unholy People Holy God"]
tags = ["Holy", "God", "Marriage", "Sacrifice", "Religion"]
title = "Holy People Holy God"

+++
1. What is holiness? (Leviticus 18:1-5)

2. Holy Religion (Leviticus 17)

3. Holy Relationships (Leviticus 18)

4. Holy Lives (Leviticus 19)

5. Be Holy, Because I Am Holy

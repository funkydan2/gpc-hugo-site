---
title: "Will You Listen?"
passage: "Mark 4:1-34"
date: 2021-02-08T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210207_WillYouListen_TD.mp3"]
audio_duration: "19:05"
audio_size: 4811567
preachers: ["Thomas Dean"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Parable"]
---
Listening is important. Jesus came announcing the good news. Will you listen to him?

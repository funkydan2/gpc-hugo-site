---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160424_GreatnessInChristsKingdom_DS.mp3"]
audio_duration: '28:37'
date: 2016-04-24 10:40:14+00:00
draft: false
passage: Luke 9:28-50
title: Greatness in Christ's Kingdom
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
tags:
- Glory
- Greatness
- Power
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---

1. Greatness above (9:28-36)
2. Greatness below (9:37-43)
3. Greatness handed over (9:44-45)
4. Greatness of the least (9:46-50)

Greatness of the King

Greatness in His Kingdom

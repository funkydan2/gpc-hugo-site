---
title: "Unhearing Oppressors"
passage: "Micah 2"
date: 2021-05-09T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Micah/20210509_UnhearingOppressors_DS.mp3"]
audio_duration: "23:35"
audio_size: 5890941
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/04/Micah.jpg"]
series: ["Micah"]
tags: ["Justice", "Greed"]
---
What will God do about those who rip off and oppress those who are poor and powerless?

In this message from Micah 2, we hear God will bring poetic justice to those who refuse to listen and continue to steal from powerless people. But even in this, there is hope for forgiveness and freedom.

# Outline
1. Daylight robbery (vv1-2)
2. Poetic justice (vv3-5)
3. Refusing to hear (vv6-7)
4. Daylight robbery (again!) (vv8-9)
5. Prophets for profit (vv10-11)
6. Following the King to freedom (vv12-13)

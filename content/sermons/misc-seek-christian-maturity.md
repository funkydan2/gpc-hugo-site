---
title: "Seek Christian Maturity"
passage: "Philippians 1:1-11"
date: 2023-06-18T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20230618_SeekChristianMaturity_LW.mp3"]
audio_duration: "30:32"
audio_size: 7558587
preachers: ["Leo Woodward"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
---
How do we find joy and grow in spiritual maturity in the Christian life?

# Outline
1. Introduction (vv1-2)
2. Four reasons for joy (vv3-8)
3. Four part prayer for spiritual growth (vv9-11)

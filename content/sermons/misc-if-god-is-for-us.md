---
title: "If God is For Us"
passage: "Romans 8:31-39"
date: 2024-06-02T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20240602_IfGodIsForUs_PS.mp3"]
audio_duration: "23:00"
audio_size: 5751694
preachers: ["Phil Stolk"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
tags: ["Assurance","Justification"]
---
A message from Romans 8 by guest preacher, Phil Stolk, Minister of [Noosa Presbyterian Church](https://www.noosapc.org.au)

# Outline
Intro: The voices of the martyr’s

If God is for us...
1. Who can be against us? (v.31-32)
2. Who can condemn us? (v.33-34)
3. Who can separate us from the love of God? (v.35-39)

More than conquerors

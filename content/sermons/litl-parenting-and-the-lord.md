---
title: "Parenting and the Lord"
passage: "Ephesians 6:1-4"
date: 2020-09-13T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/OpenTheEyesOfYourHeart/20200913_ParentingAndTheLord_DS.mp3"]
audio_duration: "27:04"
audio_size: 6724628
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/07/Ephesians_LITL.jpg"]
series: ["Living in the Light"]
tags: ["Parenting", "Children", "Law", "Discipleship"]
---
The new life Jesus gives is reflected in all our relationships. In this message, we hear God’s good design for parents and children.

# Outline

1. Children: Obey in the Lord (vv1-3)
2. Parents: Raise without Wrath (v4)
3. Raising Disciples

---
title: Partners in Love
passage: Philippians 1:1-11
date: 2024-10-20T09:30:00+10:00
audio:
    - https://f001.backblazeb2.com/file/GympieSermonAudio/GospelPartners/20241020_PartnersInLove_DS.mp3
audio_duration: 23:12
audio_size: 5799301
preachers:
    - Daniel Saunders
images:
    - /uploads/2024/10/GospelPartners.jpeg
series:
    - Gospel Partners
tags:
    - Church
    - Hospitality
    - Love
    - Prayer
    - Godliness
---
What do you feel about other believers? In this opening section to Paul's letter to the Christian believers in Philippi, we're challenged to overflow in Christ's love for his people.

# Outline
1. To God's holy people (vv1-2)
2. Are you thankful? (vv3-6)
3. What fills your heart? (vv7-8)
4. What do you pray? (vv9-11)

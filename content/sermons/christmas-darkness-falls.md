---
title: "Darkness Fails"
passage: "John 1:1-14"
date: 2020-12-25T08:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20201225_DarknessFails_DS.mp3"]
audio_duration: "0:15:52"
audio_size: 4039483
preachers: ["Daniel Saunders"]
images: ["/uploads/2020/12/Christmas2020-1.jpg"]
series: ["Christmas"]
tags: ["Light", "Dark"]
---
“The light shines in the darkness, and the darkness has not overcome it.” (John 1:5 NIV)

In this Christmas Day message, we see how Jesus is the light who's come to shine into our darkness.

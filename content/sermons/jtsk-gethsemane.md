---
title: "Gethsemane"
passage: "Mark 14:26-52"
date: 2021-12-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211205_Gethsemane_MD.mp3"]
audio_duration: "28:56"
audio_size: 7173398
preachers: ["Malcolm Diefenbach"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Cross"]
---
This message looks at the Thursday of Jesus' final week when *his hour* had finally come.

# Outline
1. Jesus always knew
2. His hour is coming
3. What a week
4. Friday is coming

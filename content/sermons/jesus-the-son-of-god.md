---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160207_JesusTheSonOfGod_DS.mp3"]
audio_duration: '23:00'
date: 2016-02-07 03:32:53+00:00
draft: false
passage: Luke 3:21-4:13
title: 'Jesus: The Son of God'
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---

1. The Beloved Son
2. The Faithful Son
3. ... for Us

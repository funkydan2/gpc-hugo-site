---
title: "Fulfilled"
passage: "Matthew 1:18-25, Isaiah 7:14-17"
date: 2019-12-22T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Christmas/20191222_Fulfilled_DS.mp3"]
audio_duration: "13:11"
audio_size: 3390844
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/12/Christmas2019.jpg"]
series: ["Christmas 2019"]
tags: ["Promises", "Incarnation","Christmas"]
---

The message from our 2019 Christmas Carols service.

# Outline

1. The Promise of God
2. Fulfilled in Christ

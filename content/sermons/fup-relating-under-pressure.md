---
title: "Relating Under Pressure"
passage: "1 Thessalonians 5:12-28"
date: 2023-11-05T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FaithUnderPressure/20231105_RelatingUnderPressure_DS.mp3"]
audio_duration: "26:50"
audio_size: 6671781
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/07/FUP-Sermons.jpg"]
series: ["Faith Under Pressure"]
tags: ["Holiness","Leadership","Joy","Thankfulness","Grace"]
---
As 'children of the light' (1 Thess 5:5) believers are being shaped by God's Spirit to live in his new family—a family of joy, growth, patience, holiness, and grace.

# Outline
1. Relating within church (vv12-15)
2. Relating with God (vv16-22)
3. Relating in Holiness (vv23-28)

---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/FindLife/20180204_NewLife_DS.mp3"]
audio_duration: '25:29'
date: 2018-02-04 06:40:13+00:00
audio_size: 6344108
draft: false
passage: John 2:23-3:36
title: New Life
preachers: ["Daniel Saunders"]
series:
- Find Life
tags:
- Born Again
- Eternal Life
- Grace
- Life
- Religion
images: ["/uploads/2018/01/Find-Life_Slide-768x576.jpg"]
---

1. Judging Jesus (2:23-3:2)

2. Religion can't give new life (3:3-15, 31-34)

3. Only Jesus can give new life (3:16-21, 36)

4. Do you have new life?

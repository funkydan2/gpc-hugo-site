---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Hosea/20160904_TrueLove_DS.mp3"]
audio_duration: '27:12'
date: 2016-09-04 07:22:59+00:00
draft: false
passage: Hosea 4-7
title: True Love?
preachers: ["Daniel Saunders"]
series:
- Unbreakable Love
tags:
- Idolatry
- Love
images: ["/uploads/2015/12/Hosea.jpg"]
---

1. The LORD against Israel (Hosea 4-5)
    1. Something's missing (4:1-3)
    2. The problem with priests (4:4-6)
    3. ...is not lack of religion (4:7-19)
    4. The trouble with treaties (5:8-15; also 7:11-16)
2. Israel's fickle repentance (6:1-6)
3. What God desires (Matthew 9:13; 12:7)

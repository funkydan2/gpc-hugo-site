+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/UnholyPeopleHolyGod/20181014_BloodAndFire_DS.mp3"]
audio_duration = "30:45"
audio_size = 7606283
date = "2018-10-14T14:49:37+10:00"
images = ["/uploads/2018/10/Unholy-People-Holy-God.jpg"]
passage = "Leviticus 1-7"
preachers = ["Daniel Saunders"]
series = ["Unholy People Holy God"]
tags = ["Atonement", "Sacrifice", "Offering"]
title = "Blood and Fire"

+++
1. Six Offerings (Leviticus 7:37-38)

2. Burnt Offering (Leviticus 1)

3. Grain Offering (Leviticus 2)

4. Fellowship Offering (Leviticus 3)

5. Jesus, our *once for all* offering (Hebrews 7:27; 10:19)

---
title: "How Long?"
passage: "Habakkuk 1:1-2:1"
date: 2022-05-01T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Habakkuk/20220501_HowLong_DS.mp3"]
audio_duration: "31:13"
audio_size: 7721789
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/05/Habakkuk.jpg"]
series: ["Questioning God"]
tags: ["Suffering","Justice","Prayer"]
---
Have you ever cried out to God? Felt overwhelmed by suffering or injustice? And wondered if God has heard?

In this message, we hear one man's cry to God and God's surprising answer.

# Outline
1. Questioning God (1:1-4)
2. See God act! (1:5-11)
3. but God!! (1:12-2:1)

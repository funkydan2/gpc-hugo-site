---
title: "Say among the nations"
passage: "Psalm 96"
date: 2024-09-08T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20240908_Psalm96_BC.mp3"]
audio_duration: "26:25"
audio_size: 6570540
preachers: ["Bill Colyer"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
tags: ["Mission","Singing"]
---
What gets you excited? And what comes from this?

In this message from Psalm 96 from Bill Colyer, our [international mission partner]({{< ref "about#international-ministry-partners" >}}), we're encouraged to sing about God's goodness to all the nations.

---
title: "Saved by God"
passage: "Romans 5:6-11"
date: 2019-10-13T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/BackToBasics/20191013_SavedByGod_DS.mp3"]
audio_duration: "26:16"
audio_size: 6531361
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/10/back-to-basics.jpg"]
series: ["Back To Basics"]
tags: ["Justification", "Salvation", "Reconciliation"]
---

We have a problem—a broken relationship with God. But because of what God's done in Jesus, there's a way you can be reconciled and be certain about the future

# Outline

1. While we were…
2. Christ died…
3. to justify, reconcile, and save.
4. God alone saves sinners.

(There was a problem with the recording during the service. The final ⅓ was re-recorded later.)

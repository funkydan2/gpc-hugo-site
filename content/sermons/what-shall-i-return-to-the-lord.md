---
title: "What Shall I Return to the LORD?"
passage: "Psalm 116"
date: 2020-01-26T09:20:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Misc/20200126_WhatShallIReturnToTheLord_DS.mp3"]
audio_duration: "25:24"
audio_size: 6327338
preachers: ["Daniel Saunders"]
images: ["/uploads/2016/02/sermons.jpg"]
series: ["Miscellaneous"]
tags: ["Sacrifice", "Suffering"]
---

The first [Christian sermon in Australia](https://monumentaustralia.org.au/display/23175-first-christian-service) was preached on February 3, 1788. The preacher was [Richard Johnson, chaplain to the prison colony in New South Wales](https://en.wikipedia.org/wiki/Richard_Johnson_(chaplain)), and he chose to preach on Psalm 116:12.

On Australia Day 2020, we opened the same Psalm and asked how we can respond to God's kindness even in the midst of suffering.

# Outline

1. Crying out to the LORD (vv1-4)
2. …who hears and saves (vv5-11)
3. So sacrifice to and praise him (vv12-19)
4. A sacrifice of praise (Hebrews 13:15-16)

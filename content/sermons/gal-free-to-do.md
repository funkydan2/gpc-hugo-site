---
title: "Free to Do?"
passage: "Galatians 5:1-15"
date: 2022-11-27T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Galatians/20221127_FreeToDo_DS.mp3"]
audio_duration: "26:20"
audio_size: 6549769
preachers: ["Daniel Saunders"]
images: ["/uploads/2022/09/Galatians.jpg"]
series: ["Freedom"]
tags: ["Freedom","Law","Love"]
---
What is true freedom? How do our cultural stories of freedom relate to the freedom Jesus brings?

# Outline
1. Christ frees from the law (vv1-6)
2. Running true (vv7-12)
3. Christ frees to love (vv13-15)

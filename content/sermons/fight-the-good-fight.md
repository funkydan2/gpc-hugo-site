---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/GodsHouse/20180506_FightTheGoodFight_DS.mp3"]
audio_duration: '23:19'
audio_size: 5823416
date: 2018-05-06 08:47:25+00:00
draft: false
passage: 1 Timothy 1:18-20
title: Fight The Good Fight
preachers: ["Daniel Saunders"]
series:
- God's House
images: ["/uploads/2018/04/Gods-House-768x644.jpg"]
---
1. What makes a fight good?
2. How to fight well
3. Two (bad) examples
4. Are you *fighting the good fight*?

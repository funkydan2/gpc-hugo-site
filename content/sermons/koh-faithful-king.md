---
title: "Faithful King"
passage: "Matthew 4:1-11"
date: 2024-01-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/KingOfHeaven/20240128_FaithfulKing_DS.mp3"]
audio_duration: "24:32"
audio_size: 6119428
preachers: ["Daniel Saunders"]
images: ["/uploads/2023/12/KingOfHeaven.jpg"]
series: ["King of Heaven"]
tags: ["Satan","Sin"]
---
As Jesus is tested in the wilderness we see he is the Faithful King, our sinless saviour, and compassionate high priest.

# Outline
1. Wilderness and Weakness (vv1-2)
2. Passing the Test (vv3-11)
3. Satan, Sin, and our Saviour (2 Corinthians 5:21; Hebrews 4:15-16)

(In this recording it's said that Jesus' fast meant he neither ate nor drank. The Bible says Jesus' fast was from food. Thanks to the church member who pointed this out.)

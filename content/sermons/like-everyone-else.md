+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/WaitingForTheTrueKing/20181007_LikeEveryoneElse_DS.mp3"]
audio_duration = "23:31"
audio_size = 5872734
date = "2018-10-07T14:15:41+10:00"
images = ["/uploads/2018/09/Waiting-for-the-True-King.jpg"]
passage = "1 Samuel 708"
preachers = ["Daniel Saunders"]
series = ["Waiting for the True King"]
tags = ["Church", "Leadership"]
title = "Like Everyone Else"

+++
1. The end of the *Judges* (1 Samuel 7)

2. ... because 'we want a king' (1 Samuel 8)

3. Wanting to be like everyone else

4. Our King: not like the nations'

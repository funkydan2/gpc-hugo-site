---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Judges/20170820_BargainingWithGod_DS.mp3"]
audio_duration: '25:47'
audio_size: 6419419
date: 2017-08-20 06:08:23+00:00
draft: false
passage: Judges 10-12
title: Bargaining with God
preachers: ["Daniel Saunders"]
series:
- Crying Out For A Saviour
tags:
- God
- Prayer
images: ["/uploads/2017/06/Judges.jpg"]
---

1. Israel bargains with God (10:6-18)

2. Gilead bargains with Jephthah (11:1-11)

3. Jephthah's bargain with God (11:29-33)

4. ...has disastrous results (11:34-39)

5. We don't need to bargain with God!

    Matthew 7:9-11

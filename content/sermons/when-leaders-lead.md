---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Judges/20170730_WhenLeadersLead_DS.mp3"]
audio_duration: '28:26'
audio_size: 7055228
date: 2017-07-30 06:09:29+00:00
draft: false
passage: Judges 4-5
title: When Leaders Lead?
preachers: ["Daniel Saunders"]
series:
- Crying Out For A Saviour
images: ["/uploads/2017/06/Judges.jpg"]
---
1. A Prophetess and a Reluctant Leader (4:1-16)

2. An Inglorious Victory (4:17-23)

3. Judges and Us...

    1. What does it show about people?
    2. What does it show about God?
    3. What does it show about how God works in Jesus?

        Luke 9:51

        Philippians 2:9

---
title: "Why Are You Afraid?"
passage: "Mark 4:35-5:20"
date: 2021-02-15T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/WhoDoYouSayHeIs/20210214_WhyAreYouAfraid_DS.mp3"]
audio_duration: "24:55"
audio_size: 6210319
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/01/Mark1-8.png"]
series: ["Jesus: Who Do You Say He Is?"]
tags: ["Faith", "Power"]
---
When have you been most afraid?

As we see Jesus' powerful authority to calm the raging sea and the tormented man, we not only see who he truly is, but also that we can trust him when chaos rages.


# Outline
1. Afraid of the storm (4:25-41)
2. Afraid of the calm (5:1-20)
3. Who is this man?
4. Will you trust him?

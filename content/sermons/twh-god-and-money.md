---
title: "God and Money"
passage: "Luke 16:1-18"
date: 2019-07-28T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190728_GodAndMoney_DS.mp3"]
audio_duration: "24:00"
audio_size: 5989710
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Parable", "Money"]
---

Who do you serve?

This week, we hear Jesus give a warning to religious people who love money—'no one can serve two masters.' This is a stark warning as Jesus urges us to enter his kingdom.

# Outline
1. The background: Herod and the Pharisees (vv 16-18)
2. A parable of a *shrewd* manager (vv 1-8)
3. ...which means (vv 9-15)
4. you can't serve God and money!


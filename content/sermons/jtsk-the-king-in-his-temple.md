---
title: "The King in His Temple"
passage: "Luke 11:1-25"
date: 2021-10-31T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheServantKing/20211031_TheKingInHisTemple_DS.mp3"]
audio_duration: "31:46"
audio_size: 7854748
preachers: ["Daniel Saunders"]
images: ["/uploads/2021/09/JTSK.jpg"]
series: ["Jesus: The Servant King"]
tags: ["Mission", "Temple", "Apocalyptic", "Eschatology"]
---
What does Jesus' think about corrupt, exclusive, religion? In this message, we see how Jesus responds to religion that's all show and without fruit.

# Outline
1. The king arrives (vv1-11)
2. A tree without fruit (vv12-21)
3. Prayer at the end of the world (vv22-25)

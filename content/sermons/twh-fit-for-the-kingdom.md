+++
audio = ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190203_FitForTheKingdom_DS.mp3"]
audio_duration = "24:27"
audio_size = 6093817
date = "2019-02-03T09:30:00+10:00"
images = ["/uploads/2019/02/TheWayHome3.jpg"]
passage = "Luke 9:51-10:24"
preachers = ["Daniel Saunders"]
series = ["The Way Home (Part 3)"]
tags = ["Joy", "Heaven", "Mission"]
title = "Fit for the Kingdom"

+++
1. Jesus' Radical Direction (9:51-56)

2. Jesus' Radical Requirements (9:57-62)

3. Jesus' Radical Priorities (10:1-16)

4. Jesus' Radical Joy (10:17-24)

---
title: "Master and Servant"
passage: "Luke 17:1-19"
date: 2019-08-11T09:30:00+10:00
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/TheWayHome/20190811_MasterAndServant_DS.mp3"]
audio_duration: "25:28"
audio_size: 6338916
preachers: ["Daniel Saunders"]
images: ["/uploads/2019/07/TWH4.jpg"]
series: ["The Way Home (Part 4)"]
tags: ["Thankfulness"]
---

What would you do if someone paid off all your debts tomorrow? What would you owe them?

In this message, we hear how in Jesus, we freely receive new life under a new master, and he demands all our service and praise.

# Outline

1. Watch yourselves (vv1-6)
2. Serve humbly (vv7-10)
3. with thankfulness (vv11-19)
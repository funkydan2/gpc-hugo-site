---
audio: ["https://f001.backblazeb2.com/file/GympieSermonAudio/Luke3-9/20160214_ManOnAMission_DS.mp3"]
audio_duration: '23:52'
date: 2016-02-13 23:29:05+00:00
draft: false
passage: Luke 4:14-44
title: Man on a Mission
preachers: ["Daniel Saunders"]
series:
- Investigating Jesus
images: ["/uploads/2016/02/Luke-1-9_Web.jpg"]
---

1. Announcing the Kingdom (vv14-30)
    1. ...of freedom (vv18-19)
    2. ...which is rejected (vv22-30)
2. Demonstrating the Kingdom (vv31-44)
3. Jesus' Mission and Ours

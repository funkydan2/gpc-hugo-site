---
title: "Little Arrows"
subtitle: "Playgroup"
date: 2023-10-13T07:33:24+10:00
lastmod: 2024-02-21T16:11:52+10:00
tags: ["Event","Children"]
---
Little Arrows Playgroup offers families the opportunity to enjoy a friendly and relaxed morning chat, and allow children to explore and play together in a fully-fenced play area. 

Little Arrows happens from 9 am on Wednesdays at [Cooloola Christian College Prep Classrooms](https://www.google.com/maps/@-26.1980528,152.6434007,20z?entry=ttu).

![Little Arrows Flyer](/uploads/2024/09/LittleArrows.png)

Little Arrows runs in partnership with [Cooloola Christian College](https://www.ccc.qld.edu.au).

[Get in touch]({{< ref "contact.md" >}}) to find out more!

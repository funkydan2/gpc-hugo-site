---
date: 2021-01-27T13:34:57+10:00
lastmod: 2023-11-08T20:17:11+10:00
title: Growth Groups
images: ["uploads/2015/11/BibleStudy_Stock.jpg"]
---

During the week there's a number of Growth Groups meeting around Gympie where we meet to read the Bible and encourage each other to *grow* as followers of Jesus.

## What happens in a Growth Group?

Growth Groups are a great time to dig deep into the Bible to discover God. Since we believe that God speaks to us in the Bible, a big part of what we do at Bible studies is to read and discuss God's word—understanding it and applying it to how we live. Although Growth Groups involes discussion in a small group, when you come there's no expectation that you read aloud, answer questions, or join in the discussion—you're more than welcome to take part by listening in.

Although reading and learning from the Bible is the centre of our Growth Groups it's not the only thing we do. This is also a time for growing as followers of Jesus as we care for one another, pray for each other, and encourage each other to keep following Jesus.


## How can I join a Growth Groups?

We have Growth Groups that meet midweek both during the day and the evening. Some groups meet in our hall at [11 Crown Road, Gympie,](https://www.google.com.au/maps/place/Gympie+Presbyterian+Church/@-26.1943571,152.6707316,19z/data=!4m5!3m4!1s0x6b94a36d244bfc9b:0x1218e6e7242a2f06!8m2!3d-26.1943571!4d152.6712788?hl=en) others in homes around our region.

Please [contact us]({{< relref contact.md >}}) to find out more.

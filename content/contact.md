---
date: 2015-11-26 06:53:38+00:00
lastmod: 2024-09-25T12:40:34+10:00
title: Contact
images: ["/uploads/2015/11/lightstock_121230_medium_daniel_saunders-940x350.jpg"]
---
{{< contact.inline >}}{{ partial "contact" . }}{{< /contact.inline >}}

<i class="icon fab fa-facebook"></i>&nbsp;Connect on [Facebook](//facebook.com/gympiepresbyterian)

<i class="icon fab fa-facebook-messenger"></i>&nbsp;Send a [Facebook Message](//m.me/gympiepresbyterian)

<i class="icon fas fa-phone-square"></i>&nbsp;Call (07) 5482 7629

<i class="icon fas fa-map-marker-alt"></i>&nbsp;Visit us, Sundays in the [One Mile State School hall](https://www.google.com.au/maps/place/26°11'59.6%22S+152°40'29.0%22E/@-26.1998837,152.6740639,19z/data=!3m1!4b1!4m4!3m3!8m2!3d-26.1998849!4d152.6747076?hl=en&entry=ttu&g_ep=EgoyMDI0MDkyMi4wIKXMDSoASAFQAw%3D%3D) or at [11 Crown Road, Gympie, 4570](https://www.google.com/maps/place/Gympie+Presbyterian+Church/@-26.194343,152.668958,17z/data=!3m1!4b1!4m5!3m4!1s0x6b94a36d244bfc9b:0x1218e6e7242a2f06!8m2!3d-26.194343!4d152.671152)

<i class="icon fas fa-envelope-square"></i>&nbsp;Mail PO Box 513, Gympie, 4570

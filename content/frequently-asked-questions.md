---
date: 2016-01-27T13:34:02+10:00
lastmod: 2024-08-10T17:53:37+10:00
title: Frequently Asked Questions
images: ["uploads/2018/06/question-300x200.jpg"]
---
# What happens during your church service?
When you arrive at church, you'll be greeted by someone and given a notice sheet. During the service, we'll sing songs together (don't worry, you don't have to sing if you don't want to), be led in prayer, and listen to the Bible read and explained. Our service runs for about an hour. After the service, you're invited to join us for morning tea in the undercover area behind the hall.

# What if I don't normally go to church?
That's ok. You're not the only one! Some have been part of churches since they were born, others used to be part of a church and only recently have come back, and for others, church is entirely new. We try our best to be welcoming to everyone, especially when it's your first time, and to make everything we do clear.

We want Gympie Presbyterian Church to be a place where you can seek, wrestle, question, explore and grow.

# What should I wear?
You can wear whatever you feel comfortable wearing. We all come to God as we are--he doesn't accept us based on our looks or whether we're 'good enough' for him.

You'll see people wearing different styles of clothing at church. Some of the men wear ties, and some wear thongs. Some women wear dresses, and others shorts and a t-shirt.

# How do I get to church?
We meet for church in the [One Mile State School hall, McLeod Lane, Gympie](https://www.google.com.au/maps/place/McLeod+Ln,+Gympie+QLD+4570/@-26.2001105,152.672437,17z/data=!3m1!4b1!4m5!3m4!1s0x6b94a30dec7cdfa7:0xdca259c42aa4551e!8m2!3d-26.2001105!4d152.6746257?hl=en). One Mile State School is near St Vinnies' Op Shop, the Gympie Regional Council works depo, and the One Mile Sporting Ovals.

Most people drive to church. Please [get in touch]({{<ref "contact.md">}}) if you need a lift.

# Where do I park?
There's plenty of parking along McLeod Lane and in the large school car park off Phoenix Street.

# Is church accessible?
The One Mile State School Hall is highly accessible. There are no stairs between the car parks and the hall. The hall has wide doorways and there's an accessible toilet.

The Bible teaches all people are inherently valuable, being made in the image of God (Genesis 1:27). In Jesus, we meet someone who welcomed and showed understanding and compassion to people in all sorts of circumstances. However, we also know that we'll make mistakes when it comes to being accessible and inclusive. If there's anything we can do to make our church more accessible, please [contact us]({{<ref "contact.md">}})

# Is there anything for kids?
KidsChurch runs during the school terms. KidsChurch is held in the undercover area behind the hall during the message/teaching time. At KidsChurch, children are taught about Jesus from the Bible through stories, games, and crafts. All our KidsChurch teachers and helpers hold a valid [Blue Card](https://www.bluecard.qld.gov.au) and receive training and accreditation according to the [Presbyterian Church of Queensland's Safe Ministry with Children policy](https://www.pcq.org.au/safe-ministry-with-children.php).

# What songs do you sing?
We sing songs that come from different times and in different styles. On most Sundays we'll sing some songs that are considered 'hymns' and more recently written songs too. You can get an idea of the songs we sing by listening to [our playlist on Spotify](https://open.spotify.com/playlist/4WFiAwFanfswRjUCgYQhhz?si=889bd068ddc84d57) (we continue to add songs to this playlist).

# I've got another question
Please [contact us]({{<ref "contact.md">}}).

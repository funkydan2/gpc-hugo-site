---
date: 2015-11-26 06:46:14+00:00
draft: false
title: Midweek
images: ["uploads/2015/11/BibleStudy_Stock.jpg"]
---

During the week there's a number of [Growth Groups]({{< relref "growth-groups.md" >}}) meeting around Gympie where we meet to read the Bible and encourage each other to live for Jesus.

On Wednesday mornings we also have a [Bible and craft group]({{< relref "craft-group.md" >}}) which meets from 9:30 am.

On Tuesday mornings during school terms we partner with Cooloola Christian College to run the [Little Arrows Playgroup]({{< relref "little-arrows.md" >}}).

And on Friday nights during school terms we partner with Gympie Baptist church in our [combined youth group]({{< relref "combined-youth.md">}}).
